<footer style="padding-top:8vh">

<div class="hidden-sm visible-md visible-lg row" style="padding-left: 5%; padding-right: 5%;">
<div class="col-sm-2" style="padding-right:20px; border-right: 1px solid #ccc;">
<div class="foot-header">
SERVICES
</div>
<br>
<div class="foot-links">
<a href="http://emergency.csulb.edu/" target="_blank" class="footerlink" >Emergency Information</a><br>
<a href="http://web.csulb.edu/divisions/students/caps/crisis.htm" target="_blank" class="footerlink" >Crisis Assistance</a><br>
<a href="http://web.csulb.edu/divisions/students/cares/" target="_blank" class="footerlink" >CARES Team</a><br>
<a href="http://web.csulb.edu/divisions/students/ati/" target="_blank" class="footerlink" >Accessibility</a><br>
<a href="http://web.csulb.edu/divisions/students/shs/" target="_blank" class="footerlink" >Student Health Center</a>
</div>
</div><!--/col-sm-2-->
<div class="col-sm-4" style="padding-right:20px; border-right: 1px solid #ccc;">
<div class="foot-header">
COLLEGES
</div>
<br>
<div class="col-sm-6">
<div class="foot-links">
<a href="http://csulb.edu/cota/?utm_source=website&utm_medium=homepage&utm_content=collegeslink&utm_campaign=web" target="_blank" class="footerlink" >The Arts</a><br>
<a href="http://csulb.edu/coe?utm_source=website&utm_medium=homepage&utm_content=collegeslink&utm_campaign=web" target="_blank" class="footerlink" >Engineering</a><br>
<a href="http://www.ced.csulb.edu/?utm_source=website&utm_medium=homepage&utm_content=collegeslink&utm_campaign=web" target="_blank" class="footerlink" >Education</a><br>
<a href="http://csulb.edu/cla/?utm_source=website&utm_medium=homepage&utm_content=collegeslink&utm_campaign=web" target="_blank" class="footerlink" >Liberal Arts</a><br>
</div>
</div>
<div class="col-sm-6">
<div class="foot-links">
<a href="http://csulb.edu/cba/?utm_source=website&utm_medium=homepage&utm_content=collegeslink&utm_campaign=web" target="_blank" class="footerlink" >Business Administration</a><br>
<a href="http://csulb.edu/chhs/?utm_source=website&utm_medium=homepage&utm_content=collegeslink&utm_campaign=web" target="_blank" class="footerlink" >
Health & Human Services</a><br>
<a href="http://www.ccpe.csulb.edu/?utm_source=website&utm_medium=homepage&utm_content=collegeslink&utm_campaign=web" target="_blank" class="footerlink" >Continuing & Professional Education</a><br>
<a href="http://csulb.edu/cnsm/?utm_source=website&utm_medium=homepage&utm_content=collegeslink&utm_campaign=web" target="_blank" class="footerlink" >Natural Sciences & Mathematics
</a>
</div>
</div>
</div><!--/col-sm-4-->
<div class="col-sm-4" style="padding-right:20px; border-right: 1px solid #ccc;">
<div class="foot-header">
ADDITIONAL RESOURCES
</div>
<br>
<div class="col-sm-6">
<div class="foot-links">
<a href="http://web.csulb.edu/newsroom?utm_source=website&utm_medium=homepage&utm_content=footerlink&utm_campaign=web" target="_blank" class="footerlink" >Media and Government Relations
</a><br>
<a href="http://www.csulb.edu/employment/" target="_blank" class="footerlink" >Careers</a><br>
<a href="http://www.csulb.edu/feedback/?utm_source=website&utm_medium=homepage&utm_content=footerlink&utm_campaign=web" target="_blank" class="footerlink" >Contact Us</a><br>
<a href="http://www.csulb.edu/help/" target="_blank" class="footerlink" >Help</a><br>
</div>
</div>
<div class="col-sm-6">
<div class="foot-links">
<a href="http://www.csulb.edu/university-relations-and-development/marketing-communications?utm_source=website&utm_medium=homepage&utm_content=footerlink&utm_campaign=web" target="_blank" class="footerlink" >Marketing & Communications</a><br>
<a href="http://daf.csulb.edu/offices/ppfm/police/?utm_source=website&utm_medium=homepage&utm_content=footerlink&utm_campaign=web" target="_blank" class="footerlink" >University Police</a><br>
<a href="http://daf.csulb.edu/offices/its/catalog/directory.php" target="_blank" class="footerlink" >Campus Directory</a><br>
<a href="http://www.csulb.edu/document-readers" target="_blank" class="footerlink" >Download Document Reader
</a>
</div>
</div>
</div><!--/col-sm-4-->
<div class="col-sm-2">
<div class="foot-header">

</div>
<br><br>
<div class="foot-links">
<a href="http://www.calstate.edu/" class="footerlink" >CSU</a><br>
<a href="http://www.csulb.edu/sitemap?utm_source=website&utm_medium=homepage&utm_content=footerlink&utm_campaign=web" class="footerlink" >Site Index</a><br>
<a href="http://www.csulb.edu/contact" class="footerlink" >Feedback</a>
</div>
</div><!--/col-sm-2-->
</div><!--/row-->
<br><br>
<div class="row">
<div class="col-lg-12 text-center">
<h4>CALIFORNIA STATE UNIVERSITY <strong>LONG BEACH<strong></h4>
</div>
</div>
<div class="vcard">
<div class="adr">
<div class="street-address">1250 Bellflower Boulevard</div>
<div><span class="locality">Long Beach, California</span> <span class="postal-code">90840</span></div>
</div>
<div class="tel">562.985.4111</div>
</div>
</footer>
