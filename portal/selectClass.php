<?php
    include '../dbmanage.php';
    //Class ID
    $cid = $_POST['cid'];
    $eid = $_POST['eid'];
    $cc = $_POST['cc'];

    $withoutPrereq = $_POST['withoutPrereq'];

    $course_id = getCourseIDByClassID($cid);

    $enrolled = getEnrollmentStatusByClassIDEmployeeID($cid,$eid);
    if($enrolled==1){
      echo "Already enrolled!";
    }
    else{
      $ret = getLastCertificateStatusFromCourseIDEmployeeID($course_id,$eid);
      if($ret==1 || $withoutPrereq==0){
        //Passed
      }
      else{
        //Failed or withoutPrereq is enabled
        //Create New Prereq before enrolling class
        $ret2 = setCertificate($eid, $course_id);
        if($ret2['Status']==1){
          $cc = $ret2['Certficate_Code'];
        }
        else{
          exit();
        }
      }

      $ret = enrollEmployee($eid, $cid, $cc);

      if(check_waitlist($cid, $eid)==1)
      {
          dropFromWaitlist($cid, $eid);
      }

      if($ret['Status']==1)
      {
        $ret2 = getClass($cid);
        addActivity($eid, "Enrolled to Class: " . $ret2['Class_Name'] . ' and Course: ' . $ret2['Course_Name']);
      }

      echo $ret['Message'];
    }

?>
