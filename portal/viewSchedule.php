<?php
    include '../dbmanage.php';
    $cc = $_POST['cc'];
    $ret=getSchedule($cc);
    $selecteddates = "";
    for($j=0;$j<count($ret['Class_Dates']);$j++)
    {
        $selecteddates =  $ret['Class_Dates'][$j] . "," . $selecteddates;
    }
    echo "<input type='text' value='$selecteddates' id= 'selecteddates_2' hidden >";

?>

<script>
$(document).ready(function(){
                  $('#disableclick').click(function(){
                    });
                  var selecteddates = document.getElementById("selecteddates_2").value;
                  new Kalendae({
                               attachTo:document.getElementById('calendaredit'),
                               months:1,
                               mode:'multiple',
                               selected:selecteddates,
                               direction:'future',
                               blackout: function (date) {
                               return true;
                               }
                               });
                  });
</script>


<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Course</th>
<th>Class Name</th>
<th>Class Dates</th>
<th>Practical Exam Date</th>
<th>Written Exam Date</th>
<th>Class Location</th>
</tr>
</thead>
<tbody>
<tr class="odd gradeX">
<td>
<?php
    echo $ret['Course_Name'];
    ?>
</td>
<td>
<?php
    echo $ret['Class_Name'];
    ?>
</td>
<!--Calender Click Disabled using z-index so new div will be in front of older one-->
<td style="position: relative;">
<div style="height: 100%;position: absolute;width: 100%;margin: -8px -8px;z-index: 999;"></div>
<div id="disableclick">
<center id='calendaredit'>
</div>
</center>
<?php
    echo "All Classes will be from " . $ret['Class_TimeFrom'][0] . " to " . $ret['Class_TimeTo'][0] . ".";
?>
</td>
<td>
<?php
    echo $ret['Class_Practical_Date'];
    ?>
</td>
<td>
<?php
    echo $ret['Class_Written_Date'];
    ?>
</td>
<td>
<?php
    echo $ret['Location_Name'];
    ?>
</td>
</tr>

</table>
