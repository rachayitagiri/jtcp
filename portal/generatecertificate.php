<?php
    include '../dbmanage.php';
    $employee_id = $_POST['employee_id'];
    $course_id = $_POST['course_id'];
    $ret = setCertificate($employee_id, $course_id);
    if($ret['Status']==1)
    {
      $ret2 = getCourse($course_id);
      addActivity($employee_id, "Prerequisite Passed for Course: " . $ret2);
    }
    echo json_encode($ret);
?>
