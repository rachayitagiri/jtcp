<?php
    include 'template1.php';
?>

<title>Payments</title>
<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Payments</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php

$ret = getEmployeeDetail($lid);
$eid = $ret['Employee_ID'];
$ret = getEmployeeDetailsByEmployeeID($eid);
if($ret==null){
  echo "Invalid Employee Id";
}
else{

  $ret2 = getAllPaypalDataByEmployeID($eid);

  ?>

  <table class="table">
  <thead>
    <tr>
      <th>No</th>
      <th>Class</th>
      <th>Amount</th>
      <th>Txn No</th>
      <th>Status</th>
      <th>Timestamp</th>
    </tr>
  </thead>
  <tbody>
    <?php
      for($i=0;$i<count($ret2);$i++)
      {
        $ret3 = getClass($ret2[$i]['Class_ID']);
        $paypal_json = json_decode($ret2[$i]['Paypal_Data'], true);
        $amount = $paypal_json['payment_gross'];
        $txtno = $paypal_json['txn_id'];
    ?>
    <tr>
      <td><?php echo $i+1; ?></td>
      <td><?php echo $ret3['Class_Name'] . " / " . $ret3['Course_Name']; ?></td>
      <td><?php echo $amount; ?></td>
      <td><?php echo $txtno; ?></td>
      <td><?php echo $paypal_json['payment_status']; ?></td>
      <td><?php echo $ret2[$i]['Timestamp']; ?></td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>

  <?php

}

?>


<?php
    include 'template2.php';
?>
