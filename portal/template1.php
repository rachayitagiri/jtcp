<?php

    include '../ssl_check.php';

    include 'check.php';
    include '../dbmanage.php';
    ?>

<!DOCTYPE html>
<html lang="en">

<head>
  
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">


<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="../vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="../js/jquery-1.11.0.min.js" type="text/javascript"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<!-- Favicon -->
<link rel="shortcut icon" href="../img/favicon.ico">

<link rel="stylesheet" href="../js/Kalendae-master/build/kalendae.css" type="text/css" charset="utf-8">
<script src="../js/Kalendae-master/build/kalendae.standalone.js" type="text/javascript" charset="utf-8"></script>

<script>
$(document).ready(function(){
                  $(".schedule").click(function(){
                                       var cc = $(this).attr('id');
                                       $('#viewschedule').modal('toggle');
                                       document.getElementById('viewschedule-dialog').style.width = '80%';

                                       $.ajax({
                                              url: "viewSchedule.php",
                                              type: "post",
                                              data: {'cc' : cc} ,
                                              success: function (response) {
                                              $('#viewscheduledata').empty();
                                              $('#viewscheduledata').append(response);
                                              },
                                              error: function(jqXHR, textStatus, errorThrown) {
                                              alert(errorThrown);
                                              console.log(textStatus, errorThrown);
                                              }
                                              });

                                       });

                  $(".drop").click(function(){
                                   var r = confirm("Are you sure you want to drop the class?");
                                   if (r == true) {
                                    var cc = $(this).attr('id');
                                    $.ajax({
                                          url: "dropClass.php",
                                          type: "post",
                                          data: {'cc' : cc} ,
                                          success: function (response) {
                                          alert(response);
                                          location.reload();
                                          },
                                          error: function(jqXHR, textStatus, errorThrown) {
                                          alert(errorThrown);
                                          console.log(textStatus, errorThrown);
                                          }
                                    });
                                   } else {
//                                   txt = "You pressed Cancel!";
                                   }
                                   });


                  })

</script>

</head>

<body>

<script>
function gotoPrereq(cid)
{
    var quizlink = "<form method='post' action='quiz.php'><input type='hidden' name='quiz_id' value="+cid+"><input type='submit' id='gotoquiz' value='submit' hidden></form>";
    $('#tempform').append(quizlink);
    $("#gotoquiz").click();
    //    $(quizlink).submit();
    //    $(quizlink).trigger('submit')
}
</script>

<div id="tempform">
</div>

<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0; background-color: #333; opacity: 0.9;">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="dashboard.php"><!--<img src="../img/logo.png" style="height: 20px; width:30px; float: left;">-->
<span style="float: left; margin-left: 10px; margin-right: 10px; color: white;">CalTrans Industry <span class="hidden-sm hidden-xs">- Joint Training and Certification Program</span></span>
<!--<img src="../img/csulb.png" style="height: 20px; width:30px; float: left;">-->
</a>
</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right" style="color: white;">
<!-- /.dropdown -->

<?php
    $lid = $_SESSION['Login_ID'];
    $ret = getEmployeeDetail($lid);
    echo $ret['Employee_FirstName'] . " " . $ret['Employee_LastName'];
    ?>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu dropdown-user">
<li><a href="settings.php"><i class="fa fa-gear fa-fw"></i> Settings</a>
</li>
<li class="divider"></li>
<li><a href="../logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
</li>
</ul>
<!-- /.dropdown-user -->
</li>
<!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
<div class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">
<li>
<a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
</li>
<li>
<a href="notification.php"><i class="fa fa-bell fa-fw"></i> Notifications</a>
</li>
<li>
<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Courses<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">
<li>
<a href="../HMA1/HMA1_overview.php" target="_blank" >Hot Mix Asphalt I</a>
</li>
<li>
<a href="../HMA2/HMA2_overview.php" target="_blank" >Hot Mix Asphalt II</a>
</li>
<li>
<a href="../SA/SA_overview.php" target="_blank" >Soils & Aggregates</a>
</li>
<li>
<a href="../PCC/PCC_overview.php" target="_blank" >Portland Cement Concrete</a>
</li>
</ul>
<!-- /.nav-second-level -->
</li>
<li>
<a href="payments.php"><i class="fa fa-dollar fa-fw"></i> Payments</a>
</li>
<li>
<a href="../help.php" target="_blank"><i class="fa fa-hand-paper-o fa-fw"></i> Help</a>
</li>
<li>
<a href="../feedback.php" target="_blank"><i class="fa fa-comments-o fa-fw"></i> Feedback</a>
</li>
<li>
<a href="transfer.php"><i class="fa fa-exchange"></i> Course Transfer</a>
</li>
<li>
<a href="settings.php"><i class="fa fa-wrench fa-fw"></i> Settings</a>
</li>
</ul>
<div style="margin-left: 15px; margin-top: 15px; margin-right: 15px; text-align: justify;">
  <b>Please note:</b><br>
  Internet Explorer does not support certain features of the website. Use Google Chrome, Safari, Mozilla Firefox or Microsoft Edge for a seamless experience.
</div>

</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
<br>
