<?php
    include 'template1.php';
    $eid = $ret['Employee_ID'];

    if($_POST)
    {
        if($_POST['addTransferRequest']!=NULL)
        {
            $cid = $_POST['class'];
            $email = $_POST['email'];
            $notes = $_POST['notes'];
            $ret2 = addTransferRequest($cid,$eid,$email,$notes);
        }
    }
?>

<title>Course Transfer Request</title>
<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Course Transfer Request</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<h5>You can not drop course 15 days prior to the starting date, but you can create transfer request, so someone else can attend class behalf of you.</h5>
<h6>Note: Plesae make sure that the person who is going to attend class behalf of you has account on JTCP and has completed FERPA, Safety Quiz and Prereq Exam.</h6>

<hr>
<h5>New Transfer Request:</h5>

<form id="transferrequest" role="form" method="POST">
            <div class="form-group col-lg-6">
                <label>Class</label>
                <select name="class" class="form-control" id="class" required>
                <option disabled selected value> -- select your class -- </option>
                <?php
                    $ret = getClassesByEmployeeID($eid);
                    for($i=0;$i<count($ret);$i++)
                    {
                        $class_id = $ret[$i]['Class_ID'];
                        $ret3 = getClass($class_id);
                        echo "<option value='$class_id'>". $ret3['Class_Name']."</option>";
                    }
                    ?>
                    </select>
            </div>

            <div class="form-group col-lg-6">
                <label>Email</label>
                <input type="text" name="email" class="form-control" id="email" value="" required>
            </div>

            <div class="form-group col-lg-12">
                <label>Reason for Transfer</label>
                <input type="text" name="notes" class="form-control" id="notes" value="" required>
            </div>

            <div class="col-sm-12">
                <input type="submit" class="btn btn-primary" id="addTransferRequest" name="addTransferRequest" value="Request">
            </div>

    </form>
<hr>

<h5>Note: Please contact JTCP, to cancel transfer request! </h5>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Class</th>
        <th>Tranfer to Email</th>
        <th>Transfer Notes</th>
        <th>Transfer Status</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $ret2 = getTransferRequestByEmployeeID($eid);
        for($i=0;$i<count($ret2);$i++){
          echo "<tr>";
          $class_id = $ret2[$i]['Class_ID'];
          $ret3 = getClass($class_id);
          echo "<td>" . $ret3['Class_Name'] . "</td>";
          echo "<td>" . $ret2[$i]['Email'] . "</td>";
          echo "<td>" . $ret2[$i]['Notes'] . "</td>";
          if($ret2[$i]['Status']==0){
            echo "<td> Not Approved </td>";
          }
          else{
            echo "<td> Approved </td>";
          }
          echo "</tr>";
        }
      ?>
    </tbody>
  </table>

</div>
</div>

</div>
</div>


<?php
    include 'template2.php';
?>
