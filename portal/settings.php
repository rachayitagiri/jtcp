<head>
    <title>Settings</title>

<script type="text/javascript">
window.onload = function () {
    document.getElementById("oldpassword").onchange = validatePassword;
    document.getElementById("newpassword").onchange = validatePassword;
}
function validatePassword(){
    var pass2=document.getElementById("newpassword").value;
    var pass1=document.getElementById("oldpassword").value;
    if(pass2.length<5)
    {
        document.getElementById("newpassword").setCustomValidity("Passwords Must be at least 5 characters long.");
    }
    else
    {
        document.getElementById("oldpassword").setCustomValidity('');
        document.getElementById("newpassword").setCustomValidity('');
    }
    //empty string means no validation error
}
</script>

</head>
<?php
    include 'template1.php';

    $ret = getEmployeeDetail($lid);
    $eid = $ret['Employee_ID'];

    if($_POST)
    {
        if($_POST['changepassword']!=NULL)
        {
            $oldpassword = $_POST['oldpassword'];
            $newpassword = $_POST['newpassword'];
            $ret2 = updatePassword($ret['Login_Email'],$oldpassword, $newpassword);
            if($ret2['Status']==1){
              addActivity($eid, "Password Changed");
            }
            else{
              addActivity($eid, "Failed to change password");
            }
            echo "<script>alert('" . $ret2['Message'] . "')</script>";
        }
        else if($_POST['changebasicdetails']!=NULL)
        {
            $email = $_POST['email'];
            $phonenumber = $_POST['phonenumber'];
            $address = $_POST['address'];
            $city = $_POST['city'];
            $postalcode = $_POST['postalcode'];
            $ret2 = updateEmployeeBasicDetails($ret['Employee_ID'],$email, $phonenumber, $address, $city, $postalcode);
            if($ret2['Status']==1){
              addActivity($eid, "Basic Details Changed");
            }
            else{
              addActivity($eid, "Failed to change basic details");
            }
            echo "<script>alert('" . $ret2['Message'] . "')</script>";
        }
        else if($_POST['deleteaccount']!=NULL)
        {
            $ret2 = disableEmployee($lid);
            if($ret2['Status']==1){
              addActivity($eid, "Account Disabled by user");
            }
            else{
              addActivity($eid, "Failed to disable account (By User)");
            }
            echo "<script>alert('" . $ret2['Message'] . "')</script>";
            echo "<script>window.open('../logout.php','_self',false);</script>";
        }
    }


    ?>
<!-- /#page-wrapper -->
<!-- Page Content -->


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Settings</h1>
    </div>

    <!-- /.row -->
    <div class="row">
    <form id="passwordchangeform" role="form" method="POST" onsubmit="return confirm('Are you sure you want to change the password?');">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Change Password
                </div>
                <div class="form-group col-lg-6" style="margin-top: 10px;">
                    <label>Old Password</label>
                    <input type="password" name="oldpassword" class="form-control" id="oldpassword" value="">
                </div>

                <div class="form-group col-lg-6" style="margin-top: 10px;">
                    <label>New Password</label>
                    <input type="password" name="newpassword" class="form-control" id="newpassword" value="">
                </div>

                <div class="col-sm-12">
                    <input type="submit" class="btn btn-primary" id="changepassword" name="changepassword" value="Change">
                </div>

            </div>
        </div>
        </form>
    <!-- /.panel -->
    </div>

    <br>

    <!-- /.row -->
    <div class="row">
    <form id="basicdetailschangeform" role="form" method="POST">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Details
                </div>
                <div class="form-group col-lg-5" style="margin-top: 10px;">
                    <label>First Name</label>
                    <input type="" name="firstname" class="form-control" id="firstname" value="<?php echo $ret['Employee_FirstName']; ?>" disabled>
                </div>

                <div class="form-group col-lg-2" style="margin-top: 10px;">
                    <label>Initial</label>
                    <input type="" name="initial" class="form-control" id="initial" value="<?php echo $ret['Employee_Initial']; ?>" disabled>
                </div>

                <div class="form-group col-lg-5" style="margin-top: 10px;">
                    <label>Last Name</label>
                    <input type="" name="lastname" class="form-control" id="lastname" value="<?php echo $ret['Employee_LastName']; ?>" disabled>
                </div>

                <div class="form-group col-lg-12">
                    <label>Email Address</label>
                    <input type="email" name="email" class="form-control" id="email" value="<?php echo $ret['Login_Email']; ?>" required >
                </div>

                <div class="form-group col-lg-12">
                    <label>Phone Number</label>
                    <input type="tel" name="phonenumber" class="form-control" id="phonenumber" value="<?php echo $ret['Employee_Mobile']; ?>" pattern="^\d{3}-\d{3}-\d{4}$" required>
                </div>

                <div class="form-group col-lg-12">
                    <label>Address</label>
                    <input type="" name="address" class="form-control" id="address" value="<?php echo $ret['Employee_Address']; ?>">
                </div>

                <div class="form-group col-lg-6">
                    <label>City</label>
                    <input type="" name="city" class="form-control" id="city" value="<?php echo $ret['Employee_City']; ?>" required>
                </div>

                <div class="form-group col-lg-6">
                    <label>Postal Code</label>
                    <input type="" name="postalcode" class="form-control" id="postalcode" value="<?php echo $ret['Employee_Postal']; ?>" required>
                </div>

                <div class="col-sm-12">
                    <input type="submit" class="btn btn-primary" name="changebasicdetails" id="changebasicdetails" value="Change">
<br><br>
                </div>

        </div>
    </form>
    <!-- /.panel -->

    <br>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                Employer Details
            </div>

            <div class="col-sm-12" style="margin-bottom: 20px; margin-top: 10px;">
                <label>Company</label>
                <input type="" name="other" class="form-control" id="" value="<?php echo $ret['Company_Name']; ?>" disabled>
            </div>


        <div class="form-group col-lg-12">
            <label>Position</label>
            <input type="" name="position" class="form-control" id="" value="<?php echo $ret['Employee_Position']; ?>" disabled>
        </div>

<!--
        <div class="col-sm-12">
            <button type="submit" class="btn btn-primary">Change</button>
        </div>
-->

    </div>
</div>
<!-- /.panel -->
</div>


<center><form id="deleteform" role="form" method="POST" onsubmit="return confirm('Are you sure you want to delete your account? Warning: It cannot be undone online!');" >
<input type="submit" class="btn btn-danger" value="Delete Account" name="deleteaccount" id="deleteaccount">
</form></center>

<br>
<br>


</div>
<!-- /.row -->


<?php
    include 'template2.php';
?>
