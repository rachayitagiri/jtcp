<meta http-equiv="refresh" content="30" >

<?php
    include '../dbmanage.php';
    include '../paypal/paypal_config.php';

    if($_POST){
      $employee_id = $_POST['eid'];
      $class_id = $_POST['class_id'];
      $course_id = $_POST['course_id'];
      $course_name = $_POST['course_name'];
      $ret = getTimeStampFromFeesCheckByClassIDEmployeeID($class_id, $employee_id);

      //Read file flag.php to check whether it is true or not. If true then only allowed to pay fees
      $myfile = fopen("../cronjobs/flag.txt", "r") or die("Unable to open file!");
      $check = fread($myfile,filesize("../cronjobs/flag.txt"));
      fclose($myfile);
      if($ret!=null && strcasecmp( $check, 'true' ) == 0)
      {
        // $timestamp = $ret['Timestamp'];
        // echo round((strtotime($today) - strtotime($timestamp))/3600, 1);
        ?>
        <center>
        <br><br><br><br>
        <?php 
          echo '<script language="javascript">';
          echo 'alert("Please confirm that you are paying for the same class as you are enrolled in:\n'.$course_name.'\n\nIn case of a discrepancy, contact us on the website.");';
          echo '</script>';
        ?>
        <h4>Pay Fees for: <h3>&quot;<?php echo $course_name; ?>&quot;</h3></h4>
        <h5>Click &quot;Buy Now&quot; to pay using Paypal.</h5>
        <h6>This session will expire in <font style="color:red;">30</font> seconds.</h6>
        <h5>Class can be dropped 15 days prior to the start date. No refund will be issued past that date, students can only create request for transfer (change student in the same class)</h5>
        <h5>The class will be confirmed 15 day prior to the first day. Please contact the program coordinator <a href='../contact.php' target='_blank'>here</a> to confirm the class at that time and before booking your travel arrangement.</h5>
        <?php
        return_paypal_code($course_id,$class_id,$employee_id);
        ?>
        <br><br><br><br>
        <h5>Payment Provided by: </h5>
        <img src='../img/CalAPA_Logo.jpg'>
        </center>
      <?php
      }
      else{
        echo "Error! Please try again in a while.";
      }
    }
    else{
      echo "Session Expired! Please try again!";
    }
?>
