<?php

    session_start();
    if($_SESSION['Login_ID']==null || ($_SESSION['Role']!=1 && $_SESSION['Role']!=4))
    {
        header("Location: ../login.php");
    }

    function coursestatus($cid, $ret)
    {
      // return "<a href=''>Under Maintenance!</a>";
        if($ret['Ferpa']==0)
        {
            return "<a href='javascript:void(0)' onclick=\"document.getElementById('ferpabutton').click()\">Sign Ferpa</a>";
        }
        else if(count($ret['Certificate'])==0 || $ret['Certificate'][0]['Status_ID']!=5)
        {
            return "<a href='javascript:void(0)' onclick=\"document.getElementById('saferyquizbutton').click()\">Take Safety Quiz</a>";
        }
        else
        {
            $myCertificate = array();
            $myFinalCertificate = array();
            for($i=0;$i<count($ret['Certificate']);$i++)
            {
                if($ret['Certificate'][$i]['Certificate_Final']==0)
                {
                    $temp_cid = $ret['Certificate'][$i]['Course_ID'];
                    $temp_certi = array();
                    $temp_certi['Status_ID'] = $ret['Certificate'][$i]['Status_ID'];
                    $temp_certi['Course_Name'] = $ret['Certificate'][$i]['Course_Name'];
                    $temp_certi['Certificate_Code'] = $ret['Certificate'][$i]['Certificate_Code'];
                    $temp_certi['Certificate_ExpireDate'] = $ret['Certificate'][$i]['Certificate_ExpireDate'];
                    $myCertificate[$temp_cid] = $temp_certi;
                }
                else
                {
                    $temp_cid = $ret['Certificate'][$i]['Course_ID'];
                    $temp_certi = array();
                    $temp_certi['Status_ID'] = $ret['Certificate'][$i]['Status_ID'];
                    $temp_certi['Course_Name'] = $ret['Certificate'][$i]['Course_Name'];
                    $temp_certi['Certificate_Code'] = $ret['Certificate'][$i]['Certificate_Code'];
                    $temp_certi['Certificate_ExpireDate'] = $ret['Certificate'][$i]['Certificate_ExpireDate'];
                    $myFinalCertificate[$temp_cid] = $temp_certi;
                }
            }
            $Certificate = $myCertificate[$cid+1];
            $FinalCertificate = $myFinalCertificate[$cid+1];


            $Course_Name = $Certificate['Course_Name'];
            //For HMA 2
            /*
            if($cid==2)
            {
                if($myCertificate[2]==null || $myFinalCertificate[2]['Status_ID']!=3)
                {
                    return "<a href='#'>Complete Hot Mix Asphalt I First</a>";
                }
            }
             */
            $today = date("Y-m-d");
            $cc = $Certificate['Certificate_Code'];
            $materialfile = getMaterialFile($cid+1);

            if(count($FinalCertificate)>0)
            {
                if($FinalCertificate['Status_ID']==3)
                {
                    if($FinalCertificate['Certificate_ExpireDate']>=$today)
                    {
                        return "<a href='../certificate.php?code=" . $FinalCertificate['Certificate_Code'] . "' target='_blank'>Passed | Print Certificate </a>";
                    }
                    else
                    {
                        if($Certificate['Certificate_ExpireDate']>=$today)
                        {
                            return "<a href='javascript:void(0)' class='classEnroll' id='" . ($cid+1) . "' val='" . $cc . "'>Enroll to Class</a>";
                        }
                        else
                        {
                            // return "<a href=''>Under Maintenance!</a>";
                            // return "<a href='#' onClick='gotoPrereq($cid+1)'>Take Prerequisite Test</a> | <a href='$materialfile' target='_blank'>Download Manual</a>";
                            //temp solution to bypass Prereq Exam for all courses
                            // return "<a href='javascript:void(0)' class='classEnroll_withoutPrereq' id='" . ($cid+1) . "' val='" . $cc . "'>Enroll to Class</a>";

                            // if($cid==2 && ($myCertificate[2]==null || $myFinalCertificate[2]['Status_ID']!=3)){
                            //   return "<a href='javascript:void(0)'>Pass Hot Mix Asphalt I to Enroll</a>";
                            // }
                            return "<a href='#' onClick='gotoPrereq($cid+1)'>Take Prerequisite Test</a> | <a href='$materialfile' target='_blank'>Download Manual</a>";
                        }
                    }
                }
                else if($FinalCertificate['Status_ID']==2)
                {
                    return "<a href='javascript:void(0)' id='$cc' class='schedule'>View Schedule</a> | <a href='#' id='$cc' class='drop'>Drop Class</a>";
                }
                // else if($cid==2 && ($myCertificate[2]==null || $myFinalCertificate[2]['Status_ID']!=3))
                // {
                //   return "<a href='#' onClick='gotoPrereq($cid+1)'>Take Prerequisite Test</a> | <a href='$materialfile' target='_blank'>Download Manual</a>";
                //
                //     // return "<a href=''>Under Maintenance!</a>";
                //     // return "<a href='javascript:void(0)'>Pass Hot Mix Asphalt I to Enroll</a>";
                //     // return "<a href='javascript:void(0)' class='classEnroll_withoutPrereq' id='" . ($cid+1) . "' val='" . $cc . "'>Enroll to Class</a>";
                // }
                else
                {
                    //return "<a href='#'>Enroll to Class</a>";
                    return "<a href='javascript:void(0)' class='classEnroll' id='" . ($cid+1) . "'>Enroll to Class</a>";
                }
                /*
                else if($Certificate['Status_ID']==4)
                {
                    return "<a href='#'>Failed</a>";
                }
                 */
            }
            else
            {
                if($Course_Name==null || $Certificate['Certificate_ExpireDate']<=$today)
                {
                    // return "<a href=''>Under Maintenance!</a>";
                    // return "<a href='#' onClick='gotoPrereq($cid+1)'>Take Prerequisite Test</a> | <a href='$materialfile' target='_blank'>Download Manual</a>";
                    //temp solution to bypass Prereq Exam for all courses
                    // return "<a href='javascript:void(0)' class='classEnroll_withoutPrereq' id='" . ($cid+1) . "' val='" . $cc . "'>Enroll to Class</a>";
                    return "<a href='#' onClick='gotoPrereq($cid+1)'>Take Prerequisite Test</a> | <a href='$materialfile' target='_blank'>Download Manual</a>";
                    //
                    // if($cid==2 && ($myCertificate[2]==null || $myFinalCertificate[2]['Status_ID']!=3)){
                    //   return "<a href='javascript:void(0)'>Pass Hot Mix Asphalt I to Enroll</a>";
                    // }
                }
                else if($Certificate['Status_ID']==2)
                {
                    return "<a href='javascript:void(0)' id='$cc' class='schedule'>View Schedule</a> | <a href='#' id='$cc' class='drop'>Drop Class</a>";
                }
                // else if($cid==2 && ($myCertificate[2]==null || $myFinalCertificate[2]['Status_ID']!=3))
                // {
                //     // return "<a href=''>Under Maintenance!</a>";
                //     // return "<a href='javascript:void(0)'>Pass Hot Mix Asphalt I to Enroll</a>";
                //     return "<a href='#' onClick='gotoPrereq($cid+1)'>Take Prerequisite Test</a> | <a href='$materialfile' target='_blank'>Download Manual</a>";
                //     // return "<a href='javascript:void(0)' class='classEnroll_withoutPrereq' id='" . ($cid+1) . "' val='" . $cc . "'>Enroll to Class</a>";
                // }
                else
                {
//                    return "<a href='#'>Enroll to Class</a>";
                    return "<a href='javascript:void(0)' class='classEnroll' id='" . ($cid+1) . "' val='" . $cc . "'>Enroll to Class</a>";
                }
            }
        }
    }

?>



<!-- Modal -->
<div class="modal fade" id="viewschedule" role="dialog">
<div class="modal-dialog" id="viewschedule-dialog" >

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="viewschedule-tittle">Class Schedule</h4>
</div>
<div class="modal-body" style="overflow-y: scroll;">
<p id="viewscheduledata">Loading...</p>
</div>
<div class="modal-footer">
<button type="button" id="viewscheduleclose" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>


<!-- Modal -->
<div class="modal fade" id="classenrollmodel" role="dialog">
<div class="modal-dialog" id="classenrollmodal-dialog" >

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="userprogressmodel-tittle">Enroll to Class</h4>
</div>
<div class="modal-body" style="overflow-y: scroll;">
<p id="showclassdata">Loading...</p>
</div>
<div class="modal-footer">
<button type="button" id="closeclasscloseenrollmodel" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>
