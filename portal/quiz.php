<?php
    include 'template1.php';
    if(!$_POST)
    {
        echo "<script type='text/javascript'>window.location.href='dashboard.php';</script>";
    }
    $quiz_id = $_POST['quiz_id'];
    //$ret is from Template1.php
    $emp_id = $ret['Employee_ID'];
    $ret = getQuiz($quiz_id);
    for($i=0;$i<count($ret);$i++)
    {
        $results[] = $ret[$i]['Questions_Answer'];
    }
?>

<script type="text/javascript">

$(document).ready(function(){
                  var jArray= <?php echo json_encode($results); ?>;
                  var count=0;

                  $("#sub").click(function(){
                                  count=0;
                                  for(var i=0;i<jArray.length;i++)
                                  {
                                    var myclass = ".optradio"+i+":checked";
                                    var val = $(myclass).val();
                                    if(val==null)
                                    {
                                        alert("Please attend all the questions!");
                                        return;
                                    }
                                    if(val==jArray[i])
                                    {
                                        document.getElementById("question"+i).style.color = 'black';
                                        count++;
                                    }
                                  }
                                  if(count==jArray.length)
                                  {
                                    $.ajax({
                                         url: "generatecertificate.php",
                                         type: "post",
                                         data: {'employee_id' : '<?php echo $emp_id; ?>', 'course_id':'<?php echo $quiz_id; ?>'} ,
                                         success: function (response) {
                                         // you will get response from your php page (what you echo or print)
                                           var json = JSON.parse(response);
                                           if(json['Status']==1)
                                           {
//                                                var link = "../certificate.php?code=" + json['Certficate_Code'];
                                                var link = "dashboard.php";
                                                window.location.href=link;
                                           }
                                           else
                                           {
                                                alert(json['Message']);
                                           }
                                         },
                                               error: function(jqXHR, textStatus, errorThrown) {
                                               console.log(textStatus, errorThrown);
                                         }
                                    });
                                  }
                                  else
                                  {
                                    for(var j=0;j<jArray.length;j++)
                                    {
                                        var myclass = ".optradio"+j+":checked";
                                        var val = $(myclass).val();
                                        if(val==jArray[j])
                                        {
                                            document.getElementById("question"+j).style.color = 'black';
                                        }
                                        else
                                        {
                                            document.getElementById("question"+j).style.color = 'red';
                                        }
                                    }
                                    alert("Please correct your answers to pass the quiz.");
                                  }
                               });
                  });



</script>

<title>Prerequisite Quiz</title>

            <div class="row">
                <div class="col-lg-9">
                    <h3 class="page-header"><?php echo getCourse($quiz_id); ?></h3>
                </div>
                <div class="col-lg-3">
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /.row -->
            <div class="row">
                <!-- Extra Space -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                    <h4>Reference Material</h4>
<p id="filedata">
<iframe src="<?php echo getMaterialFile($quiz_id); ?>" frameborder="0" width="100%" height="500px" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</p>
                    </div>

                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-6">
                    <div class="panel panel-default" style="overflow-y: scroll; height:500px; padding: 9px;">
<?php
    for($i=0;$i<count($ret);$i++)
    {
?>

                        <span id="<?php echo 'question'.$i; ?>"><h4><?php echo $i+1 . ". " . iconv("UTF-8", "ISO-8859-1//TRANSLIT//IGNORE", $ret[$i]['Questions _Name']);?></h4></span>
                        <?php
                            if($ret[$i]['Questions_Image']!=null)
                            {
                                echo '<center><img src="data:image/jpeg;base64,'.base64_encode( $ret[$i]['Questions_Image'] ).'" height=90px width=90px/></center>';
                            }
                        ?>
                        <h5>Select best case from options.</h5>

<div class="modal-body">
<div class="col-xs-3 col-xs-offset-5">
<div id="loadbar" style="display: none;">
<div class="blockG" id="rotateG_01"></div>
<div class="blockG" id="rotateG_02"></div>
<div class="blockG" id="rotateG_03"></div>
<div class="blockG" id="rotateG_04"></div>
<div class="blockG" id="rotateG_05"></div>
<div class="blockG" id="rotateG_06"></div>
<div class="blockG" id="rotateG_07"></div>
<div class="blockG" id="rotateG_08"></div>
</div>
</div>

<?php
    for($j=0;$j<count($ret[$i]['Question_Options']);$j++)
    {
?>
<div class="radio">
<label><input type="radio" name="<?php echo 'optradio'.$i; ?>" value="<?php echo $j+1; ?>" class="<?php echo 'optradio'.$i; ?>">
<?php echo iconv("UTF-8", "ISO-8859-1//TRANSLIT//IGNORE", $ret[$i]['Question_Options'][$j]);  ?></label>
</div>

<?php
    }
?>

                        <!-- /.panel-body -->
                    </div>
<?php
    }
?>

                    <!-- /.panel -->
                    <!-- /.panel .chat-panel -->
                </div>
<h1><button type="button" class="btn btn-success" id="sub">Submit</button></h1>

                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->


<?php
    include 'template2.php';
?>
