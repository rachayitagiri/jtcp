<?php
    error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
    /*
      Developed by Rushi Jash at CSULB
    */
    require 'db_config.php';

    date_default_timezone_set('America/Los_Angeles');

    session_start();

    $today = date("Y-m-d H:i:s");

    //Fetch all companies
    function getCompany()
    {
        $ret = array();
        $query = "SELECT * FROM `Company`";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Company_ID']=$row[0];
            $temp_ret['Company_Other']=$row[1];
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    //Get company by employee ID
    function getCompanyByEmployeeID($eid) 
    {
        $ret = array();
        $query = "SELECT * FROM `Employee` WHERE `Employee_ID` = '$eid';";
        $res = mysql_query($query);
        while($row = mysql_fetch_array($res))
        {
            $company_id = $row['Company_ID'];

            $ret['Company_ID'] = $company_id;
            //If company id is zero, company name should be in the company_other column of employee table
            if($company_id!=0)
            {
                $companyother = $row['Company_Other'];
                
                //if company name is null, then check for the name in the company table
                if($companyother=="")
                {
                    $query2="SELECT * FROM `Company` WHERE `Company_ID`='$company_id';";
                    $res2 = mysql_query($query2);
                    if($row2=mysql_fetch_array($res2))
                    {
                        $ret['Company_Name'] = $row2['Company_Name'];
                    }
                }
                else
                {
                    $ret['Company_Name'] = $companyother;
                }
            }
            else
            {
                $ret['Company_Name'] = $row['Company_Other'];
            }
        }
        return $ret;
    }

    //get Employee/Class/Instructor Counts
    function getCounts()
    {
        $ret = array();
        $query = "select count(*) from `Employee`";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $ret['Employee_Count'] = $row[0];
        }
        $query = "select count(*) from `Class`";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $ret['Class_Count'] = $row[0];
        }
        $query = "select count(*) from `Instructor`";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $ret['Instructor_Count'] = $row[0];
        }
        return $ret;
    }

    //Employee Signup
    function signup($prefix, $firstname, $initial, $lastname, $email, $password, $mobile, $company_id, $company_other, $position, $address, $city, $postal, $type)
    {
        $email = strtolower($email);
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $ret['Status']=0;
            $ret['Message']='Email Address Already Exist!';
            return $ret;
        }
        $currentdate= date("Y-m-d");
        //Login 0 - for verify
        $query = "insert into `Login` values (NULL,'$email', '$password', $type, 0, '$currentdate')";
        $res = mysql_query($query);
        $lid = mysql_insert_id();

        if($res>0)
        {
            $address = mysql_real_escape_string($address);
            if(strlen($company_id)==0){
              $query = "insert into `Employee` values (NULL,'$prefix', '$firstname', '$initial', '$lastname', '$mobile', 0, '$company_other', '$position', '$address', '$city', '$postal', $lid)";
            }
            else{
              $query = "insert into `Employee` values (NULL,'$prefix', '$firstname', '$initial', '$lastname', '$mobile', '$company_id', '$company_other', '$position', '$address', '$city', '$postal', $lid)";
            }
            $res = mysql_query($query);

            if($res>0)
            {
                $ret['Status']=1;
                $ret['Login_ID']=$lid;
                $ret['Message']='User Registed!';
                return $ret;
            }
            else
            {
                $query = "delete from `Login` where `Login_Email`='$email'";
                $res = mysql_query($query);
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;

            }
        }
    }

    function resetPassword($email)
    {
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $newpassword = randomPassword();
            $newpassword_hash = password_hash($newpassword, PASSWORD_DEFAULT);
            $query = "update `Login` SET `Login_Password`='$newpassword_hash' where `Login_Email`='$email'";
            $res = mysql_query($query);
            if($res>0)
            {
                $ret['Status']=1;
                $ret['New_Password']=$newpassword;
                $ret['Message']='Password reset!';
                return $ret;
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;
            }
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Email Address does not Exist!';
            return $ret;

        }
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function addFerpa($eid, $signature, $signdate)
    {
        $ret = array();
        $signature = mysql_real_escape_string($signature);
        // $signdate = mysql_real_escape_string($signdate);

        $query = "insert into `Ferpa` values(NULL,$eid,'$signature','$signdate')";

        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Ferpa Signed!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

/*
    function validateDate($date, $format = 'Y-m-d')
    {
      $d = DateTime::createFromFormat($format, $date);
      return $d && $d->format($format) == $date;
    }
*/

    function checkFerpa($eid)
    {
        $ret = array();
        $query = "select * from `Ferpa` where `Employee_ID`='$eid'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $ret['Status']=1;
            $ret['Message']='Ferpa is signed by the user!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Ferpa is not signed by the user!';
        }
        return $ret;
    }

    function deleteCoordinator($lid)
    {
        $ret = array();
        $query = "delete from `Coordinator` where `Login_ID`='$lid'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $query = "delete from `Login` where `Login_ID`='$lid'";
            $res = mysql_query($query);
            if($res>0)
            {
                $ret['Status']=1;
                $ret['Message']='Coordinator Deleted';
            }
            else{
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
            }
            return $ret;
        }

    }

    //Admin Function
    //Completely Delete Employee from the database
    //Keep adding tables as required
    function deleteEmployee($email){
      $ret = array();
      $query = "select `Login_ID` from `Login` where `Login_Email`='$email'";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res))
      {
        $lid = $row[0];
        $query = "select `Employee_ID` from `Employee` where `Login_ID`='$lid'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $eid = $row[0];

            //Login Table
            $query = "delete from `Login` where `Login_ID`='$lid'";
            $res = mysql_query($query);
            //Employee Table
            $query = "delete from `Employee` where `Employee_ID`='$eid'";
            $res = mysql_query($query);
            //Class Deleted Employee Table
            $query = "delete from `Class_Deleted_Employee` where `Employee_ID`='$eid'";
            $res = mysql_query($query);
            //Class Employee Table
            $query = "delete from `Class_Employee` where `Employee_ID`='$eid'";
            $res = mysql_query($query);
            //Class Waitlist Table
            $query = "delete from `Class_Waitlist` where `Employee_ID`='$eid'";
            $res = mysql_query($query);
            //Class Course Certificate Table
            $query = "delete from `Course_Certificate` where `Employee_ID`='$eid'";
            $res = mysql_query($query);
            //Class Employee Activity Table
            $query = "delete from `Employee_Activity` where `Employee_ID`='$eid'";
            $res = mysql_query($query);
            //Class Ferpa Table
            $query = "delete from `Ferpa` where `Employee_ID`='$eid'";
            $res = mysql_query($query);
            //Class Prerequisite Certificate Table
            $query = "delete from `Prerequisite_Certificate` where `Employee_ID`='$eid'";
            $res = mysql_query($query);
            //Course Evaluation Table
            $query = "delete from `Course_Evaluation_Answer` where `Employee_ID`='$eid'";
            $res = mysql_query($query);
            //Fees Check Table
            $query = "delete from `Fees_Check` where `Login_ID`='$lid'";
            $res = mysql_query($query);

            $ret['Status']=1;
            $ret['Message']='Employee Deleted';
        }
        else{
          $ret['Status']=0;
          $ret['Message']='Error! Please try again in a while.';
        }
      }
      else{
        $ret['Status']=0;
        $ret['Message']='Error! Please try again in a while.';
      }
      return $ret;
    }

    function signup_coordinator($firstname, $lastname, $email, $password)
    {
        $email = strtolower($email);
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $ret['Status']=0;
            $ret['Message']='Email Address Already Exist!';
            return $ret;
        }

        $currentdate= date("Y-m-d");
        $password = password_hash($password, PASSWORD_DEFAULT);
        $query = "insert into `Login` value (NULL,'$email', '$password', 3, 0, '$currentdate')";
        $res = mysql_query($query);
        $lid = mysql_insert_id();

        if($res>0)
        {
            $query = "insert into `Coordinator` values (NULL,'$firstname', '$lastname',$lid)";
            $res = mysql_query($query);

            if($res>0)
            {
                $ret['Status']=1;
                $ret['Message']='Co-ordinator Registed!';
                return $ret;
            }
            else
            {
                $query = "delete from `Login` where `Login_Email`='$email'";
                $res = mysql_query($query);
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;
            }
        }
    }

//    signup_admin("shadi.saadeh@csulb.edu","admin@jtcp05062017");
    function signup_admin($email, $password)
    {
        $email = strtolower($email);
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $ret['Status']=0;
            $ret['Message']='Email Address Already Exist!';
            return $ret;
        }

        $currentdate= date("Y-m-d");
        $password = password_hash($password, PASSWORD_DEFAULT);
        $query = "insert into `Login` value (NULL,'$email', '$password', 5, 0, '$currentdate')";
        $res = mysql_query($query);
        $lid = mysql_insert_id();

        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Co-ordinator Registed!';
            return $ret;
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
            return $ret;
        }
    }


    function signup_instructor($firstname, $lastname, $email, $password)
    {
        $email = strtolower($email);
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $ret['Status']=0;
            $ret['Message']='Email Address Already Exist!';
            return $ret;
        }

        $currentdate= date("Y-m-d");

        $query = "insert into `Login` value (NULL,'$email', '$password', 2, 0, '$currentdate')";
        $res = mysql_query($query);
        $lid = mysql_insert_id();

        if($res>0)
        {
            $query = "insert into `Instructor` values (NULL,'$firstname', '$lastname',$lid)";
            $res = mysql_query($query);

            if($res>0)
            {
                $ret['Status']=1;
                $ret['Message']='Instructor Registed!';
                return $ret;
            }
            else
            {
                $query = "delete from `Login` where `Login_Email`='$email'";
                $res = mysql_query($query);
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;
            }
        }
    }

    function getCoordinatorsDetails()
    {
        $ret = array();
        $query = "select * from `Login`,`Coordinator` where `Coordinator`.`Login_ID`=`Login`.`Login_ID`";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Login_ID'] = $row[0];
            $temp_ret['Login_Email'] = $row[1];
            $temp_ret['Coordinator_ID'] = $row[6];
            $temp_ret['Coordinator_FirstName'] = $row[7];
            $temp_ret['Coordinator_LastName'] = $row[8];
            $ret[] = $temp_ret;
        }
        return $ret;
    }


    function getInstructorsDetails()
    {
        $ret = array();
        $query = "select * from `Instructor`,`Login` where `Instructor`.`Login_ID`=`Login`.`Login_ID`";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Instructor_ID']=$row[0];
            $temp_ret['Instructor_FirstName']=$row[1];
            $temp_ret['Instructor_LastName']=$row[2];
            $temp_ret['Login_ID']=$row[3];
            $temp_ret['Login_Email']=$row[5];
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    function getInstructorDetails($lid)
    {
        $ret = array();
        $query = "select * from `Instructor`,`Login` where `Instructor`.`Login_ID`=`Login`.`Login_ID` and `Login`.`Login_ID`='$lid'";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $ret['Instructor_ID'] = $row[0];
            $ret['Instructor_FirstName']=$row[1];
            $ret['Instructor_LastName']=$row[2];
            $ret['Login_ID']=$row[3];
            $ret['Login_Email']=$row[5];
        }
        return $ret;
    }

    function getInstructorsByCourse($cid)
    {
        $ret = array();
        $query = "select * from `Instructor`,`Class` where `Instructor`.`Instructor_ID`=`Class`.`Instructor_ID` AND `Class`.`Course_ID`='$cid' GROUP BY `Instructor`.`Instructor_ID`";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Instructor_ID']=$row[0];
            $temp_ret['Instructor_FirstName']=$row[1];
            $temp_ret['Instructor_LastName']=$row[2];
            $ret[] = $temp_ret;
        }
        return $ret;
    }


    function getExtraCourseDetails($cid)
    {
        $ret = array();
        $query = "select * from `Course_Details`,`Course` where `Course`.`Course_ID`=`Course_Details`.`Course_ID` AND `Course_Details`.`Course_ID`='$cid'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $ret['Course_Cost']=$row[1];
            $ret['Course_Description']=$row[2];
            $ret['Course_Details_Extra']=$row[3];
            $ret['Course_ID']=$row[4];
            $ret['Course_Name']=$row[6];
        }
        return $ret;
    }

    function updateExtraCourseDetails($cid,$cost,$details)
    {
        $ret = array();
        $query = "update `Course_Details` SET `Course_Details_Cost`='$cost', `Course_Details_Extra`='$details' where `Course_ID`='$cid'";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Course Details Changed!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }


    function getExtraCoursesDetails()
    {
        $ret = array();
        $query = "select * from `Course_Details`,`Course` where `Course`.`Course_ID`=`Course_Details`.`Course_ID`";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Course_Cost']=$row[1];
            $temp_ret['Course_Description']=$row[2];
            $temp_ret['Course_Details_Extra']=$row[3];
            $temp_ret['Course_ID']=$row[4];
            $temp_ret['Course_Name']=$row[6];
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    function updateInstructorDetails($lid, $email, $password, $firstname, $lastname)
    {
        $email = strtolower($email);
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $query = "select * from `Login` where `Login_Id`='$lid'";
            $res = mysql_query($query);
            if($row=mysql_fetch_array($res))
            {
                if(strcmp($row[1],$email)!=0)
                {
                    $ret['Status']=0;
                    $ret['Message']='Email Address Already Exist!';
                    return $ret;
                }
                else
                {
                    goto updateInstructorgoto;
                }
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;
            }
        }
        else
        {
        updateInstructorgoto:
            if($password!=null)
            {
                $password = password_hash($password, PASSWORD_DEFAULT);
                $query = "update `Instructor`,`Login` SET `Login_Email`='$email', `Login_Password`='$password', `Instructor`.`Instructor_FirstName`='$firstname', `Instructor`.`Instructor_LastName`='$lastname' where `Login`.`Login_ID`='$lid' and `Instructor`.`Login_ID`=`Login`.`Login_ID`";
            }
            else
            {
                $query = "update `Instructor`,`Login` SET `Login_Email`='$email', `Instructor`.`Instructor_FirstName`='$firstname', `Instructor`.`Instructor_LastName`='$lastname' where `Login`.`Login_ID`='$lid' and `Instructor`.`Login_ID`=`Login`.`Login_ID`";

            }
            $res = mysql_query($query);
            if($res>0)
            {
                $ret['Status']=1;
                $ret['Message']='Basic Details Changed Successfully!';
                return $ret;
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;

            }
        }
    }

    function deleteInstructorDetails($lid)
    {
        $ret = array();
        $query = "delete from `Login` where `Login_ID`='$lid';";
        $res = mysql_query($query);
        if($res>0)
        {
            $query = "delete from `Instructor` where `Login_ID`='$lid'";
            $res = mysql_query($query);
            if($res>0)
            {
                //Make Assigned Class Instructor NULL
                //Your Code Here
                $query = "update `Class` SET `Instructor_ID`='0' where `Instructor_ID`='$lid'";
                $res = mysql_query($query);
                //Check

                $ret['Status']=1;
                $ret['Message']='Instructor Deleted!';
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
            }
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    //Add Class By Coordinator
    function add_class($classname, $coordinatorid, $instructorid, $courseid, $locationid, $schedule, $scheduletimeto, $scheduletimefrom, $classcapacity, $classpracticalexam, $classwrittenexam)
    {
        $ret = array();
        $query = "select * from `Class` where `Class_Name`='$classname'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $ret['Status']=0;
            $ret['Message']='Class Name Already Exist!';
            return $ret;
        }

        $currentdate= date("Y-m-d");

        $query = "insert into `Class` value (NULL,'$classname', $classcapacity, '$classpracticalexam', '$classwrittenexam', $courseid, $coordinatorid, $instructorid, $locationid)";
        $res = mysql_query($query);
        $cid = mysql_insert_id();

        if($res>0)
        {
            $scheduleArray = explode(', ', $schedule);
            $scheduletimetoArray = explode(', ', $scheduletimeto);
            $scheduletimefromArray = explode(', ', $scheduletimefrom);


            for($i=0;$i<count($scheduleArray);$i++)
            {
                $tempdate = $scheduleArray[$i];
                $temptotime = $scheduletimetoArray[$i] . ":00";
                $tempfromtime = $scheduletimefromArray[$i] . ":00";

                $query = "insert into `Class_Schedule` values (NULL,'$tempdate',$cid, '$tempfromtime', '$temptotime')";
                $res = mysql_query($query);
            }
            $ret['Status']=1;
            $ret['Message']='Class Added!';
            return $ret;
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
            return $ret;
        }
    }


    function getClasses()
    {
        $ret = array();
        $query = "SELECT * from `Class`,`Course`,`Location`,`Instructor` where `Class`.`Course_ID`=`Course`.`Course_ID` AND `Class`.`Instructor_ID`=`Instructor`.`Instructor_ID` AND `Location`.`Location_ID`=`Class`.`Location_ID`";
        $res = mysql_query($query);
        $fetchedclasses = mysql_affected_rows();
        $query2 = "select * from `Class`";
        $res2 = mysql_query($query2);
        $totalclasses = mysql_affected_rows();
        if($totalclasses!=$fetchedclasses)
        {
            $query = "SELECT * from `Class`,`Course`,`Location` where `Class`.`Course_ID`=`Course`.`Course_ID` AND `Location`.`Location_ID`=`Class`.`Location_ID`";
            $res = mysql_query($query);
        }
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $cid = $row[0];
            $temp_ret['Class_ID'] =$cid;
            $temp_ret['Class_Name'] = $row[1];
            $temp_ret['Class_Capacity'] = $row[2];
            $temp_ret['Class_Practical_Date'] = $row[3];
            $temp_ret['Class_Written_Date'] = $row[4];
            $temp_ret['Course_Name'] = $row[10];
            $temp_ret['Instructor_LastName'] = $row[19];
            $temp_ret['Instructor_FirstName'] = $row[18];
            $temp_ret['Location_Name'] = $row[12];


            $query2 = "SELECT * FROM `Class_Employee` WHERE `Class_ID`='$cid'";
            $res2 = mysql_query($query2);
            $temp_ret['Class_Filled'] = mysql_affected_rows()==-1 ? 0 : mysql_affected_rows();


            $temp_ret['Class_Dates'] = array();
            $query2 = "SELECT * FROM `Class_Schedule` WHERE `Class_ID`='$cid'";
            $res2 = mysql_query($query2);
            while($row2=mysql_fetch_array($res2))
            {
                $temp_ret2 = array();
                $temp_ret2 = $row2[1];
                $temp_ret['Class_Dates'][] = $temp_ret2;
                $temp_ret['Class_TimeFrom'][] = $row2[3];
                $temp_ret['Class_TimeTo'][] = $row2[4];
            }
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    function getCourseIDByClassID($cid){
      $ret = array();
      $query = "select * from `Class` where `Class_ID`='$cid'";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res)){
        return $row[5];
      }
      return 0;

    }

    //Get Classes that are starting in 15 days and has less than 20 students
    function getLowStudentsClasses()
    {
        $ret = array();
        $query = "select * from `Class`";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $cid = $row[0];
            $temp_ret['Class_ID'] =$cid;
            $temp_ret['Class_Name'] = $row[1];
            $temp_ret['Class_Capacity'] = $row[2];
            $temp_ret['Class_Practical_Date'] = $row[3];
            $temp_ret['Class_Written_Date'] = $row[4];
            $temp_ret['Course_Name'] = $row[10];
            $temp_ret['Instructor_LastName'] = $row[19];
            $temp_ret['Instructor_FirstName'] = $row[18];
            $temp_ret['Location_Name'] = $row[12];

            $query2 = "select * from `Class_Employee` where `Class_ID`='$cid'";
            $res2 = mysql_query($query2);
            $temp_ret['Class_Filled'] = mysql_affected_rows()==-1 ? 0 : mysql_affected_rows();
            $temp_ret['Class_Dates'] = array();
            $query2 = "select * from `Class_Schedule` where `Class_ID`='$cid'";
            $res2 = mysql_query($query2);
            if($row2=mysql_fetch_array($res2))
            {
                $temp_ret2 = array();
                $temp_ret2 = $row2[1];
                $temp_ret['Class_Dates'][0] = $temp_ret2;
            }
            $date1=date_create($temp_ret['Class_Dates'][0]);
            $date2=date_create(date("Y-m-d"));
            $diff = date_diff($date1,$date2)->format("%R%a");
            if(($diff<=15 && $diff>0) && $temp_ret['Class_Filled']<20)
            {
              $temp_ret['Class_Diff'] = $diff;
              $ret[] = $temp_ret;
            }
            // else {
            //   $ret['dif'][] = $diff;
            //   $ret['capacity'][] = $temp_ret['Class_Filled'];
            // }
            // else {
            //   $ret[]['date1'] = $date1;
            //   $ret[]['date2'] = $date2;
            //   $ret[]['date_diff'] = $diff;
            //   $ret[]['capacity'] = $temp_ret['Class_Capacity'];
            // }
        }
        return $ret;
    }


    function getClassesByInstructor($iid)
    {
        $ret = array();
        $query = "select * from `Class`,`Course`,`Instructor`,`Location` where `Class`.`Course_ID`=`Course`.`Course_ID` AND `Class`.`Instructor_ID`=`Instructor`.`Instructor_ID` AND `Class`.`Instructor_ID`='$iid' AND `Location`.`Location_ID`=`Class`.`Location_ID`";
        $res = mysql_query($query);
        if(mysql_affected_rows()==0)
        {
            $query = "select * from `Class`,`Course` where `Class`.`Course_ID`=`Course`.`Course_ID`";
            $res = mysql_query($query);
        }
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $cid = $row[0];
            $temp_ret['Class_ID'] =$cid;
            $temp_ret['Class_Name'] = $row[1];
            $temp_ret['Class_Capacity'] = $row[2];
            $temp_ret['Class_Practical_Date'] = $row[3];
            $temp_ret['Class_Written_Date'] = $row[4];
            $temp_ret['Course_Name'] = $row[10];
            $temp_ret['Instructor_LastName'] = $row[12];
            $temp_ret['Instructor_FirstName'] = $row[13];
            $temp_ret['Location_Name'] = $row[16];


            $query2 = "select * from `Class_Employee` where `Class_ID`='$cid'";
            $res2 = mysql_query($query2);
            $temp_ret['Class_Filled'] = mysql_affected_rows()==-1 ? 0 : mysql_affected_rows();


            $temp_ret['Class_Dates'] = array();
            $query2 = "select * from `Class_Schedule` where `Class_ID`='$cid'";
            $res2 = mysql_query($query2);
            while($row2=mysql_fetch_array($res2))
            {
                $temp_ret2 = array();
                $temp_ret2 = $row2[1];
                $temp_ret['Class_Dates'][] = $temp_ret2;
                $temp_ret['Class_TimeFrom'][] = $row2[3];
                $temp_ret['Class_TimeTo'][] = $row2[4];

            }
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    function getClassesByEmployeeID($eid)
    {
        $ret = array();
        $query = "select * from `Class_Employee` where `Employee_ID`='$eid'";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $cid = $row[2];
            $temp_ret['Class_ID'] =$cid;
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    //Get List of Class
    function getClass($cid)
    {
        $ret = array();

        $query = "SELECT * FROM `Class`,`Course`,`Instructor`,`Location` WHERE `Class`.`Course_ID`=`Course`.`Course_ID` AND `Class`.`Instructor_ID`=`Instructor`.`Instructor_ID` AND `Location`.`Location_ID`=`Class`.`Location_ID` AND `Class`.`Class_Id`='$cid'";
        $res = mysql_query($query);
        if(mysql_affected_rows()==0)
        {
            $query = "SELECT * FROM `Class`,`Course`,`Location` WHERE `Class`.`Course_ID`=`Course`.`Course_ID` AND `Location`.`Location_ID`=`Class`.`Location_ID` AND `Class`.`Class_Id`='$cid'";
            $res = mysql_query($query);
        }
        while($row=mysql_fetch_array($res))
        {
            $cid = $row[0];
            $ret['Class_ID'] =$cid;
            $ret['Class_Name'] = $row[1];
            $ret['Class_Capacity'] = $row[2];
            $ret['Class_Practical_Date'] = $row[3];
            $ret['Class_Written_Date'] = $row[4];
            $ret['Course_ID'] = $row[9];
            $ret['Course_Name'] = $row[10];
            $ret['Instructor_ID'] = $row[11];
            $ret['Instructor_LastName'] = $row[12];
            $ret['Instructor_FirstName'] = $row[13];
            $ret['Location_ID'] = $row[15];
            $ret['Location_Name'] = $row[16];


            $query2 = "select * from `Class_Employee` where `Class_ID`='$cid'";
            $res2 = mysql_query($query2);
            $ret['Class_Filled'] = mysql_affected_rows()==-1 ? 0 : mysql_affected_rows();


            $ret['Class_Dates'] = array();
            $ret['Class_TimeFrom'] = array();
            $ret['Class_TimeTo'] = array();

            $query2 = "select * from `Class_Schedule` where `Class_ID`='$cid' ORDER BY Class_Schedule_Date ASC;";
            $res2 = mysql_query($query2);
            while($row2=mysql_fetch_array($res2))
            {
                $temp_ret2 = array();
                $temp_ret2 = $row2[1];
                $ret['Class_Dates'][] = $temp_ret2;
                $ret['Class_TimeFrom'][] = $row2[3];
                $ret['Class_TimeTo'][] = $row2[4];
            }
        }
        return $ret;
    }

    function getClassesByCourse($cid)
    {
        $ret = array();
        $query = "select * from `Class`,`Course`,`Instructor`,`Location` where `Class`.`Course_ID`=`Course`.`Course_ID` AND `Class`.`Instructor_ID`=`Instructor`.`Instructor_ID` AND `Location`.`Location_ID`=`Class`.`Location_ID` AND `Course`.`Course_ID`='$cid'";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $stopflag=0;
            $temp_ret = array();
            $cid = $row[0];
            $temp_ret['Class_ID'] =$cid;
            $temp_ret['Class_Name'] = $row[1];
            $temp_ret['Class_Capacity'] = $row[2];
            $temp_ret['Class_Practical_Date'] = $row[3];
            $temp_ret['Class_Written_Date'] = $row[4];
            $temp_ret['Coordinator_ID'] = $row[6];
            $temp_ret['Course_Name'] = $row[10];
            $temp_ret['Instructor_LastName'] = $row[12];
            $temp_ret['Instructor_FirstName'] = $row[13];
            $temp_ret['Location_Name'] = $row[16];
            $temp_ret['Location_City'] = $row[18];



            $query2 = "select * from `Class_Employee` where `Class_ID`='$cid'";
            $res2 = mysql_query($query2);
            $temp_ret['Class_Filled'] = mysql_affected_rows()==-1 ? 0 : mysql_affected_rows();


            $temp_ret['Class_Dates'] = array();
            $query2 = "select * from `Class_Schedule` where `Class_ID`='$cid'";
            $res2 = mysql_query($query2);

            while($row2=mysql_fetch_array($res2))
            {
                $temp_ret2 = $row2[1];
                $currentdate= date("Y-m-d");
                if($currentdate>$temp_ret2)
                {
                    $stopflag=1;
                    break;
                }
                $temp_ret['Class_Dates'][] = $temp_ret2;
                $temp_ret['Class_TimeFrom'][] = $row2[3];
                $temp_ret['Class_TimeTo'][] = $row2[4];

            }
            if($stopflag==1)
            {
                continue;
            }
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    /*
    getClassesByCourse Coroordinator Function is just like getClassesByCourse
    but Coordinator can enroll students to class even after class
    has been started and till last day of class.
    */
    function getClassesByCourse_Coordinator($cid)
    {
        $ret = array();
        $query = "select * from `Class`,`Course`,`Instructor`,`Location` where `Class`.`Course_ID`=`Course`.`Course_ID` AND `Class`.`Instructor_ID`=`Instructor`.`Instructor_ID` AND `Location`.`Location_ID`=`Class`.`Location_ID` AND `Course`.`Course_ID`='$cid'";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $stopflag=0;
            $temp_ret = array();
            $cid = $row[0];
            $temp_ret['Class_ID'] =$cid;
            $temp_ret['Class_Name'] = $row[1];
            $temp_ret['Class_Capacity'] = $row[2];
            $temp_ret['Class_Practical_Date'] = $row[3];
            $temp_ret['Class_Written_Date'] = $row[4];
            $temp_ret['Course_Name'] = $row[10];
            $temp_ret['Instructor_LastName'] = $row[12];
            $temp_ret['Instructor_FirstName'] = $row[13];
            $temp_ret['Location_Name'] = $row[16];
            $temp_ret['Location_City'] = $row[18];



            $query2 = "select * from `Class_Employee` where `Class_ID`='$cid'";
            $res2 = mysql_query($query2);
            $temp_ret['Class_Filled'] = mysql_affected_rows()==-1 ? 0 : mysql_affected_rows();


            $temp_ret['Class_Dates'] = array();
            $query2 = "select * from `Class_Schedule` where `Class_ID`='$cid' ORDER BY `Class_Schedule_Date` DESC";
            $res2 = mysql_query($query2);

            if($row2=mysql_fetch_array($res2))
            {
                $temp_ret2 = $row2[1];
                $currentdate= date("Y-m-d");
                if($currentdate>$temp_ret2)
                {
                    $stopflag=1;
                    continue;
                    // break;
                }
                $temp_ret['Class_Dates'][] = $temp_ret2;
                $temp_ret['Class_TimeFrom'][] = $row2[3];
                $temp_ret['Class_TimeTo'][] = $row2[4];

            }
            // if($stopflag==1)
            // {
            //     continue;
            // }
            $ret[] = $temp_ret;
        }
        return $ret;
    }


    //Update Class Data
    function updateClass($cid, $classname, $instructorid, $courseid, $locationid, $schedule, $scheduletimeto, $scheduletimefrom, $classcapacity, $classpracticalexam, $classwrittenexam)
    {
        $ret = array();
        $query = "select * from `Class` where `Class_Name`='$classname'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $query = "select * from `Class` where `Class_ID`='$cid'";
            $res = mysql_query($query);
            if($row=mysql_fetch_array($res))
            {
                if(strcmp($row[1],$classname)!=0)
                {
                    $ret['Status']=0;
                    $ret['Message']='Class Name Already Exist!';
                    return $ret;
                }
                else
                {
                    goto updateClassgoto;
                }
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;
            }
        }
        else
        {
        updateClassgoto:
            $query = "update `Class` SET `Class_Name`='$classname', `Instructor_ID`='$instructorid', `Course_ID`='$courseid', `Class_Capacity`='$classcapacity', `Class_Practical_Date`='$classpracticalexam', `Class_Written_Date`='$classwrittenexam', `Location_Id`='$locationid' where `Class_ID`='$cid'";
            $res = mysql_query($query);
            if($res>0)
            {
                $query="delete from `Class_Schedule` where `Class_ID`='$cid'";
                $res = mysql_query($query);

                $scheduleArray = explode(', ', $schedule);
                $scheduletimetoArray = explode(', ', $scheduletimeto);
                $scheduletimefromArray = explode(', ', $scheduletimefrom);

                for($i=0;$i<count($scheduleArray);$i++)
                {
                    $tempdate = $scheduleArray[$i];
                    $temptotime = $scheduletimetoArray[$i] . ":00";
                    $tempfromtime = $scheduletimefromArray[$i] . ":00";

                    $query = "insert into `Class_Schedule` values (NULL,'$tempdate',$cid, '$tempfromtime', '$temptotime')";
                    $res = mysql_query($query);
                }

                $ret['Status']=1;
                $ret['Message']='Class Changed Successfully!';
                return $ret;
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;

            }
        }
    }

    //Delete Class and Backup data in Class_Deleted
    function deleteClass($cid)
    {
        $ret = array();
        $emails = array();
        $eids = array();
        $classdata = getClass($cid);
        $query = "select * from `Class` where `Class_ID`='$cid';";
        $res = mysql_query($query);
        if($row= mysql_fetch_array($res))
        {
          $name = $row[1];
          $capacity = $row[2];
          $practical = $row[3];
          $written = $row[4];
          $course_id = $row[5];
          $coordinator_id = $row[6];
          $instructor_id = $row[7];
          $location_id = $row[8];
          $query = "insert into `Class_Deleted` values (NULL,$cid,'$name',$capacity, '$practical', '$written', $course_id, $coordinator_id, $instructor_id,$location_id);";
          $res = mysql_query($query);
          if($res>0)
          {
            //Get List of Students to drop them
            $temp_ret = searchEmployeesByClass($cid);
            for($i=0;$i<count($temp_ret);$i++)
            {
              $cc = $temp_ret[$i]['Certificate_Final'];
              $prereq_cc = $temp_ret[$i]['Prerequisite_Certificate_Code'];
              $eid = $temp_ret[$i]['Employee_ID'];
              $emails[] = $temp_ret[$i]['Login_Email'];
              $eids[] = $temp_ret[$i]['Employee_ID'];

              //Add Dropped Students to Class_Deleted_Employee
              $query = "insert into `Class_Deleted_Employee` values (NULL, $eid, $cid, '$prereq_cc');";
              $res = mysql_query($query);

              dropClass($cc,TRUE);
            }
            $query = "delete from `Class` where `Class_ID`='$cid';";
            $res = mysql_query($query);
            //Add to refund table
            /*
             Your Code Here
            */
            if($res>0)
            {
              $ret['Status']=1;
              $ret['Message']='Class Deleted!';
              $ret['Emails']=$emails;
              $ret['Employee_IDs']=$eids;
              $ret['ClassData'] = $classdata;
              // $query = "delete from `Class_Schedule` where `Class_ID`='$cid'";
              // $res = mysql_query($query);
              // if($res>0)
              // {
              //     //Add Employees to refund class Table
              //     //Remove it from Class Employee Table
              //
              //     $query = "delete from `Class_Employee` where `Class_ID`='$cid'";
              //     $res = mysql_query($query);
              //     if($res>0)
              //     {
              //         $ret['Status']=1;
              //         $ret['Message']='Instructor Deleted!';
              //     }
              //     else
              //     {
              //         $ret['Status']=0;
              //         $ret['Message']='Error! Please try again in a while.';
              //     }
            }
            else {
              $ret['Status']=0;
              $ret['Message']='Error! Please try again in a while.';
            }
          }
          else {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
          }
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function getCoordinatorDetails($lid)
    {
        $ret = array();
        $query = "select * from `Login`,`Coordinator` where `Coordinator`.`Login_ID`='$lid' AND `Coordinator`.`Login_ID`=`Login`.`Login_ID`";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $ret['Login_ID'] = $row[0];
            $ret['Login_Email'] = $row[1];
            $ret['Coordinator_ID'] = $row[6];
            $ret['Coordinator_FirstName'] = $row[7];
            $ret['Coordinator_LastName'] = $row[8];
        }
        return $ret;
    }

    function getAdminDetails($lid)
    {
        $ret = array();
        $query = "select * from `Login` where `Login_ID`='$lid'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $ret['Login_ID'] = $row[0];
            $ret['Login_Email'] = $row[1];
        }
        return $ret;
    }


    //password_verify($password, $passDB)
    function login($email, $password)
    {
        $email = strtolower($email);
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $passDB = $row[2];
            if (password_verify($password, $passDB)) {
                //Login Verify to check verified and disable status
                $verify = $row[4];
                if($verify==3)
                {
                    $ret['Status']=0;
                    $ret['Message']='Your account is deleted, Please contact Customer Care/Program Coordinator to regain access.';
                    return $ret;
                }
                $ret['Status']=1;
                $ret['Message']='Success';
                $ret['Login_ID']=$row[0];
                $ret['Role']=$row[3];
                return $ret;
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Incorrect Email Address or Password!';
                return $ret;
            }
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Incorrect Email Address or Password!';
            return $ret;
        }
    }

    function login_admin($email)
    {
        $email = strtolower($email);
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
          $ret['Status']=1;
          $ret['Message']='Success';
          $ret['Login_ID']=$row[0];
          $ret['Role']=$row[3];
          return $ret;
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Incorrect Email Address or Password!';
            return $ret;
        }
    }

    function getEmployeeDetail($lid)
    {
        $ret = array();
        $query = "select * from `Employee`,`Login` where `Employee`.`Login_ID`='$lid' AND `Employee`.`Login_ID`=`Login`.`Login_ID`";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $employeeid = $row[0];
            $ret['Employee_ID'] = $row[0];
            $ret['Employee_Prefix'] = $row[1];
            $ret['Employee_FirstName'] = $row[2];
            $ret['Employee_Initial'] = $row[3];
            $ret['Employee_LastName'] = $row[4];
            $ret['Employee_Mobile'] = $row[5];
            $ret['Company_ID'] = $row[6];
            $ret['Company_Other'] = $row[7];
            $ret['Employee_Position'] = $row[8];
            $ret['Employee_Address'] = $row[9];
            $ret['Employee_City'] = $row[10];
            $ret['Employee_Postal'] = $row[11];
            $ret['Login_Email'] = $row[14];
            $ret['Login_Verify'] = $row[17];

            $ret['Ferpa'] = checkFerpa($employeeid)['Status'];

            $companyid = $ret['Company_ID'];
            $query2 = "select * from `Company` where `Company_ID`='$companyid'";
            $res2 = mysql_query($query2);
            if($row2=mysql_fetch_array($res2))
            {
                $ret['Company_Name']=$row2[1];
            }
            else
            {
                $ret['Company_Name']=$ret['Company_Other'];
            }

            //Get PreReq Certified Courses
            $eid = $row[0];
            $query2 = "select * from `Prerequisite_Certificate`,`Course` where `Employee_ID`='$eid' and `Prerequisite_Certificate`.`Course_ID`=`Course`.`Course_ID`";
            $res2 = mysql_query($query2);

            $ret2 = array();
            while($row2=mysql_fetch_array($res2))
            {
                $temp_ret2 = array();
                $temp_ret2['Certificate_ID']=$row2[0];
                $temp_ret2['Course_ID']=$row2[2];
                $temp_ret2['Certificate_Code']=$row2[3];
                $temp_ret2['Certificate_ExpireDate']=$row2[5];
                $temp_ret2['Status_ID']=$row2[6];
                $temp_ret2['Course_Name']=$row2[8];
                $temp_ret2['Certificate_Final']=0;
                $ret2[] = $temp_ret2;
            }

            //Get Final Certified Courses

            $query2 = "select * from `Course_Certificate`,`Course` where `Employee_ID`='$eid' and `Course_Certificate`.`Course_ID`=`Course`.`Course_ID`";
            $res2 = mysql_query($query2);

            while($row2=mysql_fetch_array($res2))
            {
                $temp_ret2 = array();
                $temp_ret2['Certificate_ID']=$row2[0];
                $temp_ret2['Course_ID']=$row2[2];
                $temp_ret2['Certificate_Code']=$row2[3];
                $temp_ret2['Certificate_EnrollDate']=$row2[4];
                $temp_ret2['Certificate_ExpireDate']=$row2[5];
                $temp_ret2['Course_Certificate_Practical']=$row2[6];
                $temp_ret2['Course_Certificate_Written']=$row2[7];
                $temp_ret2['Status_ID']=$row2[8];
                $temp_ret2['Course_Name']=$row2[10];
                $temp_ret2['Certificate_Final']=1;
                $ret2[] = $temp_ret2;
            }
            $ret['Certificate'] = $ret2;
        }
        return $ret;
    }

    function updatePassword($email, $oldpassword, $newpassword)
    {
        $email = strtolower($email);
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $passDB = $row[2];
            if (password_verify($oldpassword, $passDB)) {
                $newpassword = password_hash($newpassword, PASSWORD_DEFAULT);
                $query2 = "update `Login` SET `Login_Password`='$newpassword' where `Login_Email`='$email'";
                $res2 = mysql_query($query2);
                if($res2>0)
                {
                    $ret['Status']=1;
                    $ret['Message']='Password Changed Successfully!';
                    return $ret;
                }
                else
                {
                    $ret['Status']=0;
                    $ret['Message']='Error! Please try again in a while.';
                    return $ret;
                }
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Incorrect Password!';
                return $ret;
            }
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Incorrect Password!';
            return $ret;
        }
    }

    function updateEmployeeBasicDetails($eid, $email, $phonenumber, $address, $city, $postalcode)
    {
        $email = strtolower($email);
        $ret = array();
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $query = "select * from `Login`,`Employee` where `Employee`.`Employee_Id`='$eid' and `Employee`.`Login_ID`=`Login`.`Login_ID`";
            $res = mysql_query($query);
            if($row=mysql_fetch_array($res))
            {
                if(strcmp($row[1],$email)!=0)
                {
                    $ret['Status']=0;
                    $ret['Message']='Email Address Already Exist!';
                    return $ret;
                }
                else
                {
                    goto updateEmployeeBasicDetailsgoto;
                }
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;
            }
        }
        else
        {
        updateEmployeeBasicDetailsgoto:
            $query = "update `Employee`,`Login` SET `Login_Email`='$email', `Employee`.`Employee_Mobile`='$phonenumber', `Employee`.`Employee_Address`='$address',`Employee`.`Employee_City`='$city',`Employee`.`Employee_Postal`='$postalcode' where `Employee`.`Employee_ID`='$eid' and `Employee`.`Login_ID`=`Login`.`Login_ID`";
            $res = mysql_query($query);
            if($res>0)
            {
                $ret['Status']=1;
                $ret['Message']='Basic Details Changed Successfully!';
                return $ret;
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;

            }
        }
    }

    function updateEmployee($eid, $firstname, $initial, $lastname, $email, $password, $mobile, $company_id, $company_other, $position, $address, $city, $postal)
    {
        $email = strtolower($email);
        $ret = array();
        $query = "select * from `Login` where `Login_Email`='$email'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $query = "select * from `Login`,`Employee` where `Employee`.`Employee_Id`='$eid' and `Employee`.`Login_ID`=`Login`.`Login_ID`";
            $res = mysql_query($query);
            if($row=mysql_fetch_array($res))
            {
                if(strcmp($row[1],$email)!=0)
                {
                    $ret['Status']=0;
                    $ret['Message']='Email Address Already Exist!';
                    return $ret;
                }
                else
                {
                    goto updateEmployeegoto;
                }
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;
            }
        }
        else
        {
        updateEmployeegoto:
            if($password==null)
            {
                $query = "update `Employee`,`Login` SET `Employee`.`Employee_FirstName`='$firstname', `Employee`.`Employee_Initial`='$initial', `Employee`.`Employee_LastName`= '$lastname', `Employee`.`Employee_Mobile`='$mobile', `Employee`.`Company_ID`='$company_id', `Employee`.`Company_Other`='$company_other', `Employee`.`Employee_Position`='$position', `Login`.`Login_Email`='$email', `Employee`.`Employee_Address`='$address',`Employee`.`Employee_City`='$city',`Employee`.`Employee_Postal`='$postal' where `Employee`.`Employee_ID`='$eid' and `Employee`.`Login_ID`=`Login`.`Login_ID`";
            }
            else
            {
                $password = password_hash($password, PASSWORD_DEFAULT);
                $query = "update `Employee`,`Login` SET `Employee`.`Employee_FirstName`='$firstname', `Employee`.`Employee_Initial`='$initial', `Employee`.`Employee_LastName`= '$lastname', `Employee`.`Employee_Mobile`='$mobile', `Employee`.`Company_ID`='$company_id', `Employee`.`Company_Other`='$company_other', `Employee`.`Employee_Position`='$position', `Login`.`Login_Email`='$email', `Login`.`Login_Password`='$password', `Employee`.`Employee_Address`='$address',`Employee`.`Employee_City`='$city',`Employee`.`Employee_Postal`='$postal' where `Employee`.`Employee_ID`='$eid' and `Employee`.`Login_ID`=`Login`.`Login_ID`";
            }
            $res = mysql_query($query);
            if($res>0)
            {
                $ret['Status']=1;
                $ret['Message']='Details Changed Successfully!';
                return $ret;
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
                return $ret;

            }
        }
    }


    function disableEmployee($lid)
    {
        $ret = array();
        $query = "update `Login` SET `Login_Verify`='3' where `Login_ID`='$lid'";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Account Removed!';
            return $ret;
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
            return $ret;

        }

    }

    function enableEmployee($lid)
    {
        $ret = array();
        $query = "update `Login` SET `Login_Verify`='1' where `Login_ID`='$lid'";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Account Restored!';
            return $ret;
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
            return $ret;

        }

    }

    function getEmployeeFullName($eid)
    {
        $query = "select * from `Employee` where `Employee_ID`='$eid'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            return $row[2] . " " . $row[3] . " " . $row[4];
        }
        return "";
    }

    function getEmployeeDetailsByEmployeeID($eid)
    {
        $ret = array();
        $query = "select * from `Login`,`Employee` where `Login`.`Login_ID`=`Employee`.`Login_ID` and `Employee_ID`='$eid'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $ret['Email'] = $row[1];
            $ret['FirstName'] = $row[8];
            $ret['LastName'] = $row[10];
            $ret['Mobile'] = $row[11];
            $ret['Company_Other'] = $row[13];
            $ret['LoginID'] = $row[18];
        }
        return $ret;
    }

    function getCourse($cid)
    {
        $query = "select * from `Course` where `Course_ID`='$cid'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            return $row[1];
        }
        return "";
    }

    function getCourses()
    {
        $ret = array();
        $query = "select * from `Course`";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Course_ID']= $row[0];
            $temp_ret['Course_Name']= $row[1];
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    function getLocations()
    {
        $ret = array();
        $query = "select * from `Location`";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Location_ID']= $row[0];
            $temp_ret['Location_Name']= $row[1];
            $temp_ret['Location_Address']= $row[2];
            $temp_ret['Location_City']= $row[3];
            $temp_ret['Location_Postal']= $row[4];
            $temp_ret['Location_Contact']= $row[5];
            $ret[] = $temp_ret;
        }
        return $ret;

    }


    function getMaterialFile($cid)
    {
        $query = "select * from `Material` where `Course_ID`='$cid'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            return $row[1];
        }
        return "";
    }


    function getQuiz($cid)
    {
        $ret = array();
        $query = "select * from `Questions` where `Course_ID`='$cid'";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Questions _ID'] = $row[0];
            $temp_ret['Questions _Name'] = $row[2];
            $temp_ret['Questions_Image'] = $row[3];
            $temp_ret['Questions_Answer'] = $row[4];

            $query2 = "select * from `Question_Options` where `Questions _ID`='$row[0]'";
            $res2 = mysql_query($query2);
            $temp_ret2 = array();
            while($row2=mysql_fetch_array($res2))
            {
                $temp_ret2[] = $row2[2];
            }
            $temp_ret['Question_Options'] = $temp_ret2;
            $ret[] = $temp_ret;
        }
        return $ret;

    }

    //Add to waitlist upto 15 users
    function add_waitlist($cid, $eid)
    {
        $ret = array();
        $query = "select * from `Class_Waitlist` where `Employee_ID`='$eid' AND `Class_ID`='$cid'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>15)
        {
            $ret['Status']=0;
            $ret['Message']='Waitlist is full!.';
        }
        else
        {
            $query = "INSERT INTO `Class_Waitlist` (`Class_Waitlist_ID`, `Employee_ID`, `Class_ID`) VALUES (NULL, $eid, $cid);";
            $res = mysql_query($query);
            if($res>0)
            {
                $ret['Status']=1;
                $ret['Message']='You are added to waitlist!';
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Can not be added to the list. Waitlist is full!';
            }
        }
        return $ret;
    }

    function check_waitlist($cid, $eid)
    {
        $query = "SELECT * from `Class_Waitlist` where `Employee_ID`='$eid' AND `Class_ID`='$cid'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            return 1;
        }
        return 0;
    }

    function getWaitlist($cid)
    {
        $ret = array();
        $query = "select * from `Class_Waitlist`,`Employee`,`Login` where `Class_ID`='$cid' AND `Class_Waitlist`.`Employee_ID`=`Employee`.`Employee_ID` AND `Login`.`Login_ID`=`Employee`.`Login_ID`";
        $res = mysql_query($query);
        $ret['Email_Addresses'] = array();
        $ret['First_Names'] = array();
        $ret['Last_Names'] = array();

        while($row=mysql_fetch_array($res))
        {
            $ret['First_Names'][] = $row[5];
            $ret['Last_Names'][] = $row[7];
            $ret['Email_Addresses'][] = $row[17];
        }
        return $ret;
    }

    function getWaitlistByClassID($cid)
    {
        $ret = array();
        $query = "select * from `Class_Waitlist`,`Employee`,`Login` where `Class_ID`='$cid' AND `Class_Waitlist`.`Employee_ID`=`Employee`.`Employee_ID` AND `Login`.`Login_ID`=`Employee`.`Login_ID`";
        $res = mysql_query($query);

        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Employee_ID'] = $row[3];
            $temp_ret['Employee_FirstName'] = $row[5];
            $temp_ret['Employee_LastName'] = $row[7];
            $temp_ret['Employee_EmailAddress'] = $row[17];
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    //Function to drop employee, $eid, from class, $cid
    function dropFromWaitlist($cid, $eid)
    {
        $ret = array();
        $query = "DELETE from `Class_Waitlist` where `Employee_ID`='$eid' AND `Class_ID`='$cid'";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Removed from the waitlist!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    //Function to add employee, $eid, to class, $cid
    function addToWaitlist($cid, $eid)
    {
        $ret = array();
        $query = "INSERT INTO `Class_Waitlist` VALUES (NULL, '$eid', '$cid')";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Added to the waitlist!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function getUnexpiredPrereqCertificateByEmployeeIDCourseId($employee_id, $course_id){
        $ret = array();
        $query = "SELECT * from `Prerequisite_Certificate` where `Employee_ID`='$employee_id' and `Course_ID`='$course_id' and `Prerequisite_Certificate_ExpireDate`>='$today'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res)){
            $ret['Status']=1;
            $ret['Certficate_Code']= $row['Certificate _Code'];
            $ret['Message']='Certificate Generated';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    //Prereq
    function setCertificate($employee_id, $course_id)
    {
        $ret = array();
        $certificate_code = $course_id . generateRandomString() . $employee_id;
        $certificate_issue = date("Y-m-d");
        $certificate_expire = (new DateTime())->add(new DateInterval('P12M'))->format('Y-m-d');
        $status_id = 5;
        $query = "INSERT INTO `Prerequisite_Certificate` VALUES (NULL,$employee_id,$course_id,'$certificate_code','$certificate_issue','$certificate_expire',$status_id)";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Certficate_Code']= $certificate_code;
            $ret['Message']='Certificate Generated';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    //function to get Prerequisite Certificate Data by Status ID
    function getPrereqCertiDataByStatusID($sid) {
        $ret = array();
        $query = "SELECT * FROM `Prerequisite_Certificate` WHERE `Status_ID`='$sid';";
        $res = mysql_query($query) or die(mysql_error());;
        while($row=mysql_fetch_array($res)){
            $temp_ret = array();
            $temp_ret['Prerequisite_Certificate_ID'] = $row[0];
            $temp_ret['Employee_ID'] = $row[1];  
            $temp_ret['Course_ID'] = $row[2];
            $temp_ret['Certificate_Code'] = $row[3];
            $temp_ret['Prerequisite_Certificate_IssueDate'] = $row[4];
            $temp_ret['Prerequisite_Certificate_ExpireDate'] = $row[5];
            $temp_ret['Status_ID'] = $row[6];
            $ret[] = $temp_ret;      
        }
        return $ret;
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function getCertificate($code)
    {
        $ret = array();
        $employee_id = 0;
        $course_id = 0;
        $employee_name = "";
        $issue_date = "";
        $expire_date = "";
        $final_status = 0;
        $course_status = 0;
        $query = "select * from `Prerequisite_Certificate` where `Certificate _Code`='$code'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $employee_id = $row[1];
            $course_id = $row[2];
            $issue_date = $row[4];
            $expire_date = $row[5];
            $course_status = $row[6];
            $final_status=0;
        }
        else
        {
            $query = "select * from `Course_Certificate` where `Certificate _Code`='$code'";
            $res = mysql_query($query);
            if($row=mysql_fetch_array($res))
            {
                $employee_id = $row[1];
                $course_id = $row[2];
                $issue_date = $row[4];
                $expire_date = $row[5];
                $course_status = $row[8];
                $final_status=1;
            }
            else
            {
                return null;
            }
        }
        $query = "select * from `Class`,`Class_Employee` where `Class`.`Class_ID`=`Class_Employee`.`Class_ID` and `Course_ID`='$course_id' and `Employee_ID`='$employee_id'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $class_id = $row[0];
            $class_name = $row[1];
        }
        $course_name = getCourse($course_id);
        $employee_name = getEmployeeFullName($employee_id);
        $ret['Employee_ID'] = $employee_id;
        $ret['Employee_Name'] = $employee_name;
        $ret['Course_Name'] = $course_name;
        $ret['Grade'] = "100%";
        $ret['Issue_Date'] = $issue_date;
        $ret['Certificate_ExpireDate'] = $expire_date;
        $ret['Course_Status'] = $course_status;
        $ret['Course_ID'] = $course_id;
        $ret['Code'] = $code;
        $ret['Class_ID'] = $class_id;
        $ret['Class_Name'] = $class_name;
        $ret['Code'] = $code;

        $ret['Final_Status'] = $final_status;

        return $ret;
    }

    function searchEmployees($formdata,$fullaccess)
    {
        $ret = array();
        $company = $formdata['company'];
        $firstname = $formdata['firstname'];
        $lastname = $formdata['lastname'];
        $zipcode = $formdata['zipcode'];
        $city = $formdata['city'];
        $certificate_expire = $formdata['certificate_expire'];
        $coordinator_added = $formdata['coordinator_added'];
        $phone = $formdata['phone'];
        $comq = 1;
        $fnq = 1;
        $lnq = 1;
        $zq = 1;
        $cq = 1;
        $ccq = 1;
        $phnq = 1;
        if($company!=null)
        {
            $comq = "`Company`.`Company_ID`='$company'";
        }
        if($firstname!=null)
        {
            $fnq = "`Employee`.`Employee_FirstName` LIKE '%$firstname%'";
        }
        if($lastname!=null)
        {
            $lnq = "`Employee`.`Employee_LastName`='$lastname'";
        }
        if($zipcode!=null)
        {
            $zq = "`Employee`.`Employee_Postal`='$zipcode'";
        }
        if($city!=null)
        {
            $cq = "`Employee`.`Employee_City`='$city'";
        }
        if ($phone!=null) 
        {
            $phnq = "`Employee`.`Employee_Mobile`='$phone'";    
        }

        /*
         Old query

         $query = "SELECT * FROM `Employee`,`Company`,`Prerequisite_Certificate`,`Login`,`Course`,`Status` where `Employee`.`Company_ID`=`Company`.`Company_ID` AND `Login`.`Login_ID`=`Employee`.`Login_ID` AND `Course`.`Course_ID`=`Prerequisite_Certificate`.`Course_ID` AND  `Prerequisite_Certificate`.`Employee_ID` = `Employee`.`Employee_ID` AND `Prerequisite_Certificate`.`Status_ID` = `Status`.`Status_ID` AND  ($fnq AND $lnq AND $zq AND $cq AND $comq) AND `Prerequisite_Certificate`.`Prerequisite_Certificate_ExpireDate`<'$certificate_expire' AND `Course`.`Course_ID`!=1 GROUP BY `Employee`.`Employee_ID`";
         */
        $companyid=0;
        $employeeid=0;
        $temp_query = "";
        if($coordinator_added==1)
        {
            $temp_query = "AND `Login`.`Login_Type`='4'";
        }
        if($company==null)
        {
            $query = "SELECT * FROM `Employee`,`Prerequisite_Certificate`,`Login`,`Course`,`Status` where `Login`.`Login_ID`=`Employee`.`Login_ID` AND ( $fnq AND $lnq AND $zq AND $cq AND $phnq) AND `Prerequisite_Certificate`.`Prerequisite_Certificate_ExpireDate`<'$certificate_expire' AND `Course`.`Course_ID`!=1 ". $temp_query ." GROUP BY `Employee`.`Employee_ID`;";
        }
        else
        {
            $query = "SELECT * FROM `Employee`, `Company`, `Prerequisite_Certificate`, `Login`, `Course`, `Status` where `Employee`.`Company_ID`=`Company`.`Company_ID` AND `Login`.`Login_ID`=`Employee`.`Login_ID` AND ($fnq AND $lnq AND $zq AND $cq AND $comq AND $phnq) AND `Prerequisite_Certificate`.`Prerequisite_Certificate_ExpireDate`<'$certificate_expire' AND `Course`.`Course_ID`!=1 ". $temp_query ." GROUP BY `Employee`.`Employee_ID`";
            $companyid=1;
        }


        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $ret2 = array();
            $ret2['Employee_ID'] = $row[0];
            $employeeid = $row[0];
            $ret2['Employee_Prefix'] = $row['Employee_Prefix'];
            $ret2['Employee_FirstName'] = $row['Employee_FirstName'];
            $ret2['Employee_Initial'] = $row['Employee_Initial'];
            $ret2['Employee_LastName'] = $row['Employee_LastName'];
            $ret2['Company_ID'] = $row['Company_ID'];
            $ret2['Company_Other'] = $row['Company_Other'];
            $ret2['Employee_Position'] = $row['Employee_Position'];
            $ret2['Employee_City'] = $row['Employee_City'];
            $ret2['Employee_Postal'] = $row['Employee_Postal'];
            if($companyid==0)
            {
                $companyother = $row['Company_Other'];
                if($companyother=="")
                {
                    $query2="SELECT * FROM `Company`,`Employee` WHERE `Employee`.`Company_ID`=`Company`.`Company_ID` AND `Employee`.`Employee_ID`='$employeeid'";
                    $res2 = mysql_query($query2);
                    if($row2=mysql_fetch_array($res2))
                    {
                        $ret2['Company_Name'] = $row2['Company_Name'];
                    }
                }
                else
                {
                    $ret2['Company_Name'] = $companyother;
                }
            }
            else
            {
                $ret2['Company_Name'] = $row['Company_Name'];
            }
            /*
             $ret2['Certificate_ID'] = $row[15];
             $ret2['Certificate_Code'] = $row[18];
             $ret2['Certificate_ExpireDate'] = $row[21];
             $ret2['Course_ID'] = $row[29];
             if($row[22]==0)
             {
             $ret2['Course_Name'] = $row[30] . " (Prerequisite)";
             }
             else
             {
             $ret2['Course_Name'] = $row[30];
             }
             */
            if($fullaccess==1)
            {
                $ret2['Employee_Mobile'] = $row[5];
                $ret2['Employee_Address'] = $row[9];
                $ret2['Login_ID'] = $row['Login_ID'];
                $ret2['Login_Email'] = $row[21];
                $ret2['Certificate_Final'] = $row[22];
                $ret2['Status_Message'] = $row[32];
            }
            $ret[] = $ret2;
        }
        return $ret;
    }

    function searchEmployeesByClass($cid)
    {
        $ret = array();

        /*
         $query = "SELECT * FROM `Employee`,`Company`,`Course_Certificate`,`Login`,`Course`,`Class`,`Class_Employee` where `Employee`.`Employee_ID`=`Course_Certificate`.`Employee_ID` AND `Employee`.`Employee_ID`=`Class_Employee`.`Employee_ID` AND `Employee`.`Company_ID`=`Company`.`Company_ID` AND `Employee`.`Login_ID`=`Login`.`Login_ID` AND `Course`.`Course_ID`=`Course_Certificate`.`Course_ID` AND `Course`.`Course_ID`=`Class`.`Course_ID` AND `Class`.`Class_ID`=`Class_Employee`.`Class_ID` and `Class`.`Class_ID`='$cid' GROUP BY `Employee`.`Employee_ID`";
         */
        $query = "SELECT `Course_ID` FROM `Class` where `Class_ID`='$cid'";
        $res = mysql_query($query);
        $course_id = 0;
        if($row=mysql_fetch_array($res)){
          $course_id = $row[0];
        }
        $employee_ids = array();
        $prereq_ccs = array();
        $course_cc_location = array();
        $course_ccs = array();

        $query = "SELECT * FROM `Class_Employee` where `Class_ID`='$cid'";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res)){
          $employee_ids[] = $row[1];
          $prereq_ccs[] = $row[3];
          $course_cc_locations[] = 0;
        }
        for($i=0;$i<count($employee_ids);$i++){
          $employee_id = $employee_ids[$i];
          $prereq_cc = $prereq_ccs[$i];
          $query = "SELECT * FROM `Prerequisite_Certificate` where `Employee_ID`='$employee_id' and `Course_ID`='$course_id'";
          $res = mysql_query($query);
          $count = 0;
          while($row=mysql_fetch_array($res)){
            $prereq_cc_q = $row[3];
            if ( strcasecmp( $prereq_cc_q, $prereq_cc ) == 0 ){
              $course_cc_locations[$i] = $count;
            }
            $count++;
          }
        }

        for($i=0;$i<count($employee_ids);$i++){
          $employee_id = $employee_ids[$i];
          $query = "SELECT * FROM `Course_Certificate` where `Employee_ID`='$employee_id' and `Course_ID`='$course_id'";
          $res = mysql_query($query);
          $count = 0;
          $count_check = $course_cc_locations[$i];
          if(mysql_affected_rows()>0){
            while($row=mysql_fetch_array($res)){
              if ($count==$count_check){
                $course_ccs[$i] = $row[3];
              }
              else{
                $course_ccs[$i] = $row[3];
              }
              $count++;
            }
          }
          else{
            $course_ccs[$i] = "";
          }
        }

        for($i=0;$i<count($course_ccs);$i++){
          $course_cc = $course_ccs[$i];
          // echo $course_cc;
          $query = "SELECT * FROM `Employee`,`Course_Certificate`,`Login`,`Course`,`Class`,`Class_Employee` where `Employee`.`Employee_ID`=`Course_Certificate`.`Employee_ID` AND `Employee`.`Employee_ID`=`Class_Employee`.`Employee_ID` AND  `Employee`.`Login_ID`=`Login`.`Login_ID` AND `Course`.`Course_ID`=`Course_Certificate`.`Course_ID` AND `Course`.`Course_ID`=`Class`.`Course_ID` AND `Class`.`Class_ID`=`Class_Employee`.`Class_ID` and `Class`.`Class_ID`='$cid' and `Course_Certificate`.`Certificate _Code`='$course_cc' GROUP BY `Employee`.`Employee_ID`";
          $res = mysql_query($query);
          if($row=mysql_fetch_array($res))
          {
              $ret2 = array();
              $ret2['Employee_ID'] = $row[0];
              $ret2['Employee_Prefix'] = $row[1];
              $ret2['Employee_FirstName'] = $row[2];
              $ret2['Employee_Initial'] = $row[3];
              $ret2['Employee_LastName'] = $row[4];
              $ret2['Company_ID'] = $row[6];
              $ret2['Company_Other'] = $row[7];
              $ret2['Employee_Position'] = $row[8];
              $ret2['Employee_City'] = $row[10];
              $ret2['Employee_Postal'] = $row[11];
              $ret2['Employee_Mobile'] = $row[5];
              $ret2['Employee_Address'] = $row[9];
              $ret2['Login_ID'] = $row[22];
              $ret2['Login_Email'] = $row[23];
              $ret2['Certificate_Final'] = $row[42];
              $ret2['Certificate_Code'] = $row[16];
              $ret2['Prerequisite_Certificate_Code'] = $row[42];
              $ret2['Status_ID'] = $row[21];
              $ret2['Class_ID'] = $row[30];
              $ret2['Class_Name'] = $row[31];
              $ret2['Course_Certificate_Practical'] = $row[19];
              $ret2['Course_Certificate_Written'] = $row[20];

              $eid = $row[0];
              $query_fees = "SELECT * FROM `Paypal` where `Class_ID`='$cid' and `Employee_ID`='$eid'";
              $res_fees = mysql_query($query_fees);
              if(mysql_affected_rows()%2!=0){
                $ret2['Fees_Paid'] = 'Yes';
              }
              else{
                $ret2['Fees_Paid'] = 'No';
              }

              $ret[] = $ret2;
          }

        }

        return $ret;
    }


    function getEmployeeProgress($eid)
    {
        $ret = array();

        //Get Ferpa Date
        $ferpadate;
        $query2 = "select * from `Ferpa` where `Employee_ID`='$eid'";
        $res2 = mysql_query($query2);
        $fq = 0;
        if($row2=mysql_fetch_array($res2))
        {
            $fq = 1;
            $ferpadate = $row2[3];
        }

        //Get SafetyQuiz Date
        $sqdate;
        $query = "select * from `Prerequisite_Certificate`,`Course` where `Employee_ID`='$eid' and `Prerequisite_Certificate`.`Course_ID`=1";
        $res = mysql_query($query);
        $sq = 0;
        if($row=mysql_fetch_array($res))
        {
            $sq = 1;
            $sqdate = $row[4];
        }

        //Get PreReq Certified Courses
        $query = "SELECT * from `Prerequisite_Certificate`,`Course` where `Employee_ID`='$eid' and `Prerequisite_Certificate`.`Course_ID`=`Course`.`Course_ID` and `Prerequisite_Certificate`.`Course_ID`!=1";
        $res = mysql_query($query);
        $count=0;
        $offsets = array();
        $offsets[0] = 0;
        $offsets[1] = 0;
        $offsets[2] = 0;
        $offsets[3] = 0;
        $offsets[4] = 0;
        $offsets[5] = 0;

        while($row=mysql_fetch_array($res))
        {
            $course_id = $row[7];
            $cc = $row[3];
            $course_name = $row[8];
            $ret[$count]['Course_Name'] = $course_name;
            $ret[$count]['Certificate_Code'] = $cc;
            $ret[$count][0]['Status']=1;
            $ret[$count][0]['Date']=$ferpadate;

            $ret[$count][1]['Status']=$sq;
            $ret[$count][1]['Date']=$sqdate;

            $ret[$count][2]['Status']=0;
            $ret[$count][3]['Status']=0;
            $ret[$count][4]['Status']=0;
            $ret[$count][5]['Status']=0;
            $ret[$count][6]['Status']=0;

            $status_id = $row[6];
            $prereq = $row[4];

            if($status_id!=1)
            {
                $ret[$count][2]['Status']=1;
                $ret[$count][2]['Date']=$prereq;
                $course_id = $row[2];
                $query2 = "select * from `Course_Certificate`,`Course` where `Course_Certificate`.`Course_ID`=`Course`.`Course_ID` and `Employee_ID`='$eid' and `Course_Certificate`.`Course_ID`='$course_id' LIMIT 1 OFFSET " . $offsets[$course_id];
                $res2 = mysql_query($query2);

                if($row2=mysql_fetch_array($res2))
                {
                    //Get Enroll Date
                    $ret[$count][3]['Status']=1;
                    $enrolldate= $row2[4];
                    $ret[$count][3]['Date']= $enrolldate;

                    //Get Enrolled Batch
                    $batch="";
                    $query3 = "SELECT * from `Class_Employee`,`Class`,`Prerequisite_Certificate` where `Class_Employee`.`Class_ID`=`Class`.`Class_ID` and `Prerequisite_Certificate`.`Employee_ID`=`Class_Employee`.`Employee_ID` and `Prerequisite_Certificate`.`Course_ID`=`Class`.`Course_ID` and `Prerequisite_Certificate`.`Certificate _Code`='$cc' LIMIT 1 OFFSET " . $offsets[$course_id];
                    $res3 = mysql_query($query3);
                    if($row3=mysql_fetch_array($res3))
                    {
                        $batch = $row3[5];
                    }

                    $ret[$count][3]['Batch']= $batch;

                    //Get Written Date
                    $row3=null;
                    $written = $row2[7];
                    if($written==1)
                    {
                        $ret[$count][4]['Status']=1;
                        $query3 = "SELECT * from `Class`,`Class_Employee` where `Class`.`Class_ID`=`Class_Employee`.`Class_ID` and `Class_Employee`.`Employee_ID`='$eid' and `Class`.`Course_ID`=$count+2";
                        $res3 = mysql_query($query3);
                        if($row3=mysql_fetch_array($res3))
                        {
                            $ret[$count][4]['Date']=$row3[4];
                        }
                    }


                    $practical = $row2[6];
                    $status_id = $row2[8];

                    if($practical==1)
                    {
                        $ret[$count][5]['Status']=1;
                        $ret[$count][5]['Date']=$row3[3];
                    }
                    if($status_id==3)
                    {
                        $ret[$count][6]['Status']=1;
                    }
                }
                $offsets[$course_id] = $offsets[$course_id] + 1;
            }
            $count++;
        }
        if($count==0)
        {
            $count = 0;
            $ret[$count]['Course_Name'] = "No Course Selected";
            $ret[$count][0]['Status']=$fq;
            $ret[$count][0]['Date']=$ferpadate;
            $ret[$count][1]['Status']=$sq;
            $ret[$count][1]['Date']=$sqdate;
        }
        return $ret;

    }

    function getSchedule($cc)
    {
        $ret = array();
        $query = "select * from `Class_Employee`,`Class`,`Prerequisite_Certificate`,`Course`,`Location` where `Class_Employee`.`Class_ID`=`Class`.`Class_ID` and `Prerequisite_Certificate`.`Employee_ID`=`Class_Employee`.`Employee_ID` and `Prerequisite_Certificate`.`Course_ID`=`Class`.`Course_ID` and `Course`.`Course_ID`=`Class`.`Course_ID` and `Course`.`Course_ID`=`Prerequisite_Certificate`.`Course_ID` and `Location`.`Location_ID`=`Class`.`Location_ID` and `Prerequisite_Certificate`.`Certificate _Code`= `Class_Employee`.`Certificate _Code` and `Prerequisite_Certificate`.`Certificate _Code`='$cc'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $classid = $row[4];
            $ret['Class_ID'] = $classid;
            $ret['Class_Name'] = $row[5];
            $ret['Course_Name'] = $row[21];
            $ret['Class_Practical_Date'] = $row[7];
            $ret['Class_Written_Date'] = $row[8];
            $ret['Location_Name'] = $row[23];

            $query2 = "select * from `Class_Schedule` where `Class_ID`='$classid'";
            $res2 = mysql_query($query2);
            $ret['Class_Dates']=array();
            while($row2=mysql_fetch_array($res2))
            {
                $ret['Class_Dates'][] = $row2[1];
                $ret['Class_TimeFrom'][] = $row2[3];
                $ret['Class_TimeTo'][] = $row2[4];

            }
        }
        return $ret;
    }

    //Drop Enrolled Class of Student By Prereq Certificate Code
    function dropClass($cc,$admin=FALSE)
    {
        $ret = array();
        $query = "select * from `Class_Employee` where `Certificate _Code`='$cc'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $employeeid = $row[1];
            $classid = $row[2];
            $query2 = "select * from `Class_Schedule` where `Class_ID`='$classid' ORDER BY Class_Schedule_Date ASC;";
            $res2 = mysql_query($query2);
            if($row2=mysql_fetch_array($res2))
            {
                $startdate = $row2[1];
                $current = date("Y-m-d");
                $diff = abs(strtotime($startdate) - strtotime($current));
                //In days
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                if($days>15 || $admin==TRUE)
                {
                    //Check if employee has paid fees and if true, add it to refund table
                    $ret_paypal = getPaypalIDByClassIDEmployeeID($classid, $employeeid);
                    if($ret_paypal!=null){
                      $pid = $ret_paypal['Paypal_ID'];
                      $ret_paypalRefund = addPaypalRefund($pid);
                      if($ret_paypalRefund!=true){
                        $ret['Status']=0;
                        $ret['Message']='Error! Please try again in a while.';
                        return $ret;
                      }
                    }
                    //
                    $query3 = "DELETE from `Class_Employee` where `Certificate _Code`='$cc'";
                    $res3 = mysql_query($query3);
                    if($res3>0)
                    {
                        $query = "UPDATE `Prerequisite_Certificate` set `Status_ID`='5' where `Certificate _Code`='$cc'";
                        $res = mysql_query($query);
                        $temp_ret = getCertificate($cc);
                        $temp_ret2 = deleteFinalCertificate($temp_ret['Employee_ID'], $temp_ret['Course_ID']);
                        $temp_ret3 = removeFromFeesCheck($classid, $temp_ret['Employee_ID']);
                        $ret['Status']=1;
                        $ret['Message']='Class Dropped';

                    }
                    else
                    {
                        $ret['Status']=0;
                        $ret['Message']='Error! Please try again in a while.';
                    }
                }
                else
                {
                    $ret['Status']=0;
                    $ret['Message']='Course can be dropped only 15 days prior to start date!';
                }
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
            }

        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function getLastCertificateStatusFromCourseIDEmployeeID($cid, $eid){
      $ret = array();
      $query = "SELECT * FROM `Course_Certificate` WHERE `Course_ID`='$cid' AND `Employee_ID`='$eid' ORDER BY `Course_Certificate_EnrollDate` DESC LIMIT 1;";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res))
      {
          $practical = $row[6];
          $written = $row[7];
          if($practical==1 && $written==1){
            return 1;
          }
      }
      return 0;
    }

    function getEnrollmentStatusByClassIDEmployeeID($cid, $eid){
      $ret = array();
      $query = "SELECT * FROM `Class_Employee` WHERE `Class_ID`='$cid' AND `Employee_ID`='$eid';";
      $res = mysql_query($query);
      if(mysql_affected_rows()>0)
      {
        return 1;
      }
      return 0;
    }

    //Function to fetch all the data from the table Class_Employee
    function getClassEmployeeData()
    {
        $ret = array();
        $query = "SELECT * FROM `Class_Employee`;";
        $res = mysql_query($query);
        while($row = mysql_fetch_array($res)){
          $temp_ret = array();
          $temp_ret['Class_Employee_ID'] = $row[0];
          $temp_ret['Employee_ID'] = $row[1];
          $temp_ret['Class_ID'] = $row[2];
          $temp_ret['Certificate_Code'] = $row[3];
          $ret[] = $temp_ret;
        }
        return $ret;
    }

    //Function to check if the employee is enrolled in any class or not
    function checkEnrolmentInClassEmployeeByEmployeeID($eid) 
    {
        $ret = array();
        $query = "SELECT * FROM `Class_Employee` WHERE `Employee_ID` = '$eid';";
        $res = mysql_query($query);
        if (mysql_affected_rows() > 0) 
        {
            $ret['Enrolment_Status'] = 1;
        }
        else 
        {
            $ret['Enrolment_Status'] = 0;
        }
        return $ret;
    }

    function getOldClassIDfromPendingRefundsByEmployeeIDClassID($eid, $cid){
      $ret = array();
      $ret_course = getCourseIDByClassID($cid);
      $query = "select * from `Paypal_Refund`,`Paypal` where `Paypal`.`Paypal_ID`=`Paypal_Refund`.`Paypal_ID` and `Paypal`.`Employee_ID`='$eid' and `Paypal_Refund`.`Paypal_Refund_Flag`=0";
      $res = mysql_query($query);
      $ret['Status']='no';
      while($row=mysql_fetch_array($res)){
        $paypal_json = $row[5];
        $temp_class_id = $row[6];
        $temp_course_id = getCourseIDByClassID($temp_class_id);
        //return first matching
        //HMA - 2
        if($ret_course==3 && $temp_course_id==3){
          $ret['Status']='yes';
          $ret['old_class'] = $temp_class_id;
          return $ret;
        }
        //HMA - 1 or SA
        else if(($ret_course==2 || $ret_course==4) && ($temp_course_id==2 || $temp_course_id==4)){
          $ret['Status']='yes';
          $ret['old_class'] = $temp_class_id;
          return $ret;
        }
      }
      return $ret;
    }

    //type 0 student and type 1 coordinator

    function enrollEmployee($eid, $cid, $cc, $type = 0)
    {
        //wait list
        $count=10;
        $ret = array();
        $query = "SELECT * from `Class_Employee` where `Employee_ID`='$eid' AND `Class_ID`='$cid' AND `Certificate _Code`='$cc'";
        $res = mysql_query($query);
        if(mysql_affected_rows()>0)
        {
            $query = "UPDATE `Class_Employee` set `Class_ID`='$cid', `Certificate _Code`='$cc' where `Employee_ID`='$eid'";
            $res = mysql_query($query);
        }
        else
        {
            $query = "SELECT * from `Class_Employee` where `Class_ID`='$cid'";
            $res = mysql_query($query);
            $count = mysql_affected_rows();

            $query = "SELECT * from `Class` where `Class_ID`='$cid'";
            $res = mysql_query($query);
            if($row=mysql_fetch_array($res))
            {
                if($row[2]>$count || $type==1)
                {
                    $count=$row[2];
                    $query = "INSERT into `Class_Employee` values (NULL, $eid,$cid, '$cc')";
                    $res = mysql_query($query);
                    //Type 1 means added by Coordinator, so no need to check for Fees
                    if($type==0){
                      $fees_ret = getOldClassIDfromPendingRefundsByEmployeeIDClassID($eid, $cid);
                      $temp_stat = $fees_ret['Status'];
                      if(strcmp($temp_stat,'yes')==0){
                        //if fees available in Account
                        $old_cid = $fees_ret['old_class'];
                        updatePaypalRefundTableTrasnferredByClasIDEmployeeID($old_cid,$eid,$cid);
                        addActivity($eid, "Transfer to Class: " . $cid . ' from class: ' . $old_cid);
                      }
                      else{
                        $query2 = "INSERT into `Fees_Check` values (NULL, $cid,$eid, NULL)";
                        $res2 = mysql_query($query2);
                        if(!($res2>0)){
                          $ret['Status']=0;
                          $ret['Message']='Error! Please try again in a while.';
                          return $ret;
                        }
                      }
                    }
                }
                else
                {
                    $ret['Status']=0;
                    $ret['Message']='Class is Full! Do you want to add it to waitlist?';
                    return $ret;
                }
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
            }
        }
        if($res>0)
        {
            $query = "UPDATE `Prerequisite_Certificate` set `Status_ID`='2' where `Certificate _Code`='$cc'";
            $res = mysql_query($query);
            $temp_ret = getCertificate($cc);
            $temp_ret = setFinalCertificateInit($eid, $temp_ret['Course_ID']);

            //Paypal Refund Remove
            $paypal_refund = getPaypalRefundByClassIDEmployeeID($cid,$eid);
            if($paypal_refund!=null){
              $paypal_id = $paypal_refund['Paypal_ID'];
              removePaypalRefund($paypal_id);
            }
            //
            $ret['Status']=1;
            $ret['Message']='Class Assigned!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function setFinalCertificateInit($employee_id, $course_id)
    {
        $ret = array();
        $certificate_code = $course_id . generateRandomString() . $employee_id;
        $certificate_issue = date("Y-m-d");
        $certificate_expire = "";
        //If PCC Course
        if($course_id==5)
        {
            $certificate_expire = (new DateTime())->add(new DateInterval('P60M'))->format('Y-m-d');
        }
        else
        {
            $certificate_expire = (new DateTime())->add(new DateInterval('P36M'))->format('Y-m-d');
        }
        $status_id = 2;
        $query = "INSERT INTO `Course_Certificate` VALUES (NULL,$employee_id,$course_id,'$certificate_code','$certificate_issue','$certificate_expire',0,0,$status_id)";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Certficate_Code']= $certificate_code;
            $ret['Message']='Certificate Generated';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function deleteFinalCertificate($employee_id, $course_id)
    {
        $query = "delete from `Course_Certificate` where `Employee_ID`='$employee_id' AND `Course_ID`='$course_id'";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']="Final Certificate Deleted";
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function changeCertificatePracticalScore($cc, $practical)
    {
        $ret = array();
        $query = "UPDATE `Course_Certificate` SET `Course_Certificate_Practical`='$practical' where `Certificate _Code`='$cc'";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Practical Score Changed!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function changeCertificateWrittenScore($cc, $written)
    {
        $ret = array();
        $query = "UPDATE `Course_Certificate` SET `Course_Certificate_Written`='$written' where `Certificate _Code`='$cc'";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Written Score Changed!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function releaseCertificate($cc, $cc2)
    {
        $ret = array();
        $query = "select * from `Course_Certificate` where `Certificate _Code`='$cc'";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $practical = $row[6];
            $written = $row[7];
            //
            //Get  last date of class and use it as certiciate date and calculate expire date
            $temp_ret = getClassIDfromPrerequisiteCertificate($cc2);
            $class_id = $temp_ret['Class_ID'];
            $temp_ret = getClass($class_id);
            $course_id = $temp_ret['Course_ID'];
            $class_dates = $temp_ret['Class_Dates'];
            $index = count($class_dates);
            $enroll_date = $class_dates[$index-1];
            $enroll_date_format = strtotime($enroll_date);
            if($course_id==5)
            {
                $certificate_expire = date("Y-m-d", strtotime($enroll_date ." +5 years") );
            }
            else
            {
              $certificate_expire = date("Y-m-d", strtotime($enroll_date ." +3 years") );
            }
            //
            // $certificate_expire = (new DateTime())->add(new DateInterval('P36M'))->format('Y-m-d');
            if($practical==1 && $written==1)
            {
                //Passed
                $query = "UPDATE `Course_Certificate` SET `Course_Certificate_EnrollDate`='$enroll_date', `Course_Certificate_ExpireDate`='$certificate_expire', `Status_ID`='3' where `Certificate _Code`='$cc'";
                $res = mysql_query($query);
                $query = "UPDATE `Prerequisite_Certificate` SET `Status_ID`='3' where `Certificate _Code`='$cc2'";
                $res = mysql_query($query);
            }
            else
            {
                //Failed
                $query = "UPDATE `Course_Certificate` SET `Course_Certificate_EnrollDate`='$enroll_date', `Course_Certificate_ExpireDate`='$certificate_expire', `Status_ID`='4' where `Certificate _Code`='$cc'";
                $res = mysql_query($query);
                $query = "UPDATE `Prerequisite_Certificate` SET `Status_ID`='4' where `Certificate _Code`='$cc2'";
                $res = mysql_query($query);
            }
            if($res>0)
            {
                $ret['Status']=1;
                $ret['Message']='Certificate has been released!';
            }
            else
            {
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
            }

        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function getClassIDfromPrerequisiteCertificate($cc)
    {
        $ret = array();
        $query = "select * from `Class_Employee`,`Class`,`Course` where `Certificate _Code`='$cc' AND `Class_Employee`.`Class_ID`=`Class`.`Class_ID` AND `Course`.`Course_ID`=`Class`.`Course_ID`";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $ret['Status']=1;
            $ret['Employee_ID'] = $row[1];
            $ret['Class_ID'] = $row[2];
            $ret['Class_Name'] = $row[5];
            $ret['Course_Name'] = $row[14];
            $ret['Message']='Fetched Successfully!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function getEmployeeDetailFromCourseCertificate($cc)
    {
        $ret = array();
        $query = "select * from `Course_Certificate`,`Course`,`Employee`,`Login` where `Certificate _Code`='$cc' AND `Course_Certificate`.`Employee_ID`=`Employee`.`Employee_ID` AND `Course_Certificate`.`Course_ID`=`Course`.`Course_ID` AND `Login`.`Login_ID`=`Employee`.`Login_ID`";
        $res = mysql_query($query);
        if($row=mysql_fetch_array($res))
        {
            $ret['Status']=1;
            $practical = $row[6];
            $written = $row[7];
            $ret['Employee_ID'] =$row[1];
            $ret['First_Name'] = $row[13];
            $ret['Last_Name'] = $row[15];
            $ret['Email_Address'] = $row[25];
            $ret['Course_ID'] = $row[9];

            $ret['Course_Name'] = $row[10];
            if($practical==1)
            {
                $ret['Practical']="Passed";
            }
            else
            {
                $ret['Practical']="Failed";
            }
            if($written==1)
            {
                $ret['Written']="Passed";
            }
            else
            {
                $ret['Written']="Failed";
            }
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    function addFeedback($data){

        $ret = array();

        $name = $data['name'];
        $email = $data['email'];
        $location = $data['location'];
        $type = $data['type'];
        $message = $data['message'];

        $query = "insert into `Feedback` values(NULL,'$name','$email','$location','$type','$message',null, 0)";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Feedback submitted!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;



    }

    function getFeedback()
    {
        $ret = array();
        $query = "select * from `Feedback`";
        $res = mysql_query($query);
        while($row=mysql_fetch_array($res))
        {
            $temp_ret = array();
            $temp_ret['Feedback_ID'] = $row[0];
            $temp_ret['Feedback_Name'] = $row[1];
            $temp_ret['Feedback_Email'] = $row[2];
            $temp_ret['Feedback_Page'] = $row[3];
            $temp_ret['Feedback_Type'] = $row[4];
            $temp_ret['Feedback_Comment'] = $row[5];
            $temp_ret['Feedback_TimeStamp'] = $row[6];
            $temp_ret['Feedback_Status'] = $row[7];
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    function changeFeedbackStatus($data)
    {
        $ret = array();
        $feedbackid = $data['Feedback_ID'];
        $status = $data['Status'];
        $query = "update `Feedback` SET `Feedback_Status`='$status' where `Feedback_ID`='$feedbackid'";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Feedback Updated!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
        return $ret;
    }

    //Change it later
    function changeCertificateStatus($cid, $sid, $eid, $courseid, $finals)
    {
        $ret = array();
        $query = "UPDATE `Certificate` SET `Status_ID`='$sid' where `Employee_ID`='$eid' AND `Course_ID`='$courseid'";
        $res = mysql_query($query);
        if($finals!=0)
        {
            $certificate_code = $cid . generateRandomString() . $eid;
            $certificate_start = date("Y-m-d");
            $certificate_issue = date("Y-m-d");
            $certificate_expire = (new DateTime())->add(new DateInterval('P6M'))->format('Y-m-d');
            $status_id = 3;
            $query = "select * from `Certificate` where `Employee_ID`='$eid' AND `Course_ID`='$courseid' AND `Certificate_Final`=1";
            $res = mysql_query($query);
            if(mysql_affected_rows()==0)
            {
                $query = "INSERT INTO `Certificate` VALUES (NULL,$eid,$courseid,'$certificate_code','$certificate_start','$certificate_issue','$certificate_expire',1,$status_id)";
                $res = mysql_query($query);
                if($res>0)
                {
                    $ret['Status']=1;
                    $ret['Message']='Certificate Generated';
                }
                else
                {
                    $ret['Status']=0;
                    $ret['Message']='Error! Please try again in a while.';
                }
            }
        }
        else
        {
            $ret['Status']=1;
            $ret['Message']='Certificate Generated';
        }

        return $ret;

    }

    function addActivity($eid, $message, $notification=0){
      $ret = array();
      $query = "insert into `Employee_Activity` values(NULL,'$message',NULL,$notification,$eid)";
      $res = mysql_query($query);
      if($res>0)
      {
        $ret['Status']=1;
        $ret['Message']='Activity Added';
      }
      else {
        $ret['Status']=0;
        $ret['Message']='Error! Please try again in a while.';
      }
      return $ret;
    }

    function addActivity_coordinator($coid, $message){
      $ret = array();
      $query = "insert into `Coordinator_Activity` values(NULL,'$message',NULL,$coid)";
      $res = mysql_query($query);
      if($res>0)
      {
        $ret['Status']=1;
        $ret['Message']='Activity Added';
      }
      else {
        $ret['Status']=0;
        $ret['Message']='Error! Please try again in a while.';
      }
      return $ret;
    }

    function getActivities($eid){
      $ret = array();
      $query = "select * from `Employee_Activity` where `Employee_ID`='$eid' ORDER BY Employee_Activity_Date DESC;";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res))
      {
        $temp_ret = array();
        $temp_ret['Message'] = $row[1];
        $temp_ret['Date'] = $row[2];
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function getActivitiesCoordinator($coid){
      $ret = array();
      $query = "select * from `Coordinator_Activity` where `Coordinator_ID`='$coid' ORDER BY Coordinator_Activity_Date DESC;";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res))
      {
        $temp_ret = array();
        $temp_ret['Message'] = $row[1];
        $temp_ret['Date'] = $row[2];
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function getNotifications($eid){
      $ret = array();
      $query = "select * from `Employee_Activity` where `Employee_ID`='$eid' AND `Employee_Activity_Notification`=1 ORDER BY Employee_Activity_Date DESC;";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res))
      {
        $temp_ret = array();
        $temp_ret['Message'] = $row[1];
        $temp_ret['Date'] = $row[2];
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function setLoginAttempts($ip){
      $ret = array();
      $query = "select * from `Login_Failed` where `Login_Failed_IP`='$ip'";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res))
      {
        $lfid = $row[0];
        $count = $row[2] + 1;
        $query = "update `Login_Failed` SET `Login_Failed_Attempts`='$count' where `Login_Failed_ID`='$lfid'";
        $res = mysql_query($query);
        if($res>0)
        {
          $ret['Status']=1;
          $ret['Message']='Attempt Added';
        }
        else {
          $ret['Status']=0;
          $ret['Message']='Error! Please try again in a while.';
        }
      }
      else {
        $query = "insert into `Login_Failed` values (NULL,'$ip',1)";
        $res = mysql_query($query);
        if($res>0)
        {
          $ret['Status']=1;
          $ret['Message']='Attempt Added';
        }
        else {
          $ret['Status']=0;
          $ret['Message']='Error! Please try again in a while.';
        }
      }
      return $ret;
    }

    function getLoginAttempts($ip){
      $query = "select * from `Login_Failed` where `Login_Failed_IP`='$ip'";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res))
      {
        return $row[2];
      }
      else {
        return 0;
      }
    }

    function clearLoginAttempts($ip)
    {
      $ret = array();
      $query = "delete from `Login_Failed` where `Login_Failed_IP`='$ip'";
      $res = mysql_query($query);
      if($res>0)
      {
        clearLoginAttemptsTable();
        $ret['Status']=1;
        $ret['Message']='Login Attempts Cleared';
      }
      else {
        $ret['Status']=0;
        $ret['Message']='Error! Please try again in a while.';
      }
      return $ret;
    }

    function clearLoginAttemptsTable(){
      $ret = array();
      $query = "select * from `Login_failed`";
      $res = mysql_query($query);
      if(mysql_affected_rows()==0)
      {
        $query = "TRUNCATE TABLE `Login_Failed`;";
        $res = mysql_query($query);
        if($res>0)
        {
          $ret['Status']=1;
          $ret['Message']='Login Attempt Table Cleared';
        }
        else {
          $ret['Status']=0;
          $ret['Message']='Error! Please try again in a while.';
        }
      }
      return $ret;
    }

    function getLoginFailedTable()
    {
      $ret = array();
      $query = "select * from `Login_Failed`";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res))
      {
        $temp_ret = array();
        $temp_ret['Col1'] = $row[0];
        $temp_ret['Col2'] = $row[1];
        $temp_ret['Col3'] = $row[2];
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    //Course_Evaluation

    function checkCourseEvaluationAnswerByClassIdEmployeeId($cid,$eid){
      $ret = array();
      $query = "select * from `Course_Evaluation_Answer` where `Class_ID`='$cid' and `Employee_ID`='$eid'";
      $res = mysql_query($query);
      if(mysql_affected_rows()>0)
      {
        return 1;
      }
      return 0;
    }

    function getCourseEvaluationQuestion(){
      $ret = array();
      $query = "select * from `Course_Evaluation_Question`";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res))
      {
        $temp_ret = array();
        $temp_ret['Course_Evaluation_Question_ID'] = $row[0];
        $temp_ret['Course_Evaluation_Question_Question'] = $row[1];
        $temp_ret['Course_Evaluation_Question_Multichoice'] = $row[2];
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function getCourseEvaluationAnswerByClassId($cid){
      $ret = array();
      $query = "select * from `Course_Evaluation_Question`,`Course_Evaluation_Answer`,`Employee` where `Course_Evaluation_Question`.`Course_Evaluation_Question_ID`=`Course_Evaluation_Answer`.`Course_Evaluation_Question_ID` and `Employee`.`Employee_ID`=`Course_Evaluation_Answer`.`Employee_ID` and `Class_ID`='$cid' ORDER BY `Course_Evaluation_Answer`.`Employee_ID`";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res))
      {
        $temp_ret = array();
        $temp_ret['Course_Evaluation_Question_Question'] = $row[1];
        $temp_ret['Course_Evaluation_Answer_Value'] = $row[4];
        $temp_ret['Employee_ID'] = $row[6];
        $temp_ret['Employee_FirstName'] = $row[10];
        $temp_ret['Employee_LastName'] = $row[12];

        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function setCourseEvaluationAnswerByClassIdEmployeeId($answer,$qid,$cid,$eid){
      $ret = array();
      $query = "insert into `Course_Evaluation_Answer` values (NULL,'$answer',$qid,$cid,$eid)";
      $res = mysql_query($query);
      if($res>0){
        return 1;
      }
      else {
        return 0;
      }
    }

    //Paypal

    function addPaypalData($data,$cid,$eid){
      $ret = array();
      $query = "INSERT INTO `Paypal` VALUES (NULL, '$data',$cid,$eid,NULL);";
      $res = mysql_query($query);
      if($res>0)
      {
        $ret['Status']=1;
        $ret['Message']='Paypal Data Added';
      }
      else {
        $ret['Status']=0;
        $ret['Message']='Error! Please try again in a while.';
      }
      return $ret;
    }

    function addCalAPAData($tid, $amt, $date, $payee, $class) {
        $ret = array();
        $query = "INSERT INTO `CalAPA` VALUES ('$tid', $amt, '$date', '$payee', '$class');";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='CalAPA transaction data added successfully.';
      }
      else {
        $ret['Status']=0;
        $ret['Message']='Error! Please try again in a while.';
      }
      return $ret;
    }

    function getPaypalData(){
      $ret = array();
      $query = "select * from `Paypal`;";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
          $temp_ret = array();
          $pid = $row[0];
          $query2 = "select * from `Paypal_Refund` where `Paypal_ID`=$pid";
          $res2 = mysql_query($query2);
          if($row2=mysql_fetch_array($res2)){
            $temp_ret['Refund_Status'] = $row2[2];
          }
          else{
            $temp_ret['Refund_Status'] = "none";
          }
          $temp_ret['data'] = $row[1];
          $temp_ret['Class_ID'] = $row[2];
          $temp_ret['Employee_ID'] = $row[3];
          $temp_ret['TimeStamp'] = $row[4];
          $ret[] = $temp_ret;
      }
      return $ret;
    }

    function getCalAPAData(){
        $ret = array();
        $query = "SELECT * FROM `CalAPA` ORDER BY `Timestamp` DESC;";
        $res = mysql_query($query);
        while ($row=mysql_fetch_array($res)) {
            $temp_ret = array();
            $temp_ret['Transaction_ID'] = $row[0];
            $temp_ret['Amount'] = $row[1];
            $temp_ret['Timestamp'] = $row[2];
            $temp_ret['Payee_Name'] = $row[3];
            $temp_ret['Paypal_Classes'] = $row[4];
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    function getCalAPADataByEmployeeID($eid) {
        $ret = array();
        $query = "SELECT * FROM `CalAPA` WHERE `Employee_ID`=$eid;";
        $res = mysql_query($query);
        while ($row=mysql_fetch_array($res)) {
            $temp_ret = array();
            $temp_ret['Transaction_ID'] = $row[0];
            $temp_ret['Amount'] = $row[1];
            $temp_ret['Timestamp'] = $row[2];
            $temp_ret['Payee_Name'] = $row[3];
            $temp_ret['Paypal_Classes'] = $row[4];
            $ret[] = $temp_ret;
        }
        return $ret;
    }

    function getPaypalDataByDate($startdate, $enddate){
      $ret = array();
      $query = "select * from `Paypal` where `Timestamp`>='$startdate' and `Timestamp`<='$enddate';";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
          // $temp_ret['data'] = $row[1];
          // $temp_ret['TimeStamp'] = $row[4];
          $paypal_json = json_decode($row[1], true);
          $temp_tid = $paypal_json['txn_id'];
          $temp_status = $paypal_json['payment_status'];
          $temp_gross = $paypal_json['payment_gross'];
          $temp_timestemp = $row[4];

          $temp_ret['Transcation_ID'] = $temp_tid;
          $temp_ret['Status'] = $temp_status;
          $temp_ret['Gross'] = $temp_gross;
          $temp_ret['Timestamp'] = $temp_timestemp;
          $ret[$temp_tid] = $temp_ret;
      }
      return $ret;
    }

    function getCalAPADataByDate($startdate, $enddate) {
        $ret = array();
        $query = "SELECT * FROM `CalAPA` WHERE `Timestamp`>='$startdate' AND `Timestamp`<='$enddate';";
        $res = mysql_query($query);
        while ($row = mysql_fetch_array($res)) {
            $temp_tid = $row[0];
            $temp_ret['Transcation_ID'] = $temp_tid;
            $temp_ret['Gross'] = $row[1];
            $ret[$temp_tid] = $temp_ret;
        }
        return $ret;
    }


    function getPaypalDataByClassIDEmployeeID($cid, $eid){
      $ret = array();
      $query = "select * from `Paypal` where `Class_ID`='$cid' and `Employee_ID`='$eid';";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res)){
        $paypal_data = $row[1];
        $ret['Paypal_Data'] = $paypal_data;
      }
      return $ret;
    }

    function getPaypalDataByTransactionID($tid){
      $ret = array();
      $query = "select * from `Paypal`;";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
          $temp_ret = array();
          $paypal_data = $row[1];
          $paypal_json = json_decode($paypal_data, true);
          $temp_tid = $paypal_json['txn_id'];

          if(strcmp($temp_tid,$tid)==0){
            $pid = $row[0];
            $query2 = "select * from `Paypal_Refund` where `Paypal_ID`=$pid";
            $res2 = mysql_query($query2);
            if($row2=mysql_fetch_array($res2)){
              $temp_ret['Refund_Status'] = $row2[2];
            }
            else{
              $temp_ret['Refund_Status'] = "none";
            }
            $temp_ret['data'] = $row[1];
            $temp_ret['Class_ID'] = $row[2];
            $temp_ret['Employee_ID'] = $row[3];
            $temp_ret['TimeStamp'] = $row[4];
            $ret[] = $temp_ret;
            return $ret;
          }
      }
      return $ret;
    }

    function getPaypalDataByPayeeName($name){
      $ret = array();
      $query = "SELECT * from `Paypal`;";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
          $temp_ret = array();
          $paypal_data = $row[1];
          $paypal_json = json_decode($paypal_data, true);
          $temp_name = $paypal_json['address_name'];

          if(stripos($temp_name,$name)!==false){
            $pid = $row[0];
            echo '<script type="text/javascript"> console.log("'.$pid.'"); </script>';
            $query2 = "SELECT * from `Paypal_Refund` where `Paypal_ID`=$pid";
            $res2 = mysql_query($query2);
            if($row2=mysql_fetch_array($res2)){
              $temp_ret['Refund_Status'] = $row2[2];
            }
            else{
              $temp_ret['Refund_Status'] = "none";
            }
            $temp_ret['data'] = $row[1];
            $temp_ret['Class_ID'] = $row[2];
            $temp_ret['Employee_ID'] = $row[3];
            $temp_ret['TimeStamp'] = $row[4];
            $ret[] = $temp_ret;
          }
      }
      return $ret;
    }

    function getFeesCheckData(){
      $ret = array();
      $query = "select * from `Fees_Check`;";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
          $temp_ret = array();
          $temp_ret['Class_ID'] = $row[1];
          $temp_ret['Employee_ID'] = $row[2];
          $temp_ret['TimeStamp'] = $row[3];
          $ret[] = $temp_ret;
      }
      return $ret;
    }

    //Check Fees Check for timings
    function getTimeStampFromFeesCheckByClassIDEmployeeID($classid, $eid){
      $ret = array();
      $query = "select * from `Fees_Check` where `Class_ID`='$classid' and `Employee_ID`='$eid';";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res))
      {
        $ret['Status']=1;
        $ret['Timestamp']=$row[3];
      }
      return $ret;
    }

    function removeFromFeesCheck($classid, $eid){
      $ret = array();
      $query = "delete from `Fees_Check` where `Class_ID`='$classid' and `Employee_ID`='$eid';";
      $res = mysql_query($query);
      if($res>0)
      {
        $ret['Status']=1;
        $ret['Message']='Removed From Fees Check';
      }
      else {
        $ret['Status']=0;
        $ret['Message']='Error! Please try again in a while.';
      }
      return $ret;
    }

    //get all unpaid fees by eid
    function checkPaypalDataByEmployeID($eid){
      $ret = array();
      $query = "select * from `Fees_Check` where `Employee_ID`='$eid'";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
        $class_id = $row[1];
        $query2 = "select * from `Paypal` where `Class_ID`='$class_id' and `Employee_ID`='$eid';";
        $res2 = mysql_query($query2);
        $ret2 = array();
        $total_charge=0;
        $count = 0;
        while($row2=mysql_fetch_array($res2)){
            $temp_ret2 = array();
            $jsondata = $row2[1];
            $arr = json_decode($jsondata,true);
            $payment_gross = $arr['payment_gross'];
            $total_charge = $total_charge + $payment_gross;
            $ret2[] = $temp_ret2;
            $count++;
        }
        if($total_charge==0 || $count==0){
          //Fees Not Paid
          $query3 = "select * from `Class` where `Class_ID`='$class_id';";
          $res3 = mysql_query($query3);
          if($row3=mysql_fetch_array($res3)){
            $temp_ret['Class_ID'] = $row3[0];
            $temp_ret['Class_Name'] = $row3[1];
            $temp_ret['Course_ID'] = $row3[5];
            $ret[]=$temp_ret;
          }
        }
      }
      return $ret;
    }

    //get all paid fees by eid
    function getPaypalPaidDataByEmployeID($eid){
      $ret = array();
      $query = "select * from `Fees_Check` where `Employee_ID`='$eid'";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
        $class_id = $row[1];
        $query2 = "select * from `Paypal` where `Class_ID`='$class_id' and `Employee_ID`='$eid';";
        $res2 = mysql_query($query2);
        $ret2 = array();
        $total_charge=0;
        $count = 0;
        while($row2=mysql_fetch_array($res2)){
            $temp_ret2 = array();
            $jsondata = $row2[1];
            $arr = json_decode($jsondata,true);
            $payment_gross = $arr['payment_gross'];
            $total_charge = $total_charge + $payment_gross;
            $ret2[] = $temp_ret2;
            $count++;
        }
        if($total_charge>0 || $count==0){
          //Fees Paid
          $query3 = "select * from `Class` where `Class_ID`='$class_id';";
          $res3 = mysql_query($query3);
          if($row3=mysql_fetch_array($res3)){
            $temp_ret['Class_ID'] = $row3[0];
            $temp_ret['Class_Name'] = $row3[1];
            $temp_ret['Course_ID'] = $row3[5];
            $ret[]=$temp_ret;
          }
        }
        return $ret;
      }
    }

    //get all paid and unpaid fees by eid
    function getAllPaypalDataByEmployeID($eid){
      $ret = array();
      $query = "select * from `Paypal` where `Employee_ID`='$eid' order by Timestamp desc";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
        $temp_ret = array();
        $temp_ret['pid'] = $row[0];
        $temp_ret['Paypal_Data'] = $row[1];
        $temp_ret['Class_ID'] = $row[2];
        $temp_ret['Timestamp'] = $row[4];
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function getPaypalDataByPaypalID($pid){
      $ret = array();
      $query = "select * from `Paypal` where `Paypal_ID`='$pid'";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res)){
        $ret['Paypal_Data'] = $row[1];
        $ret['Class_ID'] = $row[2];
        $ret['Employee_ID'] = $row[3];
        $ret['Timestamp'] = $row[4];
      }
      return $ret;
    }

    //Returns Employee and it's unpaid classes with prereq certificate code
    function checkPaypalData(){
      $ret = array();
      $query = "select * from `Fees_Check` where 1";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
        $class_id = $row[1];
        $eid = $row[2];
        $timestamp = $row[3];
        $query2 = "select * from `Paypal` where `Class_ID`='$class_id' and `Employee_ID`='$eid';";
        $res2 = mysql_query($query2);
        $ret2 = array();
        $total_charge=0;
        $count = 0;
        while($row2=mysql_fetch_array($res2)){
            $temp_ret2 = array();
            $jsondata = $row2[1];
            $arr = json_decode($jsondata,true);
            $payment_gross = $arr['payment_gross'];
            $total_charge = $total_charge + $payment_gross;
            $ret2[] = $temp_ret2;
            $count++;
        }
        if($total_charge==0 || $count==0){
          //Fees Not Paid
          $query3 = "select * from `Class` where `Class_ID`='$class_id';";
          $res3 = mysql_query($query3);
          if($row3=mysql_fetch_array($res3)){
            $temp_ret['Class_ID'] = $row3[0];
            $temp_ret['Class_Name'] = $row3[1];
            $temp_ret['Course_ID'] = $row3[5];
            $temp_ret['Employee_ID'] = $eid;
            $temp_ret['Timestamp'] = $timestamp;
            //Fetch Certificate Code
            $query4 = "select * from `Class_Employee` where `Class_ID`='$class_id' and `Employee_ID`='$eid'";
            $res4 = mysql_query($query4);
            if($row4=mysql_fetch_array($res4)){
              $temp_ret['Certificate_Code'] = $row4[3];
              $ret[]=$temp_ret;
            }
          }
        }
      }
      return $ret;
    }

    function getPaypalIDByClassIDEmployeeID($cid, $eid){
      $ret = array();
      $query = "select * from `Paypal` where `Class_ID`='$cid' and `Employee_ID`='$eid' order by Timestamp desc";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res)){
        $ret['Paypal_ID'] = $row[0];
      }
      return $ret;
    }

    function logCronJob($cronjobname, $status){
      $query = "insert into `Cron_Log` values (NULL, '$cronjobname', '$status', NULL)";
      $res = mysql_query($query);
      if($res>0)
        return true;
      return false;
    }

    function getlogsForCronJob(){
      $ret = array();
      $query = "select * from `Cron_Log` where 1";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
        $temp_ret = array();
        $temp_ret['CronJobName'] = $row[1];
        $temp_ret['Status'] = $row[2];
        $temp_ret['Timestamp'] = $row[3];
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function addPaypalRefund($pid){
      $query = "insert into `Paypal_Refund` values (NULL, $pid, 0, NULL)";
      $res = mysql_query($query);
      if($res>0)
        return true;
      return false;
    }

    function removePaypalRefund($pid){
      $query = "delete from `Paypal_Refund` where `Paypal_ID`='$pid'";
      $res = mysql_query($query);
      if($res>0)
        return true;
      return false;
    }

    function getPaypalRefundByClassIDEmployeeID($cid,$eid){
      $ret = array();
      $query = "select * from `Paypal_Refund`,`Paypal` where `Class_ID`='$cid' and `Employee_ID`='$eid' and `Paypal_Refund_Flag`=0 and `Paypal_Refund`.`Paypal_ID`=`Paypal`.`Paypal_ID`";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res)){
        $ret['Paypal_ID'] = $row[1];
      }
      return $ret;
    }

    function getPaypalRefund($type){
      //fetch all refunds
      $ret = array();
      $query = "";
      if($type==3){
        $query = "select * from `Paypal_Refund` where 1";
      }
      else{
        $query = "select * from `Paypal_Refund` where `Paypal_Refund_Flag`=$type";
      }
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
        $temp_ret = array();
        $prid = $row[0];
        $pid = $row[1];
        $ret2 = getPaypalDataByPaypalID($pid);
        $cid = $ret2['Class_ID'];
        $eid = $ret2['Employee_ID'];
        /*$retclass = getClassesByEmployeeID($eid); //this gives recent values. Paypal table contains obsolete data
        $cid = $retclass['Class_ID'];*/

        $ret3 = getEmployeeDetailsByEmployeeID($eid);
        $temp_ret['FirstName'] = $ret3['FirstName'];
        $temp_ret['LastName'] = $ret3['LastName'];
        $temp_ret['Email'] = $ret3['Email'];

        $ret4 = getClass($cid);
        $temp_ret['Class_Name'] = $ret4['Class_Name'];
        $temp_ret['Course_Name'] = $ret4['Course_Name'];

        $temp_ret['Paypal_Data'] = $ret2['Paypal_Data'];
        $temp_ret['Refunded'] = $row[2];
        $temp_ret['Timestamp'] = $row[3];

        $temp_ret['Class_ID'] = $cid;
        $temp_ret['Employee_ID'] = $eid;
        $temp_ret['Paypal_Refund_ID'] = $prid;
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function updatePaypalRefundTableManually($paypal_refund_id){
      $ret = array();
      $query = "update `Paypal_Refund` SET `Paypal_Refund_Flag`=1 where `Paypal_Refund_ID`='$paypal_refund_id'";
      $res = mysql_query($query);
      if($res>0)
      {
          $ret['Status']=1;
          $ret['Message']='Course Details Changed!';
      }
      else
      {
          $ret['Status']=0;
          $ret['Message']='Error! Please try again in a while.';
      }
    }

    function updatePaypalRefundTableByClasIDEmployeeID($cid,$eid){
      $ret = array();
      $query = "select * from `Paypal_Refund`,`Paypal` where `Class_ID`='$cid' and `Employee_ID`='$eid' and `Paypal_Refund_Flag`=0 and `Paypal_Refund`.`Paypal_ID`=`Paypal`.`Paypal_ID`";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res)){
        $pid = $row[1];
        $query = "update `Paypal_Refund` SET `Paypal_Refund_Flag`=1 where `Paypal_ID`='$pid'";
        $res = mysql_query($query);
        if($res>0)
        {
            $ret['Status']=1;
            $ret['Message']='Course Details Changed!';
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
      }
      else{
        $ret['Status']=0;
        $ret['Message']='Error! Please try again in a while.';
      }
      return $ret;
    }

    function updatePaypalRefundTableTrasnferredByClasIDEmployeeID($cid,$eid,$new_Class_ID){
      $ret = array();
      $query = "SELECT * from `Paypal_Refund`,`Paypal` where `Class_ID`=$cid and `Employee_ID`=$eid and `Paypal_Refund`.`Paypal_ID`=`Paypal`.`Paypal_ID` and `Paypal_Refund`.`Paypal_Refund_Flag`=0";
      $res = mysql_query($query);
      if($row=mysql_fetch_array($res)){
        $pid = $row[1];
        $query = "UPDATE `Paypal_Refund` SET `Paypal_Refund_Flag`=2 where `Paypal_ID`=$pid";
        $res = mysql_query($query);
        if($res>0)
        {
            if($new_Class_ID!=0){
              $query = "UPDATE `Paypal` SET `Class_ID`=$new_Class_ID where `Paypal_ID`=$pid";
              $res = mysql_query($query);
              if($res>0)
              {
                $ret['Status']=1;
                $ret['Message']='Refund Status Changed!';
              }
              else{
                $ret['Status']=0;
                $ret['Message']='Error! Please try again in a while.';
              }
            }
            else{
              $ret['Status']=1;
              $ret['Message']='Refund Status Changed!';
            }
        }
        else
        {
            $ret['Status']=0;
            $ret['Message']='Error! Please try again in a while.';
        }
      }
      else{
        $ret['Status']=0;
        $ret['Message']='Error! Please try again in a while.';
      }
      return $ret;
    }

    //Transfer request

    function addTransferRequest($cid,$eid,$email,$notes){
      $query = "INSERT INTO `Class_Transfer` VALUES (NULL, $cid, $eid, '$email','$notes',0, NULL)";
      $res = mysql_query($query);
      if($res>0)
        return true;
      return false;
    }

    function getTransferRequest(){
      $ret = array();
      $query = "SELECT * FROM `Class_Transfer` WHERE 1";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
        $temp_ret = array();
        $temp_ret['Class_ID'] = $row[1];
        $temp_ret['Employee_ID'] = $row[2];
        $temp_ret['Email'] = $row[3];
        $temp_ret['Notes'] = $row[4];
        $temp_ret['Status'] = $row[5];
        $temp_ret['Timestamp'] = $row[6];
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function getTransferRequestByEmployeeID($eid){
      $ret = array();
      $query = "select * from `Class_Transfer` where `Employee_ID`='$eid'";
      $res = mysql_query($query);
      while($row=mysql_fetch_array($res)){
        $temp_ret = array();
        $temp_ret['Class_Transfer_ID'] = $row[0];
        $temp_ret['Class_ID'] = $row[1];
        $temp_ret['Employee_ID'] = $row[2];
        $temp_ret['Email'] = $row[3];
        $temp_ret['Notes'] = $row[4];
        $temp_ret['Status'] = $row[5];
        $ret[] = $temp_ret;
      }
      return $ret;
    }

    function updateTransferRequestByEmployeeIDClassID($eid,$cid,$status){
      $query = "update `Class_Transfer` set `Class_Transfer_Status`='$status' where `Class_ID`='$cid' and `Employee_ID`='$eid'";
      $res = mysql_query($query);
      if($res>0)
        return true;
      return false;
    }

    function removeTransferRequest($cid,$eid){
      $query = "delete from `Class_Transfer` where `Class_ID`='$cid' and `Employee_ID`='$eid'";
      $res = mysql_query($query);
      if($res>0)
        return true;
      return false;
    }

    ?>
