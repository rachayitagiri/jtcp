<?php
    /**
     * This call sends an email to one recipient, using a validated sender address
     * Do not forget to update the sender address used in the sample
     */
    include("mailjet-apiv3-php-no-composer-master/vendor/autoload.php");

    $apiKey = "53c7ca762e72b62659344ef3a20736ef";
    $secretKey = "600f75304db7997cb5ca575cf52e12a7";
    

    use \Mailjet\Resources;
    
    // use your saved credentials
    $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');
    

    function welcome_email($firstname,$lastname,$emailaddress)
    {
        $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');
        $fullname = $firstname . " " . $lastname;
        $body = [
        'FromEmail' => "csulbjtcp@gmail.com",
        'FromName' => "JTCP",
        'Subject' => "Welcome to Joint Training & Certification Program!",
        'Text-part' => "Dear " $fullname . ", welcome to JTCP Program! Now you can Take Safety Quiz, Download prerequisite exam materials, Pass prerequisites, Enroll to the course and Get certified.",
        'Html-part' => '
        <!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
        <title>Verify</title>
        <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <style type="text/css">
            #outlook a { padding: 0; }
            .ReadMsgBody { width: 100%; }
        .ExternalClass { width: 100%; }
        .ExternalClass * { line-height:100%; }
        body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
        p { display: block; margin: 13px 0; }
        </style>
        <!--[if !mso]><!-->
            <style type="text/css">
            @media only screen and (max-width:480px) {
                @-ms-viewport { width:320px; }
                @viewport { width:320px; }
            }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if lte mso 11]>
                <style type="text/css">
                .outlook-group-fix {
                width:100% !important;
                }
        </style>
        <![endif]-->
        <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-66 { width:66.66666666666666%!important; }
            .mj-column-per-33 { width:33.33333333333333%!important; }
            .mj-column-per-100 { width:100%!important; }
            .mj-column-per-50 { width:50%!important; }
        }
        </style>
        </head>
        <body style="background: #F4F4F4;">
        <div style="background-color:#F4F4F4;"><!--[if mso | IE]>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
            <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
            <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:396px;">
                <![endif]--><div class="mj-column-per-66 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 0px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:left;"><p style="margin: 10px 0;"></p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                    </td><td style="vertical-align:top;width:198px;">
                    <![endif]--><div class="mj-column-per-33 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 0px;padding-top:0px;padding-bottom:0px;" align="right"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:right;"></div></td></tr></tbody></table></div><!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                            </td></tr></table>
                            <![endif]-->
                            <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:0px;padding-top:0px;"><!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:600px;"><a href="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" target="_blank"><img alt="" title="" height="auto" src="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="600"></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                        </td></tr></table>
                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                            </td></tr></table>
                                            <![endif]-->
                                            <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                <![endif]--><div style="margin:0px auto;max-width:600px;background:#ffffff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#ffffff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><h2>Welcome to Joint Training & Certification Program</h2></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p style="margin: 10px 0;">Hello ' . $fullname .  ',</p><p style="margin: 10px 0;">Welcome to Joint Training & Certification Program by Caltrans and CSULB.</p><p style="margin: 10px 0;">Click here to <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/login.php">Login</a> to your account to</p><ol><li style="margin: 10px 0px;">Take safety quiz.</li><li style="margin: 10px 0px;">Download prerequisite exam materials.</li><li style="margin: 10px 0px;">Pass prerequisites.</li><li style="margin: 10px 0px;">Enroll to the course.</li><li style="margin: 10px 0px;">Get certified. </li></ol><p style="margin: 10px 0;">For any queries and help feel free to contact us <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/contact.php">here</a>.</p><p style="margin: 10px 0;">Thanks,</p><p style="margin: 10px 0;">JTCP Program.</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                        </td></tr></table>
                                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                            </td></tr></table>
                                                            <![endif]-->
                                                            <!--[if mso | IE]>
                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                <tr>
                                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:300px;">
                                                                    <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:105px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvuu.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="105"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                        </td><td style="vertical-align:top;width:300px;">
                                                                        <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:98px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvu4.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="98"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                            </td></tr></table>
                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                </td></tr></table>
                                                                                <![endif]-->
                                                                                <!--[if mso | IE]>
                                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                                    <tr>
                                                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                                    <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                                                        <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">This e-mail has been sent to ' . $emailaddress . ', <a href="[[UNSUB_LINK_EN]]" style="color:inherit;text-decoration:none;" target="_blank" >click here to unsubscribe</a>.</p></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">US</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                            </td></tr></table>
                                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                                </td></tr></table>
                                                                                                <![endif]--></div>
                                                                                                </body>
                                                                                                </html>
                                                                                                ',
                                                                                                'Recipients' => [['Email' => $emailaddress]]
                                                                                                ];
        
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        
        return $response;
    }
    
