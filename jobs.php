<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<title>CSULB | Caltrans - Joint Training & Certification Program</title>

<!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>

<script type="text/javascript">

	// check for window is loaded or not
	window.onload = function(){
	var myNav = document.getElementById('mymainnav');
	$(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
			$(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
		} else {
          $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
        }
      });
	}
</script>

</head>


<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

	<!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
			<!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
			<!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg.jpg');"></div>

            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
	<!----------------------- slider image completed ---------------------------------------->

	<!----------------------- navigation in bottom of image slider -------------------------->
	<nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav" style="width: 110%;">
                    <li>
                        <a href="index.php">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="index.php#courses">COURSES</a>
                    </li>
                    <li>
                        <a href="search.php" >SEARCH</a>
                    </li>
                    <li>
                        <a href="help.php" >FAQ</a>
                    </li>
                    <li>
                        <a href="contact.php" >CONTACT</a>
                    </li>
                    <li>
                        <a href="feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                        <a href="#" style="color: rgb(177,92,34);" >JOB OPENINGS</a>
                    </li>
					<li>
                      <a href="gallery.php">GALLERY</a>
                    </li>
                    <li>
                        <a href="login.php">LOGIN</a>
                    </li>
                    <li>
                        <a href="signup.php">ENROLL</a>
                    </li>
                    <li>
                        <a href="acknowledgment.php">ACKNOWLEDGMENT</a>
                    </li>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!----------------------------------- navigation completed ---------------------------------->

	<!----------------------------------- body started ------------------------------------------>

<div class="container-fluid">
<section class="container">
<h5><a href="http://www.csulb.edu/college-of-engineering" style="color: rgb(177,92,34);" target="_blank">College of Engineering</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="index.php" style="color: rgb(177,92,34);">JTCP</a> <span class="glyphicon glyphicon-chevron-right"></span> Job Openings</h5>
<div class="container-page">
<div class="col-md-12">
<h3 class="dark-grey">Job Openings:</h3>
    <hr>
    <!-- Start Job1 -->
    <div style="width: 90%">
    <p><a href="javascript:void();">CLASSIFICATION/STATUS:</a>
    &nbsp;Independent Contractor/Instructor
    </p>
    <p><a href="javascript:void();">POSTING DATE:</a>
        &nbsp;October 9, 2017
    </p>
    <p><a href="javascript:void();">DEPARTMENT/COLLEGE:</a>
    &nbsp;National Center for Transportation, Green Technology, and Education (TransGET)
    </p>
    <p><a href="javascript:void();">PROJECT:</a>
    &nbsp;Joint Training and Certification Program (JTCP)
    </p>
    <p><a href="javascript:void();">COMPENSATION:</a>
    &nbsp;Contract agreement will be signed for compensation.
    </p>
    <p><a href="javascript:void();">DEADLINE:</a>
    &nbsp;Open until filled (or canceled). Responses reviewed beginning November 1, 2017.
    </p>
    <p><a href="javascript:void();">SEEKING:</a>
    &nbsp;Qualified Independent Contractors skilled in the test methods listed below hired as Part Time Instructors for the Joint Training and Certification Programs implemented at CSULB and San Jose State University locations.</p>
    <hr>
    <h5 data-toggle="collapse" data-target="#minquali"><a href="javascript:void();">MINIMUM
QUALIFICATIONS:</a></h5>
    <div id="minquali" class="collapse">
        <ol>
          <!-- <li>B.S. in Civil Engineering.</li> -->
          <li>A minimum of two years of hands‐on relevant test methods experience.</li>
          <li>Experience in providing training to others.</li>
          <li>Effective oral and written communication skills.</li>
          <li>Able to present information in an unbiased manner.</li>
          <li>Demonstrated potential for effective teaching of similar training.
Commitment to and/or expertise in educating a diverse student population.</li>
        </ol>
    </div>
    <hr>
    <h5 data-toggle="collapse" data-target="#prefered"><a href="javascript:void();">PREFERRED
QUALIFICATIONS/EXPERTISE:</a></h5>
    <div id="prefered" class="collapse">
      <ol>
        <li>Registration as Professional Engineer (PE)</li>
        <li>Current or ability to attain Caltrans Independent Assurance or ACI certification in test
methods being taught</li>
        <li>Experience teaching in similar programs</li>
        <li>Demonstrated Commitment to teaching</li>
        <li>Adult Training certification</li>
        <li>Ability to work well both as a team player and individual self-starter</li>
        <li>Basic computer skills, specifically experience with Microsoft Outlook, Word, and Excel</li>
        <li>Exceptional organizational and follow-up skills</li>
        <li>Valid driver’s license with acceptable motor vehicle record.</li>
    </div>
    <hr>
    <h5 data-toggle="collapse" data-target="#reqdoc"><a href="javascript:void();">REQUIRED
DOCUMENTATION FOR CONSIDERATION:</a></h5>
    <div id="reqdoc" class="collapse">
      <ol>
        <li>Letter of interest addressing the minimum and desired/preferred qualifications and the
preferred location (Long Beach vs San Jose)</li>
        <li>Current CV/Resume including current contact information (mailing address, e-mail address,
telephone number)</li>
        <li>List of three to five current professional references and letters of recommendation</li>
        <li>Transcript from institution awarding highest degree / copies of technical certifications;</li>
        <li>Those selected will be required to submit a signed SC-1 form</li>
    </div>
    <hr>
    <h5 data-toggle="collapse" data-target="#testmethods"><a href="javascript:void();">TEST METHODS TO BE
TAUGHT:</a></h5>
    <div id="testmethods" class="collapse">
      <p>Test methods include, but are not limited to:
      <br/>
<b><u>HMA I</u></b>
<p>CT 125 Method of Test for Sampling Highway Materials and Products Used in the Roadway Structural Sections (HMA)</p>
<p>AASHTO R76 Standard Method of Test for Reducing Samples of Aggregate to Testing Size</p>
<p>AASHTO T 11 Standard Method of Test for Materials Finer Than 75-&micro;m (No. 200) Sieve in Mineral Aggregates by Washing</p>
<p>AASHTO T 27 Standard Method of Test for Sieve Analysis of Fine and Coarse Aggregates</p>
<p>CT 105 Calculations Pertaining to Gradings and Specific Gravities AASHTO T 255 Standard Method of Test for Total Evaporable Moisture Content of Aggregate by Drying</p>
<p>AASHTO T 335 Standard Method of Test for Determining the Percentage of Fracture in Coarse Aggregate</p>
<p>AASHTO T 176 Standard Method of Test for Plastic Fines in Graded Aggrega Soils by Use of the Sand Equivalent Test Standard Practice for Reducing Samples of Hot Mix Asphalt (HMA) to Version 6, 10/11/2017</p>
<p>AASHTO R 47 Testing Size</p>
<p>AASHTO T 329 Standard Method of Test for Moisture Content of Hot Mix Asphalt (HMA) by Oven Method</p>
<b><u>HMA II</u></b>
<p>AASHTO T 209 Standard Method of Test for Theoretical Maximum Specific Gravity (Gmm) and Density of Hot Mix Asphalt (HMA) (Method A)</p>
<p>AASHTO T 166 Standard Method of Test for Bulk Specific Gravity (Gmb) of Compacted Hot Mix Asphalt (HMA) Using Saturated Surface-Dry<br />Specimens</p>
<p>AASHTO T 275 Standard Method of Test for Bulk Specific Gravity (Gmb) of Compacted Hot Mix Asphalt (HMA) Using Paraffin-Coated Specimens<br />AASHTO T 269 Standard Method of Test for Percent Air Voids in Compacted Dense and Open Asphalt Mixtures</p>
<p>AASHTO T 308 Standard Method of Test for Determining the Asphalt Binder Content of Hot Mix Asphalt (HMA) by the Ignition Method (Method A) </p>
<b><u>Soil and Aggregate</u></b>
<p>CT 105 Calculations Pertaining to Gradings and Specific Gravities</p>
<p>CT 125 Method of Test for Sampling Highway Materials and Products Used in the Roadway Structural Sections (Soil and Aggregate) CT 201 Method of Test for Soil and Aggregate Sample Preparation<br />CT 202 Method of Test for Sieve Analysis of Fine and Coarse Aggregates</p>
<p>CT 205 Method of Test for Determining Percentage of Crushed Particles</p>
<p>CT 216 Method of Test for Relative Compaction of Untreated and Treated Soils and Aggregates</p>
<p>CT 217 Method of Test for Sand Equivalent CT 226 Method of Test for Determination of Moisture Content of Soil and Aggregates by Oven Drying</p>
<p>CT 227 Method of Test for Evaluating Cleanness of Coarse Aggregate CT 229 Method of Test for Durability Index</p>
</div>
<hr>
<h5 data-toggle="collapse" data-target="#apply"><a href="javascript:void();">HOW TO APPLY:</a></h5>
<div id="apply" class="collapse">
  <p>Open until filled (or canceled). Responses reviewed beginning November 1, 2017.</p>
  <p>To be considered, scan all the required documents into one pdf file (the file name should be “Applicant Last name - Applicant First Name”), and submit it via email (with the subject: JTCP instructor) to:</p>
    <br>
    <p>Shadi Saadeh, PhD, PE, M. ASCE</p>
    <p>Director, National Center for TrAnspoRtation, GrEen Technologies and Education (TARGETE)</p>
    <p>Program Manager, Joint Training and Certification Program (JTCP)</p>
    <p>E-mail:&nbsp;Shadi.Saadeh@csulb.edu</p>
</div>
<hr>
<!-- End Job1 -->
</div>
<br><br>
National Center for Transportation, Green Technology and Education (TransGET) at California State University, Long
Beach is committed to building a more diverse faculty, Instructor, staff, and student body as it responds to the changing
population and educational needs of California and the nation. We seek nominations from those who have experience
teaching, mentoring, and developing research in ways that effectively address individuals from historically
underrepresented backgrounds.
<br><br>
CSULB is committed to creating a community in which a diverse population can learn, live, and work in an atmosphere of
tolerance, civility and respect for the rights and sensibilities of each individual, without regard to race, color, national origin,
ancestry, religious creed, sex, gender identification, sexual orientation, marital status, disability, medical condition, age,
political affiliation, Vietnam era veteran status, or any other veteran&#39;s status.

</div>
</section>
</div>


    <!------------------------------ body completed -------------------------------------->

	 <!------------------------------ Footer --------------------------------------------->
<?php
    include "footer.php";
    ?>

	<!----------------------------- Footer Completed -------------------------------------->


    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
