<?php
    include 'template1.php';
?>

<title>Waitlist</title>
<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Waitlist</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php
if(!isset($_GET['cid'])){
  echo "Please visit <a href='grade.php'>Grade</a> Page and click of View Waitlist.";
}
else{
  $cid = $_GET['cid'];
  $ret = getClass($cid);
  if($ret==null){
    echo "Invalid Class Id";
  }
  else{
    ?>
    <h4>Class: <?php echo $ret['Class_Name'] . " / " . $ret['Course_Name']; ?></h4>
    <?php
    $ret2 = getWaitlistByClassID($cid);
    ?>
    <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Internal Employee ID</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>Email Address</th>
      </tr>
    </thead>
    <tbody>
      <?php
        for($i=0;$i<count($ret2);$i++)
        {
      ?>
      <tr>
        <td><?php echo $i+1; ?></td>
        <td><?php echo $ret2[$i]['Employee_ID']; ?></td>
        <td><?php echo $ret2[$i]['Employee_FirstName']; ?></td>
        <td><?php echo $ret2[$i]['Employee_LastName']; ?></td>
        <td><?php echo $ret2[$i]['Employee_EmailAddress']; ?></td>
      </tr>
      <?php
        }
      ?>
    </tbody>
  </table>

    <?php

  }
}

?>



<?php
    include 'template2.php';
?>
