<?php
    include 'template1.php';
    
    if($_POST)
    {
        $data['Feedback_ID'] = $_POST['Feedback_ID'];
        $data['Status'] = $_POST['Status'];
        
        $ret = changeFeedbackStatus($data);
        echo "<script>alert('" . $ret['Message'] . "');</script>";
    }

    
?>

<title>Feedbacks</title>
<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Feedbacks</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
Feedback List
</div>
<!-- /.panel-heading -->
<div class="panel-body" id="">
<!-- /.table-responsive -->







<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<form method="POST">
<tr>
<th>No</th>
<th>Name</th>
<th>Email</th>
<th>Web Page</th>
<th>Type</th>
<th>Comment</th>
<th>Time</th>
<th>Status</th>
<th>Action</th>

</tr>
</thead>
<tbody>
<?php
    $ret=getFeedback();
    for($i=0;$i<count($ret);$i++)
    {
        ?>
<tr class="odd gradeX">
<td>
<?php
    echo $i+1;
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Feedback_Name'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Feedback_Email'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Feedback_Page'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Feedback_Type'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Feedback_Comment'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Feedback_TimeStamp'];
    ?>
</td>
<td>
<select name="Status">
<?php
    if($ret[$i]['Feedback_Status']==0)
    {
        echo "<option value='0' selected class='feedbackstatus'>Unsolved</option>";
        echo "<option value='1' class='feedbackstatus'>Solved</option>";
    }
    else
    {
        echo "<option value='0' class='feedbackstatus'>Unsolved</option>";
        echo "<option value='1' selected class='feedbackstatus'>Solved</option>";
    }
    echo "<input type='text' name='Feedback_ID' value='" . $ret[$i]['Feedback_ID'] . "' hidden >";
    ?>
</select>
</td>
<td>
<?php
    echo "<input type='submit' value='change'>";
    ?>
</td>
</tr>
</form>

<?php
    }
    ?>
</tbody>
</table>







</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->


<?php
    include 'template2.php';
?>
