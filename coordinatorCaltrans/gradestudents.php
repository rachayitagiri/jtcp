<?php

    if($_POST['cid']==null)
    {
        header("Location: dashboard.php");
    }
    include 'template1.php';
    $cid = $_POST['cid'];
    $ret = searchEmployeesByClass($cid);
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<title>Grade/Drop Students</title>


<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Grade/Drop Students</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<a href="downloadRoaster.php?cid=<?php echo $cid; ?>" target="_blank"><input type="button" value="Download Roaster" style="margin-bottom: 10px;" /></a>
<div class="row" id="printableArea">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
Class : <?php echo $ret[0]['Class_Name']; ?>
</div>
<!-- /.panel-heading -->
<div class="panel-body">





<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Name</th>
<th>Email</th>
<th>Mobile</th>
<th>Company</th>
<th>Practical Grade</th>
<th>Written Grade</th>
<th>Fees Paid</th>
</tr>
</thead>
<tbody>
<?php
    for($i=0;$i<count($ret);$i++)
    {
        $name_results[] = $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
        $eid_results[] = $ret[$i]['Employee_ID'];
        $lid_results[] = $ret[$i]['Login_ID'];
        ?>
<input type="text" value="<?php echo $ret[$i]['Certificate_Code']; ?>" id="<?php echo 'cc_'.$i; ?>" hidden >

<input type="text" value="<?php echo $ret[$i]['Prerequisite_Certificate_Code']; ?>" id="<?php echo 'cc2_'.$i; ?>" hidden>



<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
    ?>
</td>

<td>
<?php
    echo $ret[$i]['Login_Email'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Employee_Mobile'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Company_Other'];
    ?>
</td>
<td>
<?php
    echo "<select class='practical_edit'>";

//    if($ret[$i]['Status_ID']==2)
//    {
//        echo "<select class='practical_edit'>";
//    }
//    else
//    {
//        echo "<select class='practical_edit' disabled>";
//    }
    if($ret[$i]['Course_Certificate_Practical']==0)
    {
        ?>
        <option value="<?php echo $i.'1'; ?>" >Passed</option>
        <option value="<?php echo $i.'0'; ?>" selected>Failed</option>
<?php
    }
    else
    {
        ?>
        <option value="<?php echo $i.'1'; ?>" selected>Passed</option>
        <option value="<?php echo $i.'0'; ?>" >Failed</option>
<?php
    }
    ?>
</select>
</td>
<td>
<?php
    echo "<select class='written_edit'>";

//    if($ret[$i]['Status_ID']==2)
//    {
//    }
//    else
//    {
//        echo "<select class='written_edit' disabled>";
//    }
    if($ret[$i]['Course_Certificate_Written']==0)
    {
        ?>
<option value="<?php echo $i.'1'; ?>" >Passed</option>
<option value="<?php echo $i.'0'; ?>" selected>Failed</option>
<?php
    }
    else
    {
        ?>
<option value="<?php echo $i.'1'; ?>" selected>Passed</option>
<option value="<?php echo $i.'0'; ?>">Failed</option>
<?php
    }
    ?>
</select>
</td>
<td>
  <?php
  echo $ret[$i]['Fees_Paid'];
  ?>
</td>
</tr>
<?php
    }
    ?>
</tbody>
</table>




<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->




        <!-- /#page-wrapper -->


<?php
    include 'template2.php';
?>
