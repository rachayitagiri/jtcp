<?php
    include 'template1.php';

    if($_POST)
    {
        if($_POST['changepassword']!=NULL)
        {
            $oldpassword = $_POST['oldpassword'];
            $newpassword = $_POST['newpassword'];
            $ret2 = updatePassword($ret['Login_Email'],$oldpassword, $newpassword);
            echo "<script>alert('" . $ret2['Message'] . "')</script>";
        }
        $ret = getCoordinatorDetails($_SESSION['Login_ID']);
        print_r($ret);
    }

    ?>

<!-- /#page-wrapper -->
<!-- Page Content -->
<title>Settings</title>
<script type="text/javascript">
window.onload = function () {
    document.getElementById("oldpassword").onchange = validatePassword;
    document.getElementById("newpassword").onchange = validatePassword;
}
function validatePassword(){
    var pass2=document.getElementById("newpassword").value;
    var pass1=document.getElementById("oldpassword").value;
    if(pass2.length<5)
    {
        document.getElementById("newpassword").setCustomValidity("Passwords Must be at least 5 characters long.");
    }
    else
    {
        document.getElementById("oldpassword").setCustomValidity('');
        document.getElementById("newpassword").setCustomValidity('');
    }
    //empty string means no validation error
}
</script>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Settings</h1>
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Details
                     (Coodinator_ID: <?php echo $ret['Coordinator_ID'];; ?>)
                </div>

                <div class="form-group col-lg-6">
                    <label>First Name</label>
                    <input type="" name="" class="form-control" id="" value="<?php echo $ret['Coordinator_FirstName']; ?>" disabled>
                </div>

                <div class="form-group col-lg-6">
                    <label>Last Name</label>
                    <input type="" name="" class="form-control" id="" value="<?php echo $ret['Coordinator_LastName']; ?>" disabled>
                </div>

                <div class="form-group col-lg-12">
                    <label>Email Address</label>
                    <input type="" name="" class="form-control" id="" value="<?php echo $ret['Login_Email']; ?>" disabled>
                </div>

            </div>

        </div>
    <!-- /.panel -->
    </div>

    <!-- /.row -->
    <div class="row">
    <form id="passwordchangeform" role="form" method="POST">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Change Password
                </div>

                <div class="form-group col-lg-6">
                    <label>Old Password</label>
                    <input type="password" name="oldpassword" class="form-control" id="oldpassword" value="">
                </div>

                <div class="form-group col-lg-6">
                    <label>New Password</label>
                    <input type="password" name="newpassword" class="form-control" id="newpassword" value="">
                </div>

                <div class="col-sm-12">
                    <input type="submit" class="btn btn-primary" id="changepassword" name="changepassword" value="Change">
                </div>

        </div>
        </form>
    <!-- /.panel -->
    </div>

</div>
<!-- /.row -->


<?php
    include 'template2.php';
    ?>
