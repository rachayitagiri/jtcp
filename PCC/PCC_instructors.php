<?php
    include '../dbmanage.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<title>CSULB | Caltrans - Joint Training & Certification Program</title>

<!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">

	// check for window is loaded or not
	window.onload = function(){
	var myNav = document.getElementById('mymainnav');
	$(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
			$(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
		} else {
          $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
        }
      });
	}
</script>


</head>


<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="../img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

	<!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
			<!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
			<!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('../img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../img/bg.jpg');"></div>

            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
	<!----------------------- slider image completed ---------------------------------------->

	<!----------------------- navigation in bottom of image slider -------------------------->
	<nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav">
                    <li>
                        <a href="../index.php">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="../index.php#courses" style="color: rgb(177,92,34);">COURSES</a>
                    </li>
                    <li>
                        <a href="../search.php">SEARCH</a>
                    </li>
                    <li>
                        <a href="../help.php">FAQ</a>
                    </li>
                    <li>
                        <a href="../contact.php">CONTACT</a>
                    </li>
                    <li>
                        <a href="../feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                      <a href="../jobs.php">JOB OPENINGS</a>
                    </li>
					          <li>
                        <a href="../login.php">LOGIN</a>
                    </li>
                    <li>
                        <a href="../signup.php">ENROLL</a>
                    </li>
                    <li>
                        <a href="../acknowledgment.php">ACKNOWLEDGMENT</a>
                    </li>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!----------------------------------- navigation completed ---------------------------------->

	<!----------------------------------- body started ------------------------------------------>

<div class="container">
<h5><a href="http://www.csulb.edu/college-of-engineering" style="color: rgb(177,92,34);" target="_blank" >College of Engineering</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="../index.php" style="color: rgb(177,92,34);">JTCP</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="../index.php#courses" style="color: rgb(177,92,34);">Courses</a> <span class="glyphicon glyphicon-chevron-right"></span> Portland Cement Concrete</h5>
<div class="col-md-2">
<h2>Menu</h2>
<ul style="list-style: none; font-size: 16px; margin: 0; padding: 0;">
<li style="margin-bottom: 7px;" ><a href="PCC_overview.php" style="color: rgb(76,76,76);" >Overview</a></li>
<li style="margin-bottom: 7px;" ><a href="PCC_schedule_fees.php" style="color: rgb(76,76,76);" >Schedule, Fees, and Registration</a></li>
<li style="margin-bottom: 7px;" ><a href="#" >Instructors</a></li>
</ul>
<hr>
</div>
<div class="col-md-10">
<!-- Data Started -->
<h2>Portland Cement Concrete</h2>
<h3>Instructors</h3>

<?php

    $ret = getInstructorsByCourse(5);
    if(count($ret)>0)
    {
        for($i=0;$i<count($ret);$i++)
        {
            echo "<h4>". $ret[$i]['Instructor_FirstName'] . " " . $ret[$i]['Instructor_LastName'] . "</h4>";
        }
    }
    else
    {
        echo "Come back soon.";
    }
    ?>

<!-- Data End -->
</div>

</div>


    <!------------------------------ body completed -------------------------------------->

	 <!------------------------------ Footer --------------------------------------------->
<?php
    include "../footer.php";
    ?>
	<!----------------------------- Footer Completed -------------------------------------->


    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
