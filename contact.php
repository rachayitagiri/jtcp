

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<title>CSULB | Caltrans - Joint Training & Certification Program</title>

<!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">
	// check for window is loaded or not
	window.onload = function(){
	var myNav = document.getElementById('mymainnav');
	$(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
			$(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
		} else {
          $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
        }
      });
	}
</script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
</head>


<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

	<!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
			<!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
			<!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg.jpg');"></div>

            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
	<!----------------------- slider image completed ---------------------------------------->

	<!----------------------- navigation in bottom of image slider -------------------------->
	<nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav" style="width: 110%;">
                    <li>
                        <a href="index.php">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="index.php#courses">COURSES</a>
                    </li>
                    <li>
                        <a href="search.php" >SEARCH</a>
                    </li>
                    <li>
                        <a href="help.php" >FAQ</a>
                    </li>
                    <li>
                        <a href="#" style="color: rgb(177,92,34);" >CONTACT</a>
                    </li>
                    <li>
                        <a href="feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                      <a href="jobs.php">JOB OPENINGS</a>
                    </li>
                    <li>
                      <a href="gallery.php">GALLERY</a>
                    </li>
					<li>
                        <a href="login.php">LOGIN</a>
                    </li>
                    <li>
                        <a href="signup.php">ENROLL</a>
                    </li>
                    <li>
                        <a href="acknowledgment.php">ACKNOWLEDGMENT</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!----------------------------------- navigation completed ---------------------------------->

	<!----------------------------------- body started ------------------------------------------>

<div class="container">
<h5><a href="http://www.csulb.edu/college-of-engineering" style="color: rgb(177,92,34);" target="_blank" >College of Engineering</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="index.php" style="color: rgb(177,92,34);">JTCP</a> <span class="glyphicon glyphicon-chevron-right"></span> Contact Us</h5>
<h2>Contact JTCP</h2>
<br>
<div class="row-fluid">
<div class="col-md-6">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3316.1420198803175!2d-118.11247526362052!3d33.78283052919873!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dd2e27df5ee969%3A0xe15700067a535731!2sVivian+Engineering+Center%2C+Long+Beach%2C+CA+90815!5e0!3m2!1sen!2sus!4v1484886040062" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<div class="col-md-6">

      		<address>
            <br>
      			<strong>JTCP - California State University, Long Beach</strong><br><br>
      			Vivian Engineering Center<br>
      			Long Beach<br>
      			CA - 90815<br><br>
            Program Coordinator: Michelle Wang<br>
      			<abbr title="Phone">Phone: </abbr> 562-985-1811 <br>
            <abbr title="Email">Email: </abbr> jtcp@csulb.edu
      		</address>
<!--
<form class="form-horizontal" action="mailto:csulbjtcp@gmail.com" method="post" enctype="text/plain" >
<fieldset>
<legend class=""></legend>

<div class="form-group">
<label class="col-md-3 control-label" for="name">Name</label>
<div class="col-md-9">
<input id="name" name="name" type="text" placeholder="Name" class="form-control">
</div>
</div>

<div class="form-group">
<label class="col-md-3 control-label" for="email">Email</label>
<div class="col-md-9">
<input id="email" name="email" type="text" placeholder="Email" class="form-control">
</div>
</div>

<div class="form-group">
<label class="col-md-3 control-label" for="message">Message</label>
<div class="col-md-9">
<textarea class="form-control" id="message" name="message" placeholder="Message" rows="5"></textarea>
</div>
</div>

<div class="form-group">
<div class="col-md-12 text-right">
<button type="submit" class="btn btn-primary btn-lg">Submit</button>
</div>
</div>
</fieldset>
</form>
-->

</div>
</div>
</div>


    <!------------------------------ body completed -------------------------------------->

	 <!------------------------------ Footer --------------------------------------------->

<?php
        include "footer.php";
?>
<!----------------------------- Footer Completed -------------------------------------->


    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
