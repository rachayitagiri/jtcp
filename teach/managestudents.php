<?php
    
    if($_POST['cid']==null)
    {
        header("Location: dashboard.php");
    }
    $cid = $_POST['cid'];
    include 'template1.php';
    $ret = searchEmployeesByClass($cid);
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

$(document).ready(function(){
                  $(".written_edit").on('change', function() {
                                        var cc = document.getElementById('cc'+this.value.charAt(0)).value;
                                        var written = this.value.charAt(1);
                                        $.ajax({
                                               url: "changeCertificateScoreWritten.php",
                                               type: "post",
                                               data: {'cc': cc, 'written': written},
                                               success: function (response) {
                                               alert(response);
                                               },
                                               error: function(jqXHR, textStatus, errorThrown) {
                                               alert(errorThrown);
                                               console.log(textStatus, errorThrown);
                                               }
                                               });
                                        });
                  $(".practical_edit").on('change', function() {
                                          var cc = document.getElementById('cc'+this.value.charAt(0)).value;
                                          var practical = this.value.charAt(1);
                                          $.ajax({
                                                 url: "changeCertificateScorePractical.php",
                                                 type: "post",
                                                 data: {'cc': cc, 'practical': practical},
                                                 success: function (response) {
                                                 alert(response);
                                                 },
                                                 error: function(jqXHR, textStatus, errorThrown) {
                                                 alert(errorThrown);
                                                 console.log(textStatus, errorThrown);
                                                 }
                                                 });
                                          });
                  $(".certify").click(function(){
                                      var cc = document.getElementById('cc'+this.id).value;
                                      var cc2 = document.getElementById('cc2'+this.id).value;
                                      $.ajax({
                                             url: "releaseCertificate.php",
                                             type: "post",
                                             data: {'cc' : cc, 'cc2' : cc2},
                                             success: function (response) {
                                             alert(response);
                                             },
                                             error: function(jqXHR, textStatus, errorThrown) {
                                             alert(errorThrown);
                                             console.log(textStatus, errorThrown);
                                             }
                                             });
                                          });
                  });
</script>

<title>Manage Students</title>


<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Manage Your Class</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
Class : <?php echo $ret[0]['Class_Name']; ?>
</div>
<!-- /.panel-heading -->
<div class="panel-body">





<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Name</th>
<th>Email</th>
<th>Mobile</th>
<th>Address</th>
<th>Practical Grade</th>
<th>Written Grade</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php
    for($i=0;$i<count($ret);$i++)
    {
        $name_results[] = $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
        $eid_results[] = $ret[$i]['Employee_ID'];
        $lid_results[] = $ret[$i]['Login_ID'];
        ?>
<input type="text" value="<?php echo $ret[$i]['Certificate_Code']; ?>" id="<?php echo 'cc'.$i; ?>" hidden>

<input type="text" value="<?php echo $ret[$i]['Prerequisite_Certificate_Code']; ?>" id="<?php echo 'cc2'.$i; ?>" hidden>



<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
    ?>
</td>

<td>
<?php
    echo $ret[$i]['Login_Email'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Employee_Mobile'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Employee_Address'];
    ?>
</td>
<td>
<?php
    if($ret[$i]['Status_ID']==2)
    {
        echo "<select class='practical_edit'>";
    }
    else
    {
        echo "<select class='practical_edit' disabled>";
    }
    if($ret[$i]['Course_Certificate_Practical']==0)
    {
        ?>
        <option value="<?php echo $i.'1'; ?>" >Passed</option>
        <option value="<?php echo $i.'1'; ?>" selected>Failed</option>
<?php
    }
    else
    {
        ?>
        <option value="<?php echo $i.'1'; ?>" selected>Passed</option>
        <option value="<?php echo $i.'0'; ?>" >Failed</option>
<?php
    }
    ?>
</select>
</td>
<td>
<?php
    if($ret[$i]['Status_ID']==2)
    {
        echo "<select class='written_edit'>";
    }
    else
    {
        echo "<select class='written_edit' disabled>";
    }
    if($ret[$i]['Course_Certificate_Written']==0)
    {
        ?>
<option value="<?php echo $i.'1'; ?>" >Passed</option>
<option value="<?php echo $i.'0'; ?>" selected>Failed</option>
<?php
    }
    else
    {
        ?>
<option value="<?php echo $i.'1'; ?>" selected>Passed</option>
<option value="<?php echo $i.'0'; ?>">Failed</option>
<?php
    }
    ?>
</select>
</td>
<td>
<?php
    if($ret[$i]['Status_ID']==2)
    {
        echo "<a href='#' class='certify' id='$i'>Released Certificate</a>";
    }
    else
    {
        echo "<a href='#' class='' id='$i'>Completed</a>";
    }
    
    ?>
</td>
</tr>
<?php
    }
    ?>
</tbody>
</table>




<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->




        <!-- /#page-wrapper -->


<?php
    include 'template2.php';
?>
