<?php
    include 'template1.php';

    $formdata = array();
    $cooid = $_GET['cooid'];
    $ret = getActivitiesCoordinator($cooid);
?>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<br><br><br><br>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Message</th>
<th>Time</th>
</tr>
</thead>
<tbody>
  <?php

    if(count($ret)==0)
    {
      echo "<tr><td>No Activities</td><td></td></tr>";
    }
    else {
      for($i=0;$i<count($ret);$i++)
      {
        echo "<tr>";
        echo "<td>" . $ret[$i]['Message'] . "</td>";
        echo "<td>" . $ret[$i]['Date'] ." </td>";
        echo "</tr>";
      }
    }
  ?>
</tbody>
</table>
