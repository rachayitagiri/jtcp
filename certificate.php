<link href="css/bootstrap.min.css" rel="stylesheet">

<?php
    include 'dbmanage.php';
    $code = $_GET['code'];
    if($_POST){
      $ret5=getCourseEvaluationQuestion();
      $cid = $_POST['Class_ID'];
      $eid = $_POST['Employee_ID'];
      for($i=0;$i<count($ret5);$i++){
          $question_id = $ret5[$i]['Course_Evaluation_Question_ID'];
          setCourseEvaluationAnswerByClassIdEmployeeId($_POST[$question_id],$question_id,$cid,$eid);
      }
    }
    $ret = getCertificate($code);
    if($ret==null)
    {
        echo "<script>alert('Incorrect Certificate Code');</script>";
    }
    else
    {
      //Check if done with Course Evaluation Feedback or if it is not a final certificate(prereq or safety)
      $classid = $ret['Class_ID'];
      $employeeid = $ret['Employee_ID'];
      $certificate_code = $ret['Code'];
      $ret2 = checkCourseEvaluationAnswerByClassIdEmployeeId($classid, $employeeid);
      if($ret2==0 && $ret['Final_Status']==1){
        //Feedback not Submitted
        //Check if logged in
        if($_SESSION['Login_ID']!=null){
          $ret3 = getEmployeeDetail($_SESSION['Login_ID']);
          if($ret3['Employee_ID']==$employeeid){
            //Show Course Evaluation Feedback
            $ret4=getCourseEvaluationQuestion();
            ?>
            <div class="container">
            <div class="col-md-12">
                <div class="form-area" style="background-color: #FAFAFA;padding: 10px 40px 60px;margin: 10px 0px 60px;border: 1px solid GREY;">
                    <form role="form" method="POST">
                    <input type='text' value='<?php echo $employeeid; ?>' name='Class_ID' hidden>
                    <input type='text' value='<?php echo $classid; ?>' name='Employee_ID' hidden>
                    <br style="clear:both">
                        <h4 style="margin-bottom: 25px; text-align: center;"><u>Please fill up following Course Evaluation Form to Access Certificate: <?php echo $ret['Course_Name'] ?></u></h4>
                        <?php
                          for($i=0;$i<count($ret4);$i++){
                            if($ret4[$i]['Course_Evaluation_Question_Multichoice']==1)
                            {
                        ?>
                         <div class="form-group">
                                <label class=""><?php echo $ret4[$i]['Course_Evaluation_Question_Question'] ?></label>
                                <br>
                                <label class="radio-inline"><input type="radio" name="<?php echo $ret4[$i]['Course_Evaluation_Question_ID']; ?>" value="Strongly Disagree" required>Strongly Disagree</label>
                                <label class="radio-inline"><input type="radio" name="<?php echo $ret4[$i]['Course_Evaluation_Question_ID']; ?>" value="Moderately Disagree" >Moderately Disagree</label>
                                <label class="radio-inline"><input type="radio" name="<?php echo $ret4[$i]['Course_Evaluation_Question_ID']; ?>" value="Neutral" >Neutral</label>
                                <label class="radio-inline"><input type="radio" name="<?php echo $ret4[$i]['Course_Evaluation_Question_ID']; ?>" value="Moderately Disagree" value="Strongly Disagree">Moderately Agree</label>
                                <label class="radio-inline"><input type="radio" name="<?php echo $ret4[$i]['Course_Evaluation_Question_ID']; ?>" value="Strongly Agree" >Strongly Agree</label>
                        </div>
                        <?php
                            }
                            if($ret4[$i]['Course_Evaluation_Question_Multichoice']==0)
                            {
                        ?>
                         <div class="form-group">
                                <label class=""><?php echo $ret4[$i]['Course_Evaluation_Question_Question'] ?></label>
                                <textarea class="form-control" type="textarea" placeholder="Message" maxlength="140" rows="4" name="<?php echo $ret4[$i]['Course_Evaluation_Question_ID']; ?>" required></textarea>
                        </div>
                        <?php
                            }
                        ?>
                        <?php
                          }
                        ?>
                    <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Print Certificate</button>
                    </form>
                </div>
            </div>
            </div>

            <?php
          }
          else{
          ?>
            <div class="alert alert-info">
              <strong>Info: </strong> Certificate is not available as Student has not Completed Course Evaluation Form!
            </div>
            <div class="alert alert-danger">
              <strong>Warning: </strong> This certificate is not associated with your account. <a href='logout.php'>Logout</a> and Login again with Account Associated with this Certificate.
            </div>
          <?php
          }
        }
        else{
          ?>
          <div class="alert alert-info">
            <strong>Info: </strong>Certificate is not available as Student has not Completed Course Evaluation Form!
          </div>
          <div class="alert alert-warning">
            <strong>Note: </strong>If you are Certificate Owner, Plesae <a href='login.php?redirect=certificate.php?code=<?php echo $certificate_code; ?>'>Login</a> and fill out the form!
          </div>
          <?php
        }
      }
      else{
        //Feedback is Submitted, Show Certificate
?>
<html>
<head>
<style>
    #mydiv
    {
        background-image: url("img/certi_bg_900x600.png") !important;
        background-size:cover;
        width:900px;
        height:600px;
        text-align:center;
    }
    @media print
    {
        * {
          -webkit-print-color-adjust:exact;
        }
    }
</style>
<script>
window.print();
</script>
</head>
<body>
<div id="mydiv">

<br><br><br><br>
<br>
<center>
<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td width="" valign="top">
<p align="center">
<img src="img/csulb.png" height="50px" width="60px">
</p>
</td>
<td width="500px" valign="top">
<p align="center">
<strong>California State University, Long Beach</strong>
<br>
<strong>College of Engineering</strong>
<br>
<strong>
Caltrans &#8211; Industry Joint Training and
Certification Program
</strong>
</p>
</td>
<td width="" valign="top">
<p align="center">
<img src="img/logo.png" height="50px" width="70px">
</p>
</td>
</tr>
</tbody>
</table>
</center>
<br>
<span style="font-size:40px; font-weight:bold">Certificate of Completion</span>
<br>
<span style="font-size:25px"><i>This is to certify that</i></span>
<br>
<span style="font-size:30px"><b><?php echo $ret['Employee_Name']; ?></b></span><br/>
<span style="font-size:25px"><i>has
<?php
    if($ret['Course_Status']==3 || $ret['Final_Status']==0)
    {
        echo "completed";
    }
    else
    {
        echo "completed";
        //show completed because person has completed the course even he failed.
        //echo "failed";
    }
    ?> the
<?php
    $temp_stat = "";
    if($ret['Final_Status']==1)
    {
        $temp_stat = "training course for";
    }
    else
    {
        $temp_stat = "prerequisite exam";
    }
    echo $temp_stat;
    ?>

</i></span> <br/>
<span style="font-size:30px">
<?php
    echo $ret['Course_Name'];
    ?>
</span> <br/>
<span style="font-size:20px">
<?php
    // if($ret['Course_Status']==3 || $ret['Final_Status']==0)
    // {
    //     echo "with score of <b> " . $ret['Grade'];
    // }
    ?>


</b></span>
<span style="font-size:20px"><i>Dated: </i></span>
<span style="font-size:25px"><?php echo $ret['Issue_Date']; ?></span> <br>
<span style="font-size:20px"><i>Valid until: </i></span>
<span style="font-size:25px"><?php echo $ret['Certificate_ExpireDate']; ?></span><br><br>

<span style="font-size:18px">Certification Code: <?php echo $ret['Code']; ?></span>


</div>

</body>
</html>
<?php
  }
}
?>
