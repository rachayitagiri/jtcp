<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CSULB | Caltrans - Joint Training & Certification Program</title>

    <!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">

	// check for window is loaded or not
	window.onload = function(){
	var myNav = document.getElementById('mymainnav');
	$(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
			$(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
		} else {
          $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
        }
      });
	}
</script>

<script>
function scrollfunction() {
    $('html, body').animate({
                            scrollTop: $("#courses").offset().top
                            }, 700);
}
</script>


</head>


<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

	<!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
			<!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
			<!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg.jpg');"></div>

            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
	<!----------------------- slider image completed ---------------------------------------->

	<!----------------------- navigation in bottom of image slider -------------------------->
	<nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav" style="width: 110%;">
                    <li>
                        <a href="#" style="color: rgb(177,92,34); ">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="#courses" onclick="scrollfunction()" >COURSES</a>
                    </li>
                    <li>
                        <a href="search.php">SEARCH</a>
                    </li>
                    <li>
                        <a href="help.php">FAQ</a>
                    </li>
                    <li>
                        <a href="contact.php">CONTACT</a>
                    </li>
                    <li>
                        <a href="feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                      <a href="jobs.php">JOB OPENINGS</a>
                    </li>
                    <li>
                      <a href="gallery.php">GALLERY</a>
                    </li>
					<li>
                        <a href="login.php">LOGIN</a>
                    </li>
                    <li>
                        <a href="signup.php">ENROLL</a>
                    </li>
                    <li>
                        <a href="acknowledgment.php">ACKNOWLEDGMENT</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!----------------------------------- navigation completed ---------------------------------->

	<!----------------------------------- body started ------------------------------------------>
    <div class="col-lg-12">
        <h1 class="custom_header">
            JOINT TRAINING & CERTIFICATION PROGRAM
        </h1>
        <hr>

    </div>
    <br>

    <!-- Page Content About -->
    <div class="container" id="about">

        <!-- Marketing Icons Section -->
        <div class="row">

            <div class="col-lg-2 custom_header2"  style="color: rgb(177,92,34);" >
                About
            </div>
            <div class="col-lg-10" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">

                <p class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >The Joint Training and Certification Program (JTCP) was established by Caltrans Materials Engineering and Testing Services to achieve consistent training and certification for all technicians involved in the transportation construction industry throughout California. The purpose of the JTCP is to improve the quality of the pavement construction practices that Caltrans and other agencies obtain, and to help reduce conflict and delay on construction projects within the state. The JTCP was developed in partnership with CSULB’s College of Engineering.
                </p>
                <p class="text-large text-thin"  style="font-size: 0.9em; line-height: 170%;" >
                Caltrans and contractor personnel attend the same classes, work together, and receive equivalent training. This practice fosters respect within the industry, while generating increased consistency. The JTCP also enables individuals and agencies to verify progress and certifications held by other individuals.</p>
                <p class="text-large text-thin"  style="font-size: 0.9em; line-height: 170%;" >This is the OFFICIAL program to obtain the necessary certifications for work requiring Caltrans testing certifications (both industry and agency).</p>

                <p class="text-large text-thin"  style="font-size: 0.9em; line-height: 170%;" ><a href='http://www.dot.ca.gov/hq/esc/Translab/ormt/IA_reports/JTCP/index.htm' target='_blank'>Click here</a> to visit Caltrans JTCP website.</p>

            </div>

        </div>
        <!-- /.row -->


        <hr>

    </div>
    <!-- /.container -->

    <!-- Page Content Courses -->
    <div class="container" id="courses">

        <!-- Marketing Icons Section -->
        <div class="row">
                    <div class="col-lg-2 custom_header2"  style="color: rgb(177,92,34);" >
                    Courses
                    </div>
                    <div class="col-lg-10" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    Training and certification are offered in:
                    <hr>
                    <h4 data-toggle="collapse" data-target="#hma1"><a href="HMA1/HMA1_overview.php" style="color: rgba(51,51,51,0.8);" >Hot Mix Asphalt I</a></h4>

                    <div id="hma1" class="collapse">
                        <p style="font-size: 0.8em;" >The Joint Training and Certification Program (JTCP) has developed a two-part series certification program in Hot Mix Asphalt (HMA) for construction materials lab technicians. To help reduce conflict and delay on construction projects within the state, Caltrans, in partnership with CSULB, has developed a series of joint training and certification programs.</p>
                    </div>
                    <hr>
                    <h4 data-toggle="collapse" data-target="#hma2"><a href="HMA2/HMA2_overview.php" style="color: rgba(51,51,51,0.8);" >Hot Mix Asphalt II</a></h4>

                    <div id="hma2" class="collapse">
                        <p style="font-size: 0.8em;" >The Joint Training and Certification Program (JTCP) has developed a two-part series certification program in Hot Mix Asphalt (HMA) for construction materials lab technicians. To help reduce conflict and delay on construction projects within the state, Caltrans, in partnership with CSULB, has developed a series of joint training and certification programs.</p>
                    </div>
                    <hr>
                    <h4 data-toggle="collapse" data-target="#sa"><a href="SA/SA_overview.php" style="color: rgba(51,51,51,0.8);" >Soils & Aggregates</a></h4>

                    <div id="sa" class="collapse">
                        <p style="font-size: 0.8em;" >The Joint Training and Certification Program (JTCP) has developed a certification program in Soils and Aggregates testing for construction materials lab technicians. To help reduce conflict and delay on construction projects within the state, Caltrans, in partnership with CSULB, has developed a series of joint training and certification programs.</p>
                    </div>
                    <hr>
                    <h4 data-toggle="collapse" data-target="#pcc"><a href="PCC/PCC_overview.php" style="color: rgba(51,51,51,0.8);" >Portland Cement Concrete</a></h4>

                    <div id="pcc" class="collapse">
                        <p style="font-size: 0.8em;" >The Joint Training and Certification Program (JTCP) has developed a certification program in Portland Cement Concrete testing for construction materials lab technicians. To help reduce conflict and delay on construction projects within the state, Caltrans, in partnership with CSULB, has developed a series of joint training and certification programs.</p>
                    </div>
                    <hr>

                    </div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container -->

    <!------------------------------ body completed -------------------------------------->

	 <!------------------------------ Footer --------------------------------------------->
<?php
    include "footer.php";
    ?>

	<!----------------------------- Footer Completed -------------------------------------->


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>

<?php
// include 'maintenance.php';
?>
