#!/opt/rh/php55/root/usr/bin/php

<?php

// require '../dbmanage.php';
require '/webtree/colleges/coe/jtcp/dbmanage.php';


logCronJob("dropstudent", "begin");

// $filepath = 'flag.txt';
$filepath = '/webtree/colleges/coe/jtcp/cronjobs/flag.txt';
//Change flag to false
$myfile = fopen($filepath, "w") or die("Unable to open file!");
$txt = "false";
fwrite($myfile, $txt);

//timeout increased to 1 hour
set_time_limit(3600);// seconds

$ret = checkPaypalData();
for($i=0;$i<count($ret);$i++){
  $temp_ret = $ret[$i];
  $eid = $temp_ret['Employee_ID'];
  $class_name = $temp_ret['Class_Name'];
  $class_id = $temp_ret['Class_ID'];
  $course_id = $temp_ret['Course_ID'];
  $timestamp = $temp_ret['Timestamp'];
  $cc = $temp_ret['Certificate_Code'];

  $employee_details = getEmployeeDetailsByEmployeeID($eid);
  $company_other = $employee_details['Company_Other'];
  //difference in hours
  $diff_hours = round((strtotime($today) - strtotime($timestamp))/3600, 1);

  if(strcasecmp($company_other,"caltrans")==0){
    //after 1 week approx if Caltrans student is not removed, fees_check warning will be removed from his/her account
    if($diff_hours>168){
        $ret2 = removeFromFeesCheck($class_id, $eid);
        if($ret2['Status']==1){
          logCronJob("dropstudent", "Success: Caltrans Student removed from Fees_Check with Employee_ID: " . $eid . " for Class: " . $class_name . " (Class ID: " . $class_id . ")");
        }
        else{
          logCronJob("dropstudent", "Fail: Caltrans Student removed from Fees_Check with Employee_ID: " . $eid . " for Class: " . $class_name . " (Class ID: " . $class_id . ")");
        }
    }
  }
  else{
    if($diff_hours>24){
      $ret2 = dropClass($cc, TRUE);
      if($ret2['Status']==1){
        $message = "Class: " . $class_name . " has been dropped from your account as fees were not paid in time. Sorry for the inconvenience!";
        addActivity($eid,$message,1);
        logCronJob("dropstudent", "Success: Student Dropped with Employee_ID: " . $eid . " for Class: " . $class_name . " (Class ID: " . $class_id . ")");
      }
      else{
        logCronJob("dropstudent", "Fail: Student was not dropped with Employee_ID: " . $eid . " for Class: " . $class_name . " (Class ID: " . $class_id . ")");
      }
    }
  }

  /*
  if($diff_hours>24 && strcasecmp($company_other,"caltrans")!=0){

    $ret2 = dropClass($cc, TRUE);
    if($ret2['Status']==1){
      $message = "Class: " . $class_name . " has been dropped from your account as fees were not paid in time. Sorry for the inconvenience!";
      addActivity($eid,$message,1);
      logCronJob("dropstudent", "Success: Student Dropped with Employee_ID: " . $eid . " for Class: " . $class_name . " (Class ID: " . $class_id . ")");
    }
    else{
      logCronJob("dropstudent", "Fail: Student was not dropped with Employee_ID: " . $eid . " for Class: " . $class_name . " (Class ID: " . $class_id . ")");
    }

    //Email Drop Class Activity for late fees payment

    $email_ret = getEmployeeDetailsByEmployeeID($eid);
    $email_ret2 = getClass($class_id);
    $course_name = $email_ret2['Course_Name'];
    $firstname = $email_ret['FirstName'];
    $lastname = $email_ret['LastName'];
    $email = $email_ret['Email'];
    $subject = "Class Dropped: " . $class_name;
    $message = "This is to inform you that your Class: " . $class_name . " for Course " . $course_name . " has been dropped as fees were not paid in time. Sorry for the inconvenience!";
    generalEmail($subject, $firstname, $lastname, $email, $message);


  }
  */
}
// phpinfo();


// Change flag back to true
$myfile = fopen($filepath, "w") or die("Unable to open file!");
$txt = "true";
fwrite($myfile, $txt);
fclose($myfile);

logCronJob("dropstudent", "end");

//CronJob Backup
logCronJob("databasebackup", "start");
require '/webtree/colleges/coe/jtcp/vendor/mysqldump-php-master/src/Ifsnop/Mysqldump/Mysqldump.php';
$backup_folderpath = '/webtree/colleges/coe/jtcp/dbbackups/';
$today2 = date("Y-m-dH:i");

$backup_filename = $today2 . '.sql';
$backup_finalpath = $backup_folderpath . $backup_filename;

$temp_host = "mysql:host=$DbHost;dbname=$DbName";

$dump = new Ifsnop\Mysqldump\Mysqldump($temp_host, $DbUser, $DbPassword);
$dump->start($backup_finalpath);
logCronJob("databasebackup", "end");


?>
