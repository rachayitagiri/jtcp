<?php
//  Include PHPExcel_IOFactory

include '../dbmanage.php';


if($_POST){

  include 'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

      $inputFileName = $_FILES['fileToUpload']['tmp_name'];

      $total_dates = 3;
      $classname_start_count = 18;
      $classname = "HMA2_C";
      $instructor_id = 2;
      $course_id = 3;
      $location_id = 2; //0 csulb 2 sjsu
      $coordinatorid = 2;


      //  Read your Excel workbook
      try {
          $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
          $objReader = PHPExcel_IOFactory::createReader($inputFileType);
          $objPHPExcel = $objReader->load($inputFileName);
      } catch(Exception $e) {
          die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
      }

      //  Get worksheet dimensions
      $sheet = $objPHPExcel->getSheet(0);
      $highestRow = $sheet->getHighestRow();
      $highestColumn = $sheet->getHighestColumn();

      //  Loop through each row of the worksheet in turn

      for ($row = 11; $row <= 11; $row++){

        echo "row: " . $row;

          //  Read a row of data into an array
          $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                          NULL,
                                          TRUE,
                                          FALSE);
          print_r($rowData);
          for($i=4;$i<76;$i++){
            $student_count = $rowData[0][$i];
            if(strlen($student_count)>0){
              // echo $student_count;
              // echo "<br>";
              $rowData2 = $sheet->rangeToArray('A' . 5 . ':' . $highestColumn . 5,
                                              NULL,
                                              TRUE,
                                              FALSE);
              $dateforclass = $rowData2[0][$i];
              $UNIX_DATE = ($dateforclass - 25569) * 86400;
              $date2 = gmdate("Y-m-d", $UNIX_DATE);
              $final_dates = array();
              $final_dates[] = $date2;
              $stop_date = $date2;
              $mydates = $date2 . ", ";
              for($j=1;$j<$total_dates; $j++){
                $stop_date = date('Y-m-d', strtotime($stop_date . ' +1 day'));
                $mydates = $mydates . $stop_date  . ", ";
                $final_dates[] = $stop_date;
              }

              $class_finalname = $classname . $classname_start_count;
              $classname_start_count++;

              $datetimeto = "16:00, 16:00, 16:00,";
              $datetimefrom = "09:00, 09:00, 09:00,";


              $post = [
                'classname' => $class_finalname,
                'instructorid' => $instructor_id,
                'coordinatorid'   => $coordinatorid,
                'coursesid' => $course_id,
                'locationid' => $location_id,
                'scheduletimeto' => $datetimeto,
                'scheduletimefrom'=> $datetimefrom,
                'schedule' => $mydates,
                'classcapacity' => $student_count,
                'classpracticalexam' => $final_dates[$total_dates-1],
                'classwrittenexam'=> $final_dates[$total_dates-1]
              ];

              $ret = add_class($class_finalname, $coordinatorid, $instructor_id, $course_id, $location_id, $mydates, $datetimeto, $datetimefrom, $student_count, $final_dates[$total_dates-1], $final_dates[$total_dates-1]);
              echo $ret['Message'];

              echo "<br>";
            }
          }

      }
}


?>

<!DOCTYPE html>
<html>
<body>

<form action="" method="post" enctype="multipart/form-data">
    Select Excel File to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Add To SQL" name="submit">
</form>

</body>
</html>
