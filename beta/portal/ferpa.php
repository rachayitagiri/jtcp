<?php

    $jsonurl = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $ret['Employee_Postal'];;
    $json = file_get_contents($jsonurl);
    $statedetails = json_decode($json, true);
    $state = $statedetails['results'][0]['address_components'][2]['long_name'];
    
?>

<script>
$(document).ready(function () {
                  $("#submitform").click(function () {
                                       $("#ferpaform").submit();
                                       });
                  });
</script>
<style>
.inputferpa1 {
border: 0;
outline: 0;
background: transparent;
    border-bottom: 2px solid black;
width: 250px;
}
.inputferpa2 {
border: 0;
outline: 0;
background: transparent;
    border-bottom: 2px solid black;
width: 180px;
}

.showferpa {
border: 0;
outline: 0;
background: transparent;
    border-bottom: 2px solid black;
width: 250px;
}

</style>

<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td width="" valign="top">
<p align="center">
</p>
</td>
<td width="" valign="top">
<p align="center">
<strong>California State University, Long Beach</strong>
<br>
<strong>College of Engineering</strong>
<br>
<strong>
Caltrans &#8211; Industry Joint Training and
Certification Program
</strong>
</p>
</td>
<td width="" valign="top">
<p align="center">
<strong></strong>
</p>
</td>
</tr>
</tbody>
</table>
<p>
<strong></strong>
</p>
<div style="background-color: rgb(191,191,191); border: 1px solid black;">
<p align="center">
<strong>FERPA CONSENT TO RELEASE</strong>
<br>
<strong> STUDENT INFORMATION</strong>
</p>
</div>
<div>
<p>
<strong> </strong>
</p>
<p>
Last Name: <span class="showferpa"><?php echo $ret['Employee_LastName']; ?></span> First Name:
<span class="showferpa"><?php echo $ret['Employee_FirstName']; ?></span>
</p>
<p>
Address: <span class="showferpa"><?php echo $ret['Employee_Address']; ?></span> City: <span class="showferpa"><?php echo $ret['Employee_City']; ?></span> State:
<span class="showferpa"><?php echo $state; ?></span> Zip Code: <span class="showferpa"><?php echo $ret['Employee_Postal']; ?></span>
</p>
<p>
Email: <span class="showferpa"><?php echo $ret['Login_Email']; ?></span> Phone Number:
<span class="showferpa"><?php echo $ret['Employee_Mobile']; ?></span>
</p>
<p>
<strong></strong>
</p>
</div>
<p align="center">
<strong>This form may <u>only</u> be submitted by the student </strong>
</p>
<p>
<strong>
Please post my certification information from my training records on
the Joint Training Certification website. The information that is to be
released under this consent is:
</strong>
</p>
<ul>
<li>
Name
</li>
<li>
Student ID number
</li>
<li>
Module Training completed, date completed
</li>
<li>
Certifications, date certified, certification expiration date
</li>
</ul>
<div>
</div>
<p>
<strong>
The above information is to be released for the following purpose(s):
</strong>
</p>
<ul>
<li>
Verification of training completion and certifications</li>
</ul>
<div>
</div>
<p>
<strong>
I understand the information will be posted on the Joint Training
Certification website and will be accessible to the public.
<s>
</s>
</strong>
</p>
<p>
<strong></strong>
</p>
<p>
<strong></strong>
</p>
<p>
<form id="ferpaform">
<table>
<tr>
    <td>
        <input type="text" class="inputferpa1" name="sign">
    </td>
    <td>

    </td>
    <td>
        <input type="date" class="inputferpa2" name="signdate">
    </td>
</tr>
<tr>
<td>
Signature
</td>
<td>

</td>
<td>
Date
</td>
</tr>
</table>
</form>
</p>
<p>
<strong>

All students participating in this Joint Training and Certification
Program are subject to the CSULB Standards for Student Conduct. All
student records related to the Standards for Student Conduct will be
made available to Caltrans and/or sponsoring employers.
</strong>
</p>
<p>
<strong></strong>
</p>
<p>
<strong></strong>
</p>
<p>
<strong></strong>
</p>
<div>
</div>
<input type="submit" value="Agree" id="submitform">
