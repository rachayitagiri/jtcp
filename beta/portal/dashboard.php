<?php
    include 'template1.php';


    $ret = array();
    $ret = getEmployeeDetail($lid);

    if($_POST)
    {
        $eid = $ret['Employee_ID'];
        $signature = $_POST['sign'];
        $signdate = $_POST['signdate'];
        $ret = addFerpa($eid, $signature, $signdate);
        $ret = addActivity($eid,"Ferpa Signed");
        $ret = getEmployeeDetail($lid);
    }
    $payflag = false;

?>
<head>
<title><?php echo $ret['Employee_FirstName'] . "'s Dashboard"; ?></title>

<script type="text/javascript">
//To avoid Enter press for Ferpa Form
window.addEventListener('keydown', function(e) {
                        if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
                        if (e.target.nodeName == 'INPUT' && e.target.type == 'text') {
                        e.preventDefault();
                        return false;
                        }
                        }
                        }, true);
</script>

<script>
$(document).ready(function(){


                  /*
                  $(".coursetext_header").click(function () {

                                     $header = $(this);
                                     //getting the next element
                                     $content = $header.next();
                                     //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                                     $content.slideToggle(500, function () {
                                                          //execute this after slideToggle is done
                                                          //change text of header based on visibility of content div

                                                          $header.text(function () {
                                                                       //change text based on condition
                                                                       return $content.is(":visible") ? $header.text() + "Collapse" : $header.text() + "Expand";
                                                                       });

                                                          });

                                     });
                  */
                                   });
</script>
<style>
/*
.coursetext_body {
display: none;
    padding : 5px;
}
*/

</style>
<script>
var flag_classEnroll = true;
var flag_classEnroll_withoutPrereq = true;
$(document).ready(function(){
                  $(".classEnroll").click(function(){
                                          if(flag_classEnroll==true){
                                          flag_classEnroll = false;
                                          $('#classenrollmodel').modal('toggle');
                                          document.getElementById('classenrollmodal-dialog').style.width = '70%';
                                          var cid = this.id;
                                          var eid = document.getElementById('eid').value;
                                          var cc = this.getAttribute('val');
                                          var withoutPrereq = 0;
                                          $.ajax({
                                                 url: "ShowClassesSearch.php",
                                                 type: "post",
                                                 data: {'cid' : cid, 'eid' : eid, 'cc' : cc, 'withoutPrereq': withoutPrereq} ,
                                                 success: function (response) {
                                                 $('#showclassdata').empty();
                                                 $('#showclassdata').append(response);
                                                 flag_classEnroll = true;
                                                 },
                                                 error: function(jqXHR, textStatus, errorThrown) {
                                                 console.log(textStatus, errorThrown);
                                                 flag_classEnroll = true;
                                                 }
                                                 });
                                            }
                                          });
                  $(".classEnroll_withoutPrereq").click(function(){
                                          if(flag_classEnroll_withoutPrereq==true){
                                          flag_classEnroll_withoutPrereq = false;
                                          $('#classenrollmodel').modal('toggle');
                                          document.getElementById('classenrollmodal-dialog').style.width = '70%';
                                          var cid = this.id;
                                          var eid = document.getElementById('eid').value;
                                          var cc = this.getAttribute('val');
                                          var withoutPrereq = 1;
                                          $.ajax({
                                                  url: "ShowClassesSearch.php",
                                                  type: "post",
                                                  data: {'cid' : cid, 'eid' : eid, 'cc' : cc, 'withoutPrereq': withoutPrereq} ,
                                                  success: function (response) {
                                                  $('#showclassdata').empty();
                                                  $('#showclassdata').append(response);
                                                  flag_classEnroll_withoutPrereq = true;
                                                  },
                                                  error: function(jqXHR, textStatus, errorThrown) {
                                                  console.log(textStatus, errorThrown);
                                                  flag_classEnroll_withoutPrereq = true;
                                                  }
                                                  });
                                              }
                                            });
                  });
</script>

</head>

<input type="text" value="<?php echo $ret['Employee_ID']; ?>" id="eid" hidden >

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    <?php
                        $lid = $_SESSION['Login_ID'];
                        echo $ret['Employee_FirstName'] . "'s Dashboard";
                    ?>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- Extra Space -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default" style="padding: 9px;">
                        <!--Progress Bar-->
                        <?php
                            if($ret['Ferpa']==0)
                            {
                            ?>
                            <h4>Status: Registerd</h4>
                            <h5>Next Step: Sign Ferpa</h5>
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning" style="width: 20%">
                                    <span class="sr-only">20% Used</span>
                                </div>
                        </div>
                        <?php
                            }
                            else if(count($ret['Certificate'])==0)
                            {
                        ?>
                        <h4>Status: Ferpa Signed</h4>
                        <h5>Next Step: Take Safety Quiz</h5>
                        <div class="progress">
                            <div class="progress-bar progress-bar-warning" style="width: 40%">
                                <span class="sr-only">20% Used</span>
                            </div>
                        </div>
                        <?php
                            }
                            else if(count($ret['Certificate'])==1)
                            {
                        ?>
                        <h4>Status: Passed Safety Quiz</h4>
                        <h5>Next Step: Take Course Prerequisite</h5>
                        <div class="progress">
                            <div class="progress-bar progress-bar-info" style="width: 70%">
                                <span class="sr-only">50% Used</span>
                            </div>
                        </div>
                        <?php
                            }
                            else
                            {
                        ?>
                        <h4>Status: Complete </h4>
                        <h5>Next Step: Browse Program Options</h5>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" style="width: 100%">
                                <span class="sr-only">100% Used</span>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                    </div>
                    <?php
                      $res2 = getPaypalPaidDataByEmployeID($ret['Employee_ID']);
                      if(count($res2)>0){
                        $payflag= true;
                      }
                      $res2 = checkPaypalDataByEmployeID($ret['Employee_ID']);
                      if(count($res2)>0){
                    ?>
                      <div class="panel panel-default" style="padding: 9px;">
                        <?php
                          echo "<h4>Pending Fees:</h4>";
                          $res2 = checkPaypalDataByEmployeID($ret['Employee_ID']);
                          for($k=0;$k<count($res2);$k++){
                            echo "<h5> Pay Fees for:" . $res2[$k]['Class_Name'] . "</h5>";
                            ?>
                            <form action='feesPay.php' method='post' target='_blank'>
                              <input class=hidden name='eid' value='<?php echo $ret['Employee_ID'];?>'>
                              <input class=hidden name='class_id' value='<?php echo $res2[$k]['Class_ID'];?>'>
                              <input class=hidden name='course_id' value='<?php echo $res2[$k]['Course_ID'];?>'>
                              <input class=hidden name='course_name' value='<?php echo $res2[$k]['Class_Name']; ?>'>
                              <button type='submit' name='pay' value='pay'>Pay Fees <php echo $res2[$k]['Class_Name']; ?></button>
                            </form>
                            <?php
                            echo "<h6>Warning: You need to pay fees in 20 hours in order to avoid dropping from the class</h6>";
                            echo "<h6>Note: If you have recently paid the fees, please wait for an hour to reflect it to your account.</h6>";
                            echo "<h5><a href='../Material/StateEmployeeInstruction_Temp.pdf' target='_blank'>Caltrans Employees: Click here for Instructions.</a></h5>";
                          }
                          ?>
                      </div>
                    <?php
                      }
                    ?>
                    <?php
                        if($ret['Ferpa']==0)
                        {
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> First Step
                        </div>
                        <div class="panel-body">
                            <div id="morris-donut-chart"></div>
                                    <button class="btn btn-default btn-block" data-toggle="modal" data-target="#ferpamodal" id="ferpabutton">Sign Ferpa Form</button>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    <?php
                        }
                        else if(count($ret['Certificate'])==0)
                        {
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Next Step
                        </div>
                        <div class="panel-body">
                            <div id="morris-donut-chart"></div>
                                <form action="quiz.php" method="POST">
                                    <input type="hidden" name="quiz_id" value="1" />
                                    <button class="btn btn-default btn-block" id="saferyquizbutton">Take Safety Quiz</button>
                                </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <?php
                        }
                    ?>

                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-fw"></i> Courses
                            <?php
                                if($ret['Ferpa']==0)
                                {
                                    echo " - Sign Ferpa!";
                                }
                                else if(count($ret['Certificate'])==0 || $ret['Certificate'][0]['Status_ID']!=5)
                                {
                                    echo " - Take Safety Quiz!";
                                }
                            ?>
                        </div>
                        <br>
                        <!-- /.panel-heading -->
                        <!-- /.row -->
                        <div class="row" style="padding: 9px;">
                            <div class="col-lg-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading coursetext_header">
                                        Hot Mix Asphalt I
                                    </div>
                                    <div class="panel-body coursetext_body">
                                        <p>The Joint Training and Certification Program (JTCP) has developed a two-part series certification program in Hot Mix Asphalt (HMA) for construction materials lab technicians. To help reduce conflict and delay on construction projects within the state, Caltrans, in partnership with CSULB, has developed a series of joint training and certification programs.</p>

                                        <p>Participants in Hot Mix Asphalt I will receive training and certification in various California Test Methods (CTM) and American Association of State Highway and Transportation Officials (AASHTO) methods pertaining to HMA testing in the field.</p>
                                    </div>
                                    <div class="panel-footer">
                                        <?php echo coursestatus(1, $ret); ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-lg-4 -->
                            <div class="col-lg-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading coursetext_header">
                                        Hot Mix Asphalt II
                                    </div>
                                    <div class="panel-body coursetext_body">
                                        <p>The Joint Training and Certification Program (JTCP) has developed a two-part series certification program in Hot Mix Asphalt (HMA) for construction materials lab technicians. To help reduce conflict and delay on construction projects within the state, Caltrans, in partnership with CSULB, has developed a series of joint training and certification programs.
                                        </p>
                                        <p>Participants in Hot Mix Asphalt II will receive training and certification in various American Association of State Highway and Transportation Officials (AASHTO) methods pertaining to HMA testing in the field.
                                            </p><br>
                                    </div>
                                    <div class="panel-footer">
                                        <?php echo coursestatus(2, $ret); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /.row -->
                        <div class="row" style="padding: 9px;">
                            <div class="col-lg-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading coursetext_header">
                                        Soils & Aggregates
                                    </div>
                                    <div class="panel-body coursetext_body">
                                    <p>The Joint Training and Certification Program (JTCP) has developed a certification program in Soils and Aggregates testing for construction materials lab technicians. To help reduce conflict and delay on construction projects within the state, Caltrans, in partnership with CSULB, has developed a series of joint training and certification programs.
                                    </p>
                                    <p>
                                    Participants in Soils and Aggregates will receive training and certification in various California Test Methods (CTM) and American Society for Testing and Materials (ASTM) methods pertaining to Soils and Aggregates testing in the field.</p>
                                    </div>
                                    <div class="panel-footer">
                                        <?php echo coursestatus(3, $ret); ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-lg-4 -->
                            <div class="col-lg-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading coursetext_header">
                                        Portland Cement Concrete
                                    </div>
                                    <div class="panel-body coursetext_body">
                                    <p>The Joint Training and Certification Program (JTCP) has developed a certification program in Portland Cement Concrete testing for construction materials lab technicians. To help reduce conflict and delay on construction projects within the state, Caltrans, in partnership with CSULB, has developed a series of joint training and certification programs.</p>

                                    <p>Participants in Portland Cement Concrete will receive training and certification in various American Society for Testing and Materials (ASTM) methods pertaining to PCC testing in the field.</p><br>
                                    </div>
                                    <div class="panel-footer">
                                        <?php echo coursestatus(4, $ret); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Print Certificates
                        </div>
                        <div class="list-group">
                        <?php
                            for($j=0;$j<count($ret['Certificate']);$j++)
                            {
                                if(($ret['Certificate'][$j]['Certificate_Final']==1 && ($ret['Certificate'][$j]['Status_ID']==3 || $ret['Certificate'][$j]['Status_ID']==4)) || ($ret['Certificate'][$j]['Certificate_Final']!=1))
                                {
                        ?>
                            <a href="../certificate.php?code=<?php echo $ret['Certificate'][$j]['Certificate_Code']; ?>" class="list-group-item" target="_blank">
                            <?php
                                $temp_course = $ret['Certificate'][$j]['Course_Name'];
                                if($ret['Certificate'][$j]['Certificate_Final']==0 && $j!=0)
                                {
                                    $temp_course = $temp_course . " (Prereq)";
                                }
                                echo $temp_course;
                            ?>
                            <span class="pull-right text-muted small"><em><?php echo "Valid: ". $ret['Certificate'][$j]['Certificate_ExpireDate']; ?></em>
                            </span>
                            </a>
                            <?php
                                }
                            }
                            ?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <?php
                        //For all courses
                        $finalflow = 1;
                        //For ACI
                        $finalflow2 = 1;
                        if($ret['Ferpa']==0)
                        {
                            $finalflow = 1;
                            $finalflow2 = 1;

                        }
                        else if(count($ret['Certificate'])==0 || $ret['Certificate'][0]['Status_ID']!=5)
                        {
                            $finalflow = 2;
                            $finalflow2 = 2;

                        }
                        else if(count($ret['Certificate'])==1)
                        {
                            $finalflow = 3;
                            $finalflow2 = 3;
                        }
                        else
                        {
                            for($tempi=0;$tempi<count($ret['Certificate']);$tempi++)
                            {
                                //Payment
                                if($payflag==true || $ret['Certificate'][$tempi]['Status_ID']==3)
                                {
                                    $finalflow = 6;
                                    break;
                                }
                                if($ret['Certificate'][$tempi]['Status_ID']==2)
                                {
                                    $finalflow = 5;
                                    break;
                                }
                                if($ret['Certificate'][$tempi]['Status_ID']==5 && $ret['Certificate'][$tempi]['Course_Name']!="Safety Quiz")
                                {
                                    $finalflow = 4;
                                    break;
                                }
                            }
                            for($tempi=0;$tempi<count($ret['Certificate']);$tempi++)
                            {
                              //Check if Enrolled into ACI
                              if($ret['Certificate'][$tempi]['Course_ID']==5 && ($ret['Certificate'][$tempi]['Status_ID']==2 || $ret['Certificate'][$tempi]['Status_ID']==3 || ($ret['Certificate'][$tempi]['Status_ID']==4 && $ret['Certificate'][$tempi]['Course_Name']!="Safety Quiz"))){
                                $finalflow2 = 4;
                                break;
                              }
                            }
                            //If not enrolled into ACI/PCC
                            if($finalflow2!=4){
                              $finalflow2=3;
                            }
                        }
                        echo "<center>";
//                        echo "<h5>For HMA-I, HMA-II and Soil & Aggregates</h5>";
                        echo "<img src='../img/Caltrans-Flowchart" . $finalflow . ".png' width='220px'>";
                        echo "</center>";
                        echo "<center>";
                        /*
                        echo "<h5>For Portland Cement Concrete (ACI)</h5>";
                        echo "<img src='../img/Caltrans-Flowchart_ACI" . $finalflow2 . ".png' width='220px'>";
                        */
                        echo "</center>";
                    ?>
                    <!-- /.panel -->
                    <!-- /.panel .chat-panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->


<?php

    include 'template2.php';
?>

<!-- Modal -->
<div class="modal fade" id="ferpamodal" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="userprogressmodel-tittle">Ferpa Form</h4>
</div>
<div class="modal-body">

<?php

    $jsonurl = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $ret['Employee_Postal'];;
    $json = file_get_contents($jsonurl);
    $statedetails = json_decode($json, true);
    if(strcmp($statedetails['results'][0]['address_components'][2]['types'][0],"administrative_area_level_1")==0){
      $state = $statedetails['results'][0]['address_components'][2]['long_name'];
    }
    else{
      $state = $statedetails['results'][0]['address_components'][3]['long_name'];
    }

    ?>

<script>
$(document).ready(function () {
                  $("#submitform").click(function () {
                                         var sign = document.getElementById('sign').value;
                                         var signdate = document.getElementById('signdate').value;
                                         if(sign.length>0 && signdate.length>0)
                                         {
                                            $("#ferpaform").submit();
                                         }
                                         else
                                         {
                                            alert("Please add your signature with date");
                                         }
                                         });
                  });
</script>
<style>
.inputferpa1 {
border: 0;
outline: 0;
background: transparent;
    border-bottom: 2px solid black;
padding: 2.8px;
width: 250px;
}
.inputferpa2 {
border: 0;
outline: 0;
background: transparent;
    margin-left: 20px;
    border-bottom: 2px solid black;
width: 180px;
}
.formateferpa
{
    margin-left: 20px;
}

.showferpa {
border: 0;
outline: 0;
background: transparent;
border-bottom: 2px solid black;
width: 250px;
}

</style>

<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td width="" valign="top">
<p align="center">
<img src="../img/csulb.png" height="70px" width="70px">
</p>
</td>
<td width="" valign="top">
<p align="center">
<strong>California State University, Long Beach</strong>
<br>
<strong>College of Engineering</strong>
<br>
<strong>
Caltrans &#8211; Industry Joint Training and
Certification Program
</strong>
</p>
</td>
<td width="" valign="top">
<p align="center">
<img src="../img/logo.png" height="50px" width="70px">
</p>
</td>
</tr>
</tbody>
</table>
<p>
<strong></strong>
</p>
<div style="background-color: rgb(191,191,191); border: 1px solid black;">
<p align="center">
<strong>FERPA CONSENT TO RELEASE</strong>
<br>
<strong> STUDENT INFORMATION</strong>
</p>
</div>
<div>
<p>
<strong> </strong>
</p>
<p>
Last Name: <span class="showferpa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ret['Employee_LastName']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> First Name:
<span class="showferpa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ret['Employee_FirstName']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
</p>
<p>
Address: <span class="showferpa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ret['Employee_Address']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> City: <span class="showferpa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ret['Employee_City']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> State:
<span class="showferpa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $state; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Zip Code: <span class="showferpa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ret['Employee_Postal']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
</p>
<p>
Email: <span class="showferpa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ret['Login_Email']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Phone Number:
<span class="showferpa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ret['Employee_Mobile']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
</p>
<p>
<strong></strong>
</p>
</div>
<p align="center">
<strong>This form may <u>only</u> be submitted by the student </strong>
</p>
<p>
<strong>
Please post my certification information from my training records on
the Joint Training Certification website. The information that is to be
released under this consent is:
</strong>
</p>
<ul>
<li>
Name
</li>
<li>
Student ID number
</li>
<li>
Module Training completed, date completed
</li>
<li>
Certifications, date certified, certification expiration date
</li>
</ul>
<div>
</div>
<p>
<strong>
The above information is to be released for the following purpose(s):
</strong>
</p>
<ul>
<li>
Verification of training completion and certifications</li>
</ul>
<div>
</div>
<p>
<strong>
I understand the information will be posted on the Joint Training
Certification website and will be accessible to the public.
<s>
</s>
</strong>
</p>
<p>
<strong></strong>
</p>
<p>
<strong></strong>
</p>
<p>
<form id="ferpaform" method="post">
<table>
<tr>
<td>
<input type="text" class="inputferpa1" name="sign" id="sign" >
</td>
<td>

</td>
<td>
<input type="date" class="inputferpa2" name="signdate" id="signdate" >
</td>
</tr>
<tr>
<td>
Signature
</td>
<td>

</td>
<td>
<span class="formateferpa">Date</span>
</td>
</tr>
</table>
</form>
</p>
<p>
<strong>
All students participating in this Joint Training and Certification
Program are subject to the CSULB Standards for Student Conduct. All
student records related to the Standards for Student Conduct will be
made available to Caltrans and/or sponsoring employers.
</strong>
</p>
<p>
<strong></strong>
</p>
<p>
<strong></strong>
</p>
<p>
<strong></strong>
</p>
<div>
</div>

</div>
<div class="modal-footer">
<center>
<button type="submit" id="submitform" class="btn btn-success">Agree</button>
</center>
</div>
</div>

</div>
</div>
