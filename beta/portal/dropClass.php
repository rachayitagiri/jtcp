<?php
    include '../dbmanage.php';
    $cc = $_POST['cc'];

    //get ClassID
    $temp_ret = getClassIDfromPrerequisiteCertificate($cc);
    //Using Prerequisite Certificate Code
    $ret=dropClass($cc);
    //Email Waitlist students to Enroll into the Class
    if($ret['Status']==1)
    {
        //Email Waitlisted Students
        include('../mailAPI/emails.php');
        $cid = $temp_ret['Class_ID'];
        $course_name = $temp_ret['Course_Name'];
        $eid = $temp_ret['Employee_ID'];
        $class_name = $temp_ret['Class_Name'];

        //Get add email addresses to send email
        $temp_ret = getWaitlist($cid);
        if(count($temp_ret['Email_Addresses'])>0)
        {
            waitlist_email($temp_ret, $course_name);
        }
        addActivity($eid, "Dropped class: " . $class_name . " for Course: " . $course_name, 1);
    }


    echo $ret["Message"];



?>
