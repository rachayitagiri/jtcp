<?php
  error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    include '../dbmanage.php';

    $eid = $_POST['eid'];
    $cc = $_POST['cc'];

    $cid = $_POST['cid'];

    $withoutPrereq = $_POST['withoutPrereq'];

    $ret=getClassesByCourse($cid);
?>

<script>
var flag_classselect = true;
var flag_waitlistdrop = true;
$(document).ready(function(){
//                  selecteddates=document.getElementById('selecteddates0').value;
                  var list = document.getElementsByClassName("selecteddates");
                  for (var i = 0; i < list.length; i++) {
                  selecteddates = list[i].value;
                  new Kalendae({
                                    attachTo:document.getElementById('calendaredit'+i),
                                    months:1,
                                    mode:'multiple',
                                    selected:selecteddates,
                               direction:'future',
                               blackout: function (date) {
                               return true;
                               }
                                    });
                  }
                  $(".waitlistdrop").click(function(){
                                          if(flag_waitlistdrop==true){
                                            flag_waitlistdrop=false;
                                          var cid = $(this).attr('id');
                                          var eid = document.getElementById('eid').value;
                                          $.ajax({
                                                 url: "dropWaitlist.php",
                                                 type: "post",
                                                 data: {'cid' : cid, 'eid' : eid} ,
                                                 success: function (response) {
                                                 alert(response);
                                                 document.getElementById('closeclasscloseenrollmodel').click();
                                                 },
                                                 error: function(jqXHR, textStatus, errorThrown) {
                                                 alert(errorThrown);
                                                 console.log(textStatus, errorThrown);
                                                 }
                                                 });
                                               }
                                               else{
                                                 alert("Please Wait for response!");
                                               }
                                          });
                  $(".classselect").click(function(){

                    //Compare the current date with the first date of the class and see if the class starts within 15 days or not
                    //If class starts within 15 days - show a message to select some other class and return
                    //Else continue with normal registration
                    
                    var list = document.getElementsByClassName("selecteddates");

                    //Get the first date for this class and change formatting
                    var first = list[$(this).closest('tr').index()].defaultValue;
                    first = first.split(',');
                      var date_split = first[0].split('-');                        //split yyyy-mm-dd, at "-"
                      var first_date = date_split[2].replace(/(,$)/g, "");      //assign date and remove the trailing comma
                      var first_month = date_split[1];                          //assign month
                      var first_year = date_split[0];                           //assign year

                    var courseid = '<?=$_POST['cid']?>';                        //get course id 
                    
                    var firstdate = new Date(first_year, first_month-1, first_date);  //reset firstdate
                    //var first = first.replace(/(,$)/g, "");
                    var deadline = new Date(firstdate.getFullYear(),firstdate.getMonth(),firstdate.getDate()-15); //get deadline
                    console.log(deadline);
                    var currentdate = new Date();

                    //Compare currentdate with deadline; if currentdate > deadline, disable enrolment
                    if (currentdate > deadline && courseid == 5) {
                      alert("You cannot enrol in a PCC class within 15 days from the start date of the class. Please enrol for a class with a later starting date.");
                    }
                    else {
                    if(confirm("Class can be dropped 15 days prior to the start date. No refund will be issued past that date, students can only create a request for transfer (change a student in the same class), students enrolling within 15 day of class start date will not be able to withdraw or request a refund.\n\nThe medium of instruction and language of course will be English.\n If you agree, press OK.")){

                      if(flag_classselect==true){
                        flag_classselect=false;
                       var cid = $(this).attr('id');
                        var eid = document.getElementById('eid').value;
                        var cc = document.getElementById('cc').value;
                        var withoutPrereq = document.getElementById('withoutPrereq').value;
                       $.ajax({
                              url: "selectClass.php",
                              type: "post",
                              data: {'cid' : cid, 'eid' : eid, 'cc': cc, 'withoutPrereq': withoutPrereq} ,
                              success: function (response) {
                              if(response.startsWith("Class in Full! Do you want to add it to waitlist?"))
                              {
                                var r = confirm("Class in Full! Do you want to add it to waitlist? By joining the waitlist you and all those in the waiting list will receive an automatic email indicating an available seat. No payment is required to join the wait list. The first individual to enroll will fill the seat and a payment will be required then, if you wish to join the waiting list please click OK");
                                if(r==true)
                                {
                                    waitlist(cid);
                                }
                              }
                              else
                              {
                                alert(response);
                                document.getElementById('closeclasscloseenrollmodel').click();
                                location.reload();
                              }
                              },
                              error: function(jqXHR, textStatus, errorThrown) {
                              alert(errorThrown);
                              console.log(textStatus, errorThrown);
                              }
                              });
                            }
                            else{
                              alert("Please Wait for response!");
                            }
                    }                                               }});
                                             });
function waitlist(cid)
{
    var eid = document.getElementById('eid').value;
    $.ajax({
           url: "addWaitlist.php",
           type: "post",
           data: {'cid' : cid, 'eid' : eid} ,
           success: function (response) {
            alert(response);
            document.getElementById('closeclasscloseenrollmodel').click();
            location.reload();
           },
           error: function(jqXHR, textStatus, errorThrown) {
           alert(errorThrown);
           console.log(textStatus, errorThrown);
           }
           });
}
</script>
<input type="text" value="<?php echo $eid; ?>" id="eid" hidden>
<input type="text" value="<?php echo $cc; ?>" id="cc" hidden>
<input type="text" value="<?php echo $withoutPrereq; ?>" id="withoutPrereq" hidden>


<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Class Name</th>
<th>Class Capacity</th>
<th>Instructor</th>
<!--<th>Practical Exam Date</th>-->
<!--<th>Written Exam Date</th>-->
<th>Class Dates</th>
<th>Class Location</th>
<th>Register</th>
</tr>
</thead>
<tbody>
    <?php
        for($i=0;$i<count($ret);$i++)
        {
            $selecteddates = "";
            $classid = $ret[$i]['Class_ID'];

            for($j=0;$j<count($ret[$i]['Class_Dates']);$j++)
            {
                $selecteddates =  $ret[$i]['Class_Dates'][$j] . "," . $selecteddates;
            }
            $temp_ret = getOldClassIDfromPendingRefundsByEmployeeIDClassID($eid, $ret[$i]['Class_ID']);
            print_r();
            echo "<input type='text' value='$selecteddates' class= 'selecteddates' hidden >";
    ?>
<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Class_Name'];
    ?>
</td>
<td>
<?php
    echo  $ret[$i]['Class_Filled'] . "/" .$ret[$i]['Class_Capacity'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Instructor_FirstName'] . " " . $ret[$i]['Instructor_LastName'];
    ?>
</td>
<!--
<td>
<?php
    echo $ret[$i]['Class_Practical_Date'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Class_Written_Date'];
    ?>
</td>
-->

<!--Calender Click Disabled using z-index so new div will be in front of older one-->
<td style="position: relative;">
<div class="form-group col-lg-12">
<div style="height: 100%;position: absolute;width: 100%;margin: -8px -8px;z-index: 999;"></div>
Class Dates<br>
<?php $calendaredit = "calendaredit" . $i; echo "<center id='$calendaredit'>"; ?>
</center>
<div id="calendartimeedit">

</div>

</div>

<?php
    echo "All Classes will be from " . $ret[0]['Class_TimeFrom'][0] . " to " . $ret[0]['Class_TimeTo'][0] . ".";
   /*
    for($j=0;$j<count($ret[$i]['Class_Dates']);$j++)
        {
            echo $ret[$i]['Class_Dates'][$j];
            echo " (" . $ret[$i]['Class_TimeFrom'][$j];
            echo "-" . $ret[$i]['Class_TimeTo'][$j] . ")";
            echo "<br>";
        }
    */
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Location_Name'];
    ?>
</td>
<td>
<?php
    if($ret[$i]['Coordinator_ID']==999){
        echo "<a href='#'> Class Cancelled</a>";
    }
    else{
      if($ret[$i]['Class_Capacity']>$ret[$i]['Class_Filled'])
      {
          echo "<a href='#' class='classselect' id='" . $ret[$i]['Class_ID'] . "'>Select</a>";
      }
      else
      {
          $temp_ret = check_waitlist($classid, $eid);
          if($temp_ret==1)
          {
              echo "You are in the waitlist! <br><a href='javascript: void(0)' class='waitlistdrop' id='" . $ret[$i]['Class_ID'] . "'>Remove from Waitlist!</a>";
          }
          else
          {
              echo "<a href='#' class='classselect' id='" . $ret[$i]['Class_ID'] . "'>Select</a>";
          }
      }
    }
    ?>
</td>
</tr>
<?php
    }
    ?>
</tbody>
</table>
