<?php

    include 'dbmanage.php';

    ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<title>CSULB | Caltrans - Joint Training & Certification Program</title>

<!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">
	// check for window is loaded or not
	window.onload = function(){
	var myNav = document.getElementById('mymainnav');
	$(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
			$(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
		} else {
          $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
        }
      });
	}
</script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
</head>


<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

	<!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
			<!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
			<!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg.jpg');"></div>

            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
	<!----------------------- slider image completed ---------------------------------------->

	<!----------------------- navigation in bottom of image slider -------------------------->
	<nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav" style="width: 110%;">
                    <li>
                        <a href="index.php">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="index.php#courses">COURSES</a>
                    </li>
                    <li>
                        <a href="search.php" >SEARCH</a>
                    </li>
                    <li>
                        <a href="#" style="color: rgb(177,92,34);" >FAQ</a>
                    </li>
                    <li>
                        <a href="contact.php">CONTACT</a>
                    </li>
                    <li>
                        <a href="feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                      <a href="jobs.php">JOB OPENINGS</a>
                    </li>
					<li>
                      <a href="gallery.php">GALLERY</a>
                    </li>
                    <li>
                        <a href="login.php">LOGIN</a>
                    </li>
                    <li>
                        <a href="signup.php">ENROLL</a>
                    </li>
                    <li>
                        <a href="acknowledgment.php">ACKNOWLEDGMENT</a>
                    </li>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!----------------------------------- navigation completed ---------------------------------->

	<!----------------------------------- body started ------------------------------------------>

<div class="container ">
<h5><a href="http://www.csulb.edu/college-of-engineering" style="color: rgb(177,92,34);" target="_blank">College of Engineering</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="index.php" style="color: rgb(177,92,34);">JTCP</a> <span class="glyphicon glyphicon-chevron-right"></span> FAQs</h5>
<h2>FAQs</h2>
<br>
<div class="panel-group" id="faqAccordion">
<div class="panel panel-default ">
<div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: What is JTCP?</a>
</h4>

</div>
<div id="question0" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>The Joint Training and Certification Program (JTCP) was established by Caltrans Materials Engineering and Testing Services to achieve consistent training and certification for all technicians involved in the transportation construction industry throughout California. The purpose of the JTCP is to improve the quality of construction practices that Caltrans and other agencies obtain, and to help reduce conflict and delay on construction projects within the state. The JTCP was developed in partnership with CSULB’s College of Engineering.</p>
</div>
</div>
</div>
<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: How do I register and enroll?</a>
</h4>

</div>
<div id="question1" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>Registration and enrollment is a simple six-steps process. Just go to the <a href="signup.php">Signup Page</a> and follow the flow chart. </p>
</div>
</div>
</div>
<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question2">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: How long are Certifications valid?</a>
</h4>

</div>
<div id="question2" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>HMA I, HMA II and SA course certifications are valid for 3 years, PCC course certification is valid for 5 years and all prerequisite certifications are valid for a year.</p>
</div>
</div>
</div>
<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question3">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: If my certificate expires, can I re-apply for the certificate?</a>
</h4>

</div>
<div id="question3" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>Yes.</p>
</div>
</div>
</div>

<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question4">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: How can I change my password?</a>
</h4>

</div>
<div id="question4" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>After logging into your account, select "Settings" on the menu and enter new password along with old password and click "Change."</p>
</div>
</div>
</div>

<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question5">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: How can I change my Basic Details?</a>
</h4>

</div>
<div id="question5" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>After logging into your account, select "Settings" on the menu and enter the basic information that you want to change and click "Change."</p>
</div>
</div>
</div>

<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question6">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: What types of materials should we bring?</a>
</h4>

</div>
<div id="question6" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>Yes, pen, notepad , calculator. We are providing student handbook.</p>
</div>
</div>
</div>


<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question7">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: Do we have to bring our own PPE?</a>
</h4>

</div>
<div id="question7" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>We are providing Glasses and gloves. The student should bring their own lab coat.</p>
</div>
</div>
</div>


<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question8">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: Do you have any suggestions for the dress attire?</a>
</h4>

</div>
<div id="question8" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>The typical dress code for lab is required. Closed toe shoes, long sleeves (if no lab coat), no shorts or flipflops, etc.</p>
</div>
</div>
</div>


<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question9">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: Can I drop enrolled class?</a>
</h4>

</div>
<div id="question9" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>Class can be dropped 15 days prior to the start date. No refund will be issued past that date, students can only create request for transfer (change student in the same class)</p>
</div>
</div>
</div>


<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question10">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: When can I confirm my travel arrangement to attend the class?</a>
</h4>

</div>
<div id="question10" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>The class will be confirmed 15 day prior to the first day. Please contact the program coordinator <a href='contact.php' target='_blank'>here</a> to confirm the class at that time and before booking your travel arrangement.</p>
</div>
</div>
</div>


<div class="panel panel-default ">
<div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question11">
<h4 class="panel-title">
<a href="javascript:void(0)" class="ing">Q: How do I pay for the class??</a>
</h4>

</div>
<div id="question11" class="panel-collapse collapse" style="height: 0px;">
<div class="panel-body">
<h5><span class="label label-primary">Answer</span></h5>

<p>The on-line registration process is set up to accept payments by credit card via a secure, third-party system, PayPal, managed by our administrative partner, the California Asphalt Pavement Association (CalAPA).  IMPORTANT: Do not initiate the registration process unless you are ready to complete the transaction by supplying credit card payment information, or making prior arrangements for payment. Contact Sophie You at CalAPA at (916) 791-5044 for additional information on the alternative-payment options. No one will be admitted to a class without completing the registration process, which includes confirmation that payment has been received.</p>
</div>
</div>
</div>





</div>
<!--/panel-group-->
</div>

<div class="container">
<h2>Still have questions?</h2>
<div class="row-fluid">
<h4><a href="contact.php">Click here</a> to contact us.</h4>
</div>
</div>


    <!------------------------------ body completed -------------------------------------->

	 <!------------------------------ Footer --------------------------------------------->

<?php
    include "footer.php";
    ?>

	<!----------------------------- Footer Completed -------------------------------------->


    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
