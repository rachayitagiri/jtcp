<?php
    include 'template1.php';

    if($_POST['remove'])
    {
        $ret = deleteCoordinator($_POST['lid']);
        echo "<script>alert('" . $ret['Message'] . "');</script>";

    }
    else if($_POST['add'])
    {
        $ret = signup_coordinator($_POST['firstname'], $_POST['lastname'], $_POST['email'], $_POST['password']);
        echo "<script>alert('" . $ret['Message'] . "');</script>";
    }
?>

<title>Admin Dashboard</title>

<script>
function ask() {

    if (confirm('Are you sure you want to remove Coordinator?')) {
        return true;
    } else {
        return false;
    }
}
</script>

<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Dashboard</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
<div class="col-lg-12">

<div class="panel panel-default">
<div class="panel-heading">
Add Coordinator
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<form method="POST" action="">
<div class="form-group col-lg-6">
<label for="name">First Name:</label>
<input type="text" class="form-control" name="firstname" required >
</div>
<div class="form-group col-lg-6">
<label for="name">Last Name:</label>
<input type="text" class="form-control" name="lastname" required >
</div>

<div class="form-group col-lg-6">
<label for="email">Email address:</label>
<input type="email" class="form-control" name="email" required >
</div>
<div class="form-group col-lg-6">
<label for="pwd">Password:</label>
<input type="password" class="form-control" name="password" required >
<input type='submit' class="btn btn-success" value='add' name='add'>
</div>
</form>
</div>

</div>


<div class="panel panel-default">
<div class="panel-heading">
View Coordinators
</div>
<!-- /.panel-heading -->
<div class="panel-body">


<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Coordinator Name</th>
<th>Coordinator Email</th>
<th>Action</th>
<th>Track</th>
</tr>
</thead>
<tbody>
<?php
    $ret=getCoordinatorsDetails();
    for($i=0;$i<count($ret);$i++)
    {
        ?>
<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Coordinator_FirstName'] . " " . $ret[$i]['Coordinator_LastName'];
    ?>
</td>
<td>
<?php
    echo  $ret[$i]['Login_Email'];
    ?>
</td>
<td>
<form id="" role="form" action="" method="POST">

<?php
    echo "<input type='text' value='" . $ret[$i]['Login_ID'] . "' name='lid' hidden>";
?>
<input type='submit' value='Remove' name='remove' onclick="return ask();">
</form>
</td>
<td>
<a href='trackCoordinator.php?cooid=<?php echo $ret[$i]['Coordinator_ID'];?>' target='_blank'>Track</a>
</td>
</tr>
<?php
}
    ?>
</tbody>
</table>




<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->

<script>

$(document).ready(function() {
                  $('#dataTables-example').DataTable({
                                                     responsive: true
                                                     });
                  });
</script>




        <!-- /#page-wrapper -->


<?php
    include 'template2.php';
?>
