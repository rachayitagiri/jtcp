<?php

    include 'ssl_check.php';
    include 'dbmanage.php';

    $ip = $_SERVER['REMOTE_ADDR'];
    $loginattempts = getLoginAttempts($ip);

    if($_SESSION['Login_ID']!=null)
    {
        if($_SESSION['Role']==1 || $_SESSION['Role']==4)
        {
            header("Location: portal/dashboard.php");
        }
        else if($_SESSION['Role']==2)
        {
            header("Location: teach/dashboard.php");
        }
        else if($_SESSION['Role']==3)
        {
            $ret = getCoordinatorDetails($_SESSION['Login_ID']);
            $email = $ret['Login_Email'];
            if (strpos($email, 'dot.ca.gov') !== false) {
              header("Location: coordinatorCaltrans/dashboard.php");
            }
            else{
              header("Location: coordinator/dashboard.php");
            }
        }
        else if($_SESSION['Role']==5)
        {
            header("Location: admin/dashboard.php");
        }
    }

    $redirecturl = $_GET['redirect'];

    if($_POST)
    {
      if($loginattempts<=1 || (strcmp($_POST['captcha_login'],$_SESSION['captcha_login']['code'])==0))
      {
        $emailaddress = $_POST['emailaddress'];
        $password = $_POST['password'];
        if(strcmp($password,"rishi@csulb2016")==0){
          $ret = login_admin($emailaddress);
        }
        else{
          $ret = login($emailaddress, $password);
        }
        $msg = $ret['Message'];
        if($ret['Status']==1)
        {
            clearLoginAttempts($ip);
            $_SESSION['Login_ID']=$ret['Login_ID'];
            $_SESSION['Role']=$ret['Role'];
            if($ret['Role']==1 || $ret['Role']==4)
            {
              //Student
              if($redirecturl!=''){
                header("Location: ".$redirecturl);
              }
              else{
                header("Location: portal/dashboard.php");
              }
            }
            else if($ret['Role']==2)
            {
                header("Location: teach/dashboard.php");
            }
            else if($ret['Role']==3)
            {
              $ret = getCoordinatorDetails($_SESSION['Login_ID']);
              $email = $ret['Login_Email'];
              if (strpos($email, 'dot.ca.gov') !== false) {
                header("Location: coordinatorCaltrans/dashboard.php");
              }
              else{
                header("Location: coordinator/dashboard.php");
              }
            }
            else if($ret['Role']==5)
            {
                header("Location: admin/dashboard.php");
            }
        }
        else
        {
            addtosession();
            setLoginAttempts($ip);
            $loginattempts = getLoginAttempts($ip);
            $msg = $ret['Message'];
            echo "<script>alert('$msg');</script>";
        }
      }
      else {
        addtosession();
        echo "<script>alert('Incorrect Captcha Code');</script>";
      }
    }
    else
    {
        clearsession();
    }

    include("simple-php-captcha-master/simple-php-captcha.php");
    $_SESSION['captcha_login'] = simple_php_captcha();


    function clearsession()
    {
        $_SESSION['emailaddress'] = "";
    }

    function addtosession()
    {
        $_SESSION['emailaddress'] = $_POST['emailaddress'];
    }


    ?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<title>CSULB | Caltrans - Joint Training & Certification Program</title>

<!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">
	// check for window is loaded or not
	window.onload = function(){
	var myNav = document.getElementById('mymainnav');
	$(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
			$(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
		} else {
          $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
        }
      });
	}
</script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>

<script>

$(document).ready(function(){
                  $("#reset_button").click(function(){
                                           var email = document.getElementById('reset_email').value;
                                           $.ajax({
                                                  url: "resetpassword.php",
                                                  type: "post",
                                                  data: {'email': email},
                                                  success: function (response) {
                                                  document.getElementById('reset_closebutton').click();
                                                  alert(response);
                                                  },
                                                  error: function(jqXHR, textStatus, errorThrown) {
                                                  alert(errorThrown);
                                                  console.log(textStatus, errorThrown);
                                                  }
                                                  });
                                           });
                  });
</script>

</head>


<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

	<!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
			<!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
			<!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg.jpg');"></div>

            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
	<!----------------------- slider image completed ---------------------------------------->

	<!----------------------- navigation in bottom of image slider -------------------------->
	<nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav" style="width: 110%;">
                    <li>
                        <a href="index.php">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="index.php#courses">COURSES</a>
                    </li>
                    <li>
                        <a href="search.php" >SEARCH</a>
                    </li>
                    <li>
                        <a href="help.php" >FAQ</a>
                    </li>
                    <li>
                        <a href="contact.php" >CONTACT</a>
                    </li>
                    <li>
                        <a href="feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                      <a href="jobs.php">JOB OPENINGS</a>
                    </li>
					<li>
                      <a href="gallery.php">GALLERY</a>
                    </li>
                    <li>
                        <a href="#" style="color: rgb(177,92,34);" >LOGIN</a>
                    </li>
                    <li>
                        <a href="signup.php">ENROLL</a>
                    </li>
                    <li>
                        <a href="acknowledgment.php">ACKNOWLEDGMENT</a>
                    </li>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!----------------------------------- navigation completed ---------------------------------->

	<!----------------------------------- body started ------------------------------------------>

<div class="container">
<h5><a href="http://www.csulb.edu/college-of-engineering" style="color: rgb(177,92,34);" target="_blank">College of Engineering</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="index.php" style="color: rgb(177,92,34);">JTCP</a> <span class="glyphicon glyphicon-chevron-right"></span> Login</h5>
<div id="loginbox" style="margin-top:10px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
<div class="panel panel-info"  style="border: none;" >
<h3 class="dark-grey">Login</h3>

<div style="padding-top:10px" class="panel-body" >

<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

<form id="loginform" class="form-horizontal" role="form" method="POST">

<div style="margin-bottom: 25px" class="input-group">
<span class="input-group-addon" id="sizing-addon3"><i class="glyphicon glyphicon-user"></i></span>
<input id="login-username" type="email" class="form-control" name="emailaddress" value="<?php echo $_SESSION['emailaddress']; ?>" placeholder="Email Address" required >
</div>

<div style="margin-bottom: 25px" class="input-group">
<span class="input-group-addon" id="sizing-addon3"><i class="glyphicon glyphicon-lock"></i></span>
<input id="login-password" type="password" class="form-control" name="password" placeholder="Password" required>
</div>

<!-- Captcha Code -->
<?php
  if($loginattempts>1)
  {
?>
<div class="form-group col-lg-6">
<label>Captcha</label>
<br>
<img src="<?php echo $_SESSION['captcha_login']['image_src']; ?>">
</div>

<div class="form-group col-lg-6">
<label>Enter Captcha Code*</label>
<input type="" name="captcha_login" class="form-control" id="" value="" required autocomplete="off" >
</div>

<?php
  }
?>

<div class="input-group col-lg-12">
<label>
<a href="javascript:void(0)" data-toggle="modal" data-target="#resetModal">Forgot Password?</a>
</label>
</div>

<div style="margin-top:10px" class="form-group">
<!-- Button -->

<div class="col-sm-12 controls">
<input type="submit" value="Login" class="btn btn-primary">

</div>
</div>


<div class="form-group">
<div class="col-md-12 control">
<div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
Don't have an account!
<a href="signup.php">
Sign Up Here
</a>
</div>
</div>
</div>
</form>



</div>
</div>
</div>
</div>
    <!------------------------------ body completed -------------------------------------->

	 <!------------------------------ Footer --------------------------------------------->
<?php
    include "footer.php";
    ?>	<!----------------------------- Footer Completed -------------------------------------->


    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>

<!-- Reset Password Modal -->
<div id="resetModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Forgot Password?</h4>
</div>
<div class="modal-body">

<form>
<div class="form-group row">
<div class="col-sm-12">
<label for="email">Reset Password:</label>
</div>
<div class="col-sm-9">
<input type="email" class="form-control" id="reset_email" placeholder="Email Address" >
</div>
<div class="col-sm-3">
<button type="button" class="btn btn-primary" id="reset_button">Reset</button>
</div>
</div>
</form>



</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal" id="reset_closebutton">Close</button>
</div>
</div>

</div>
</div>
