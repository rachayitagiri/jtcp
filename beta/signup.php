<?php

    include 'ssl_check.php';

    include 'dbmanage.php';

    if($_POST)
    {
        if(strcmp($_POST['captcha'],$_SESSION['captcha']['code'])==0)
        {
            $prefix="";
            $firstname = $_POST['firstname'];
            $initial = $_POST['initial'];
            $lastname = $_POST['lastname'];
            $emailaddress = $_POST['emailaddress'];
            $password = $_POST['password'];

            $password = password_hash($password, PASSWORD_DEFAULT);

            $phonenumber = $_POST['phonenumber'];
            $addressline = $_POST['addressline1'];
            $city = $_POST['city'];
            $company = $_POST['company'];
            $other = $_POST['other'];
            $postal = $_POST['postalcode'];
            $position = $_POST['position'];
            $ret = signup($prefix, $firstname, $initial, $lastname, $emailaddress, $password, $phonenumber, $company, $other, $position, $addressline, $city, $postal, 1);

            if($ret['Status']==1)
            {
                //Send Email
                /*
                include('mailAPI/emails.php');
                welcome_email($firstname,$lastname,$emailaddress);
                */

                $_SESSION['Login_ID']=$ret['Login_ID'];
                $_SESSION['Role']=1;
                echo "<script>window.open('login.php', '_top');</script>";
            }
            else
            {
                addtosession();
                $msg = $ret['Message'];
                echo "<script>alert('$msg');</script>";
            }

        }
        else
        {
            addtosession();
            echo "<script>alert('Incorrect Captcha Code');</script>";
        }
    }
    else
    {
        clearsession();
    }

    include("simple-php-captcha-master/simple-php-captcha.php");
    $_SESSION['captcha'] = simple_php_captcha();

    function clearsession()
    {
        $_SESSION['firstname'] = "";
        $_SESSION['initial'] =  "";
        $_SESSION['lastname'] = "";
        $_SESSION['emailaddress'] = "";
        $_SESSION['phonenumber'] = "";
        $_SESSION['addressline1'] = "";
        $_SESSION['city'] = "";
        $_SESSION['company'] = "";
        $_SESSION['other'] = "";
        $_SESSION['postalcode'] = "";
        $_SESSION['position'] = "";
    }

    function addtosession()
    {
        $_SESSION['firstname'] = $_POST['firstname'];
        $_SESSION['initial'] =  $_POST['initial'];
        $_SESSION['lastname'] = $_POST['lastname'];
        $_SESSION['emailaddress'] = $_POST['emailaddress'];
        $_SESSION['phonenumber'] = $_POST['phonenumber'];
        $_SESSION['addressline1'] = $_POST['addressline1'];
        $_SESSION['city'] = $_POST['city'];
        $_SESSION['company'] = $_POST['company'];
        $_SESSION['other'] = $_POST['other'];
        $_SESSION['postalcode'] = $_POST['postalcode'];
        $_SESSION['position'] = $_POST['position'];
    }

    ?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<title>CSULB | Caltrans - Joint Training & Certification Program</title>

<!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>


<script type="text/javascript">
var company;
var othercompany;
window.onload = function () {

    var myNav = document.getElementById('mymainnav');
    $(window).scroll(function() { // check if scroll event happened
                     if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
                     $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
                     $(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
                     } else {
                     $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
                     }
                     });

    document.getElementById("password2").onchange = validatePassword;
    document.getElementById("company").onchange = validateCompany;
    document.getElementById("othercompany").onchange = validateCompany;
    document.getElementById("city").onchange = validateCompany;
    company = document.getElementById("company");
    othercompany = document.getElementById("othercompany");

}
function validatePassword(){
    var pass2=document.getElementById("password2").value;
    var pass1=document.getElementById("password1").value;
    if(pass1.length<5)
    {
        document.getElementById("password1").setCustomValidity("Passwords Must be at least 5 characters long.");
    }
    else if(pass1!=pass2)
    {
        document.getElementById("password2").setCustomValidity("Passwords Don't Match");
    }
    else
    {
        document.getElementById("password1").setCustomValidity('');
        document.getElementById("password2").setCustomValidity('');
    }
    //empty string means no validation error
}
function validateCompany(){
    if(company.value=="none" && othercompany.value.length==0)
    {
        document.getElementById("othercompany").setCustomValidity("Select company from the list or enter Other Company Name.");
    }
    else
    {
        document.getElementById("othercompany").setCustomValidity('');

    }

}
function checkForm(){
    //var ferba = document.getElementById("ferba");
    if(company.value=="none" && othercompany.value.length==0)
    {
        document.getElementById("othercompany").setCustomValidity("Select company from the list or enter Other Company Name.");
        checkForm();
        return false;

    }
    else
    {
        document.getElementById("othercompany").setCustomValidity('');
        return true;

    }
    /*
     if(!(ferba.checked==true))
     {
     alert("Ferba must be agreed in order to Register!");
     return false;
     }
     */
    return false;
}
function toggle() {
     var checkbox = document.getElementById('box');
     var toggle = document.getElementById('othercompany');
     updateToggle = checkbox.checked ? (toggle.readOnly=true, toggle.value="Caltrans") : (toggle.readOnly=false, toggle.value="");
}
</script>

</head>


<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

	<!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
			<!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
			<!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg.jpg');"></div>

            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
	<!----------------------- slider image completed ---------------------------------------->

	<!----------------------- navigation in bottom of image slider -------------------------->
	<nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav" style="width: 110%;">
                    <li>
                        <a href="index.php">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="index.php#courses">COURSES</a>
                    </li>
                    <li>
                        <a href="search.php" >SEARCH</a>
                    </li>
                    <li>
                        <a href="help.php" >FAQ</a>
                    </li>
                    <li>
                        <a href="contact.php" >CONTACT</a>
                    </li>
                    <li>
                        <a href="feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                      <a href="jobs.php">JOB OPENINGS</a>
                    </li>
					<li>
                      <a href="gallery.php">GALLERY</a>
                    </li>
                    <li>
                        <a href="login.php">LOGIN</a>
                    </li>
                    <li>
                        <a href="#" style="color: rgb(177,92,34);" >ENROLL</a>
                    </li>
                    <li>
                        <a href="acknowledgment.php">ACKNOWLEDGMENT</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!----------------------------------- navigation completed ---------------------------------->

	<!----------------------------------- body started ------------------------------------------>

<div class="container-fluid">
<section class="container">
<h5><a href="http://www.csulb.edu/college-of-engineering" style="color: rgb(177,92,34);" target="_blank">College of Engineering</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="index.php" style="color: rgb(177,92,34);">JTCP</a> <span class="glyphicon glyphicon-chevron-right"></span> Enroll to a Course</h5>
<div class="container-page">
<div class="col-md-6">
<h3 class="dark-grey">Sign up</h3>
<hr>

<form method="POST" onsubmit="return checkForm(this);">

<h4>Basic Details</h4>

<div class="form-group col-lg-5">
<label>First Name*</label>
<input type="" name="firstname" class="form-control" id="" value="<?php echo $_SESSION['firstname']; ?>" required>
</div>

<div class="form-group col-lg-2">
<label>Initial</label>
<input type="" name="initial" class="form-control" id="" value="<?php echo $_SESSION['initial']; ?>" >
</div>

<div class="form-group col-lg-5">
<label>Last Name*</label>
<input type="" name="lastname" class="form-control" id="" value="<?php echo $_SESSION['lastname']; ?>" required>
</div>


<div class="form-group col-lg-12">
<label>Email Address*</label>
<input type="email" name="emailaddress" class="form-control" id="emailaddress" value="<?php echo $_SESSION['emailaddress']; ?>" required >
</div>

<div class="form-group col-lg-6">
<label>Password*</label>
<input type="password" name="password" class="form-control" id="password1" value="" required>
</div>

<div class="form-group col-lg-6">
<label>Repeat Password*</label>
<input type="password" name="" class="form-control" id="password2" value="">
</div>

<div class="form-group col-lg-12">
<label>Phone Number* (Format: 123-456-7890)</label>
<input type="tel" name="phonenumber" class="form-control" id="" value="<?php echo $_SESSION['phonenumber']; ?>" pattern="^\d{3}-\d{3}-\d{4}$" required>
</div>

<h4>Home/Business Address</h4>

<div class="form-group col-lg-12">
<label>Address line*</label>
<input type="" name="addressline1" class="form-control" id="" value="<?php echo $_SESSION['addressline1']; ?>" required >
</div>

<div class="form-group col-lg-6">
<label>City*</label>
<input type="" name="city" class="form-control" id="city" value="<?php echo $_SESSION['city']; ?>" pattern="^([a-zA-Z\u0080-\u024F]+(?:. |-| |'))*[a-zA-Z\u0080-\u024F]*$" required>
</div>

<div class="form-group col-lg-6">
<label>Postal Code*</label>
<input type="" name="postalcode" class="form-control" id="" value="<?php echo $_SESSION['postalcode']; ?>" required>
</div>

<h4>Employer Information</h4>

<!--
<div class="col-sm-12" style="margin-bottom: 20px;">
<label>Company*</label>
<select name="company" class="form-control" id="company">
<option disabled selected value="none"> -- select your company -- </option>
<?php
    $ret = getCompany();
    for($i=0;$i<count($ret);$i++)
    {
        $company_id = $ret[$i]['Company_ID'];
        $company_name = $ret[$i]['Company_Name'];
        echo "<option value='$company_id'>$company_name</option>";
    }
    ?>
</select>
</div>

<div class="form-group col-lg-12">
<label>Other</label>
<input type="text" name="other" class="form-control" id="othercompany" value="<?php echo $_SESSION['other']; ?>">
</div>

-->

<div class="form-group col-lg-12">
<label>Company*</label>
<input type="text" name="other" class="form-control" id="othercompany" value="<?php echo $_SESSION['other']; ?>" required >
<input type="checkbox" name="caltranbox" id="box"  onClick="toggle()"> Please check this box if you are a Caltrans employee</input>
</div>

<div class="form-group col-lg-12">
<label>Position</label>
<input type="" name="position" class="form-control" id="" value="<?php echo $_SESSION['position']; ?>" >
</div>

<div class="form-group col-lg-6">
<label>Captcha</label>
<br>
<img src="<?php echo $_SESSION['captcha']['image_src']; ?>">
</div>

<div class="form-group col-lg-6">
<label>Enter Captcha Code*</label>
<input type="" name="captcha" class="form-control" id="" value="" required autocomplete="off" >
</div>



<!--
<div class="form-group col-lg-12">
<div class="checkbox">
<label><input type="checkbox" value="" id="ferba" name="ferba">I agree to the terms and condition of CalTrans.</label>
</div>
</div>
-->

<div class="col-sm-12">
* Fields required.<br>
<button type="submit" class="btn btn-primary">Register</button>
</div>

</form>

</div>

<div class="col-md-6">
<h3 class="dark-grey">Steps to follow</h3>

<div class="col-md-6">
  <center>
    <h5>For HMA-I, HMA-II and Soil & Aggregates</h5>
    <img src="img/Caltrans-Flowchart.png" width="220px">
  </center>
</div>
<div class="col-md-6">
  <center>
    <h5>For Portland Cement Concrete<br>(ACI)</h5>
    <img src="img/Caltrans-Flowchart_ACI.png" width="220px">
  </center>
</div>

<div class="col-md-12">
<br>
<p>1. In order to register for classes, you must create an account. (If you already have an account, <a href=
                                                                     "login.php" style="font-size: 15px;">LOGIN</a>.)</p>
<p>2. Select the course(s) you’d like to enroll in and take prerequisite test(s).</p>
<p>3. When you have passed the prerequisite course and are ready to check out, enter your payment information and submit your order.**</p>
<p>4. After you've submitted your order, you will receive an email with your confirmation number and other details.</p>

**You will be redirected to another website to complete your payment.<br>
</div>
</div>
</div>
</section>
</div>


    <!------------------------------ body completed -------------------------------------->

	 <!------------------------------ Footer --------------------------------------------->
<?php
    include "footer.php";
    ?>

	<!----------------------------- Footer Completed -------------------------------------->


    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
