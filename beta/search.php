<?php

    include 'dbmanage.php';

    ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CSULB | Caltrans - Joint Training & Certification Program</title>

    <!-- Favicon -->
    <!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">
	// check for window is loaded or not
	window.onload = function(){
	var myNav = document.getElementById('mymainnav');
	$(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
			$(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
		} else {
          $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
        }
      });
	}
</script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>

<script type="text/javascript">

$(document).ready(function(){
                  $("#search_btn").click(function(){
                                         var company = document.getElementById('comp').value;
                                         var firstname = document.getElementById('fn').value;
                                         var lastname = document.getElementById('ln').value;
                                         var zipcode = document.getElementById('zc').value;
                                         var city = document.getElementById('c').value;
                                         var certificate_expire = document.getElementById('calendar').value;

                                         if(certificate_expire!="")
                                         {
                                         var dateParts = certificate_expire.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
                                         certificate_expire = dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
                                         }
                                         $.ajax({
                                                url: "getsearch.php",
                                                type: "post",
                                                data: {'company' : company, 'firstname':firstname, 'lastname':lastname, 'zipcode' : zipcode, 'city' : city, 'certificate_expire':certificate_expire} ,
                                                success: function (response) {
                                                $('#showtable').empty();
                                                $('#showtable').append(response);
                                                },
                                                error: function(jqXHR, textStatus, errorThrown) {
                                                console.log(textStatus, errorThrown);
                                                }
                                                });
                                         });
                  });
</script>

</head>


<body>

<script>
$(function() {
  $( "#calendar" ).datepicker();
  });
</script>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

	<!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
			<!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
			<!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg.jpg');"></div>

            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
	<!----------------------- slider image completed ---------------------------------------->

	<!----------------------- navigation in bottom of image slider -------------------------->
	<nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav" style="width: 110%;">
                    <li>
                        <a href="index.php">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="index.php#courses">COURSES</a>
                    </li>
                    <li>
                        <a href="#" style="color: rgb(177,92,34);" >SEARCH</a>
                    </li>
                    <li>
                        <a href="help.php">FAQ</a>
                    </li>
                    <li>
                        <a href="contact.php">CONTACT</a>
                    </li>
                    <li>
                        <a href="feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                      <a href="jobs.php">JOB OPENINGS</a>
                    </li>
                    <li>
                      <a href="gallery.php">GALLERY</a>
                    </li>
					<li>
                        <a href="login.php">LOGIN</a>
                    </li>
                    <li>
                        <a href="signup.php">ENROLL</a>
                    </li>
                    <li>
                        <a href="acknowledgment.php">ACKNOWLEDGMENT</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!----------------------------------- navigation completed ---------------------------------->

	<!----------------------------------- body started ------------------------------------------>

<div class="container">
        <div id="loginbox" class="mainbox" >
            <div class="panel panel-info" style="border: none;" >
                <div class="row">
                    <div class="col-lg-12">
                        <h5><a href="http://www.csulb.edu/college-of-engineering" style="color: rgb(177,92,34);" target="_blank">College of Engineering</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="index.php" style="color: rgb(177,92,34);">JTCP</a> <span class="glyphicon glyphicon-chevron-right"></span> Search</h5>
                        <h1 class="page-header">Employee Search</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

                <div class="form-group col-lg-4">
                    <label>Company</label>
                        <select name="company" class="form-control" id="comp">
                        <option disabled selected value> -- select your company -- </option>
                        <?php
                            $ret = getCompany();
                            for($i=0;$i<count($ret);$i++)
                            {
                                $company_id = $ret[$i]['Company_ID'];
                                $company_name = $ret[$i]['Company_Name'];
                                echo "<option value='$company_id'>$company_name</option>";
                            }
                            ?>
                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <label>First Name</label>
                            <input type="" name="" class="form-control" id="fn" value="">
                        </div>

                        <div class="form-group col-lg-4">
                            <label>Last Name</label>
                            <input type="" name="" class="form-control" id="ln" value="">
                        </div>

                        <div class="form-group col-lg-4">
                            <label>Zip Code</label>
                            <input type="" name="" class="form-control" id="zc" value="">
                        </div>

                        <div class="form-group col-lg-4">
                            <label>City</label>
                            <input type="" name="" class="form-control" id="c" value="">
                        </div>

                        <div class="form-group col-lg-4">
                            <label>Certificate Expiration</label>
                            <input type="" name="" class="form-control" id="calendar" value="">
                        </div>

                        <div class="col-sm-12">
                            <button type="submit" id="search_btn" class="btn btn-primary">Search</button>
                        </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Search Result
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body" id="showtable">
                                    <!-- /.table-responsive -->
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.panel -->
                            </div>
                            <!-- /.col-lg-6 -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!------------------------------ body completed -------------------------------------->

	 <!------------------------------ Footer --------------------------------------------->
<?php
    include "footer.php";
    ?>
<!----------------------------- Footer Completed -------------------------------------->


    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
