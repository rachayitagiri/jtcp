<?php

    include '../../dbmanage.php';
    $classname = $_POST['classname'];
    $instructorid = $_POST['instructorid'];
    $coordinatorid = $_POST['coordinatorid'];
    $courseid = $_POST['coursesid'];
    $locationid = $_POST['locationid'];
    $scheduletimeto = $_POST['scheduletimeto'];
    $scheduletimefrom = $_POST['scheduletimefrom'];
    $schedule = $_POST['schedule'];

    $classcapacity = $_POST['classcapacity'];
    $classpracticalexam = $_POST['classpracticalexam'];
    $classwrittenexam = $_POST['classwrittenexam'];

    $ret = add_class($classname, $coordinatorid, $instructorid, $courseid, $locationid, $schedule, $scheduletimeto, $scheduletimefrom, $classcapacity, $classpracticalexam, $classwrittenexam);
    echo $ret['Message'];

    $coordinatorid = $_SESSION['Coordinator_ID'];
    addActivity_coordinator($coordinatorid, 'Class Added: '. $classname);
?>
