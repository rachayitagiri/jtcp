<?php
    include '../../dbmanage.php';
    
    $eid = $_POST['eid'];
    $cc = $_POST['cc'];

?>

<script>
$(document).ready(function(){
                  $(".classselect").click(function(){
                                               var cid = $(this).attr('id');
                                                var eid = document.getElementById('eid').value;
                                                var cc = document.getElementById('cc').value;
                                               $.ajax({
                                                      url: "jqScripts/selectClass.php",
                                                      type: "post",
                                                      data: {'cid' : cid, 'eid' : eid, 'cc': cc} ,
                                                      success: function (response) {
                                                      alert(response);
                                                      document.getElementById('closeclasscloseenrollmodel').click();
                                                      setTimeout(getCerti, 500);
                                                      },
                                                      error: function(jqXHR, textStatus, errorThrown) {
                                                      alert(errorThrown);
                                                      console.log(textStatus, errorThrown);
                                                      }
                                                      });
                                               });
                                             });
</script>
<input type="text" value="<?php echo $eid; ?>" id="eid" hidden>
<input type="text" value="<?php echo $cc; ?>" id="cc" hidden>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Class Name</th>
<th>Class Capacity</th>
<th>Instructor</th>
<th>Practical Exam Date</th>
<th>Written Exam Date</th>
<th>Class Dates</th>
<th>Action</th>
</tr>
</thead>
<tbody>
    <?php
        $cid = $_POST['cid'];
        $ret=getClassesByCourse($cid);
        for($i=0;$i<count($ret);$i++)
        {
    ?>
<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Class_Name'];
    ?>
</td>
<td>
<?php
    echo  $ret[$i]['Class_Filled'] . "/" .$ret[$i]['Class_Capacity'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Instructor_FirstName'] . " " . $ret[$i]['Instructor_LastName'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Class_Practical_Date'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Class_Written_Date'];
    ?>
</td>
<td>
<?php
    echo json_encode($ret[$i]['Class_Dates']);
    ?>
</td>
<td>
<?php
    
    echo "<a href='#' class='classselect' id='" . $ret[$i]['Class_ID'] . "'>Select</a>";
    ?>
</td>
</tr>
<?php
    }
    ?>
</tbody>
</table>
