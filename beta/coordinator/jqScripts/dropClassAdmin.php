<?php
    error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);
    include '../../dbmanage.php';
    $cc = $_POST['cc2'];

    //get ClassID
    $temp_ret = getClassIDfromPrerequisiteCertificate($cc);
    //Using Prerequisite Certificate Code
    $ret=dropClass($cc,TRUE);

    //Email Waitlist students to Enroll into the Class
    if($ret['Status']==1)
    {
        //Email Waitlisted Students
        include('../../mailAPI/emails.php');
        $cid = $temp_ret['Class_ID'];
        $course_name = $temp_ret['Course_Name'];
        $eid = $temp_ret['Employee_ID'];
        $class_name = $temp_ret['Class_Name'];

        //Email the students only if the checkbox is not checked i.e. no_waitlist_email is empty
        $temp_ret = getWaitlist($cid);
        if(count($temp_ret['Email_Addresses'])>0 && empty($_POST['no_waitlist_email']))
        {
            waitlist_email($temp_ret, $course_name);
        }

        $coordinatorid = $_SESSION['Coordinator_ID'];
        addActivity_coordinator($coordinatorid, 'Employee Dropped: '. $eid . 'Class: ' . $class_name);

        addActivity($eid, "Dropped class student: " . $class_name . " for Course: " . $course_name);

        $emp_details = getEmployeeDetailsByEmployeeID($eid);
        $class_details = getClass($cid);
        $email_message = 'Hello, You have been dropped from JTCP Class: ' . $class_details['Class_Name'] .' by Program Coordinator. Please contact us if you have any questions.';
        generalEmail('Class Drop Confirmation',$emp_details['FirstName'],$emp_details['LastName'],$emp_details['Email'],$email_message);

    }

    echo $ret["Message"];



?>
