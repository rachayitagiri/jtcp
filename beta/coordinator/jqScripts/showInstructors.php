<?php
    include '../../dbmanage.php';

?>

<script>
$(document).ready(function(){
                  $(".instructordelete").click(function(){
                                               var lid = $(this).attr('value');
                                               $.ajax({
                                                      url: "jqScripts/deleteInstructor.php",
                                                      type: "post",
                                                      data: {'lid' : lid} ,
                                                      success: function (response) {
                                                      showinstructors();
                                                      },
                                                      error: function(jqXHR, textStatus, errorThrown) {
                                                      alert(errorThrown);
                                                      console.log(textStatus, errorThrown);
                                                      }
                                                      });
                                               });
                  $(".instructorchange").click(function(){
                                             $('#instructormodel').modal('toggle');
                                            var lid = this.id;
                                             $.ajax({
                                                    url: "jqScripts/editInstructor.php",
                                                    type: "post",
                                                    data: {'lid' : lid} ,
                                                    success: function (response) {
                                                    $('#showinstructordata').empty();
                                                    $('#showinstructordata').append(response);
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus, errorThrown);
                                                    }
                                                    });
                                             });
});
</script>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>First Name</th>
<th>Last Name</th>
<th>Email</th>
<th>Action</th>
</tr>
</thead>
<tbody>
    <?php
        $ret=getInstructorsDetails();
        for($i=0;$i<count($ret);$i++)
        {
    ?>
<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Instructor_FirstName'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Instructor_LastName'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Login_Email'];
    ?>
</td>
<td>
<?php

    echo "<a href='javascript:void(0)' class='instructorchange' id='" . $ret[$i]['Login_ID'] . "'>Change</a>";
    echo " | ";
    echo "<a href='javascript:void(0)' class='instructordelete' value='" . $ret[$i]['Login_ID'] . "'>Delete</a>";
    ?>
</td>
</tr>
<?php
    }
    ?>
</tbody>
</table>





<!-- Modal -->
<div class="modal fade" id="instructormodel" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="userprogressmodel-tittle">Instructor Edit</h4>
</div>
<div class="modal-body">
<p id="showinstructordata">Loading...</p>
</div>
<div class="modal-footer">
<button type="button" id="closeinstructormodel" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>
