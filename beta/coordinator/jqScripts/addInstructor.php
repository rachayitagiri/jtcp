<?php

    include '../../dbmanage.php';

    $fn = $_POST['fn'];
    $ln = $_POST['ln'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $password = password_hash($password, PASSWORD_DEFAULT);
    $ret = signup_instructor($fn, $ln, $email, $password);
    echo $ret['Message'];

    $coordinatorid = $_SESSION['Coordinator_ID'];
    addActivity_coordinator($coordinatorid, 'Added Instructor: '. $email);


?>
