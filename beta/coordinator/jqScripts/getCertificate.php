<?php
include '../../dbmanage.php';
$cc = $_POST['cc'];
$validdate=0;
$ret = getCertificate($cc);
if($ret!=null)
{
    ?>

<script>
$(document).ready(function(){
                  $(".classEnroll").click(function(){
                                          $('#classenrollmodel').modal('toggle');
                                          var cid = this.id;
                                          var eid = document.getElementById('eid').value;
                                          var cc = document.getElementById('cc').value;
                                          $.ajax({
                                                 url: "jqScripts/ShowClassesSearch.php",
                                                 type: "post",
                                                 data: {'cid' : cid, 'eid' : eid, 'cc' : cc} ,
                                                 success: function (response) {
                                                 $('#showclassdata').empty();
                                                 $('#showclassdata').append(response);
                                                 },
                                                 error: function(jqXHR, textStatus, errorThrown) {
                                                 console.log(textStatus, errorThrown);
                                                 }
                                                 });
                                          });
                  });
</script>

<input type="text" value="<?php echo $ret['Employee_ID']; ?>" id="eid" hidden>
<input type="text" value="<?php echo $cc; ?>" id="cc" hidden>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Name</th>
<th>Course</th>
<th>Valid</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<tr class="odd gradeX">
<td>
<?php
    echo $ret['Employee_Name'];
    ?>
</td>
<td>
<?php
    echo  $ret['Course_Name'];
    ?>
</td>
<td>
<?php
    $today = date("Y-m-d");
    $today = strtotime($today);
    $new_date = strtotime($ret['Certificate_ExpireDate']);
    if($today>$new_date)
    {
        echo '<span class="glyphicon glyphicon-remove"></span>';
    }
    else
    {
        $validdate = 1;
        echo '<span class="glyphicon glyphicon-ok"></span>';
    }
    ?>
</td>
<td>
<?php
    if($ret['Final_Status']==1)
    {
        if($validdate==1)
        {
            echo "Certified";
        }
        else
        {
            echo "Certificate Expired";
        }
    }
    else if($validdate==1 && $ret['Course_Status']==3)
    {
        echo "Certified";
    }
    else if($validdate==1 && $ret['Course_Status']==2)
    {
        echo "Enrolled to " . $ret['Class_Name'];
        echo "<br><a href='#' class='classEnroll' id='" . $ret['Course_ID'] . "'>Change Class</a>";
    }
    else if($validdate==1 && $ret['Course_Status']==5)
    {
        echo "<a href='#' class='classEnroll' id='" . $ret['Course_ID'] . "'>Enroll</a>";
    }
    else
    {
        echo "No Actions";
    }
    ?>
</td>
</tr>
</tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="classenrollmodel" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="userprogressmodel-tittle">Enroll Employee</h4>
</div>
<div class="modal-body">
<p id="showclassdata">Loading...</p>
</div>
<div class="modal-footer">
<button type="button" id="closeclasscloseenrollmodel" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>

<?php
    }
    else
    {
        echo "<br><center>";
        echo "<h5>Invalid Certificate Code</h5></center>";
    }
?>
