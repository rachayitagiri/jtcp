<?php

    include '../../dbmanage.php';
    $eid = $_POST['eid'];
    $ret = getCourses();
?>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script>
  $(document).ready(function () {
    $('#course_enroll').change(function() {
    var cid_val = $("#course_enroll option:selected").val();
    var eid_val = document.getElementById('eid').value;
    //
    $.ajax({
      url: "jqScripts/enrollToClass.php",
      type: "post",
      data: {'cid' : cid_val, 'eid': eid_val} ,
      success: function (response) {
      $('#enrollClassShow').empty();
      $('#enrollClassShow').append(response);
      },
      error: function(jqXHR, textStatus, errorThrown) {
      console.log(textStatus, errorThrown);
      }
      });
    });
  });
</script>
</head>

<input type="text" value="<?php echo $eid; ?>" id="eid" hidden>

<label>Course</label>
  <select name="course_enroll" class="form-control" id="course_enroll">
    <option disabled selected value> -- select course -- </option>
<?php
for($i=1;$i<count($ret);$i++){
  $course_id = $ret[$i]['Course_ID'];
  $course_name = $ret[$i]['Course_Name'];
  echo "<option value='$course_id'>$course_name</option>";
}
?>
  </select>

  <div id="enrollClassShow">
  </div>
