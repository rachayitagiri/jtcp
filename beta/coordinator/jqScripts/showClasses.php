<?php
    include '../../dbmanage.php';

?>

<script>
$(document).ready(function(){
                  $(".classdelete").click(function(){
                                               var cid = $(this).attr('value');
                                               $.ajax({
                                                      url: "jqScripts/deleteClass.php",
                                                      type: "post",
                                                      data: {'cid' : cid} ,
                                                      success: function (response) {
                                                        alert(response);
                                                        showclass();
                                                      },
                                                      error: function(jqXHR, textStatus, errorThrown) {
                                                      alert(errorThrown);
                                                      console.log(textStatus, errorThrown);
                                                      }
                                                      });
                                               });
                  $(".classchange").click(function(){
                                             $('#classmodel').modal('toggle');
                                            var cid = this.id;
                                             $.ajax({
                                                    url: "jqScripts/editClass.php",
                                                    type: "post",
                                                    data: {'cid' : cid} ,
                                                    success: function (response) {
                                                    $('#showclassdata').empty();
                                                    $('#showclassdata').append(response);
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus, errorThrown);
                                                    }
                                                    });
                                             });
});
</script>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Class Name</th>
<th>Class Capacity</th>
<th>Course</th>
<th>Instructor</th>
<th>Practical Exam Date</th>
<th>Written Exam Date</th>
<th>Class Dates</th>
<th>Class Location</th>
<th>Action</th>
</tr>
</thead>
<tbody>
    <?php
        $ret=getClasses();
        for($i=0;$i<count($ret);$i++)
        {
    ?>
<tr class="odd gradeX">
<td>
<?php
    echo "ID: " . $ret[$i]['Class_ID'] . " | " . $ret[$i]['Class_Name'];
    ?>
</td>
<td>
<?php
    echo  $ret[$i]['Class_Filled'] . "/" .$ret[$i]['Class_Capacity'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Course_Name'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Instructor_FirstName'] . " " . $ret[$i]['Instructor_LastName'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Class_Practical_Date'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Class_Written_Date'];
    ?>
</td>
<td>
<?php

    $classstartingdate = $ret[$i]['Class_Dates'][0];
    for($j=0;$j<count($ret[$i]['Class_Dates']);$j++)
    {
        echo $ret[$i]['Class_Dates'][$j];
        echo " (" . $ret[$i]['Class_TimeFrom'][$j];
        echo "-" . $ret[$i]['Class_TimeTo'][$j] . ")";
        echo "<br>";
    }
?>
</td>
<td>
<?php
    echo $ret[$i]['Location_Name'];
    ?>
</td>
<td>
<?php
    $currentdate= date("Y-m-d");
    // if($classstartingdate<$currentdate){
    //   echo "<a href='javascript:void(0);' class='' id='" . $ret[$i]['Class_ID'] . "' style='color: #7f8c8d;'>Change</a>";
    // }
    // else {
      echo "<a href='javascript:void(0);' class='classchange' id='" . $ret[$i]['Class_ID'] . "'>Change</a>";
    // }

    echo " | ";
    $currentdate= date("Y-m-d");
    // if($classstartingdate<$currentdate)
    // {
    //   echo "<a href='javascript:void(0);' class='' value='" . $ret[$i]['Class_ID'] . "' style='color: #7f8c8d;'>Delete</a>";
    // }
    // else {
      echo "<a href='javascript:void(0);' class='classdelete' value='" . $ret[$i]['Class_ID'] . "'>Delete</a>";
    // }
    ?>
</td>
</tr>
<?php
    }
    ?>
</tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="classmodel" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="userprogressmodel-tittle">Class Edit</h4>
</div>
<div class="modal-body">
<p id="showclassdata">Loading...</p>
</div>
<div class="modal-footer">
<button type="button" id="closeclassmodel" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>
