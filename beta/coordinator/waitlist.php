<?php
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
    include 'template1.php';

    $cid = $_GET['cid'];
    
    if ($_POST['drop']) {
      $eid = $_POST['employee_id_drop'];
      dropFromWaitlist($cid, $eid);
    }

    if ($_POST['add']) {
      $eid = $_POST['employee_id_add'];
      addToWaitlist($cid, $eid);
    }
   
?>

<title>Waitlist</title>
<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Waitlist</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php
if(!isset($_GET['cid'])){
  echo "Please visit <a href='grade.php'>Grade</a> Page and click of View Waitlist.";
}
else{
  $cid = $_GET['cid'];
  $ret = getClass($cid);
  if($ret==null){
    echo "Invalid Class Id";
  }
  else{
    ?>
    <h4><b>Class:</b> <?php echo $ret['Class_Name'] . " / " . $ret['Course_Name']; ?></h4>
    <br>
    <?php
      $ret2 = getWaitlistByClassID($cid);

      //Add employee to Waitlist 
      echo "<form method='POST' action=''>
            <input type='text' name='employee_id_add' placeholder=' Enter the Employee ID' size='30' style='height: 32px; border-radius: 3px;'>
            <input type='submit' name='add' value='Add to Waitlist' class='btn btn-primary small'>
            </form>"
    ?>
    
    <br>
    <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Internal Employee ID</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>Email Address</th>
      </tr>
    </thead>
    <tbody>
      <?php
        for($i=0;$i<count($ret2);$i++)
        {
          //$eid = $ret2[$i]['Employee_ID'];
      ?>
      <tr>
        <td><?php echo $i+1; ?></td>
        <td><?php echo $ret2[$i]['Employee_ID']; ?></td>
        <td><?php echo $ret2[$i]['Employee_FirstName']; ?></td>
        <td><?php echo $ret2[$i]['Employee_LastName']; ?></td>
        <td><?php echo $ret2[$i]['Employee_EmailAddress']; ?></td>
        <?php 
          $eid = $ret2[$i]['Employee_ID'];
          echo "<td><form method='post' action=''>
          <input type='text' name='employee_id_drop' hidden value=".$eid.">
          <input type='submit' name='drop' value='Drop from Waitlist'/></form></td>";
        ?>
      </tr>
      <?php
        }
      ?>
    </tbody>
  </table>

<?php

  }
}
?>

<?php
    include 'template2.php';
?>
