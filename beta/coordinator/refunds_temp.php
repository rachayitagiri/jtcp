<?php
    //error_reporting(0);
    //error_reporting(E_ALL & ~E_NOTICE);
    include 'template1.php';

    if($_POST)
    {
      if($_POST['transfer'])
      {
        $cid = $_POST['Class_ID'];
        $eid = $_POST['Employee_ID'];
        $status = $_POST['Status'];
        if($status == 0){
          $status = 1;
        }
        else {
          $status = 0;
        }
        $ret = updatePaypalRefundTableTrasnferredByClasIDEmployeeID($cid,$eid);
      }
    }
?>

<title>Refunds</title>
<div class="row">
<div class="col-lg-12">
<?php
  if($_GET['show']=='all'){
?>
<h1 class="page-header">All Refunds</h1>
<?php
  }
  else{
?>
<h1 class="page-header">Pending Refunds</h1>
<?php
  }
?>
</div>
<!-- /.col-lg-12 -->
</div>
<h3> Refund Status </h3>
<ul>
  <li> 0 - Pending Refund</li>
  <li> 1 - Refunded</li>
  <li> 2 - Course Trasnferred to another Class </li>
</ul>
<!-- /.row -->
<?php
  if($_GET['show']=='all'){
    $ret2 = getPaypalRefund(true);
    echo "<a href='refunds.php'>Click here to Show Only Pending Refunds</a>";
  }
  else{
    $ret2 = getPaypalRefund(false);
    echo "<a href='refunds.php?show=all'>Click here to Show All Refunds</a>";
  }
  ?>
  <table class="table">
  <thead>
    <tr>
      <th>No</th>
      <th>Name/Email</th>
      <th>Class</th>
      <th>Amount</th>
      <th>Txt ID</th>
      <th>Timestamp</th>
      <th>Refund Status</th>
      <th>Additional Paypal Data</th>
      <th>Set Class Transfer Status</th>
    </tr>
  </thead>
  <tbody>
    <?php
      for($i=0;$i<count($ret2);$i++)
      {
        $paypal_json = json_decode($ret2[$i]['Paypal_Data'], true);
        $amount = $paypal_json['payment_gross'];
        $txtid = $paypal_json['txn_id'];

    ?>
    <form method="POST" id="data">
    <tr>
      <td><?php echo $i+1; ?></td>
      <td><?php echo $ret2[$i]['FirstName'] . " " . $ret2[$i]['LastName'] . " / " . $ret2[$i]['Email']; ?></td>
      <td><?php echo $ret2[$i]['Class_Name'] . " / " . $ret2[$i]['Course_Name']; ?></td>
      <td><?php echo $amount; ?></td>
      <td><?php echo $txtid; ?></td>
      <td><?php echo $ret2[$i]['Timestamp']; ?></td>
      <td><?php echo $ret2[$i]['Refunded']; ?></td>
      <td><?php echo $ret2[$i]['Paypal_Data']; ?></td>
      <td><?php
        echo "<input type='text' name='Employee_ID' value='" . $ret2[$i]['Employee_ID'] . "' hidden>";
        echo "<input type='text' name='Class_ID' value='" . $ret2[$i]['Class_ID'] . "' hidden>";
        /*
         * Employee refunds page with dropdown code
         * BEGIN
        */
        $ret3 = getClass($ret2[$i]['Class_ID']);
        $ret3 = getClassesByCourse($ret3['Course_ID']);
        if(count($ret3)>0){
          $select= '<select name="Class_ID_Name">';
          for($j=0;$j<count($ret3);$j++){
                $select.='<option id="classid" value="'.$ret3[$j]['Class_ID'].'">'.$ret3[$j]['Class_Name'].'</option>';
            }
        }
        $select.='</select>';
        echo $select;
        /*
         * Employee refunds page with dropdown code
         * END
        */
        echo "<input type='submit' name='transfer' value='Set Transfer'>";
        ?></td>
    </tr>
  </form>
    <?php
      }
    ?>
  </tbody>
</table>
<?php
    include 'template2.php';
?>
