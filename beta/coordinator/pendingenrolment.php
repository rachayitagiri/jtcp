<!-- This page enlists the employees whose Prereq Certificate ID is 2 but they are not enrolled to any classes. -->
<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
include 'template1.php';
?>

<!DOCTYPE html>
<html>
<body>
<title>Pending Fee</title>
<div class="row">
<div class="col-lg-12">
  <h1 class="page-header">Enrollment Status</h1>
  <h5>The following students have a prerequisite status ID equal to 2 but are not enrolled into a course:<br>
  (To double check the Roster)</h5>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<!-- Table to display the contents - details about the students for whom the fee is pending-->
<table class="table">
	<col width="60">
	<col width="100">
	<col width="100">
	<col width="100">
	<col width="100">
	<col width="100">
	<col width="100">
	<col width="100">
  <thead>
    <tr>
      <th>S. No.</th>
      <th>Name</th>
      <th>Employee ID</th>
      <th>Company</th>
      <th>Course ID</th>
      <th>Certificate ID</th>
      <th>Certificate Code</th>
      <th>Certificate Issue Date</th>
    </tr>
  </thead>
  <tbody>
  	<?php  
  		$counter = 1;
  		$ret_prereq = getPrereqCertiDataByStatusID(2);
  		for ($i=0; $i < count($ret_prereq); $i++) { 
  			//student's employee ID
  			$eid = $ret_prereq[$i]['Employee_ID'];

  			//student's employer or company name
  			$ret_company = getCompanyByEmployeeID($eid);
  			$company_name = $ret_company['Company_Name'];

  			//Get all the employee details by employee ID
  			$ret_emp = getEmployeeDetailsByEmployeeID($eid);

  			//student's prereq certi details
  			$cid = $ret_prereq[$i]['Prerequisite_Certificate_ID']; 			//prereq certi ID
  			$certistatus = $ret_prereq[$i]['Status_ID'];					//prereq certi status
  			if ($certistatus == 2) {

  				$ret_enrolment_status = checkEnrolmentInClassEmployeeByEmployeeID($eid);
  				$status = $ret_enrolment_status['Enrolment_Status'];

  				//get data to display if student is not enrolled in a class
  				if ($status == 0) {
  					
  					//student's full name
		  			$fname = $ret_emp['FirstName'];
		  			$lname = $ret_emp['LastName'];

		  			//certificate details of the employee
  					$certicode = $ret_prereq[$i]['Certificate_Code'];					//prereq certi code
	  				$courseid = $ret_prereq[$i]['Course_ID'];							//course ID
	  				$certidate = $ret_prereq[$i]['Prerequisite_Certificate_IssueDate']; //date of issue of the prereq certi
  				

  	?> 
  	<tr>
  		<td><?php echo $counter; $counter++; ?></td>
  		<td><?php echo $fname." ".$lname; ?></td>
  		<td><?php echo $eid; ?></td>
  		<td><?php echo $company_name; ?></td>
  		<td><?php echo $courseid; ?></td>
  		<td><?php echo $cid; ?></td>
  		<td><?php echo $certicode; ?></td>
  		<td><?php echo $certidate; ?></td>
  	</tr>
  	<?php
  				}
  			}
  		}
  	?>
  </tbody>
</table>

<?php
    include 'template2.php';
?>


