<?php

    if($_GET['cid']==null)
    {
        header("Location: dashboard.php");
    }
    include '../dbmanage.php';
    $cid = $_GET['cid'];
    $ret = searchEmployeesByClass($cid);

    $classname = $ret[0]['Class_Name'];

    $filename = "Roster_" . $classname ;

    function compareByFirstName($a, $b) {
    return strcmp($a["Employee_FirstName"], $b["Employee_FirstName"]);
    }
    usort($ret, 'compareByFirstName');

    $ret2 = array();
    $index = 1;
    for($i=0;$i<count($ret);$i++){
      $temp_ret = array();
      $temp_ret['No'] = $index;
      $index++;
      $temp_ret['Name'] = $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
      $temp_ret['Email'] = $ret[$i]['Login_Email'];
      $temp_ret['Mobile'] = $ret[$i]['Employee_Mobile'];
      $temp_ret['Company'] = $ret[$i]['Company_Other'];
      $temp_ret['Practical Grade'] = $ret[$i]['Course_Certificate_Practical'];
      $temp_ret['Written Grade'] = $ret[$i]['Course_Certificate_Written'];
      $temp_ret['Fees Paid'] = $ret[$i]['Fees_Paid'];

      $ret3 = getPaypalDataByClassIDEmployeeID($ret[$i]['Class_ID'],$ret[$i]['Employee_ID']);
      $paypal_data =  $ret3['Paypal_Data'];
      $json = json_decode($paypal_data, true); // decode the JSON into an associative array
      $tid = $json["txn_id"];
      $temp_ret['Transaction ID'] = $tid;
      $ret2[] = $temp_ret;
    }

    function array2csv(array &$array)
    {
      if (count($array) == 0) {
        return null;
      }
      ob_start();
      $df = fopen("php://output", 'w');
      fputcsv($df, array_keys(reset($array)));
      foreach ($array as $row) {
        fputcsv($df, $row);
      }
      fclose($df);
      return ob_get_clean();
    }

    function download_send_headers($filename) {
      // disable caching
      $now = gmdate("D, d M Y H:i:s");
      header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
      header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
      header("Last-Modified: {$now} GMT");

      // force download
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");

      // disposition / encoding on response body
      header("Content-Disposition: attachment;filename={$filename}");
      header("Content-Transfer-Encoding: binary");
    }

    download_send_headers($filename . date("Y-m-d") . ".csv");
    echo array2csv($ret2);
    die();

?>
