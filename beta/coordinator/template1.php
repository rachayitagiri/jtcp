<?php

    include '../ssl_check.php';

    include '../dbmanage.php';
    include 'check.php';

    $ret = getCoordinatorDetails($_SESSION['Login_ID']);
    $_SESSION['Coordinator_ID'] = $ret['Coordinator_ID'];
?>

<!DOCTYPE html>
<html lang="en">

<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">



<!-- Bootstrap and Font Awesome css -->
<link href="../css/font-awesome.css" rel="stylesheet">
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->


<!-- Page-Level Demo Scripts - Tables - Use for reference -->




<!-- Custom stylesheet - for your changes -->
<link href="../css/custom.css" rel="stylesheet">

<!-- CSS Animations -->
<link href="../css/animate.css" rel="stylesheet">


<!-- Mordernizr -->
<script src="../js/modernizr-2.6.2.min.js"></script>

<!-- Responsivity for older IE -->
<!--[if lt IE 9]>
<script src="../js/respond.min.js"></script>
<![endif]-->


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="../js/Kalendae-master/build/kalendae.css" type="text/css" charset="utf-8">
<script src="../js/Kalendae-master/build/kalendae.standalone.js" type="text/javascript" charset="utf-8"></script>

<script>
$( document ).ready(function() {
var loc = window.location.href;
var lis = document.getElementById("mynavbar").getElementsByTagName("a");
for(var i=0;i<lis.length;i++)
{
    if(lis[i].href==loc)
    {
                 lis[i].style.backgroundColor="rgb(238,238,238)";
    }
}
    });

</script>

<!-- Favicon -->
<link rel="shortcut icon" href="../img/favicon.ico">

</head>

<body>

    <div id="wrapper">


        <!-- Navigation -->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0; background-color: #333; opacity: 0.9;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="dashboard.php"><!--<img src="../img/logo.png" style="height: 20px; width:30px; float: left;">-->
                    <span style="float: left; margin-left: 10px; margin-right: 10px; color: white;">CalTrans Industry <span class="hidden-sm hidden-xs">- Joint Training and Certification Program</span></span>
                    <!--<img src="../img/csulb.png" style="height: 20px; width:30px; float: left;">-->
                </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right" style="color: white;">
                <!-- /.dropdown -->
                <?php
                    echo $ret['Coordinator_FirstName'] . " " . $ret['Coordinator_LastName'];
                ?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="settings.php"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse" id="mynavbar">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="employees.php"><i class="fa fa-mortar-board fa-fw"></i> Manage Students</a>
                        </li>
                        <li>
                            <a href="classes.php"><i class="fa fa-calendar fa-fw"></i> Manage Classes</a>
                        </li>
                        <li>
                            <a href="classedetails.php"><i class="fa fa-calendar fa-fw"></i> Manage Courses</a>
                        </li>
                        <li>
                            <a href="instructors.php"><i class="fa fa-user fa-fw"></i> Manage Instructors</a>
                        </li>
                        <li>
                            <a href="grade.php"><i class="fa fa-user fa-fw"></i> Class Roster/Grade/Drop Student</a>
                        </li>
                        <li>
                            <a href="pendingenrolment.php"><i class="fa fa-user fa-fw"></i> Pending Enrollment Status</a>
                        </li>
                        <li>
                            <a href="feedback.php"><i class="fa fa-comments-o fa-fw"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="courseevaluation.php"><i class="fa fa-comments-o fa-fw"></i> Course Evaluation</a>
                        </li>
                        <li>
                            <a href="transfer_requests.php"><i class="fa fa-exchange"></i> Course Transfer Requests</a>
                        </li>
                        <li>
                            <a href="transactions.php"><i class="fa fa-dollar fa-fw"></i> Transactions</a>
                        </li>
                        <li>
                            <a href="refunds.php"><i class="fa fa-dollar fa-fw"></i> Refunds</a>
                        </li>
                        <li>
                            <a href="pendingfee.php"><i class="fa fa-dollar fa-fw"></i> Pending Fee Status</a>
                        </li>
                        <li>
                            <a href="verifypaypal.php"><i class="fa fa-dollar fa-fw"></i>Verify PayPal Transactions</a>
                        </li>
                        <li>
                            <a href="settings.php"><i class="fa fa-wrench fa-fw"></i> Settings</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
        <br>
