<?php
    include 'template1.php';

    if($_POST)
    {
      if($_POST['paypal_complete']){
        $paypal_refund_id = $_POST['Paypal_Refund_ID'];
        $ret = updatePaypalRefundTableManually($paypal_refund_id);
        print_r($ret);
      }
      else if($_POST['transfer'])
      {
        $new_Class_ID = $_POST['new_Class_ID'];
        if($new_Class_ID==-999){
          echo "<script>alert('Please select Class to Transfer');</script>";
        }
        else{
          $cid = $_POST['Class_ID'];
          $eid = $_POST['Employee_ID'];
          $status = $_POST['Status'];
          if($status == 0){
            $status = 1;
          }
          else {
            $status = 0;
          }
          $ret = updatePaypalRefundTableTrasnferredByClasIDEmployeeID($cid,$eid,$new_Class_ID);
          print_r($ret);
        }
      }
    }
?>

<title>Refunds</title>
<div class="row">
<div class="col-lg-12">
<?php
  if($_GET['show']=='all'){
?>
<h1 class="page-header">All Refunds</h1>
<?php
  }
  else if($_GET['show']=='refunded'){
?>
<h1 class="page-header">Completed Refunds</h1>
<?php
  }
  else if($_GET['show']=='transferred'){
?>
<h1 class="page-header">Transferred Refunds</h1>
<?php
  }
  else{
?>
<h1 class="page-header">Pending Refunds</h1>
<?php
  }
?>
</div>
<!-- /.col-lg-12 -->
</div>
<h3> Refund Status </h3>
<ul>
  <li> 0 - Pending Refund</li>
  <li> 1 - Refunded</li>
  <li> 2 - Course Trasnferred to another Class </li>
</ul>
<!-- /.row -->
<?php
  if($_GET['show']=='all'){
    $ret2 = getPaypalRefund(3);
  }
  else if($_GET['show']=='refunded'){
    $ret2 = getPaypalRefund(1);
  }
  else if($_GET['show']=='transferred'){
    $ret2 = getPaypalRefund(2);
  }
  else{
    $ret2 = getPaypalRefund(0);
  }
  echo "<a href='refunds.php?'>Click here to Show Only Pending Refunds</a>";
  echo "<br>";
  echo "<a href='refunds.php?show=refunded'>Click here to Show Only Completed Refunds</a>";
  echo "<br>";
  echo "<a href='refunds.php?show=all'>Click here to Show all Refunds</a>";
  echo "<br>";
  echo "<a href='refunds.php?show=transferred'>Click here to Show Only Transferred Refunds</a>";
  echo "<br>";

  ?>
  <table class="table">
  <thead>
    <tr>
      <th>No</th>
      <th>Name/Email</th>
      <th>Class</th>
      <th>Amount</th>
      <th>Txt ID</th>
      <th>Timestamp</th>
      <th>Refund Status</th>
      <th>Additional Paypal Data</th>
      <th>Set Class Transfer Status</th>
      <th>Set Manual Refund</th>
    </tr>
  </thead>
  <tbody>
    <?php
      for($i=0;$i<count($ret2);$i++)
      {
        $paypal_json = json_decode($ret2[$i]['Paypal_Data'], true);
        $amount = $paypal_json['payment_gross'];
        $txtid = $paypal_json['txn_id'];

    ?>
    <form method="POST">
    <tr>
      <td><?php echo $i+1 . ' / ' . 'Refund_ID:' . $ret2[$i]['Paypal_Refund_ID']; ?></td>
      <td><?php echo $ret2[$i]['FirstName'] . " " . $ret2[$i]['LastName'] . " / " . $ret2[$i]['Email']; ?></td>
      <td><?php echo $ret2[$i]['Class_Name'] . " / " . $ret2[$i]['Course_Name']; ?></td>
      <td><?php echo $amount; ?></td>
      <td><?php echo $txtid; ?></td>
      <td><?php echo $ret2[$i]['Timestamp']; ?></td>
      <td><?php echo $ret2[$i]['Refunded']; ?></td>
      <td><?php echo $ret2[$i]['Paypal_Data']; ?></td>
      <td><?php
        echo "<input type='text' name='Employee_ID' value='" . $ret2[$i]['Employee_ID'] . "' hidden >";
        echo "<input type='text' name='Class_ID' value='" . $ret2[$i]['Class_ID'] . "' hidden >";
        /*
         * Employee refunds page with dropdown code
         * BEGIN
        */
        $ret3 = getClass($ret2[$i]['Class_ID']);
        $ret3 = getClassesByCourse($ret3['Course_ID']);
        if(count($ret3)>0){
          $select= '<select name="new_Class_ID" required>';
          $select.='<option id="classid" value="-999" selected="true">Select Option</option>';
          for($j=0;$j<count($ret3);$j++){
                $select.='<option id="classid" value="'.$ret3[$j]['Class_ID'].'">'.$ret3[$j]['Class_Name'].'</option>';
            }
            $select.='<option id="classid" value="0">Transfer to another student</option>';
        }
        $select.='</select>';
        echo $select;
        /*
         * Employee refunds page with dropdown code
         * END
        */
        echo "<input type='submit' name='transfer' value='Set Transfer'>";
        ?></td>
        <td><?php
          echo "<input type='text' name='Paypal_Refund_ID' value='" . $ret2[$i]['Paypal_Refund_ID'] . "' hidden >";
          echo "<input type='submit' name='paypal_complete' value='Set Complete'>";
          ?>
        </td>
    </tr>
  </form>
    <?php
      }
    ?>
  </tbody>
</table>



<?php
    include 'template2.php';
?>
