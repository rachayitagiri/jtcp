<?php
    include 'template1.php';
?>

<title>Payments</title>
<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Payments</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php
if(!isset($_GET['eid'])){
  echo "Please visit <a href='employees.php'>Manage Students</a> Page and click of View Payments to view payment details.";
}
else{
  $eid = $_GET['eid'];
  $ret = getEmployeeDetailsByEmployeeID($eid);
  if($ret==null){
    echo "Invalid Employee Id";
  }
  else{
    echo "Name: " . $ret['FirstName'] . " " . $ret['LastName'];
    echo "<br>";
    echo "Email: " . $ret['Email'];
    echo "<br>";
    echo "Phone: " . $ret['Mobile'];
    echo "<hr>";

    $ret2 = getAllPaypalDataByEmployeID($eid);

    ?>

    <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Class</th>
        <th>Amount</th>
        <th>Timestamp</th>
        <th>Internal Paypal ID</th>
        <th>Additional Paypal Data</th>
      </tr>
    </thead>
    <tbody>
      <?php
        for($i=0;$i<count($ret2);$i++)
        {
          $ret3 = getClass($ret2[$i]['Class_ID']);
          $paypal_json = json_decode($ret2[$i]['Paypal_Data'], true);
          $amount = $paypal_json['payment_gross'];

      ?>
      <tr>
        <td><?php echo $i+1; ?></td>
        <td><?php echo $ret3['Class_Name'] . " / " . $ret3['Course_Name']; ?></td>
        <td><?php echo $amount; ?></td>
        <td><?php echo $ret2[$i]['Timestamp']; ?></td>
        <td><?php echo $ret2[$i]['pid']; ?></td>
        <td><?php echo $ret2[$i]['Paypal_Data']; ?></td>
      </tr>
      <?php
        }
      ?>
    </tbody>
  </table>

    <?php

  }
}

?>



<?php
    include 'template2.php';
?>
