<!-- This page enlists the non-Caltrans employees who haven't paid the fees yet-->
<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
include 'template1.php';
?>

<!DOCTYPE html>
<html>
<body>
<title>Pending Fee</title>
<div class="row">
<div class="col-lg-12">
  <h1 class="page-header">Pending Fee Status - non-Caltrans</h1>
  <h5>The fee for the following students is still pending:</h5><br>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<!-- Table to display the contents - details about the students for whom the fee is pending-->
<table class="table">
  <thead>
    <tr>
      <th>S. No.</th>
      <th>Name</th>
      <th>Employee ID</th>
      <th>Class ID</th>
      <th>Class Name</th>
      <th>Employer</th>
    </tr>
  </thead>
  <tbody>
  	<?php 
  		$counter = 1;
  		$ret_class_emp = getClassEmployeeData();
  		for ($i=0; $i < count($ret_class_emp); $i++) { 
  			//student's employee ID
  			$eid = $ret_class_emp[$i]['Employee_ID'];		

  			//student's class ID
  			$cid = $ret_class_emp[$i]['Class_ID'];

        //student's class name
        $class = getClass($cid);
        $classname = $class['Class_Name'];

  			//Get all the employee details by employee ID
  			$ret_emp = getEmployeeDetailsByEmployeeID($eid);

  			//student's full name
  			$fname = $ret_emp['FirstName'];
  			$lname = $ret_emp['LastName'];

  			//student's employer
  			$company_name = $ret_emp['Company_Other'];

  			//check if company is Caltrans or not
  			if ((stripos($company_name, "Caltrans") === false) AND 
            (stripos($company_name, "CA Dept of Transportation") === false) AND 
            (stripos($company_name, "State of California") === false) ) {

  				//get data from the Paypal table
  				$ret_paypal = getAllPaypalDataByEmployeID($eid);

          //if there is no corresponding entry in the Paypal table, display the row
  				if(!$ret_paypal) {
  			
  	?>
  	<tr>
  		<td><?php echo $counter; $counter++?></td>
  		<td><?php echo $fname." ".$lname; ?></td>
  		<td><?php echo $eid; ?></td>
  		<td><?php echo $cid; ?></td>
      <td><?php echo $classname; ?></td>
  		<td><?php echo $company_name; ?></td>
  	</tr>
  	<?php	
  				}
  			}
  		}
  	?>
  </tbody>
</table>
<?php
    include 'template2.php';
?>

<?php
	
/*
 * Steps to follow:
 * 1. Get all the enrolled students for every class
 * 2. For each student:
 * 3. 	Check the employer
 * 4. 		If (Caltrans)	ignore;
 * 5. 		Else
 * 6.			Check the entry in Paypal Table
 * 7.			If (exists)	ignore;
 * 8. 			Else display on the page;
 */

?>