<?php
    include 'template1.php';
    
?>


<script>
$(document).ready(function(){
                  showinstructors();
                    $("#addinstructor").click(function(){
                                                var fn = document.getElementById('fn').value;
                                                var ln = document.getElementById('ln').value;
                                                var email = document.getElementById('email').value;
                                                var password = document.getElementById('password').value;
                                               $.ajax({
                                                      url: "jqScripts/addInstructor.php",
                                                      type: "post",
                                                      data: {'fn' : fn, 'ln' : ln, 'email' : email, 'password' : password},
                                                      success: function (response) {
                                                      showinstructors();
                                                      },
                                                      error: function(jqXHR, textStatus, errorThrown) {
                                                      alert(errorThrown);
                                                      console.log(textStatus, errorThrown);
                                                      }
                                                });
                                });
                  });
function showinstructors()
{
    $.ajax({
           url: "jqScripts/showInstructors.php",
           type: "post",
           data: {} ,
           success: function (response) {
           $('#showtable_instructors').empty();
           $('#showtable_instructors').append(response);
           },
           error: function(jqXHR, textStatus, errorThrown) {
           alert(errorThrown);
           console.log(textStatus, errorThrown);
           }
           });
}
</script>
<title>Manage Instructors</title>

<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Manage Instructors</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="form-group col-lg-6">
<label>Email Address</label>
<input type="" name="email" class="form-control" id="email" value="">
</div>

<div class="form-group col-lg-6">
<label>Temporary Password</label>
<input type="" name="password" class="form-control" id="password" value="">
</div>

<div class="form-group col-lg-6">
<label>First Name</label>
<input type="" name="fn" class="form-control" id="fn" value="">
</div>

<div class="form-group col-lg-6">
<label>Last Name</label>
<input type="" name="ln" class="form-control" id="ln" value="">
</div>

<div class="col-sm-12">
<input type="button" id="addinstructor" class="btn btn-primary" name="addinstructor" value="Add Instructor">
</div>


<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
Instructors
</div>
<!-- /.panel-heading -->
<div class="panel-body" id="showtable_instructors">
<!-- /.table-responsive -->

</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->


<?php
    include 'template2.php';
?>
