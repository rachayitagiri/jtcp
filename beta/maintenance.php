<script type="text/javascript">
    $(window).on('load',function(){
        $('#maintenance').modal('show');
    });
</script>
<!-- JTCP website and education portal is developed by Rushi Jash at CSULB -->

<!-- Under Maintenance -->
<!-- Modal -->
<div id="maintenance" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Warning: Website is Under Maintenance</h4>
      </div>
      <div class="modal-body">
            <p class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >The website is under Maintenance.
            </p>
            <p class="text-large text-thin"  style="font-size: 0.9em; line-height: 170%;" >
            For any questions regarding enrollment, please email: shadi.saadeh@csulb.edu or call on: (562) 985-4147</p>
    </div>

  </div>
</div>
