<!-- This page displays all the photos in the gallery -->
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<title>CSULB | Caltrans - Joint Training & Certification Program</title>

<!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- add loto fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">
    // check for window is loaded or not
    window.onload = function(){
    var myNav = document.getElementById('mymainnav');
    $(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
            $(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
        } else {
          $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
        }
      });
    }
</script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
</head>


<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

    <!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg.jpg');"></div>

            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
    <!----------------------- slider image completed ---------------------------------------->

    <!----------------------- navigation in bottom of image slider -------------------------->
    <nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav" style="width: 110%;">
                    <li>
                        <a href="index.php">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="index.php#courses">COURSES</a>
                    </li>
                    <li>
                        <a href="search.php" >SEARCH</a>
                    </li>
                    <li>
                        <a href="help.php" >FAQ</a>
                    </li>
                    <li>
                        <a href="contact.php" >CONTACT</a>
                    </li>
                    <li>
                        <a href="feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                      <a href="jobs.php">JOB OPENINGS</a>
                    </li>
                    <li>
                      <a href="#" style="color: rgb(177,92,34);" >GALLERY</a>
                    </li>
                    <li>
                        <a href="login.php">LOGIN</a>
                    </li>
                    <li>
                        <a href="signup.php">ENROLL</a>
                    </li>
                    <li>
                        <a href="acknowledgment.php">ACKNOWLEDGMENT</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!----------------------------------- navigation completed ---------------------------------->

    <!----------------------------------- body started ------------------------------------------>

<!-- Container starts -->
<div class="container">
<h5>
<a href="http://www.csulb.edu/college-of-engineering" style="color: rgb(177,92,34);" target="_blank" >College of Engineering</a> 
<span class="glyphicon glyphicon-chevron-right"></span> 
<a href="index.php" style="color: rgb(177,92,34);">JTCP</a> 
<span class="glyphicon glyphicon-chevron-right"></span> 
Gallery </h5>
<h2>Gallery</h2>
<br>
<!-- New row starts -->
<div class="row">
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/HMA2_C4_CSULB_20180607.jpeg" target="_blank">
    <img src="img/class-photos/HMA2_C4_CSULB_20180607.jpeg" alt="HMA2_C4_CSULB" style="width:100%">
    <div class="caption">
      <p>HMA2_C4 at CSU Long Beach (June 07, 2018)</p>
    </div>
  </a>
</div>
</div>
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/HMA1_C5_CSULB_20180614.jpg" target="_blank">
    <img src="img/class-photos/HMA1_C5_CSULB_20180614.jpg" alt="HMA1_C5_CSULB" style="width:100%">
    <div class="caption">
      <p>HMA1_C5 at CSU Long Beach (June 14, 2018)</p>
    </div>
  </a>
</div>
</div>
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/HMA2_C5_CSULB_20180627_resize.jpg" target="_blank">
    <img src="img/class-photos/HMA2_C5_CSULB_20180627_resize.jpg" alt="HMA2_C5_CSULB" style="width:100%">
    <div class="caption">
      <p>HMA2_C5 at CSU Long Beach (June 27, 2018)</p>
    </div>
  </a>
</div>
</div>
</div> <!-- Row ends-->

<!-- New row starts -->
<div class="row">
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/HMA1_C11_SJSU_20180504.JPG" target="_blank">
    <img src="img/class-photos/HMA1_C11_SJSU_20180504.JPG" alt="HMA1_SJSU" style="width:100%">
    <div class="caption">
      <p>HMA1_C11 at San Jose State University (May 04, 2018)</p>
    </div>
  </a>
</div>
</div>
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/HMA2_C20_SJSU_20180525.JPG" target="_blank">
    <img src="img/class-photos/HMA2_C20_SJSU_20180525.JPG" alt="HMA2_SJSU" style="width:100%">
    <div class="caption">
      <p>HMA2_C20 at San Jose State University (May 25, 2018)</p>
    </div>
  </a>
</div>
</div>
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/HMA2_C19_SJSU_20180419.JPG" target="_blank">
    <img src="img/class-photos/HMA2_C19_SJSU_20180419.JPG" alt="HMA2_SJSU" style="width:100%">
    <div class="caption">
      <p>HMA2_C19 at San Jose State University (April 19, 2018)</p>
    </div>
  </a>
</div>
</div>
</div> <!-- Row ends-->

<!-- New row starts -->
<div class="row">
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/HMA1_20180524_152218_resize.jpg" target="_blank">
    <img src="img/class-photos/HMA1_20180524_152218_resize.jpg" alt="HMA1_CSULB" style="width:100%;">
    <div class="caption">
      <p>HMA_1 at CSU Long Beach (May 24, 2018)</p>
    </div>
  </a>
</div>
</div>
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/HMA2_C6_CSULB_20180717.jpg" target="_blank">
    <img src="img/class-photos/HMA2_C6_CSULB_20180717.jpg" alt="HMA2_CSULB" style="width:100%">
    <div class="caption">
      <p>HMA2_C6 at CSU Long Beach (July 17, 2018)</p>
    </div>
  </a>
</div>
</div>
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/HMA2_C7_CSULB_20180810.jpg" target="_blank">
    <img src="img/class-photos/HMA2_C7_CSULB_20180810.jpg" alt="HMA2_CSULB" style="width:100%">
    <div class="caption">
      <p>HMA2_C7 at CSU Long Beach (August 7, 2018)</p>
    </div>
  </a>
</div>
</div>
</div> <!-- Row ends-->

<!-- New row starts -->
<div class="row">
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/SA_C6_CSULB_20180621.jpg" target="_blank">
    <img src="img/class-photos/SA_C6_CSULB_20180621.jpg" alt="SA_CSULB" style="width:100%;">
    <div class="caption">
      <p>SA_C6 at CSU Long Beach (June 21, 2018)</p>
    </div>
  </a>
</div>
</div>
<div class="col-md-4">
<div class="thumbnail">
  <a href="img/class-photos/SA_C7_CSULB_20180802.jpg" target="_blank">
    <img src="img/class-photos/SA_C7_CSULB_20180802.jpg" alt="SA_CSULB" style="width:100%">
    <div class="caption">
      <p>SA_C7 at CSU Long Beach (August 2, 2018)</p>
    </div>
  </a>
</div>
</div>
</div> <!-- Row ends-->

<!-- container end-->
</div>

<br>
<br>
<br>
<br>
<br>
</body>
</html>
