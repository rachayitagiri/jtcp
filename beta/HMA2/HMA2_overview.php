<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<title>CSULB | Caltrans - Joint Training & Certification Program</title>

<!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">

	// check for window is loaded or not
	window.onload = function(){
	var myNav = document.getElementById('mymainnav');
	$(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 100) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#000000"); // if yes, then change the color
			$(".navbar-fixed-top").css("opacity", "0.75"); //add opacity
		} else {
          $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
        }
      });
	}
</script>


</head>


<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="../img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">College of Engineering</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

	<!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
			<!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
			<!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('../img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../img/bg.jpg');"></div>

            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
	<!----------------------- slider image completed ---------------------------------------->

	<!----------------------- navigation in bottom of image slider -------------------------->
	<nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav">
                    <li>
                        <a href="../index.php">JTCP HOME</a>
                    </li>
                    <li>
                        <a href="../index.php#courses" style="color: rgb(177,92,34);">COURSES</a>
                    </li>
                    <li>
                        <a href="../search.php">SEARCH</a>
                    </li>
                    <li>
                        <a href="../help.php">FAQ</a>
                    </li>
                    <li>
                        <a href="../contact.php">CONTACT</a>
                    </li>
                    <li>
                        <a href="../feedback.php">FEEDBACK</a>
                    </li>
                    <li>
                      <a href="../jobs.php">JOB OPENINGS</a>
                    </li>
					          <li>
                        <a href="../login.php">LOGIN</a>
                    </li>
                    <li>
                        <a href="../signup.php">ENROLL</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!----------------------------------- navigation completed ---------------------------------->

	<!----------------------------------- body started ------------------------------------------>

<div class="container">
<h5><a href="http://www.csulb.edu/college-of-engineering" style="color: rgb(177,92,34);" target="_blank" >College of Engineering</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="../index.php" style="color: rgb(177,92,34);">JTCP</a> <span class="glyphicon glyphicon-chevron-right"></span> <a href="../index.php#courses" style="color: rgb(177,92,34);">Courses</a> <span class="glyphicon glyphicon-chevron-right"></span> HOT MIX ASPHALT II</h5>
<div class="col-md-2">
<h2>Menu</h2>
<ul style="list-style: none; font-size: 16px; margin: 0; padding: 0;">
<li style="margin-bottom: 7px;" ><a href="" >Overview</a></li>
<li style="margin-bottom: 7px;" ><a href="HMA2_schedule_fees.php" style="color: rgb(76,76,76);" >Schedule, Fees, and Registration</a></li>
<li style="margin-bottom: 7px;" ><a href="HMA2_instructors.php" style="color: rgb(76,76,76);" >Instructors</a></li>
</ul>
<hr>
</div>
<div class="col-md-10">
<!-- Data Started -->
<h2>HOT MIX ASPHALT II</h2>

<p>The Joint Training and Certification Program (JTCP) has developed a two-part series certification program in Hot Mix Asphalt (HMA) for construction materials lab technicians. To help reduce conflict and delay on construction projects within the state, Caltrans, in partnership with CSULB, has developed a series of joint training and certification programs.
</p>
<p>
Participants in Hot Mix Asphalt II will receive training and certification in various AASHTO methods pertaining to HMA testing in the field. This session is for the testing associated with asphalt mix quality and acceptance.
</p>
<!--HMA I must be completed before taking HMA II.-->
<br>

<hr>
<h4 data-toggle="collapse" data-target="#wsa"><a href="javascript:void();">Who Should Attend</a></h4>

<div id="wsa" class="collapse">
<p>This program has been developed for anyone seeking a basic understanding of HMA pavements. It is intended to focus on areas critical to the successful completion of an asphalt pavement.</p>
</div>
<hr>
<h4 data-toggle="collapse" data-target="#ctp2"><a href="javascript:void();">Completing the Prerequisite</a></h4>
<div id="ctp2" class="collapse">
<p>To complete the prerequisite students must review provided resource material, view course videos, and pass an assessment quiz. Students <b><u>must</u></b> successfully pass the prerequisite course before registering in the Training & Lab course.</p>
    <br>
</div>
<hr>
<h4 data-toggle="collapse" data-target="#ctp"><a href="javascript:void();">Completing the Program</a></h4>
<div id="ctp" class="collapse">
<p>This certificate program consists of completing an online prerequisite, in-class training, and a lab.</p>

<p><font color="#C70039">The medium of instruction and language of the course will be English.</font></p>

<p>The timely delivery of transportation improvement projects requires highly skilled, knowledgeable materials sampling and testing technicians. To help reduce conflict and delay on construction projects within the state, the California Department of Transportation (Caltrans), in partnership with CSULB, has developed a joint training and certification program in HMA testing for construction materials lab technicians. This program is the second in a two-part series.
The goal of this program is to ensure that all materials testers — both in the public and private sector — possess equal knowledge and skill in performing lab tests. This will, in turn, help ensure consistent testing, promote quality improvement, and foster positive working relationships between public and private sector employees.</p>

<p>Participants in this program will receive training and certification in various AASHTO methods pertaining to HMA testing in the field.</p>
<p><font color="#C70039">Note: In accordance with the IA Manual Amendment, “Testers must attend all training days for the HMA I, HMA II, and/or S&amp;A modules prior to receiving a JTCP Certificate of Training Completion for the module(s) and taking the associated exams.”</font></p>
</div>
<hr>
<h4 data-toggle="collapse" data-target="#ti"><a href="javascript:void();">Topics Include</a></h4>
<div id="ti" class="collapse">
<ul>
<li>AASHTO T166: Standard Method of Test for Bulk Specific Gravity (Gmb) of Compacted HMA Using Saturated Surface-Dry Method</li>
<li>AASHTO T209: Standard Method of Test for Theoretical Maximum Specific Gravity (Gmm) and Density of HMA (Method A)</li>
<li>AASHTO T269: Standard Method of Test for Percent Air Voids in Compacted Dense and Open Asphalt Mixtures Synopsis</li>
<li>AASHTO T275: Standard Method of Test for Bulk Specific Gravity (Gmb) of Compacted HMA Using Paraffin-Coated Specimens Synopsis</li>
<li>AASHTO T308: Standard Method of Test for Determining the Asphalt Binder Content of HMA by the Ignition Method (Method A)</li>
</ul>

</div>
<hr>
<h4 data-toggle="collapse" data-target="#ucott"><a href="javascript:void();">Upon Completion of this Training, Participants will Be Able to</a></h4>
<div id="ucott" class="collapse">
<ul>
<li>Recognize the safety issues associated with each test</li>
<li>Become familiar with HMA mixtures terminology</li>
<li>Recognize the significant impact of temperature and rate of loading on the properties of HMA mixtures</li>
<li>Appreciate the importance of proper calibration of laboratory equipment to maintain the required temperature and rate of loading</li>
<li>Differentiate between test method precision and bias</li>
<li>Recognize relationships between mix design parameters and HMA performance</li>
<li>Recognize the role of QA/QC in the field management process</li>
<li>Recognize the fundamental relationships between weights and volumes of aggregates and HMA mixtures</li>
<li>Define HMA mixture volumetric parameters and relationships among them</li>
<li>Measure the various weights and volumes of the compacted HMA mix</li>
<li>Calculate the bulk specific gravity of the compacted HMA mix</li>
<li>Measure the various weights and volumes of the un-compacted HMA mix</li>
<li>Calculate the maximum specific gravity of the un-compacted HMA mix</li>
<li>Calculate the percent air voids in the compacted asphalt mix</li>
<li>Measure the various asphalt binder content of field-produced HMA mix</li>
<li>Adjust the measured asphalt binder based on the specific conditions of the ignition oven and HMA mix</li>
</ul>
</div>
<hr>

<!-- Data End -->
</div>

</div>


    <!------------------------------ body completed -------------------------------------->

	 <!------------------------------ Footer --------------------------------------------->

<?php
    include "../footer.php";
    ?>

	<!----------------------------- Footer Completed -------------------------------------->


    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
