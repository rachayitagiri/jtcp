<?php
    /**
     * This call sends an email to one recipient, using a validated sender address
     * Do not forget to update the sender address used in the sample
     */
    include("mailjet-apiv3-php-no-composer-master/vendor/autoload.php");

    $apiKey = "53c7ca762e72b62659344ef3a20736ef";
    $secretKey = "600f75304db7997cb5ca575cf52e12a7";


    use \Mailjet\Resources;

    // use your saved credentials
    $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');


    function generalEmail($subject, $firstname, $lastname, $emailaddress, $message)
    {
        $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');
        $fullname = $firstname . " " . $lastname;
        $body = [
        'FromEmail' => "csulbjtcp@gmail.com",
        'FromName' => "JTCP",
        'Subject' => $subject,
        'Text-part' => "Dear " . $fullname . ", " . $message,
        'Html-part' => '
        <!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
        <title>Verify</title>
        <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <style type="text/css">
            #outlook a { padding: 0; }
            .ReadMsgBody { width: 100%; }
        .ExternalClass { width: 100%; }
        .ExternalClass * { line-height:100%; }
        body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
        p { display: block; margin: 13px 0; }
        </style>
        <!--[if !mso]><!-->
            <style type="text/css">
            @media only screen and (max-width:480px) {
                @-ms-viewport { width:320px; }
                @viewport { width:320px; }
            }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if lte mso 11]>
                <style type="text/css">
                .outlook-group-fix {
                width:100% !important;
                }
        </style>
        <![endif]-->
        <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-66 { width:66.66666666666666%!important; }
            .mj-column-per-33 { width:33.33333333333333%!important; }
            .mj-column-per-100 { width:100%!important; }
            .mj-column-per-50 { width:50%!important; }
        }
        </style>
        </head>
        <body style="background: #F4F4F4;">
        <div style="background-color:#F4F4F4;"><!--[if mso | IE]>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
            <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
            <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:396px;">
                <![endif]--><div class="mj-column-per-66 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 0px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:left;"><p style="margin: 10px 0;"></p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                    </td><td style="vertical-align:top;width:198px;">
                    <![endif]--><div class="mj-column-per-33 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 0px;padding-top:0px;padding-bottom:0px;" align="right"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:right;"></div></td></tr></tbody></table></div><!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                            </td></tr></table>
                            <![endif]-->
                            <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:0px;padding-top:0px;"><!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:600px;"><a href="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" target="_blank"><img alt="" title="" height="auto" src="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="600"></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                        </td></tr></table>
                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                            </td></tr></table>
                                            <![endif]-->
                                            <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                <![endif]--><div style="margin:0px auto;max-width:600px;background:#ffffff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#ffffff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><h2>Joint Training & Certification Program</h2></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p style="margin: 10px 0;">Hello ' . $fullname .  ',</p><p style="margin: 10px 0;">'.$message.'</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                        </td></tr></table>
                                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                            </td></tr></table>
                                                            <![endif]-->
                                                            <!--[if mso | IE]>
                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                <tr>
                                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:300px;">
                                                                    <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:105px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvuu.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="105"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                        </td><td style="vertical-align:top;width:300px;">
                                                                        <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:98px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvu4.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="98"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                            </td></tr></table>
                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                </td></tr></table>
                                                                                <![endif]-->
                                                                                <!--[if mso | IE]>
                                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                                    <tr>
                                                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                                    <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                                                        <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">This e-mail has been sent to ' . $emailaddress . ', <a href="[[UNSUB_LINK_EN]]" style="color:inherit;text-decoration:none;" target="_blank" >click here to unsubscribe</a>.</p></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">US</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                            </td></tr></table>
                                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                                </td></tr></table>
                                                                                                <![endif]--></div>
                                                                                                </body>
                                                                                                </html>
                                                                                                ',
                                                                                                'Recipients' => [['Email' => $emailaddress]]
                                                                                                ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response;
    }

    function welcome_email($firstname,$lastname,$emailaddress)
    {
        $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');
        $fullname = $firstname . " " . $lastname;
        $body = [
        'FromEmail' => "csulbjtcp@gmail.com",
        'FromName' => "JTCP",
        'Subject' => "Welcome to Joint Training & Certification Program!",
        'Text-part' => "Dear " . $fullname . ", welcome to JTCP Program! Now you can Take Safety Quiz, Download prerequisite exam materials, Pass prerequisites, Enroll to the course and Get certified.",
        'Html-part' => '
        <!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
        <title>Verify</title>
        <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <style type="text/css">
            #outlook a { padding: 0; }
            .ReadMsgBody { width: 100%; }
        .ExternalClass { width: 100%; }
        .ExternalClass * { line-height:100%; }
        body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
        p { display: block; margin: 13px 0; }
        </style>
        <!--[if !mso]><!-->
            <style type="text/css">
            @media only screen and (max-width:480px) {
                @-ms-viewport { width:320px; }
                @viewport { width:320px; }
            }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if lte mso 11]>
                <style type="text/css">
                .outlook-group-fix {
                width:100% !important;
                }
        </style>
        <![endif]-->
        <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-66 { width:66.66666666666666%!important; }
            .mj-column-per-33 { width:33.33333333333333%!important; }
            .mj-column-per-100 { width:100%!important; }
            .mj-column-per-50 { width:50%!important; }
        }
        </style>
        </head>
        <body style="background: #F4F4F4;">
        <div style="background-color:#F4F4F4;"><!--[if mso | IE]>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
            <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
            <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:396px;">
                <![endif]--><div class="mj-column-per-66 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 0px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:left;"><p style="margin: 10px 0;"></p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                    </td><td style="vertical-align:top;width:198px;">
                    <![endif]--><div class="mj-column-per-33 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 0px;padding-top:0px;padding-bottom:0px;" align="right"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:right;"></div></td></tr></tbody></table></div><!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                            </td></tr></table>
                            <![endif]-->
                            <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:0px;padding-top:0px;"><!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:600px;"><a href="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" target="_blank"><img alt="" title="" height="auto" src="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="600"></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                        </td></tr></table>
                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                            </td></tr></table>
                                            <![endif]-->
                                            <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                <![endif]--><div style="margin:0px auto;max-width:600px;background:#ffffff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#ffffff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><h2>Welcome to Joint Training & Certification Program</h2></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p style="margin: 10px 0;">Hello ' . $fullname .  ',</p><p style="margin: 10px 0;">Welcome to Joint Training & Certification Program by Caltrans and CSULB.</p><p style="margin: 10px 0;">Click here to <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/login.php">Login</a> to your account to</p><ol><li style="margin: 10px 0px;">Take safety quiz.</li><li style="margin: 10px 0px;">Download prerequisite exam materials.</li><li style="margin: 10px 0px;">Pass prerequisites.</li><li style="margin: 10px 0px;">Enroll to the course.</li><li style="margin: 10px 0px;">Get certified. </li></ol><p style="margin: 10px 0;">For any queries and help feel free to contact us <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/contact.php">here</a>.</p><p style="margin: 10px 0;">Thanks,</p><p style="margin: 10px 0;">JTCP Program.</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                        </td></tr></table>
                                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                            </td></tr></table>
                                                            <![endif]-->
                                                            <!--[if mso | IE]>
                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                <tr>
                                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:300px;">
                                                                    <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:105px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvuu.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="105"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                        </td><td style="vertical-align:top;width:300px;">
                                                                        <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:98px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvu4.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="98"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                            </td></tr></table>
                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                </td></tr></table>
                                                                                <![endif]-->
                                                                                <!--[if mso | IE]>
                                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                                    <tr>
                                                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                                    <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                                                        <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">This e-mail has been sent to ' . $emailaddress . ', <a href="[[UNSUB_LINK_EN]]" style="color:inherit;text-decoration:none;" target="_blank" >click here to unsubscribe</a>.</p></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">US</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                            </td></tr></table>
                                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                                </td></tr></table>
                                                                                                <![endif]--></div>
                                                                                                </body>
                                                                                                </html>
                                                                                                ',
                                                                                                'Recipients' => [['Email' => $emailaddress]]
                                                                                                ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response;
    }

    function welcome_email_byCoordinator($firstname,$lastname,$emailaddress, $password_temp)
    {
        $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');
        $fullname = $firstname . " " . $lastname;
        $body = [
        'FromEmail' => "csulbjtcp@gmail.com",
        'FromName' => "JTCP",
        'Subject' => "Welcome to Joint Training & Certification Program!",
        'Text-part' => "Dear " . $fullname . ", welcome to JTCP Program! You have been added to JTCP by Program Coordinator. Now you can Take Safety Quiz, Download prerequisite exam materials, Pass prerequisites, Enroll to the course and Get certified.",
        'Html-part' => '
        <!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
        <title>Verify</title>
        <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <style type="text/css">
            #outlook a { padding: 0; }
            .ReadMsgBody { width: 100%; }
        .ExternalClass { width: 100%; }
        .ExternalClass * { line-height:100%; }
        body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
        p { display: block; margin: 13px 0; }
        </style>
        <!--[if !mso]><!-->
            <style type="text/css">
            @media only screen and (max-width:480px) {
                @-ms-viewport { width:320px; }
                @viewport { width:320px; }
            }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if lte mso 11]>
                <style type="text/css">
                .outlook-group-fix {
                width:100% !important;
                }
        </style>
        <![endif]-->
        <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-66 { width:66.66666666666666%!important; }
            .mj-column-per-33 { width:33.33333333333333%!important; }
            .mj-column-per-100 { width:100%!important; }
            .mj-column-per-50 { width:50%!important; }
        }
        </style>
        </head>
        <body style="background: #F4F4F4;">
        <div style="background-color:#F4F4F4;"><!--[if mso | IE]>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
            <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
            <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:396px;">
                <![endif]--><div class="mj-column-per-66 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 0px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:left;"><p style="margin: 10px 0;"></p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                    </td><td style="vertical-align:top;width:198px;">
                    <![endif]--><div class="mj-column-per-33 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 0px;padding-top:0px;padding-bottom:0px;" align="right"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:right;"></div></td></tr></tbody></table></div><!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                            </td></tr></table>
                            <![endif]-->
                            <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:0px;padding-top:0px;"><!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:600px;"><a href="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" target="_blank"><img alt="" title="" height="auto" src="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="600"></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                        </td></tr></table>
                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                            </td></tr></table>
                                            <![endif]-->
                                            <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                <![endif]--><div style="margin:0px auto;max-width:600px;background:#ffffff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#ffffff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><h2>Welcome to Joint Training & Certification Program</h2></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p style="margin: 10px 0;">Hello ' . $fullname .  ',</p><p style="margin: 10px 0;">Welcome to Joint Training & Certification Program by Caltrans and CSULB. You have been added to JTCP by Program Coordinator.</p><p style="margin: 10px 0;">Your Credentials:</p><p style="margin: 10px 0;"><b>Email:</b> '. $emailaddress .'</p><p style="margin: 10px 0;"><b>Password:</b> '. $password_temp . '</p><p style="margin: 10px 0;">Click here to <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/login.php">Login</a> to your account to</p><ol><li style="margin: 10px 0px;">Take safety quiz.</li><li style="margin: 10px 0px;">Download prerequisite exam materials.</li><li style="margin: 10px 0px;">Pass prerequisites.</li><li style="margin: 10px 0px;">Enroll to the course.</li><li style="margin: 10px 0px;">Get certified. </li></ol><p style="margin: 10px 0;">For any queries and help feel free to contact us <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/contact.php">here</a>.</p><p style="margin: 10px 0;">Thanks,</p><p style="margin: 10px 0;">JTCP Program.</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                        </td></tr></table>
                                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                            </td></tr></table>
                                                            <![endif]-->
                                                            <!--[if mso | IE]>
                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                <tr>
                                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:300px;">
                                                                    <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:105px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvuu.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="105"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                        </td><td style="vertical-align:top;width:300px;">
                                                                        <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:98px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvu4.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="98"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                            </td></tr></table>
                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                </td></tr></table>
                                                                                <![endif]-->
                                                                                <!--[if mso | IE]>
                                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                                    <tr>
                                                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                                    <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                                                        <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">This e-mail has been sent to ' . $emailaddress . ', <a href="[[UNSUB_LINK_EN]]" style="color:inherit;text-decoration:none;" target="_blank" >click here to unsubscribe</a>.</p></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">US</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                            </td></tr></table>
                                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                                </td></tr></table>
                                                                                                <![endif]--></div>
                                                                                                </body>
                                                                                                </html>
                                                                                                ',
                                                                                                'Recipients' => [['Email' => $emailaddress]]
                                                                                                ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response;
    }

    function resetpassword_email($emailaddress, $newpassword)
    {
        $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');
        $body = [
        'FromEmail' => "csulbjtcp@gmail.com",
        'FromName' => "JTCP",
        'Subject' => "Password Reset!",
        'Text-part' => "Hello, your password has been reset for the following account: " . $emailaddress . " and new password is " . $newpassword . ".",
        'Html-part' => '
        <!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
        <title>Verify</title>
        <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <style type="text/css">
            #outlook a { padding: 0; }
            .ReadMsgBody { width: 100%; }
        .ExternalClass { width: 100%; }
        .ExternalClass * { line-height:100%; }
        body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
        p { display: block; margin: 13px 0; }
        </style>
        <!--[if !mso]><!-->
            <style type="text/css">
            @media only screen and (max-width:480px) {
                @-ms-viewport { width:320px; }
                @viewport { width:320px; }
            }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if lte mso 11]>
                <style type="text/css">
                .outlook-group-fix {
                width:100% !important;
                }
        </style>
        <![endif]-->
        <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-66 { width:66.66666666666666%!important; }
            .mj-column-per-33 { width:33.33333333333333%!important; }
            .mj-column-per-100 { width:100%!important; }
            .mj-column-per-50 { width:50%!important; }
        }
        </style>
        </head>
        <body style="background: #F4F4F4;">
        <div style="background-color:#F4F4F4;"><!--[if mso | IE]>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
            <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
            <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:396px;">
                <![endif]--><div class="mj-column-per-66 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 0px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:left;"><p style="margin: 10px 0;"></p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                    </td><td style="vertical-align:top;width:198px;">
                    <![endif]--><div class="mj-column-per-33 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 0px;padding-top:0px;padding-bottom:0px;" align="right"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:right;"></div></td></tr></tbody></table></div><!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                            </td></tr></table>
                            <![endif]-->
                            <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:0px;padding-top:0px;"><!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:600px;"><a href="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" target="_blank"><img alt="" title="" height="auto" src="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="600"></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                        </td></tr></table>
                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                            </td></tr></table>
                                            <![endif]-->
                                            <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                <![endif]--><div style="margin:0px auto;max-width:600px;background:#ffffff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#ffffff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><h2>Password reset details</h2></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p style="margin: 10px 0;">Hello, ' .  '</p><p style="margin: 10px 0;">your password has been reset for following account:</p><p style="margin: 10px 0;">Username(Email Address): ' . $emailaddress . '</p><p style="margin: 3px 0;">Password: ' . $newpassword . '</p><p style="margin: 10px 0;">For any queries and help feel free to contact us <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/contact.php">here</a>.</p><p style="margin: 10px 0;">Thanks,</p><p style="margin: 10px 0;">JTCP Program.</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                        </td></tr></table>
                                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                            </td></tr></table>
                                                            <![endif]-->
                                                            <!--[if mso | IE]>
                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                <tr>
                                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:300px;">
                                                                    <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:105px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvuu.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="105"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                        </td><td style="vertical-align:top;width:300px;">
                                                                        <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:98px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvu4.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="98"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                            </td></tr></table>
                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                </td></tr></table>
                                                                                <![endif]-->
                                                                                <!--[if mso | IE]>
                                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                                    <tr>
                                                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                                    <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                                                        <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">This e-mail has been sent to ' . $emailaddress . ', <a href="[[UNSUB_LINK_EN]]" style="color:inherit;text-decoration:none;" target="_blank" >click here to unsubscribe</a>.</p></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">US</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                            </td></tr></table>
                                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                                </td></tr></table>
                                                                                                <![endif]--></div>
                                                                                                </body>
                                                                                                </html>
                                                                                                ',
                                                                                                'Recipients' => [['Email' => $emailaddress]]
                                                                                                ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response;
    }

    function grade_email($data)
    {
        $firstname = $data['First_Name'];
        $lastname = $data['Last_Name'];
        $emailaddress = $data['Email_Address'];
        $practical = $data['Practical'];
        $written = $data['Written'];
        $course_name = $data['Course_Name'];
        $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');
        $body = [
        'FromEmail' => "csulbjtcp@gmail.com",
        'FromName' => "JTCP",
        'Subject' => "Your grade has been updated for JTCP Course " . $course_name,
        'Text-part' => "Hello " . $firstname . " " . $lastname . ", Your grade has been updated for JTCP Course " . $course_name . ". Practical: " . $practical . " Written: " . $written . ". Please login to JTCP account to download the certificate.",
        'Html-part' => '
        <!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
        <title>Verify</title>
        <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <style type="text/css">
            #outlook a { padding: 0; }
            .ReadMsgBody { width: 100%; }
        .ExternalClass { width: 100%; }
        .ExternalClass * { line-height:100%; }
        body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
        p { display: block; margin: 13px 0; }
        </style>
        <!--[if !mso]><!-->
            <style type="text/css">
            @media only screen and (max-width:480px) {
                @-ms-viewport { width:320px; }
                @viewport { width:320px; }
            }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if lte mso 11]>
                <style type="text/css">
                .outlook-group-fix {
                width:100% !important;
                }
        </style>
        <![endif]-->
        <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-66 { width:66.66666666666666%!important; }
            .mj-column-per-33 { width:33.33333333333333%!important; }
            .mj-column-per-100 { width:100%!important; }
            .mj-column-per-50 { width:50%!important; }
        }
        </style>
        </head>
        <body style="background: #F4F4F4;">
        <div style="background-color:#F4F4F4;"><!--[if mso | IE]>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
            <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
            <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:396px;">
                <![endif]--><div class="mj-column-per-66 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 0px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:left;"><p style="margin: 10px 0;"></p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                    </td><td style="vertical-align:top;width:198px;">
                    <![endif]--><div class="mj-column-per-33 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 0px;padding-top:0px;padding-bottom:0px;" align="right"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:right;"></div></td></tr></tbody></table></div><!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                            </td></tr></table>
                            <![endif]-->
                            <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:0px;padding-top:0px;"><!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:600px;"><a href="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" target="_blank"><img alt="" title="" height="auto" src="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="600"></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                        </td></tr></table>
                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                            </td></tr></table>
                                            <![endif]-->
                                            <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                <![endif]--><div style="margin:0px auto;max-width:600px;background:#ffffff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#ffffff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><h2>JTCP Course '. $course_name . ' Grade</h2></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p style="margin: 10px 0;">Hello ' . $firstname . " " . $lastname . ',</p><p style="margin: 10px 0;">your grade for JTCP course ' . $course_name . ' has been updated.</p><p style="margin: 10px 0;"><b>Practical Test:</b> ' . $practical . '</p><p style="margin: 3px 0;"><b>Written Test:</b> ' . $written . '</p><p style="margin: 10px 0;">Please login to <a href="http://web.csulb.edu/colleges/coe/jtcp/login.php">JTCP account</a> to download the certificate.</p><p style="margin: 10px 0;">For any queries and help feel free to contact us <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/contact.php">here</a>.</p><p style="margin: 10px 0;">Thanks,</p><p style="margin: 10px 0;">JTCP Program.</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                        </td></tr></table>
                                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                            </td></tr></table>
                                                            <![endif]-->
                                                            <!--[if mso | IE]>
                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                <tr>
                                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:300px;">
                                                                    <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:105px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvuu.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="105"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                        </td><td style="vertical-align:top;width:300px;">
                                                                        <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:98px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvu4.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="98"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                            </td></tr></table>
                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                </td></tr></table>
                                                                                <![endif]-->
                                                                                <!--[if mso | IE]>
                                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                                    <tr>
                                                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                                    <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                                                        <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">This e-mail has been sent to ' . $emailaddress . ', <a href="[[UNSUB_LINK_EN]]" style="color:inherit;text-decoration:none;" target="_blank" >click here to unsubscribe</a>.</p></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">US</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                            </td></tr></table>
                                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                                </td></tr></table>
                                                                                                <![endif]--></div>
                                                                                                </body>
                                                                                                </html>
                                                                                                ',
                                                                                                'Recipients' => [['Email' => $emailaddress]]
                                                                                                ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response;
    }

    function waitlist_email($data, $course_name){
        $emailaddress = $data['Email_Addresses'];
        $to=[];
        foreach ($emailaddress as $email){
            $to[] = [
            'Email' => $email
            ];
        }

        $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');
        $body = [
        'FromEmail' => "csulbjtcp@gmail.com",
        'FromName' => "JTCP",
        'Subject' => "JTCP " . $course_name . " is available for Enrollment!",
        'Text-part' => "Hello , Your waitlisted course " . $course_name . " is available for Enrollment. Login to JTCP website and enroll now.",
        'Html-part' => '
        <!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
        <title>Verify</title>
        <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <style type="text/css">
            #outlook a { padding: 0; }
            .ReadMsgBody { width: 100%; }
        .ExternalClass { width: 100%; }
        .ExternalClass * { line-height:100%; }
        body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
        p { display: block; margin: 13px 0; }
        </style>
        <!--[if !mso]><!-->
            <style type="text/css">
            @media only screen and (max-width:480px) {
                @-ms-viewport { width:320px; }
                @viewport { width:320px; }
            }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if lte mso 11]>
                <style type="text/css">
                .outlook-group-fix {
                width:100% !important;
                }
        </style>
        <![endif]-->
        <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-66 { width:66.66666666666666%!important; }
            .mj-column-per-33 { width:33.33333333333333%!important; }
            .mj-column-per-100 { width:100%!important; }
            .mj-column-per-50 { width:50%!important; }
        }
        </style>
        </head>
        <body style="background: #F4F4F4;">
        <div style="background-color:#F4F4F4;"><!--[if mso | IE]>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
            <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
            <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:396px;">
                <![endif]--><div class="mj-column-per-66 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 0px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:left;"><p style="margin: 10px 0;"></p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                    </td><td style="vertical-align:top;width:198px;">
                    <![endif]--><div class="mj-column-per-33 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 0px;padding-top:0px;padding-bottom:0px;" align="right"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:right;"></div></td></tr></tbody></table></div><!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                            </td></tr></table>
                            <![endif]-->
                            <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:0px;padding-top:0px;"><!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:600px;"><a href="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" target="_blank"><img alt="" title="" height="auto" src="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="600"></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                        </td></tr></table>
                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                            </td></tr></table>
                                            <![endif]-->
                                            <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                <![endif]--><div style="margin:0px auto;max-width:600px;background:#ffffff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#ffffff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><h2>JTCP Course '. $course_name . ' available for Enrollment</h2></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p style="margin: 10px 0;">Hello,</p><p style="margin: 10px 0;">Your waitlisted JTCP course ' . $course_name . ' is available for Enrollment.</p><p style="margin: 10px 0;">Please login to <a href="http://web.csulb.edu/colleges/coe/jtcp/login.php">JTCP account</a> to Enroll now.</p><p style="margin: 10px 0;">For any queries and help feel free to contact us <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/contact.php">here</a>.</p><p style="margin: 10px 0;">Thanks,</p><p style="margin: 10px 0;">JTCP Program.</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                        </td></tr></table>
                                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                            </td></tr></table>
                                                            <![endif]-->
                                                            <!--[if mso | IE]>
                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                <tr>
                                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:300px;">
                                                                    <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:105px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvuu.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="105"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                        </td><td style="vertical-align:top;width:300px;">
                                                                        <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:98px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvu4.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="98"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                            </td></tr></table>
                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                </td></tr></table>
                                                                                <![endif]-->
                                                                                <!--[if mso | IE]>
                                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                                    <tr>
                                                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                                    <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                                                        <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">US</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                            </td></tr></table>
                                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                                </td></tr></table>
                                                                                                <![endif]--></div>
                                                                                                </body>
                                                                                                </html>',
                                                                                                'Recipients' => $to
                                                                                                ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response;
    }

    function cancelclass_email($data){
        $emailaddress = $data['Email_Addresses'];
        $course_name = $data['Course_Name'];
        $classdate = $data['Class_Dates'];
        $location = $data['Location_Name'];
        $class_name = $data['Class_Name'];
        $to=[];
        foreach ($emailaddress as $email){
            $to[] = [
            'Email' => $email
            ];
        }

        $mj = new \Mailjet\Client('53c7ca762e72b62659344ef3a20736ef', '600f75304db7997cb5ca575cf52e12a7');
        $body = [
        'FromEmail' => "csulbjtcp@gmail.com",
        'FromName' => "JTCP",
        'Subject' => "JTCP Class " . $class_name . " has been Cancelled!",
        'Text-part' => "Hello , Your class " . $class_name . "  for course " . $course_name . " starting from " . $classdate[0] . " has been Cancelled. Login to JTCP website for refund or to enroll in another class.",
        'Html-part' => '
        <!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
        <title>Verify</title>
        <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <style type="text/css">
            #outlook a { padding: 0; }
            .ReadMsgBody { width: 100%; }
        .ExternalClass { width: 100%; }
        .ExternalClass * { line-height:100%; }
        body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
        p { display: block; margin: 13px 0; }
        </style>
        <!--[if !mso]><!-->
            <style type="text/css">
            @media only screen and (max-width:480px) {
                @-ms-viewport { width:320px; }
                @viewport { width:320px; }
            }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if lte mso 11]>
                <style type="text/css">
                .outlook-group-fix {
                width:100% !important;
                }
        </style>
        <![endif]-->
        <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-66 { width:66.66666666666666%!important; }
            .mj-column-per-33 { width:33.33333333333333%!important; }
            .mj-column-per-100 { width:100%!important; }
            .mj-column-per-50 { width:50%!important; }
        }
        </style>
        </head>
        <body style="background: #F4F4F4;">
        <div style="background-color:#F4F4F4;"><!--[if mso | IE]>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
            <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
            <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:396px;">
                <![endif]--><div class="mj-column-per-66 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 0px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:left;"><p style="margin: 10px 0;"></p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                    </td><td style="vertical-align:top;width:198px;">
                    <![endif]--><div class="mj-column-per-33 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 0px;padding-top:0px;padding-bottom:0px;" align="right"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:right;"></div></td></tr></tbody></table></div><!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                            </td></tr></table>
                            <![endif]-->
                            <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-bottom:0px;padding-top:0px;"><!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:600px;"><a href="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" target="_blank"><img alt="" title="" height="auto" src="http://web.csulb.edu/colleges/coe/jtcp/img/bg2.jpg" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="600"></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                        </td></tr></table>
                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                            </td></tr></table>
                                            <![endif]-->
                                            <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                <![endif]--><div style="margin:0px auto;max-width:600px;background:#ffffff;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#ffffff;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                    <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><h2>JTCP Class '. $class_name . ' has been Cancelled.</h2></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;" align="left"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;"><p style="margin: 10px 0;">Hello,</p><p style="margin: 10px 0;">Your JTCP class ' . $class_name . ' for Course ' . $course_name . ' starting from ' . $classdate[0] . ' has been Cancelled.</p><p style="margin: 10px 0;">Please login to <a href="http://web.csulb.edu/colleges/coe/jtcp/login.php">JTCP account</a> for refund or to enroll in another class.</p><p style="margin: 10px 0;">For any queries and help feel free to contact us <a target="_blank" style="text-decoration:inherit;" href="http://web.csulb.edu/colleges/coe/jtcp/contact.php">here</a>.</p><p style="margin: 10px 0;">Thanks,</p><p style="margin: 10px 0;">JTCP Program.</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                        </td></tr></table>
                                                        <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                            </td></tr></table>
                                                            <![endif]-->
                                                            <!--[if mso | IE]>
                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                <tr>
                                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;"><!--[if mso | IE]>
                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:300px;">
                                                                    <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:105px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvuu.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="105"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                        </td><td style="vertical-align:top;width:300px;">
                                                                        <![endif]--><div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:10px 25px;" align="center"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0"><tbody><tr><td style="width:98px;"><img alt="" title="" height="auto" src="http://hg9t.mjt.lu/img/hg9t/b/1yl5u/0nvu4.png" style="border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="98"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                            </td></tr></table>
                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                </td></tr></table>
                                                                                <![endif]-->
                                                                                <!--[if mso | IE]>
                                                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                                                                                    <tr>
                                                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                                                    <![endif]--><div style="margin:0px auto;max-width:600px;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px 20px 0px;"><!--[if mso | IE]>
                                                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:600px;">
                                                                                        <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"></div></td></tr><tr><td style="word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;padding-top:0px;padding-bottom:0px;" align="center"><div class="" style="cursor:auto;color:#55575d;font-family:Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;"><p style="margin: 10px 0;">US</p></div></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                            </td></tr></table>
                                                                                            <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
                                                                                                </td></tr></table>
                                                                                                <![endif]--></div>
                                                                                                </body>
                                                                                                </html>',
                                                                                                'Recipients' => $to
                                                                                                ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response;
    }
