<?php
    include 'template1.php';
?>

<title>Grade/Drop Students</title>

<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Grade/Drop Students</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
Select Class
</div>
<!-- /.panel-heading -->
<div class="panel-body">





<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Class Name</th>
<th>Class Capacity</th>
<th>Course</th>
<th>Practical Exam Date</th>
<th>Written Exam Date</th>
<th>Class Dates</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php
    $ret=getClasses();
    for($i=0;$i<count($ret);$i++)
    {
        ?>
<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Class_Name'];
    ?>
</td>
<td>
<?php
    echo  $ret[$i]['Class_Filled'] . "/" .$ret[$i]['Class_Capacity'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Course_Name'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Class_Practical_Date'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Class_Written_Date'];
    ?>
</td>
<td>
<?php

    for($j=0;$j<count($ret[$i]['Class_Dates']);$j++)
    {
        echo $ret[$i]['Class_Dates'][$j];
        echo " (" . $ret[$i]['Class_TimeFrom'][$j];
        echo "-" . $ret[$i]['Class_TimeTo'][$j] . ")";
        echo "<br>";
    }
    ?>
</td>
<td>
<form id="" role="form" action="gradestudents.php" method="POST">

<?php
    echo "<input type='text' value='" . $ret[$i]['Class_ID'] . "' name='cid' hidden>";

    if($ret[$i]['Class_Filled']==0)
    {
    ?>
<input type='submit' value='Select' name='selectclass' disabled >
<?php
    }
    else
    {
?>
<input type='submit' value='Select' name='selectclass'>
<a href='waitlist.php?cid=<?php echo $ret[$i]['Class_ID']; ?>' target="_blank"> Waitlist </a>
</form>
</td>
</tr>
<?php
    }
}
    ?>
</tbody>
</table>




<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->

<script>

$(document).ready(function() {
                  $('#dataTables-example').DataTable({
                                                     responsive: true
                                                     });
                  });
</script>




        <!-- /#page-wrapper -->


<?php
    include 'template2.php';
?>
