#!/usr/local/php5/bin/php

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bootstrap Example</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled Javascript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
	<h1>My first Bootstrap Page</h1>
	<p>This is some text.</p>
	<div class="row">
		<div class="col-lg-4" style="background-color:lavender;">.col-lg-4</div>
		<div class="col-lg-4" style="background-color:lavenderblush;">.col-lg-4</div>
		<div class="col-lg-4" style="background-color:lavender;">.col-lg-4</div>
	</div>
		<br>
		<blockquote>
			<p>We are in this world full of turmoil. <code>Tolerance</code> is a must. Respecting differences is a must. We have to fight for peace. Ah! The <kbd>irony</kbd>!</p>
			<footer>Rachayita Giri</footer>
		</blockquote>
		<h2>Contextual Colors</h2>
		  <p>Use the contextual classes to provide "meaning through colors":</p>
		  <p class="text-muted">This text is muted.</p>
		  <p class="text-primary">This text is important.</p>
		  <p class="text-success">This text indicates success.</p>
		  <p class="text-info">This text represents some information.</p>
		  <p class="text-warning">This text represents a warning.</p>
		  <p class="text-danger">This text represents danger.</p>
	<table class="table table-hover" title="My table">
		<thead>
			<th>Test col 1</th>
			<th>Test col 2</th>
			<th>Test col 3</th>
		</thead>
		<tbody>
			<tr>
				<td>Default</td>
				<td>Default</td>
				<td>Default</td>
			</tr>
			<tr class="success">
				<td>Success</td>
				<td>Success</td>
				<td>Success</td>
			</tr>
			<tr class="info">
				<td>Info</td>
				<td>Info</td>
				<td>Info</td>
			</tr>
			<tr class="warning">
				<td>Warning</td>
				<td>Warning</td>
				<td>Warning</td>
			</tr>
			<tr class="danger">
				<td>Danger</td>
				<td>Danger</td>
				<td>Danger</td>
			</tr>
			<tr class="active">
				<td>Active</td>
				<td>Active</td>
				<td>Active</td>
			</tr>
		</tbody>
	</table>
</div>

</body>
</html>
