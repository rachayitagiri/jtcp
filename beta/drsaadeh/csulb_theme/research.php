#!/usr/local/php5/bin/php

<!-- Template for Footer & Scripts-->
<?php
    include 'template_menu.html';
?>

    <!-- Page Content Research Interests -->
    <div class="container" id="interests">

        <!-- Marketing Icons Section -->
        <div class="row">
        
            <div class="panel-group" id="accordion">
            <a href="#collapse1" data-toggle="collapse" data-parent="#accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"></h4>
                            <font class="custom_header2" style="color: rgb(177,92,34);">
                                Research Interests
                            </font>
                    </div>   
                </div>
            </a>
            </div>
            <div class="panel-collapse collapse out" id="collapse1" >
                <div class="panel-body" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    <ul>
                        <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                        Pavement performance and preservation</li>
                        <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                        Experimental characterization of highway materials</li>
                        <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                        Constitutive modeling of highway materials at the microstructural level</li>
                        <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                        Performance evaluation of highway infrastructure</li>
                        <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                        Experimental characterization of highway materials using X-ray computed tomography (CT) image analysis techniques, and mechanical testing</li>
                    </ul>
                </div>
            </div>
           
            <a href="#collapse2" data-toggle="collapse" data-parent="#accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"></h4>
                            <font class="custom_header2" style="color: rgb(177,92,34);">
                                Awarded Research Grants
                            </font>
                    </div>   
                </div></a>
            
            <div class="panel-collapse collapse" id="collapse2">
                <div class="panel-body">
                    <table class="table" >
                        <thead style="background-color:rgba(177,92,34,0.5);">
                            <th>Sponsor</th>
                            <th>PI/Co-PI</th>
                            <th>Project Title</th>
                            <th>Status</th>
                            <th>Amount</th>
                            <th>Dates</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Caltrans via Chico</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>California Pavement Preservation Center</td>
                                <td>Pending</td>
                                <td>$200,000</td>
                                <td>Dec 2018 - Dec 2021</td>
                            </tr>
                            <tr>
                                <td>Transport</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Sensitivity Analysis on Semi-Circular Bending Test for Use as Quality Control Test in California</td>
                                <td>Active</td>
                                <td>$25,000</td>
                                <td>Jan 2018 - Jan 2019</td>
                            </tr>
                            <tr>
                                <td>Caltrans</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Joint Training and Certification Program - Phase II</td>
                                <td>Active</td>
                                <td>$1,312,375</td>
                                <td>Jun 2017 - Jun 2019</td>
                            </tr>
                            <tr>
                                <td>Caltrans</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Joint Training and Certification Program - Phase I</td>
                                <td>Completed</td>
                                <td>$775,000</td>
                                <td>Dec 2015 - Dec 2016</td>
                            </tr>
                            <tr>
                                <td>METRANS</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Sustainable Mitigation of Stormwater Runoff through Fully Permeable Pavement</td>
                                <td>Completed</td>
                                <td>$99,986</td>
                                <td>Sep 2016 - Sep 2017</td>
                            </tr>
                            <tr>
                                <td>CSU Commission on the Extended University</td>
                                <td>
                                    Elhami Naser (PI)<br>
                                    Tariq Shehab (Co-PI)<br>
                                    <b>Shadi Saadeh (Co-PI)</b><br>
                                    David Horne (Co-PI)
                                </td>
                                <td>Development of online Master of Science in Engineering Management</td>
                                <td>Completed</td>
                                <td>$85,051</td>
                                <td>Jul 2012 - Sep 2015</td>
                            </tr>
                            <tr>
                                <td>Caltrans Via CSU Chico</td>
                                <td><b>Shadi Saadeh (Co-PI)</b></td>
                                <td>Pavement Preservation Center, Identify - Assess - Develop - Deliver, existing/needed pavement preservation methods</td>
                                <td>Completed</td>
                                <td>$300,000</td>
                                <td>Apr 2014 - Apr 2018</td>
                            </tr>
                            <tr>
                                <td>CSULB IRA 2013</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Attending the Annual Meeting of the Journal of the Association of Asphalt Pavement Technologists</td>
                                <td>Completed</td>
                                <td>$4,059</td>
                                <td>Mar 2013</td>
                            </tr>
                            <tr>
                                <td>METRANS</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Development of Quality Control Test Procedure for Characterizing Fracture Properties of Asphalt Mixture</td>
                                <td>Completed</td>
                                <td>$89,945</td>
                                <td>Feb 2011 - May 2012</td>
                            </tr>
                            <tr>
                                <td>Partnered Pavement Research, UC Davis</td>
                                <td>John Harvey<br>
                                    <b>Shadi Saadeh (Co-PI)</b><br>
                                    Ashraf Rahim<br>
                                    David Johns</td>
                                <td>Development of Soil Stabilization Technical Advisory Guide</td>
                                <td>Completed</td>
                                <td>$53,016</td>
                                <td>Feb 2010 - Dec 2010</td>
                            </tr>
                            <tr>
                                <td>CSULB IRA 2012</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Attending the Annual Meeting of the Journal of the Association of Asphalt Pavement Technologists</td>
                                <td>Completed</td>
                                <td>$4,059</td>
                                <td>Mar 2012</td>
                            </tr>
                            <tr>
                                <td>CSULB IRA 2011</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Attending the Annual Meeting of the Journal of the Association of Asphalt Pavement Technologists</td>
                                <td>Completed</td>
                                <td>$3,043</td>
                                <td>Mar 2011</td>
                            </tr>
                            <tr>
                                <td>CSULB IRA 2010</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Attending the Annual Meeting of the Journal of the Association of Asphalt Pavement Technologists</td>
                                <td>Completed</td>
                                <td>$3,005</td>
                                <td>Mar 2010</td>
                            </tr>
                            <tr>
                                <td>Caltrans Via CSU Chico</td>
                                <td>Mary Strop-Gardiner<br>
                                    <b>Shadi Saadeh (Co-PI)</b><br>
                                    Dragos Andrei</td>
                                <td>Identify - Assess - Develop - Deliver, existing/needed pavement related training</td>
                                <td>Completed</td>
                                <td>$600,000</td>
                                <td>May 2009 - April 2012</td>
                            </tr>
                            <tr>
                                <td>Caltrans Via CSU Chico</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Pavement Preservation Project(investigation of fog and rejuvenating seal coat)</td>
                                <td>Completed</td>
                                <td>$35,000</td>
                                <td>Mar 2009 - Dec 2009</td>
                            </tr>
                            <tr>
                                <td>METRANS</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Development of Quality Control Test Procedure for Characterizing Fracture Properties of Asphalt Mixture</td>
                                <td>Completed</td>
                                <td>$35,000</td>
                                <td>Aug 2009 - Jul 2010</td>
                            </tr>
                            <tr>
                                <td>CSULB SCAC 2009</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Performance of Asphalt Mixtures designed using Bailey method and the Locking Point Concept</td>
                                <td>Completed</td>
                                <td>$4,650</td>
                                <td>Jun 2009 - Aug 2009</td>
                            </tr>
                            <tr>
                                <td>CSULB SCAC 2009</td>
                                <td>Shadi Saadeh (PI)</td>
                                <td>Investigation of Anisotropic behavior of Hot Mix Asphalt using Microstructural Anisotropic Damage Viscoelastic</td>
                                <td>Completed</td>
                                <td>$4,650</td>
                                <td>Jun 2008 - Aug 2008</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<!-- Template for Footer & Scripts-->
<?php
    include 'template_footer.php';
?>