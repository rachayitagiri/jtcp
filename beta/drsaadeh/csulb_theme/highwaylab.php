#!/usr/local/php5/bin/php

<!-- Template for Footer & Scripts-->
<?php
    include 'template_menu.html';
?>

<!-- Page Content Research Interests -->
    <div class="container" id="interests">

        <!-- Marketing Icons Section -->
        <div class="row">
           
                    <table class="table fixed" >
                        <col width="30%" />
                        <col width="20%" />
                        <col width="50%" />
                        <thead style="background-color:rgba(177,92,34,0.5);">
                            <th>Method/Test</th>
                            <th>Picture</th>
                            <th>Description</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>AASHTO T 209-12</td>
                                <td><img src="img/machines/IMG_1973.jpg" width="300" height="300" ></td>
                                <td><i>Standard Method of Test for Theoretical Maximum Specific Gravity (Gmm) and Density of Hot Mix Asphalt (HMA):</i><br>  This test method covers the determination of the theoretical maximum specific gravity/gravity mix maximum (Gmm) and density of uncompacted hot mix asphalt (HMA) at 25&deg; C (77&deg;F)</td>
                            </tr>
                            <tr>
                                <td>AASHTO T 308-16</td>
                                <td><img src="img/machines/IMG_1975.jpg" height="300" width="300"></td>
                                <td><i>Standard Method of Test for Determining the Asphalt Binder Content of Hot Mix Asphalt (HMA) by the Ignition Method:</i><br>  This test method covers the determination of asphalt binder content of hot mix asphalt ( HMA) by ignition at temperature that reach the flashpoint of the binder in a furnace. The means of specimen heating may be the convection method or the direct infrared (IR) irradiation method.The aggregate remaining after burning can be used for sieve analysis using T 30.</td>
                            </tr>
                            <tr>
                                <td>AASHTO T 312/ ASTM D6925</td>
                                <td><img src="img/machines/IMG_1978.jpg" height="300" width="300"></td>
                                <td><i>Standard Method of Test for Preparing and Determining the Density of Asphalt Mixture Specimens by Means of the Superpave Gyratory Compactor:</i><br>The Gyratory Compactor is used to compact hot mix asphalt (HMA) specimens. The compacted specimens obtained is used for determining the mechanical and volumetric properties of HMA. In addition to HMA, soil, emulsions, and concrete specimens can be created.
                                </td>
                            </tr>
                            <tr>
                                <td>AASHTO TP 10/ ASTMD8044/ AASHTO TP 105/ AASHTO T 342/ AASHTO T 321</td>
                                <td><img src="img/machines/IMG_1979.jpg" height="300" width="300"></td>
                                <td>The DTS-30 Dynamic Testing System is a servo-hydraulic testing machine which is used to provide accurate loading wave shapes up to 100 Hz. This is can be also operated in tension, compression dynamic loading and it is suited to testing different types of materials. For example, asphalt, soil, unbound granular materials, fibres and plastics. It is used for the following tests:
                                    <ol>
                                        <li>AASHTO TP 10 - <i>Standard Test Method for Thermal Stress Restrained Specimen Tensile Strength</i>
                                        </li>   
                                        <li>ASTM D8044 - <i>Standard Test Method for Evaluation of Asphalt Mixture Cracking Resistance using the Semi-Circular Bend Test (SCB) at Intermediate Temperatures</i>
                                        </li>
                                        <li>AASHTO TP 105 - <i>Standard Method of Test for Determining the Fracture Energy of Asphalt Mixtures Using the Semicircular Bend Geometry (SCB)</i>
                                        </li>
                                        <li>AASHTO T 342 - <i>Standard Method of Test for Determining Dynamic Modulus of Hot Mix Asphalt (HMA)</i>
                                        </li>
                                        <li>AASHTO T 321 - <i>Standard Method of Test for Determining the Fatigue Life of Compacted Asphalt Mixtures Subjected to Repeated Flexural Bending</i>
                                        </li>
                                    </ol>
                                </td>
                            </tr>
                            <tr>
                                <td>AASHTO T 324</td>
                                <td><img src="img/machines/Picture8.jpg" height="300" width="300"></td>
                                <td><i>Standard Method of Test for Hamburg Wheel-Track Testing of Compacted Hot Mix Asphalt (HMA):</i> Hamburg (Immersion) wheel tracking test is the generally referred to test standard, although many states modify this procedure to meet their specific requirements. It is used to evaluate the resistance to rutting and moisture susceptibility of asphalt mixtures. </td>
                            </tr>
                            <tr>
                                <td>AASHTO T 314</td>
                                <td><img src="img/machines/Picture9.jpg" height="300" width="300"></td>
                                <td><i>Standard method of test to determine the Fracture Properties of Asphalt Binder in Direct Tension (DT):</i><br>The Direct Tension Tester (DTT) is used for measuring stiffness and relaxation properties of asphalt binders. These parameters are used as an indicator to measure asphalt binder’s ability to resist low temperature cracking. The DTT is used as thermoelectric bending in determining the asphalt binder’s low temperature PG grade determination.</td>
                            </tr>
                            <tr>
                                <td>AASHTO T 313/ASTM D6648</td>
                                <td><img src="img/machines/IMG_1983.jpg" height="300" width="300"></td>
                                <td><i>Standard Test Method for Determining the Flexural Creep Stiffness of Asphalt Binder Using the Bending Beam Rheometer (BBR):</i> Thermoelectric-BBR is a thermoelectrically cooled bending beam rheometer used for analyzing flexural creep of asphalt binders from ambient to –40 °C (± 0.03 °C).
                                </td>
                            </tr>
                            <tr>
                                <td>AASHTO PP1/ASTM D6521</td>
                                <td><img src="img/machines/IMG_1984.jpg" height="300" width="300"></td>
                                <td><i>Standard Practice for Accelerated Aging of Asphalt Binder Using a Pressurized Aging Vessel (PAV):</i>The Model 9900 Vacuum Degassing Oven is designed specifically for degassing PAV-aged bitumen [asphalt] samples. One of the main features of this model 9900 oven is that it features a completely self-contained vacuum system.<br> Both AASHTO PP-1 and ASTM D-6521 versions of the Standard Practice for Accelerated Aging of Asphalt Binder using a Pressurized Aging Vessel (PAV) require vacuum degassing of the PAV-aged samples. </td>
                            </tr>
                            <tr>
                                <td>AASHTO T 316/ASTM D4402</td>
                                <td><img src="img/machines/IMG_1986.jpg" height="300" width="300"></td>
                                <td><i>Standard Method of Test for Viscosity Determination of Asphalt Binder Using Rotational Viscometer:</i> Brookfield Viscometer DV-II+ Pro is used to determine the viscosity of asphalt binders in the high temperature range of manufacturing and construction. The values obtained can be  used in the Superpave PG asphalt binder specification.</td>
                            </tr>
                            <tr>
                                <td>AASHTO T 48/ ASTM D92/ASTM D117</td>
                                <td><img src="img/machines/IMG_1987.jpg" height="300" width="300"></td>
                                <td><i>Standard Test Method for Flash and Fire Points by Cleveland Open Cup Tester:</i>Cleveland Flash &amp; Fire Point Tester is used to determine flash and fire points by the Cleveland open-cup method. Consists of electric Heater with rheostat, flash point platform with Thermometer holder, test burner and flash cup.
                                </td>
                            </tr>
                            <tr>
                                <td>ASTM D4065/ASTM D4440/ASTM D5279</td>
                                <td><img src="img/machines/IMG_1988.jpg" height="300" width="300"></td>
                                <td><i>Standard Practice for Plastics: Dynamic Mechanical Properties: Determination and Report of Procedures:</i> Rheometric Dynamic Analyzer RDA-II - A rheometer is a mechanical spectrometer that is used to subject a sample to a dynamic or a steady shear strain. It also measure the resultant torque exerted by the sample in response to the shear strain
                                    <ol>
                                        <li>Designation D4065: Standard Practice for Plastics: Dynamic Mechanical Properties: Determination and Report of Procedures</li>   
                                        <li>Designation D4440: Dynamic Mechanical Properties Melt Rheology</li>   
                                        <li>Designation D5279: Dynamic Mechanical Properties in Torsion</li>
                                    </ol>
                                </td>
                            </tr>
                            <tr>
                                <td>AASHTO T85/ AASHTO T166/ AASHTO T209/ AASTHTO T275</td>
                                <td><img src="img/machines/IMG_1999.jpg" height="300" width="300"></td>
                                <td>The Specific Gravity Tank with heater/circulator can be used for the following tests:
                                    <ol>
                                        <li>AASHTO T85: <i>Standard Method of Test for Specific Gravity and Absorption of Coarse Aggregate</i></li>   
                                        <li>AASHTO T166: <i>Standard Method of Test for Bulk Specific Gravity (Gmb) of Compacted Hot Mix Asphalt (HMA) Using Saturated Surface-Dry Specimens</i></li>   
                                        <li>AASHTO T209: <i>Standard Method of Test for Theoretical Maximum Specific Gravity (Gmm) and Density of Hot Mix Asphalt (HMA)</i></li>
                                        <li>AASTHTO T275: <i>Standard Method of Test for Bulk Specific Gravity (Gmb) of Compacted Asphalt Mixtures Using Paraffin-Coated Specimens</i></li>
                                    </ol>
                                </td>
                            </tr>
                            <tr>
                                <td>CT 217</td>
                                <td><img src="img/machines/IMG_1992.jpg" height="300" width="300"></td>
                                <td><i>Method of Test for Sand Equivalent:</i> <br>This test method provides the procedure for measuring the relative proportions of detrimental fine dust or clay-like material in soil or fine aggregates.
                                </td>
                            </tr>
                            <tr>
                                <td>CT 227/ CT 229</td>
                                <td><img src="img/machines/IMG_1995.jpg" height="300" width="300"></td>
                                <td><i>Method of Test for Evaluating Cleanness of Coarse Aggregate:</i> <br>The cleanness test provides an indication of the relative proportions of clay-sized material clinging to coarse aggregates or screenings.<br><br>
                                <i>Method of Test for Durability Index:</i> <br>This test method describes the procedure for measuring the relative resistance of an aggregate to producing clay-sized fines when subjected to prescribed methods of inter-particle abrasion in the presence of water. Four procedures are provided for use with materials with various nominal sizes and specific gravities
                                </td>
                            </tr>
                            <tr>
                                <td>CT 202</td>
                                <td><img src="img/machines/IMG_1994.jpg" height="300" width="300"></td>
                                <td><i>Method of Test for Sieve Analysis of Fine and Coarse Aggregates:</i> <br>This method, which includes modifications of AASHTO Designations T 11, T 27, T 30, and T 37, specifies the procedures for determining the particle-size distribution of fine and coarse aggregates.
                                </td>
                            </tr>


                        </tbody>
                    </table>

                </div>
            </div>

     </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
    <br>
    <br>
    <br>

<!-- Template for Footer & Scripts-->
<?php
    include 'template_footer.php';
?>
