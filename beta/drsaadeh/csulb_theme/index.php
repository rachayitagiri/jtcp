#!/usr/local/php5/bin/php

<!-- Template for Footer & Scripts-->
<?php
    include 'template_menu.html';
?>

<!-- Page Content About -->
<div class="container" id="about">

    <!-- Marketing Icons Section -->
    <div class="row">

    <div class="col-lg-12" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
        <img src="img/saadeh6x6.jpg" style="float:right;" height="300px" width="300px">
        <p class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
        <b>Director</b> <br>
        National Center for Transportation, Green Technology, and Education (TransGET) <br>
        <b>Professor  &vert; &vert;  Graduate Advisor  &vert; &vert;  ABET Coordinator </b><br>
        <a href="http://www.csulb.edu/college-of-engineering/civil-engineering-construction-engineering-management" target="_blank">Department of Civil Engineering and Construction Engineering Management</a> <br>
        <b>Program Manager</b> <br>
        <a href="http://www.dot.ca.gov/mets/jtcp/" target="_blank">Joint Training and Certification Program (JTCP)</a> <br><br>
        <a href="http://www.csulb.edu/">California State University, Long Beach</a> <br>
        1250 Bellflower Blvd., Long Beach, CA, 90840 <br>
        Voice: (562) 985-4147<br>
        Fax: (562) 985-2380 <br>
        Email: <a href="mailto:shadi.saadeh@csulb.edu">shadi.saadeh@csulb.edu</a> <br>
        LinkedIn profile: <a href="https://www.linkedin.com/in/shadi-saadeh-ph-d-p-e-m-asce-36457233/" target="_blank">Shadi Saadeh</a>
        <br>
        <a href="http://web.csulb.edu/colleges/coe/cecem/faculty/saadeh.shtml">CSULB Department home page</a>
        </p>

    </div>

    </div>

    <div class="panel-group" id="accordion">
            <a href="#collapse2" data-toggle="collapse" data-parent="#accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"></h4>
                            <font class="custom_header2" style="color: rgb(177,92,34);">
                                Education
                            </font>
                    </div>   
                </div>
            </a>
    </div>
    <div class="panel-collapse collapse out" id="collapse2" >
                <div class="panel-body" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    <div class="col-lg-12" style="font-size: 0.9em; color: rgba(51,51,51,0.8);">
                        <dl>
                            <dt style="margin:0;">Ph.D.</dt>
                            <dd>Civil Engineering, <a href="https://www.tamu.edu/" target="_blank">Texas A&amp;M University</a>, College Station, Texas (December 2005)</dd>
                            <br>
                            <dt>M.Sc.</dt>
                            <dd>Civil Engineering, <a href="https://wsu.edu/" target="_blank">Washington State University</a>, Pullman, Washington (May 2002)</dd>
                            <br>
                            <dt>B.Sc.</dt>
                            <dd>Civil Engineering, <a href="http://ju.edu.jo/home.aspx" target="_blank">University of Jordan</a>, Amman, Jordan (May 1997)</dd>
                        </dl>
                    </div>
                </div>
    </div>

    <div class="panel-group" id="accordion">
            <a href="#collapse3" data-toggle="collapse" data-parent="#accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"></h4>
                            <font class="custom_header2" style="color: rgb(177,92,34);">
                                Affiliation
                            </font>
                    </div>   
                </div>
            </a>
    </div>
    <div class="panel-collapse collapse out" id="collapse3" >
                <div class="panel-body" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    <div class="col-lg-12" style="font-size: 0.9em; color: rgba(51,51,51,0.8);">
                        <ul style="margin:0; padding:0;">
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            American Society of Civil Engineers <a href="https://www.asce.org/" target="_blank">(ASCE)</a></li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Geo Institute <a href="https://www.asce.org/geotechnical-engineering/geo-institute/" target="_blank">(GI)</a> </li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Association of Asphalt Paving Technologists <a href="https://asphalttechnology.org/" target="_blank">(AAPT)</a></li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Transportation Research Board <a href="http://www.trb.org/Main/Home.aspx" target="_blank">(TRB)</a></li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Pavement Preservation Task Group <a href="http://www.csuchico.edu/cp2c/PPTG/index.shtml" target="_blank">(PPTG)</a></li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            California Pavement Preservation Center <a href="https://www.csuchico.edu/cp2c/" target="_blank">(CP<sup>2</sup>C)</a></li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Transportation &amp; Development Institute <a href="https://www.asce.org/transportation-and-development-engineering/transportation-and-development-institute/" target="_blank">(T&amp;DI)</a></li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Academy of Pavement Science &amp; Engineering <a href="https://apse.wildapricot.org/" target="_blank">(APSE)</a></li>
                        </ul>
                    </div>
                </div>
    </div>

    <div class="panel-group" id="accordion">
            <a href="#collapse4" data-toggle="collapse" data-parent="#accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"></h4>
                            <font class="custom_header2" style="color: rgb(177,92,34);">
                                Courses
                            </font>
                    </div>   
                </div>
            </a>
    </div>
    <div class="panel-collapse collapse out" id="collapse4" >
                <div class="panel-body" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    <div class="col-lg-12" style="font-size: 0.9em; color: rgba(51,51,51,0.8);">
                        <ul style="margin:0; padding:0;">
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                             CE 101 Introduction to Civil Engineering and Construction Engineering Management</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            CE 200 Materials for Civil Engineering</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            CE 205 Analytical Mechanics-I (Statics)</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            CEM 335 Soil Mechanics Technology</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                             CEM 335L Soil Mechanics Technology (Laboratory)</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                             CE 345 Geotechnical Engineering I</li> 
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            CE 346 Geotechnical Engineering</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                             CE 406 Project Cost-Benefit Analysis</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            CE 427 Highway Design</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            CE 428 Highway Engineering Materials</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            CE 490 Senior Design</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                             CE 526 Pavement Engineering</li>
                             <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            CE 528 Advanced Highway Materials</li>
                           

                        </ul>
                    </div>
                </div>
    </div>

    <div class="panel-group" id="accordion">
            <a href="#collapse5" data-toggle="collapse" data-parent="#accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"></h4>
                            <font class="custom_header2" style="color: rgb(177,92,34);">
                                California Pavement Preservation Center (CP<sup>2</sup>C)
                            </font>
                    </div>   
                </div>
            </a>
    </div>
    <div class="panel-collapse collapse out" id="collapse5" >
                <div class="panel-body" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    <div class="col-lg-12" style="font-size: 0.9em; color: rgba(51,51,51,0.8);">
                        <p class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" style="margin:0; padding:0;" >The purpose of the California Pavement Preservation Center <a href="https://www.csuchico.edu/cp2c/" target="_blank">(CP<sup>2</sup>C)</a> is to: <br>
                        </p>
                        <ul style="margin:0; padding:0;">
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Define and quantify benefits of preservation</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Provide training and education</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Identify practices and research that can improve pavement preservation performance</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Document, facilitate and provide technology transfer for innovative pavement preservation materials and construction methods</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Provide technical assistance for state and local agencies</li>
                            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
                            Promote the effective pavement preservation to sustain the built environment</li>
                        </ul>
                    </div>
                </div>
    </div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->
<br>
<br>

<!-- Template for Footer & Scripts-->
<?php
    include 'template_footer.php';
?>