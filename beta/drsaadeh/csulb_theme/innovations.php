#!/usr/local/php5/bin/php

<!-- Template for Footer & Scripts-->
<?php
    include 'template_menu.html';
?>

    <!-- Page Content Research Interests -->
    <div class="container" id="interests">

        <!-- Marketing Icons Section -->
        <div class="row">

            <div class="panel-group" id="accordion">

            <!-- Collapsible Item -->
            <a href="#collapse4" data-toggle="collapse" data-parent="#accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"></h4>
                            <font class="custom_header2" style="color: rgb(177,92,34);">
                                Took the lead on ABET accreditation
                            </font>
                    </div>   
                </div>
            </a>

            <div class="panel-collapse collapse out" id="collapse4" >
                <div class="panel-body" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    <p>I have served as the ABET accreditation coordinator for this cycle of accreditation of our department. I have created, dissemniated and reviewed assessment plans for our program including all the classes in the program.</p>
                </div>
            </div>
            <!-- ./Collapsible Item-->
            <br>

        </div>
        <!-- ./row -->
    </div>
    <!-- ./container -->
    <br>
    <br>

<!-- Template for Footer & Scripts-->
<?php
    include 'template_footer.php';
?>
