#!/usr/local/php5/bin/php

<!-- Template for Footer & Scripts-->
<?php
    include 'template_menu.html';
?>

<!-- Page Content About -->
<div class="container" id="transget">

    <!-- Marketing Icons Section -->
    <div class="row">
	
	<div align="center" style="float: left; padding: 20px;">
		<img src="img/transget300.jpg" height="200" width="200">
	</div>
	<br>
	<p style="text-align: justify;">
		<b>Director: </b>Shadi Saadeh
		<br><br>
		The National Center for Transportation, Green Technologies and Education<b> (TransGET) </b>was established in 2009 to conduct interdisciplinary research and development, technology transfer, and education in transportation and green technologies. The center is the main platform for providing the <a href="http://www.dot.ca.gov/mets/jtcp/">Joint Training and Certification Program</a> for Caltrans and the industry and to perform research in transportation. The center brings together transportation researchers from different disciplines to collaborate on infrastructure research and applications, freight movements, mobility and optimization, technology development, and education and outreach.
	</p>
	

	</div>
	<!-- /.row -->

</div>
<!-- /.container -->


<!-- Template for Footer & Scripts-->
<?php
    include 'template_footer.php';
?>