#!/usr/local/php5/bin/php

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shadi Saadeh - Personal Website</title>

    <!-- Favicon -->
<!--    <link rel="shortcut icon" href="favicon.ico"> -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- add loto fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Navigation -->
    <nav id="mymainnav" class="navbar navbar-inverse navbar-fixed-top custom-main-nav" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <!-- only text header will be display -->
                <a class="navbar-brand custom-font custom-nav-header hidden-md hidden-lg" href="http://www.csulb.edu" target="_blank">CSULB</a>
                <table>
                    <tr>
                        <td><a href="http://www.csulb.edu" target="_blank"><img src="img/csulb2.png" class="hidden-sm visible-md visible-lg custom-nav-logo"/></a><span class="hidden-sm visible-md visible-lg custom-nav-logo2">|</span><a href="http://www.csulb.edu/college-of-engineering" target="_blank" class="hidden-sm visible-md visible-lg custom-nav-logo3">SHADI SAADEH</a></td>
                    </tr>
                </table>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

        </div>
        <!-- /.container -->
    </nav>

    <!------------------------ slider image --------------------------------------->
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <!-- update indecator values according to images here -->
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            <li data-target="#myCarousel" data-slide-to="6"></li>
            <li data-target="#myCarousel" data-slide-to="7"></li>
            <li data-target="#myCarousel" data-slide-to="8"></li>
            <li data-target="#myCarousel" data-slide-to="9"></li>
            <li data-target="#myCarousel" data-slide-to="10"></li>
            <li data-target="#myCarousel" data-slide-to="11"></li>
            <li data-target="#myCarousel" data-slide-to="0"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <!-- put your image here. add div -->
            <div class="item active">
                <div class="fill" style="background-image:url('img/bg2.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg5.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg10.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg31.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg6.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg7.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg9.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg8.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg4.jpg');"></div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/bg11.jpg');"></div>
            </div>



        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
    <!----------------------- slider image completed ---------------------------------------->

    <!----------------------- navigation in bottom of image slider -------------------------->
    <nav class="navbar navbar-default nav-custom-color" role="navigation" id="menunavbar">
        <div class="container" style="width:100%;">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand custom-font hidden-md hidden-lg" href="">Menu</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav center-block custom-font" id="menunav">
                    <li>
                        <a href="index.php">HOME</a>
                    </li>
                    <li>
                        <a href="transget.php">TRANSGET</a>
                    </li>
                    <li>
                        <a href="research.php">RESEARCH</a>
                    </li>
                    <li>
                        <a href="honors.php">HONORS</a>
                    </li>
                    <li>
                      <a href="gradstudents.php">GRADUATE STUDENTS</a>
                    </li>
                    <li>
                      <a href="publications.php">PUBLICATIONS</a>
                    </li>
                    <li>
                        <a href="highwaylab.php">HIGHWAY LAB @ THE BEACH</a>
                    </li>
                    <!-- <li>
                        <a href="#" style="color: rgb(177,92,34);">HOME</a>
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!----------------------------------- navigation completed ---------------------------------->

    <!----------------------------------- body started ------------------------------------------>
    <div class="col-lg-12">
        <h1 class="custom_header">
            <b>SHADI SAADEH</b>, Ph.D., P.E., M. ASCE
        </h1>
        <hr>
    </div>
    <br>
    <br>
    <br>
    
