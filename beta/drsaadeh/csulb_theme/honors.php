#!/usr/local/php5/bin/php

<!-- Template for Footer & Scripts-->
<?php
    include 'template_menu.html';
?>

    <!-- Page Content Honors -->
    <div class="container" id="honors" style="width:80%; float:right; margin-right:20px;" >

    <!-- Marketing Icons Section -->
    <div class="row">

    <div class="col-lg-2 custom_header2"  style="color: rgb(177,92,34); text-align:center;" >
        Awards <br> &amp; <br> Honors
    </div>
    <div class="col-lg-10" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
        <ul style="margin:0; padding:0;">
            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
            CSULB Research, Scholarship, and Creative Activity Award, <strong>2011, 2013, 2015, 2018</strong></li>
            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
            Enhancing Educational Effectiveness Through Technology Award, <strong>2010</strong></li>
            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
            ASCE Excellence in Civil Engineering Education Fellow, <strong>2009</strong></li>
            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
            CSULB Scholarly and Creative Activity Award, <strong>2009</strong></li>
            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
            CSULB Scholarly and Creative Activity Travel Award, <strong>2008</strong></li>
            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
            CSULB Scholarly and Creative Activity Award, <strong>2008</strong></li>
            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
            Academic Excellence Award, Texas A&amp;M University, <strong>2005</strong></li>
            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
            First place, Student research week, (<a href="http://srw.tamu.edu/"> http://srw.tamu.edu/ </a>), Texas A&amp;M University, <strong>2004</strong></li>
            <li class="text-large text-thin" style="font-size: 0.9em; line-height: 170%;" >
            International Student Travel Grant, (<a href="http://international.tamu.edu/iss/Issschol.asp"> http://international.tamu.edu/iss/Issschol.asp</a> ), Texas A&amp;M University, <strong>2005</strong></li>
        </ul>

    </div>

    </div>
    <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Page Content Honors -->
    <div class="container" id="honors" style="width:80%; float:right; margin-right:20px;" >

    <!-- Marketing Icons Section -->
    <div class="row">
    
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container -->
    <br><br><br><br><br><br><br><br><br><br><br><br>

<!-- Template for Footer & Scripts-->
<?php
    include 'template_footer.php';
?>
