<!-- Template for Footer & Scripts-->
<?php
    include 'template_menu.html';
?>
    <!-- Page Content Graduate Students -->
    <div class="container" id="gradstudents">

    <div style="font-size: 1.3em; color: rgba(177,92,34,0.8);">
        Meet the students supported by projects under my supervision.
    </div>
    <br>
        <div class="row" >
            <!-- Person 1 -->
            <div class="col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/amber.JPG" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                    <div class="card-body">
                        <h2 style="width: 350px; max-width: 30%;">Amber Ajluni</h2>
                        <p class="card-text" style="width: 350px; max-width: 30%; color:grey;">English Teacher & Salsa Dancer</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">I teach English at several different community colleges in Los Angeles and Orange County. I am also a salsa dancer and perform with a team. When I was in the graduate school, Dr. Shade Saadeh hired me to edit some documents for a project that we were working on. </p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">I am an English teacher by day, salsa dancer by night; I not judging your grammar but will help you with it if you ask nicely.</p>
                    </div>
                    </div>
                </div>
            </div>

            <!-- Person 2 -->
            <div class="col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/avinash.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%;">Avinash Ralla
                            <a href="https://www.linkedin.com/in/ralla-avinash-45222854/" target="_blank">
                                <img src="../img/students/linkedin_gold.png" style="float:right;height: 1em; width: 1em;"></a></h2>
                        <p class="card-title" style="width: 350px; max-width: 30%; color:grey;">MSc 2017 || Transportation Engineer at Caltrans</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">I am a dedicated and ardent civil engineer. I completed my Master's degree in Civil Engineering with an emphasis in Transportation from California State University, Long Beach. I like to spend time with family and friends. Currently, I am working as a Transportation Engineer at the Caltrans District 3 office.</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;"></p>
                    </div>
                </div>
            </div>

            <!-- Person 3 -->
            <div class="col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/bahar.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%;">Bahar Bandegan
                        <a href="https://www.linkedin.com/in/bahar-bandegan-0532128a/" target="_blank">
                                <img src="../img/students/linkedin_gold.png" style="float:right;height: 1em; width: 1em;"></a></h2>
                        <p class="card-title" style="width: 350px; max-width: 30%; color:grey;">Program Coordinator, JTCP</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">I received my first Master’s degree in Computer Science from Germany and now at CSULB, I'm pursuing my second graduate degree in Transportation Engineering. I love challenging positions at an organization to offer my knowledge in Computer Science and Transportation Engineering. </p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">In my spare time, I am likely to be found dancing or swimming.</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

        <div class="row">
            <!-- Person 1 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/user1.png" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%;">Ganesh Mohanchandran
                        <a href="https://www.linkedin.com/in/ganesh-mohanchandran-a7991775/" target="_blank">
                                <img src="../img/students/linkedin_gold.png" style="height: 1em; width: 1em;"></a></h2>
                        <p class="card-title" style="width: 350px; max-width: 30%; color:grey;">MSc, 2010 || Transport Analyst at Pink City Expressway Private Limited</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;"></p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;"></p>
                    </div>
                </div>
            </div>

            <!-- Person 2 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/hamed1.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%;">Hamed Hakimelahi
                        <a href="https://www.linkedin.com/in/hamed-hakimelahi-pe-env-sp-ba9a1442/" target="_blank">
                            <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                        </h2>
                        <p class="card-title" style="width: 350px; max-width: 30%; color:grey;">MSc (Civil Engineering), 2013 || Professional Engineer, Brown &amp; Caldwell, Portland, OR</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">I am a professional engineer. I received my Master's degree from CSULB in 2013. I have worked with Dr Saadeh as a research assistant and have published several papers on fracture properties of asphalt mixtures, with his support and guidance. As of now, I am working as Professional Engineer at Brown &amp; Caldwell.</p>
                    </div>
                </div>
            </div>

            <!-- Person 3 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/hooman.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%;">Hooman Jalaie
                        <a href="https://www.linkedin.com/in/hooman-jalaie-eit-91967857/" target="_blank">
                            <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                        </h2>
                        <p class="card-title" style="width: 350px; max-width: 30%; color:grey;">MSc, 2013 || EIT || Transportation Designer at Moffatt &amp; Nichol</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">I serve as a designer for various public and private sector infrastructure improvement projects. This experience includes grading, drainage, site development, and highway design. I am highly skilled in Autodesk modeling programs such as Civil 3D and Infraworks. My direct experience on Caltrans and local agency projects that involve highway design and highway drainage have made me conversant with standards associated with AASHTO, and the Manual on Uniform Traffic Control Devices (MUTCD).</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">In addition, I recently served as a Construction Field Engineer for a $1.2B Orange County Transportation Authority’s Design-Build I-405 Improvement Project, and through this experience, gained knowledge of design plan constructability review, quantity tracking, change order estimates, and maintenance of project schedules.</p>
                    </div>
                </div>
            </div>
        
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- Person 1 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/user1.png" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%;">Ignasius Parlindungan
                        <a href="https://www.linkedin.com/in/ignasius/" target="_blank">
                            <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                        </h2>
                        <p class="card-title" style="width: 350px; max-width: 30%; color:grey;">MSc, 2009 || P.Eng., P.E. || Structural Engineer, Vancouver, Canada</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;"></p>
                    </div>
                </div>
            </div>

            <!-- Person 2 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img src="../img/students/jacob1.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%">Jacob Dethero</h2>
                        <p class="title" style="width: 350px; max-width: 30%; color:grey;">MS (Civil Engineering) || Transportation Engineer, Caltrans</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">I hold, both a bachelor’s and master’s degree, in Civil Engineering from California State University, Long Beach. While at CSULB, I specialized in transportation engineering, with an emphasis in transportation materials. I also worked as the Lab Technician for the JTCP Lab at CSULB.</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;"> I am currently employed as a Transportation Engineer at the CaltransDistrict 2 headquarters. </p>
                    </div>
                </div>
            </div>

            <!-- Person 3 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/maryam.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%;">Maryam M. Hosseini</h2>
                        <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Civil Engineering), Expected Spring 2019 || CMIT</p>
                        <p class="card-text" style="width: 350px; max-width: 30%; text-align: justify;">I had my first on-campus job as a master's student in Construction Engineering Management at CSULB by being a team member of JTCP in a lab. Although it was a short period of time, I really enjoyed the time with the great team.</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align: justify;">I am currently working with a General Contractor as a Project Engineer Intern, but hope that I can continue the coordination with JTCP and stay in touch so I can deal with new challenges every day and try my best to make a positive impact. </p>
                    </div>
                </div>
            </div>
    </div>
    <!-- /.row -->

    <div class="row">
            <!-- Person 1 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/user1.png" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%">Michael Plotnik
                        <a href="https://www.linkedin.com/in/michael-plotnik-te-703b0580/" target="_blank">
                            <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                        </h2>
                        <p class="title" style="width: 350px; max-width: 30%;color:grey;">MSc, 2013 || TE || Traffic Manager at City of La Habra, Fullerton, California</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;"></p>
                    </div>
                </div>
            </div>

            <!-- Person 2 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/michelle.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%">Michelle Wang
                        <a href="https://www.linkedin.com/in/michelle-wang-3b3459133/" target="_blank">
                            <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                        </h2>
                        <p class="title" style="width: 350px; max-width: 30%;color:grey;">BFA (Animation), May 2018 || Program Coordinator, JTCP</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I am currently working part-time as one of the program coordinators for Caltrans' Joint Training & Certification Program. I graduated with a BFA degree in Animation as of May 2018 and I'm presently taking individual courses in computer animation to further educate myself.</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I'm is also a freelance graphic designer, creating ads for social media and marketing collateral. In the near future, I aspire to obtain a career in the gaming industry and create 3-D models, textures and lighting in order to bring art to life.</p>
                    </div>
                </div>
            </div>

            <!-- Person 3 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/nezar.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%">Nezar Ayoub</h2>
                        <p class="title" style="width: 350px; max-width: 30%;color:grey;">BS (Civil Engineering), Expected Fall 2019</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I am currently finishing my undergraduate degree at CSULB, where my emphasis is in structural engineering. Along with my ongoing degree, I work as a Lab Technician for the Joint Training & Certification Program by Caltrans and CSULB, under Dr. Shadi Saadeh. I get to assist the lab work regularly and therefore, have received intensive hand-on expereience in the Highway Lab at CSULB.</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">In the upcoming summer, I am planning to take the FE exam and am eager to gain an internship to apply my knowledge, to a hands-on and professional setting.</p>
                    </div>
                </div>
            </div>
    </div>
    <!-- /.row -->

    <div class="row">
            <!-- Person 1 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/omer.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%">Omer A Kh. Eljairi
                        <a href="https://www.linkedin.com/in/omer-eljairi-2a678533/" target="_blank">
                            <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                        </h2>
                        <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Civil Engineering), 2011 || Project Engineer, Swinerton Builders</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I received my BS in Civil Engineering from Sirte University (2005), Libya. Then I went on to get my MS in Civil Engineering CSULB in 2011. I was working with Dr. Shadi Saadeh on my Ph.D. in Civil Engineering at the joint program between CSULB and CGU. I was also a former part-time lecturer at CSULB, where I taught CE 205 &amp;CE 359.</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I have worked with Dr. Shadi Saadeh on several projects and carried on research work with his support for several of my academic years. I am currently working as a Project Engineer at Swinerton Builders.</p>
                    </div>
                </div>
            </div>

            <!-- Person 2 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/rachayita.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%">Rachayita Giri
                        <a href="https://www.linkedin.com/in/rachayitagiri/" target="_blank">
                            <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                        </h2>
                        <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Computer Sci.), Fall 2019 || Student Technician and Web Developer, <a href='http://csulb.edu/jtcp' taget='_blank'>JTCP</a></p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I graduated with a B.Tech degree in Information Technology from <a href="https://www.igdtuw.ac.in/" target="_blank">IGDTUW</a>, Delhi, India, in 2017. Currently, I am pursuing my Master's in Computer Science from CSULB. I have been actively working on projects related to Machine/Deep Learning, in the context of social problems. Previously, I have accomplished some research work in the field of Natural Language Processing.</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">For the JTCP program, under Dr. Saadeh, I work as the Student Technician and Web Developer. I am also the developer of this current website.</p>
                    </div>
                </div>
            </div>

            <!-- Person 3 -->
            <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                    <img class="card-img-top" src="../img/students/rohit.jpg" style="width:100%;">
                    <div class="container" style="padding:0 16px;">
                        <h2 style="width: 350px; max-width: 30%">Rohit Ranga Prasad
                        <a href="https://www.linkedin.com/in/rohitrangaprasad/" target="_blank">
                            <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                        </h2>
                        <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Civil Engineering; CEM Specialization), Spring 2019 || Lab Technician, JTCP</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I am currently pursuing my Master's in Civil Engineering with a specialization in Construction Engineering Management (CEM). I have previously worked and gained experience as a Site Engineer in India, as a Quantity Surveyor in UAE, and as a Construction Intern in USA.</p>
                        <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I also work as the Lab Technician at the Joint Training &amp; Certification Program by Caltrans and CSULB, where I benefit from the multicultural and professional experience gained from working in different countries in this field.</p>
                    </div>
                </div>
            </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- Person 1 -->
        <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
            <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <img class="card-img-top" src="../img/students/rushabh.jpg" style="width:100%;">
                <div class="container" style="padding:0 16px;">
                    <h2 style="width: 350px; max-width: 30%">Rushabh Lunkad
                    <a href="https://www.linkedin.com/in/rushabh-lunkad-315605142/" target="_blank">
                        <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                    </h2>
                    <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Civil Engineering), Spring 2019 || Project Engineer, LA</p>
                    <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I am currently pursuing my Master's degree in Civil Engineering at CSULB. I came in contact with Dr. Shadi Saadeh when he hired me to work for the JTCP labs as a Lab Technician. In the brief time I assisted the labs in the JTCP labs, I gained a lot of hands-on expreience working with the vast variety of equipment in the Highway Lab.</p>
                    <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I have a prior experience as a Field Engineer, and presently I work as a Project Engineer in Los Angeles, California.</p>
                </div>
            </div>
        </div>

        <!-- Person 2 -->
        <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
            <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <img class="card-img-top" src="../img/students/rushi1.jpg" style="width:100%;">
                <div class="container" style="padding:0 16px;">
                    <h2 style="width: 350px; max-width: 30%">Rushi Jash
                    <a href="https://www.linkedin.com/in/rishijash/" target="_blank">
                        <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                    </h2>
                    <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Computer Science), Fall 2018 | Web Developer, <a href='http://csulb.edu/jtcp' taget='_blank'>JTCP</a></p>
                    <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I have worked on Dr. Shadi's Joint Training &amp; Certification Program (<a href='http://csulb.edu/jtcp' taget='_blank'>JTCP Project</a>) from Fall 2016 to Fall  2018. I strongly believe that building Dr. Shadi's JTCP website: taking it from <i>'Concept'</i> to <i>'Something thousands of users can use' </i> while earning Masters degree in Computer Science helped me become an accomplished engineer.</p>
                </div>
            </div>
        </div>

        <!-- Person 3 -->
        <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
            <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <img class="card-img-top" src="../img/students/sebastian1.jpg" style="width:100%;">
                <div class="container" style="padding:0 16px;">
                    <h2 style="width: 350px; max-width: 30%">Sebastian Melendez Flores</h2>
                    <p class="title" style="width: 350px; max-width: 30%;color:grey;">BS (Civil Engineering), Expected Fall 2018</p>
                    <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I am young and motivated to grow in the field of engineering. I have a previously had experience with submittals and project bidding as well as direct and indirect cost analysis, project planning, field supervision, and I enjoy working with a team.</p>
                    <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I have also worked as a Lab Technician and assisted with the day-to-day working of the JTCP labs at CSLUB.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- Person 1 -->
        <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
            <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <img class="card-img-top" src="../img/students/user1.png" style="width:100%;">
                <div class="container" style="padding:0 16px;">
                    <h2 style="width: 350px; max-width: 30%">Sophea Ek</h2>
                    <p class="title" style="width: 350px; max-width: 30%;color:grey;">MSc, California State University, Long Beach, 2012 || County of LA</p>
                </div>
            </div>
        </div>

        <!-- Person 2 -->
        <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
            <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <img class="card-img-top" src="../img/students/umang.jpg" style="width:100%;">
                <div class="container" style="padding:0 16px;">
                    <h2 style="width: 350px; max-width: 30%">Umang Mutha
                    <a href="https://www.linkedin.com/in/umang-mutha/" target="_blank">
                        <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                    </h2>
                    <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Civil Engineering), Spring 2019 || Scheduler, GC</p>
                    <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I have three years of experience in the construction Industry, along with the experience of working in different positions like site engineer, Project Coordinator, Assist. Construction Manager. At present I am working as Scheduler for GC. I can work effectively within deadlines in a fast-paced, dynamic environment. I have profound knowledge of different phases of construction. Expertise in scheduling (P6, MS Project), Drafting, Cost tracking, Cost control, handling clients. I am skilled in leadership, problem-solving and multitasking.</p>
                </div>
            </div>
        </div>

        <!-- Person 3 -->
        <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
            <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <img class="card-img-top" src="../img/students/vipin1.jpg" style="width:100%;">
                <div class="container" style="padding:0 16px;">
                    <h2 style="width: 350px; max-width: 30%">Vipin Shaji
                    <a href="https://www.linkedin.com/in/vipin-shaji-eit-cmit-131b60103/" target="_blank">
                        <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                    </h2>
                    <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Civil Engineering), Expected Spring 2019 || EIT, CMIT || Lab Technician, JTCP</p>
                    <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I have an xperience of over a year as a Field Engineer. I currently work at the JTCP lab at CSULB as a Lab Technician, to carry out and help with the lab work, which gives me intensive hands-on experience and knowledge about the standard tests and equipment. I am a Master's candidate in Civil Engineering at CSULB. I like to address challenges facing construction sites and constantly learning new skills and resources on the job to better serve colleagues, clients, and employers.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- Person 1 -->
        <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
            <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <img class="card-img-top" src="../img/students/yagnik.jpg" style="width:100%;">
                <div class="container" style="padding:0 16px;">
                    <h2 style="width: 350px; max-width: 30%">Yagnik Patel
                    <a href="https://www.linkedin.com/in/yagnikpatelcmit/" target="_blank">
                        <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                    </h2>
                    <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Civil Engineering), Expected Fall 2018 || Lab Technician, JTCP</p>
                    <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I am a Certified CMIT (Construction Manager in Training) and an experienced Junior Site Engineer with a demonstrated history of working in the construction industry. Professional skills in Field Supervision, Legal aspects of contracting, Construction Safety Management, Project Planning, Civil Engineers, and Program Management.</p>
                </div>
            </div>
        </div>

        <!-- Person 2 -->
        <div class="column col-md-4" style="float:left; width:33.3%; margin-bottom: 16px; padding: 0 8px;">
            <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <img class="card-img-top" src="../img/students/yazan1.jpg" style="width:100%;">
                <div class="container" style="padding:0 16px;">
                    <h2 style="width: 350px; max-width: 30%">Yazan Ahmad Al-Zubi
                    <a href="https://www.linkedin.com/in/yazan-al-zu-bi-6bb16913a/" target="_blank">
                        <img src="../img/students/linkedin_gold.png" style="float:right;height:1em; width:1em;"></a>
                    </h2>
                    <p class="title" style="width: 350px; max-width: 30%;color:grey;">MS (Civil Engineering), Expected Spring 2019 || Lab Technician, JTCP</p>
                    <p class="card-text" style="width: 350px; max-width: 30%;text-align:justify;">I am a Master's student at CSULB, majoring in Civil Engineering. My fields of interest include Transportation and Planning. I have worked in Saudi Arabia as a site engineer before coming to the USA, and gained extensive on-site experience. I worked on multiple projects like SCB test modeling, fully permeable pavement design, and currently I work as a Lab Technician at the JTCP lab at CSULB. I enjoy challenges that help me evolve and become better.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container-->

<!-- Template for Footer & Scripts-->
<?php
    include 'template_footer.php';
?>