#!/usr/local/php5/bin/php

<!-- Template for Footer & Scripts-->
<?php
    include 'template_menu.html';
?>

<!-- Page Content Research Interests -->
<div class="container" id="publications">

    <!-- Marketing Icons Section -->
    <div class="row">
    
    	<!-- Publications Table -->
        <div class="panel-group" id="accordion">
        <a href="#collapse1" data-toggle="collapse" data-parent="#accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"></h4>
                        <font class="custom_header2" style="color: rgb(177,92,34);">
                            Publications
                        </font>
                </div>   
            </div>
        </a>
        </div>
        <div class="panel-collapse collapse out" id="collapse1" >
                <div class="panel-body" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    <table class="table" id="publishedpapers" style="font-size: 0.8em;">
				        <thead style="background-color:rgba(177,92,34,0.5);">
				        	<th>No.</th>
				            <th>Paper Title and Authors</th>
				        </thead>
				        <tbody>
				        	<tr>
				        		<td>36</td>
				        		<td>Cao. W., Elsifi, M., Cooper, S., <b>Saadeh, S.</b>, “Fatigue Performance Prediction of Asphalt Pavement Based on Semi-Circular Bending Test at Intermediate Temperature.” ASCE Journal of Materials in Civil Engineering, Volume 30, Issue 9, ISSN (print): 0899-1561 | ISSN (online): 1943-5533, <a href="https://doi.org/10.1061/(ASCE)MT.1943-5533.0002448" target="_blank">DOI</a>.</td>
				        	</tr>
				        	<tr>
				        		<td>35</td>
				        		<td><b>Saadeh, S.</b>, Eljairi, O.*, “Comparison of Fracture Properties of Asphalt Concrete in Semi-Circular Bend Test Using Non-Contact Camera and Crosshead Movement.” Journal of Materials in Civil Engineering, v 30, n 6, June 1, 2018; ISSN: 08991561; <a href="https://doi.org/10.1061/(ASCE)MT.1943-5533.0002266" target="_blank"> DOI</a> ; Article number: 04018078.</td>
				        	</tr>
				        	<tr>
				        		<td>34</td>
				        		<td><b>Saadeh, S.</b>, Renteria, D., Mahmoud, E., Eljairi, O.*, “Numerical Evaluation of Semi-Circular Bending Test Variability.” ASCE Journal of Materials in Civil Engineering, v 30, n 6, June 1, 2018;  ISSN: 08991561; <a href="https://doi.org/10.1061/(ASCE)MT.1943-5533.0002265">DOI</a> ;  Article number: 04018085.</td>
				        	</tr>
				        	<tr>
				        		<td>33</td>
				        		<td>Ralla, A.*, <b>Saadeh, S.</b>, Harvey, J., Mahmoud, E., “Sustainable Mitigation of Stormwater Runoff using Fully Permeable Pavement”, International Conference on Advances in Materials and Pavement Performance Prediction (AM3P 2018), April 16-18, 2018, Doha, Qatar, London: CRC Press.</td>
				        	</tr>
				            <tr>
				            	<td>32</td>
				                <td><b>Saadeh, Shadi</b>; and Mohammad, Louay, “Moisture Sensitivity of Warm and Hot Mix Asphalt: Comparison of Loaded Wheel Tracking and Modified Lottman Tests”, ASCE, n GSP 277, p 272-280, 2017.</td>
				                
				            </tr>
				            <tr>
				            	<td>31</td>
				                <td>Ashraf Rahim, Kevin Carstens, <b>Shadi Saadeh</b>, Omer Eljairi, and DingXin Cheng, “Field Performance Of Hot Applied Modified Binder Chip Seal In California”. Proceedings, TRB 2017 Annual Meeting, Transportation Research Board, Washington, DC, 2017.</td>
				            </tr>
				            <tr>
				            	<td>30</td>
				            	<td>Mahmoud, E. and <b>Saadeh, S.</b> and Yanz, R. “Feasibility of Discrete Element to Simulate RAP Asphalt Mixtures Fracture Behavior”, ASCE Geotechnical Special Publication, v 2016-January, n 270 GSP, p 577-587, 2016.</td>
				            </tr>
				            <tr>
				            	<td>29</td>
				            	<td><b>Shadi Saadeh</b>, Christian Castro-Quilizapa, Hamed Hakimelahi, “Fracture Properties Comparison of Asphalt Concrete Using Beam Fatigue and Semicircular Bending with Noncontact Camera and Crosshead Movement”, Proceedings TRB 2016 Annual Meeting, Transportation Research Board, Washington, DC, 2016.</td>
				            </tr>
				            <tr>
				            	<td>28</td>
				            	<td>Mahmoud, E. and <b>Saadeh, S.</b> “Discrete Element Simulation of Asphalt Mixtures Fracture” TMS Middle East - Mediterranean Materials Congress on Energy and Infrastructure Systems (MEMA 2015) January 11-14, 2015 • Doha, Qatar. </td>
				            </tr>
				            <tr>
				            	<td>27</td>
				            	<td><b>Saadeh, S.</b>, Hakimelahi H., and Harvey J. (2014) Correlation of Semi-circular Bending and Beam Fatigue Fracture Properties of Asphalt Concrete Using Non-Contact Camera and Crosshead Movement. Proceedings of the 2nd Transportation and Development Institute Congress, p 39-48, 2014, T and DI Congress doi: 10.1061/9780784413586.004.</td>
				            </tr>
				            <tr>
				            	<td>26</td>
				            	<td>Mahmoud, E., <b>Saadeh, S.</b>, Hakimelahi*, “Extended Finite Element Modeling of Asphalt Mixtures Fracture Properties Using the Semi-Circular Bending Test”.  Journal of Road Materials and Pavement Design, v 15, n 1, p 153-166, January 2014.</td>
				            </tr>
				            <tr>
				            	<td>25</td>
				            	<td>Mahmoud, E., <b>Saadeh, S.</b>, Hakimelahi*, H., Harvey, J., “Modeling and Experimental Measurements of Asphalt Mixtures Fracture Properties Using the Semi-Circular Bending Test.” Proceedings TRB 2013 Annual Meeting, Transportation Research Board, Washington, DC, 2013.</td>
				            </tr>
				            <tr>
				            	<td>24</td>
				            	<td>Hakimelahi*, H., <b>Saadeh, S.</b>, Harvey, J., “Comparison of Fracture Properties of Four Point Beam and Semi Circular Bending of Asphalt Concrete.” Journal of Road Materials and Pavement Design, V 14, Supplement 2, pp 252 - 265, 2013.</td>
				            </tr>
				            <tr>
				            	<td>23</td>
				            	<td><b>Saadeh, S.</b>, Eljairi, O.*, Kramer, B., Hajj, E “Development of Quality Control Test Procedure for Characterizing Fracture Properties of Asphalt Mixtures”. Four-Point Bending, Taylor and Francs Group, London, ISBN 978-0-415-64331-3, pp. 223 – 238, 2012.</td>
				            </tr>
				            <tr>
				            	<td>22</td>
				            	<td><b>Saadeh, S.</b>, Eljairi, O.*, Kramer, B., Hajj, E., “Development Quality Control Test Procedure for Characterizing Fracture Properties of Asphalt Mixtures”, ASCE Engineering Mechanics Institute Conference, Boston, MA, June 2-4, 2011.</td>
				            </tr>
				            <tr>
				            	<td>21</td>
				            	<td><b>Saadeh, S.</b>, Masad, E. “On the Relationship of Microstructure Properties of Asphalt Mixtures to Their Constitutive Behavior” International Journal of Materials and Structural Integrity, Vol. 4, Nos. 2/3/4, 2010.</td>
				            </tr>
				            <tr>
				            	<td>20</td>
				            	<td><b>Saadeh, S.</b>, Density Comparison of Conventional and Warm Asphalt Mixtures Containing Sasobit®, 16th Annual Great Lakes Geotechnical/ Geoenvironmental Conference, Milwaukee, WI, 2010.</td>
				            </tr>
				            <tr>
				            	<td>19</td>
				            	<td>Mohammad, L., <b>Saadeh, S.</b>, Obulareddy, S., and Cooper, S., “Characterization of Louisiana Asphalt Mixtures Using Simple Performance Tests,” ASTM Journal of Testing and Evaluation, Vol. 36, No. 1, 2008, pp. 5-16.</td>
				            </tr>
				            <tr>
				            	<td>18</td>
				            	<td>Mohammad, L., <b>Saadeh, S.</b>, and Kabir, M. D., “Evaluation of Fracture Properties of Hot Mix Asphalt” 6th RILEM International Conference on Cracking in Pavements, Pavement Cracking: Mechanisms, Modeling, Detection, Testing and Case Histories, p 427-436, Chicago, Illinois, June 16-18, 2008.</td>
				            </tr>
				            <tr>
				            	<td>17</td>
				            	<td>Mohammad, L.; <b>Saadeh, S.</b>; Kabir, M.; Othman, A.; Cooper, S. “Mechanistic properties of hot-mix asphalt mixtures containing hydrated lime”, Journal of the Transportation Research Board, n 2051, p 49-63, 2008. </td>
				            </tr>
				            <tr>
				            	<td>16</td>
				            	<td>Mohammad, L., and <b>Saadeh, S.</b>, “Performance Evaluation of Stabilized Base and Subbase Material,” ASCE Geotechnical Special Publication No. 1078, The Challenge of Sustainability in the Geoenvironment, 2008, pp. 1073-1080.</td>
				            </tr>
				            <tr>
				            	<td>15</td>
				            	<td>Mohammad, L., <b>Saadeh, S.</b>, and Cooper S., “Evaluation of Asphalt Mixtures Containing Sasobit Warm Mix Additive.” ASCE Geotechnical Special Publication No. 1078, The Challenge of Sustainability in the Geoenvironment, 2008, pp. 1016-1023. </td>
				            </tr>
				            <tr>
				            	<td>14</td>
				            	<td>Mohammad, L., <b>Saadeh, S.</b>, Qi, Y., Bottom, J., and Scherocman, J.  “Worldwide State of Practice on the Use of Tack Coats: Survey”. Journal of the Association of Asphalt Paving Technologists, Vol. 77, 2008.</td>
				            </tr>
				            <tr>
				            	<td>13</td>
				            	<td>Mohammad, L., <b>Saadeh, S.</b>, Zhang, C., Cooper, S., Abadie, C., and Khattak, J. “Comparative Study of The Mechanical Properties of HMA Mixture: Field Vs Laboratory”. Journal of the Association of Asphalt Paving Technologists, Vol. 76, 2007.</td>
				            </tr>
				            <tr>
				            	<td>12</td>
				            	<td>Mohammad L., <b>Saadeh, S.</b> “Evaluation Of The Rutting Susceptibility Of Louisiana Superpave Mixtures” The 5th International Conference on Maintenance and Rehabilitation of Pavements and Technological Control, The Canyons Resort, Park City, Utah, August 8-10, 2007.</td>
				            </tr>
				            <tr>
				            	<td>11</td>
				            	<td>Mohammad, L., <b>Saadeh, S.</b>, Obulareddy, and S., Cooper, S. “Characterization Of Louisiana Asphalt Mixtures Using Simple Performance Tests”. Accepted for presentation. Journal of the Transportation Research Board, Transportation Research Record, 2007.</td>
				            </tr>
				            <tr>
				            	<td>10</td>
				            	<td>Mohammad, L., <b>Saadeh, S.</b> “Laboratory Evaluation Of Asphalt Mixtures Using Simple Performance Tests”. 4th International Conference; Bituminous Mixture and Pavements Thessaloniki, Greece, 19-20 April 2007.</td>
				            </tr>
				            <tr>
				            	<td>9</td>
				            	<td><b>Saadeh, S.</b>, Masad, E., and Little D. “Characterization of Hot Mix Asphalt Using Anisotropic Damage Viscoelastic-Viscoplastic Model and Repeated Loading” ASCE Journal of Materials in Civil Engineering, v 19, n 10, October, 2007, p 912-924.</td>
				            </tr>
				            <tr>
				            	<td>8</td>
				            	<td><b>Saadeh, S.</b>, Masad, E. “A Viscoelastic-Viscoplastic Damage Model for Asphalt Mixes” 15th U.S. National Congress on Theoretical and Applied Mechanics, Boulder, CO, June 2006.</td>
				            </tr>
				            <tr>
				            	<td>7</td>
				            	<td>Mohammad L., Raqib M., and <b>Saadeh, S.</b>, “Laboratory Evaluation of Asphalt Tack Coat Materials on Interface Bond Strength”, 12th REAAA Conference, Philippines, November 20-24, 2006.</td>
				            </tr>
				            <tr>
				            	<td>6</td>
				            	<td>Tashman, L., Masad, E., <b>Saadeh, S.</b>, and Little, D., “Non-Associated Viscoplastic Model For Asphalt Mixes Based On Microstructure Analysis”. Proceedings of the 17th Engineering Mechanics Conference. 2004.</td>
				            </tr>
				            <tr>
				            	<td>5</td>
				            	<td>Dessouky, S., <b>Saadeh, S.</b>, Masad, E., and Little, D. (2004).  “Microstructural Viscoplastic Continuum model for asphalt mixes,” International Conference on Computational & Experimental Engineering and Sciences (ICCES’04), Madeira, Portugal, July 26-29.</td>
				            </tr>
				            <tr>
				            	<td>4</td>
				            	<td>Masad, E., <b>Saadeh, S.</b>, Al-Rousan, T., Garboczi, E., Little, D. (2004) “Computations Of Particle Surface Characteristics Using Optical and X-Ray CT Images,” Journal of Computational Materials Science, Vol. 34, No. 4, pp. 406-424. </td>
				            </tr>
				            <tr>
				            	<td>3</td>
				            	<td><b>Saadeh, S.</b>, Masad, E., Stuart, K., Abbas, A., Papagainnakis, T., Al-Khateeb, G. (2003).  "Comparative Analysis of Axial and Shear Viscoelastic Properties of Asphalt Mixes”, Journal of the Association of Asphalt Paving Technologists, Vol. 72, p 122-153.</td>
				            </tr>
				            <tr>
				            	<td>2</td>
				            	<td><b>Saadeh, S.</b>, Masad, E., Garboczi, E., Harman, T., “Aggregate Shape Analysis Using X-Ray Computed Tomography”. Proceedings of the 11th Symposium of the International Center for Aggregate Research, Austin, TX. 2003. (CD Publications).</td>
				            </tr>
				            <tr>
				            	<td>1</td>
				            	<td><b>Saadeh, S.</b>, Tashman, L., Masad, E., and Mogawer, W. (2002).  “Spatial and Directional Distributions of Aggregates in Asphalt Mixes,” Journal of Testing and Evaluation, American Society for Testing and Materials, ASTM, Vol. 30, No. 6, pp. 483-491.</td>
				            </tr>
				        </tbody>
				   	</table>
    				<hr>

                </div>
        </div>
        <!-- ./Publications Table -->

        <!-- Presentations Table -->
        <div class="panel-group" id="accordion">
        <a href="#collapse2" data-toggle="collapse" data-parent="#accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"></h4>
                        <font class="custom_header2" style="color: rgb(177,92,34);">
                            Presentations
                        </font>
                </div>   
            </div>
        </a>
        </div>
        <div class="panel-collapse collapse out" id="collapse2" >
                <div class="panel-body" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    <table class="table" id="presentations1" style="font-size: 0.8em;">
				        <thead style="background-color:rgba(177,92,34,0.5);">
				        	<th>No.</th>
				            <th>Title</th>
				        </thead>
				        <tbody>
				            <tr>
				            	<td>11</td>
				                <td>Rahim, S., Carstens, K., <b>Saadeh, S.</b>, Eljairi, O.*, and Cheng, D.* “Field Performance Of Hot Applied Modified Binder Chip Seal In California”. Proceedings, <i>TRB 2017 Annual Meeting</i>, Transportation Research Board, Washington, DC, 2017</td>
				            </tr>
				            <tr>
				            	<td>10</td>
				                <td><b>Saadeh, S.</b>, Castro-Quilizapa, C.*, Hakimelahi, H.*, “Fracture Properties Comparison of Asphalt Concrete Using Beam Fatigue and Semicircular Bending with Noncontact Camera and Crosshead Movement”, <i>Proceedings TRB 2016 Annual Meeting</i>, Transportation Research Board, Washington, DC, 2016</td>
				            </tr>
				            <tr>
				            	<td>9</td>
				                <td>Mahmoud, E. and <b>Saadeh, S.</b> “Discrete Element Simulation of Asphalt Mixtures Fracture” TMS Middle East - <i>Mediterranean Materials Congress on Energy and Infrastructure Systems</i> (MEMA 2015) January 11-14, 2015 • Doha, Qatar</td>
				            </tr>
				            <tr>
				            	<td>8</td>
				                <td>Saadeh S., Hakimelahi H.*, and Harvey J. (2014) Correlation of Semi-circular Bending and Beam Fatigue Fracture Properties of Asphalt Concrete Using Non-Contact Camera and Crosshead Movement. Proceedings of the 2 nd Transportation and Development Institute Congress, p 39-48, 2014, T and DI Congress doi: 10.1061/9780784413586.004</td>
				            </tr>
				            <tr>
				            	<td>7</td>
				                <td><b>Saadeh, S.</b>, Hakimelahi*, H. “Investigation of Fracture Properties of California Asphalt Concrete Mixtures,” CSULB Foundation, Nov. 16, 2011.</td>
				            </tr>
				            <tr>
				            	<td>6</td>
				                <td><b>Saadeh, S.</b>, Eljairi*, O., Kramer, B., Hajj, E “Development of Quality Control Test Procedure for Characterizing Fracture Properties of Asphalt Mixtures,” University of Southern California, Los Angeles, Oct. 28, 2011.</td>
				            </tr>
				            <tr>
				            	<td>5</td>
				                <td>Shadi S., Density Comparison of Conventional and Warm Asphalt Mixtures Containing Sasobit <sup>&reg;</sup>, 16th Annual Great Lakes Geotechnical/Geoenvironmental Conference, Milwaukee, WI, 2010.</td>
				            </tr>
				            <tr>
				            	<td>4</td>
				                <td>Mohammad, L., <b>Saadeh, S.</b>, Qi, Y., Button, J., Scherocman, J., “Worldwide State of Practice on the Use of Tack Coats: A Survey,” <i>Journal of the Association of Asphalt Paving Technologist</i>, Vol. 77, 2008.</td>
				            </tr>
				            <tr>
				            	<td>3</td>
				                <td>Mohammad, L.; <b>Saadeh, S.</b>; Kabir, M.; Othman, A.; Cooper, S. “Mechanistic Properties of Hot-Mix Asphalt Mixtures Containing Hydrated Lime,” <i>Transportation Research Record</i>, n 2051, p 49-63, 2008.</td>
				            </tr>
				            <tr>
				            	<td>2</td>
				                <td>Mohammad, L., <b>Saadeh, S.</b>, and Cooper S., “Evaluation of Asphalt Mixtures Containing Sasobit Warm Mix Additive,” Presented at the <i>GeoCongress 2008: The Challenge of Sustainability in the Geoenvironment</i>, New Orleans, Louisiana, March 9-12, 2008.</td>
				            </tr>
				            <tr>
				            	<td>1</td>
				                <td>Mohammad, L., and <b>Saadeh, S.</b>, “Performance Evaluation of Stabilized Base and Subbase Material,” Presented at the <i>GeoCongress 2008: The Challenge of Sustainability in the Geoenvironment</i>, New Orleans, Louisiana, March 9-12, 2008.</td>
				            </tr>
				        </tbody>
				    </table>
				    <hr>
                </div>
        </div>
        <!-- ./Presentations Table -->

        <!-- Technical Reports -->
        <div class="panel-group" id="accordion">
        <a href="#collapse3" data-toggle="collapse" data-parent="#accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"></h4>
                        <font class="custom_header2" style="color: rgb(177,92,34);">
                            Technical Reports
                        </font>
                </div>   
            </div>
        </a>
        </div>
        <div class="panel-collapse collapse out" id="collapse3" >
                <div class="panel-body" style="font-size: 1.3em; color: rgba(51,51,51,0.8);">
                    <table class="table" id="techreports" style="font-size: 0.8em;">
				        <thead style="background-color:rgba(177,92,34,0.5);">
				        	<th>No.</th>
				            <th>Title</th>
				        </thead>
				        <tbody>
				            <tr>
				            	<td>8</td>
				                <td>Ashraf Rahim, Kevin Carstens, Shadi Saadeh, Omer Eljairi, and DingXin Cheng, “Performance Evaluation of Modified Binder Seal Coat Projects for Caltrans: Summary Report”, California Pavement Preservation Center, CP2-2015-110.</td>
				            </tr>
				            <tr>
				            	<td>7</td>
				                <td>Shadi Saadeh and Omer Eljairi, “Rubber Modified Seal Coat Pilot Project SBD-95-PM 56.1/64.5 (EA 08-0Q4904) Vol 2: Interim Report-One Year Evaluation”, CP2-2016-101.</td>
				            </tr>
				            <tr>
				            	<td>6</td>
				                <td>Saadeh, S., Hakimelahi, H., “Investigation of Fracture Properties of California Asphalt Concrete Mixtures”, National Center for Metropolitan Transportation Research, Report number 11-21, 2011</td>
				            </tr>
				            <tr>
				            	<td>5</td>
				                <td>Saadeh, S., Eljairi, O.*, “Development of Quality Control Test Procedure for Characterizing Fracture Properties of asphalt mixtures”, National Center for Metropolitan Transportation Research, Report number 10-24, 2010.</td>
				            </tr>
				            <tr>
				            	<td>4</td>
				                <td>Mohammad, L., and Saadeh, S., <i>Comparison of the In Situ Strength and Laboratory Mechanical Properties of Asphalt Concrete Mixtures</i>, Louisiana Transportation Research Center, Technical Report Number 02-3B, 2006.</td>
				            </tr>
				            <tr>
				            	<td>3</td>
				                <td>Cooper, S., Mohammad, L., and Saadeh, S., “<i>Evaluation Of HMA Mixtures Containing Sasobit&reg;</i>”, Louisiana Transportation Research Center, Technical Assistance Report Number 06-1TA, July 2006. </td>
				            </tr>
				            <tr>
				            	<td>2</td>
				                <td>Masad, E., Little, D., Tashman, L., Saadeh, S., Al-Rousan, T., Sukhwani, R. (2003).  <i>Evaluation of Aggregate Characteristics Affecting HMA Concrete Performance.  Final Report of ICAR 203 Project</i>, The Aggregate Foundation of Technology, Research, and Education, VA., 204 pp.</td>
				            </tr>
				            <tr>
				            	<td>1</td>
				                <td>Saadeh, S., and Masad, E. (2002).  <i>Comparative Analysis of Axial and Shear Moduli of Asphalt Mixes</i>, Final Report Submitted to the Federal Highway Administration and the Asphalt Institute, Washington Center for Asphalt Technology, Pullman, WA (WSU/WCAT 01-3), 177 pp.</td>
				            </tr>
				        </tbody>
				    </table>
				    <hr>
                </div>
        </div>
        <!-- ./Technical Reports -->
        <p style="font-size: 1em;">*My graduate student</p>
</div>

</div>
<!-- /.container -->

<!-- Template for Footer & Scripts-->
<?php
    include 'template_footer.php';
?>