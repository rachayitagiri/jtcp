<?php

    include 'dbmanage.php';

    $formdata = array();
    $formdata['company'] = $_POST['company'];
    $formdata['firstname'] = $_POST['firstname'];
    $formdata['lastname'] = $_POST['lastname'];
    $formdata['zipcode'] = $_POST['zipcode'];
    $formdata['city'] = $_POST['city'];
    $formdata['certificate_expire'] = $_POST['certificate_expire'];

    if($formdata['certificate_expire']=="")
    {
        $formdata['certificate_expire'] = '2050-12-12';
    }

    $ret = searchEmployees($formdata,0);

?>

<head>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Name</th>
<th>Company</th>
<th>Position</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<?php
    for($i=0;$i<count($ret);$i++)
    {
        $name_results[] = $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
        $eid_results[] = $ret[$i]['Employee_ID'];
        ?>
<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
?>
</td>
<td>
<?php
    echo $ret[$i]['Company_Name'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Employee_Position'];
    ?>
</td>
<td>
<?php
    echo "<a href='javascript:void(0)' class='clickmodel' id='$i'>View</a>";
    ?>
</td>
</tr>
<?php
    }
    ?>
</tbody>
</table>


<!-- Modal -->
<div class="modal fade" id="userprogressmodel" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" id="userprogressmodel-tittle">Modal Header</h4>
</div>
<div class="modal-body">
<p id="showtable_progress">Loading...</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>



<script type="text/javascript">

$(document).ready(function(){
                  $(".clickmodel").click(function(){
                                         $('#userprogressmodel').modal('toggle');
                                         var id = this.id;
                                         var name_jArray= <?php echo json_encode($name_results); ?>;
                                         var eid_jArray= <?php echo json_encode($eid_results); ?>;
                                         var name = name_jArray[id];
                                         document.getElementById('userprogressmodel-tittle').innerHTML = name;
                                          $.ajax({
                                          url: "getsearch_progress.php",
                                          type: "post",
                                          data: {'eid' : eid_jArray[id]} ,
                                          success: function (response) {
                                          $('#showtable_progress').empty();
                                          $('#showtable_progress').append(response);
                                          },
                                          error: function(jqXHR, textStatus, errorThrown) {
                                          console.log(textStatus, errorThrown);
                                          }
                                          });
                                         });
                  });
</script>
