<?php
include '../dbmanage.php';
//This IF-block downloads the feedbacks in a CSV file 
    if (isset($_POST['export'])) 
    {
        header('Content-Type: text/csv; charset=utf-16');
        header('Content-Disposition: attachment; filename=feedback.csv');
        $output = fopen("php://output", "w");
        fputcsv($output, array('S.No.', 'Name', 'Email', 'Webpage', 'Type', 'Comment', 'Time', 'Status'));
        $result = getFeedback();
        for($i=0; $i < count($result); $i++) {
            fputcsv($output, $result[$i]);
        }
        fclose($output);

    }
?>