<?php
    include 'template1.php';

      //the if loop to check whether a CalAPA transaction is being added or not
      if ($_POST['calapa_submit']) {
        $tid = $_POST['calapa_tid'];
        $amt = $_POST['calapa_amt'];
        $date = $_POST['calapa_date'];
        $payee = $_POST['calapa_payee'];
        $class = $_POST['calapa_class'];
        $ret = addCalAPAData($tid, $amt, $date, $payee, $class);
        echo '<script type="text/javascript"> alert("'.$ret['Message'].'"); </script>';
      }
    
?>

<title>CalAPA Transactions</title>
<div class="row">
<div class="col-lg-12">
  <h1 class="page-header">CalAPA Transactions</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php
    $ret2 = getCalAPAData();
?>

<form method="post" enctype="multipart/form-data">
    <div class="form-group col-lg-4">
      <label>Transaction ID</label>
      <input type="text" name="calapa_tid" class="form-control" placeholder="Enter the Transaction ID as provided by CalAPA">
    </div>
    <div class="form-group col-lg-4">
      <label>Payee Name</label>
      <input type="text" name="calapa_payee" class="form-control" placeholder="Enter the name of payee for this transaction">
    </div>
    <div class="form-group col-lg-4">
      <label>Amount of Transaction</label>
      <input type="text" name="calapa_amt" class="form-control" placeholder="Enter the amount">
    </div>
    <div class="form-group col-lg-5">
      <label>Classes paid for</label>
      <input type="text" name="calapa_class" class="form-control" placeholder="Enter the classes the fees was paid for, separated by comma">
    </div>
    <div class="form-group col-lg-5">
      <label>Student name(s)</label>
      <input type="" name="calapa_students" id="calapa_students" class="form-control" placeholder="Enter names of the students, separated by comma">
    </div>
    <div class="form-group col-lg-2">
      <label>Date of Transaction</label>
      <input type="" name="calapa_date" id="calapa_date" class="form-control" placeholder="Date of Transaction">
    </div>
    <div class="form-group col-lg-2">
      <label></label><br>
      <input type="submit" value="Add Transaction" name="calapa_submit" class="submit btn btn-primary small" style="margin-top:5px;">
    </div>
</form>

  <table class="table">
  <thead>
    <tr>
      <th>No</th>
      <th>Transaction ID</th>
      <th>Payee Name</th>
      <th>Classes</th>
      <th>Amount</th>
      <th>Timestamp</th>
    </tr>
  </thead>
  <tbody>
    <?php
      for($i=0;$i<count($ret2);$i++)
      {
    ?>
    <tr>
      <td><?php echo $i+1; ?></td>
      <td><?php echo $ret2[$i]['Transaction_ID']; ?></td>
      <td><?php echo $ret2[$i]['Payee_Name']?></td>
      <td><?php echo $ret2[$i]['Paypal_Classes']; ?></td>
      <td><?php echo $ret2[$i]['Amount']; ?></td>
      <td><?php echo $ret2[$i]['Timestamp'];?></td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>

<script>
  $(function() {
  $('#calapa_date').datepicker({
      dateFormat: 'yy-mm-dd'
  });
  });
</script>

<?php
    include 'template2.php';
?>
