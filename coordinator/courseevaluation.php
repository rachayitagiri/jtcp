<?php
    include 'template1.php';
?>

<title>Course Evaluation Feedbacks</title>
<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Course Evaluation Feedbacks</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<form method="get">
<label>Class</label>
<select name="classid" class="form-control" id="comp">
  <option disabled selected value> -- select Class -- </option>
  <?php
    $ret = getClasses();
    for($i=0;$i<count($ret);$i++){
      echo "<option value='". $ret[$i]['Class_ID'] . "'>" . $ret[$i]['Class_Name'] . "</option>";
    }
   ?>
</select>
<input type="submit">
</form>
<br>
<?php
  if($_GET['classid']!=null)
  {
    $cid = $_GET['classid'];
    $ret = getCourseEvaluationAnswerByClassId($cid);
    if($ret==null){
      echo "Course Evaluation Feedback not Found for selected Class";
    }
    else{
?>

<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
Course Evaluation Feedbacks
</div>
<!-- /.panel-heading -->
<div class="panel-body" id="">
<!-- /.table-responsive -->
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<form method="POST">
<tr>
<th>No</th>
<th>Question</th>
<th>Answer</th>

</tr>
</thead>
<tbody>
<?php
    //for sorting based on name
    $last_eid = 0;
    //display course feedbacks
    for($i=0;$i<count($ret);$i++)
    {
?>
<?php
  if($last_eid!=$ret[$i]['Employee_ID']){
    $last_eid = $ret[$i]['Employee_ID'];
?>
<tr class="odd gradeX">
  <td colspan="3"><?php echo $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName']; ?></td>
</tr>
<?php
  }
?>
<tr class="odd gradeX">
<td>
<?php
    echo $i+1;
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Course_Evaluation_Question_Question'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Course_Evaluation_Answer_Value'];
    ?>
</td>
</tr>
</form>

<?php
    }
    ?>
</tbody>
</table>







</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->


<?php
      }
    }
    include 'template2.php';
?>
