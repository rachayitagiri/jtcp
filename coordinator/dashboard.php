<?php
    include 'template1.php';
?>
<script type="text/javascript">
$(document).ready(function(){
    $("#search_btn").click(function(){
        getCerti();
    });
});
function getCerti(){
    var cc = document.getElementById('cc').value;
    $.ajax({
           url: "jqScripts/getCertificate.php",
           type: "post",
           data: {'cc' : cc},
           success: function (response) {
             $('#showtable').empty();
             $('#showtable').append(response);
           },
           error: function(jqXHR, textStatus, errorThrown) {
             console.log(textStatus, errorThrown);
           }
    });
}
</script>
<title><?php echo  $ret['Coordinator_FirstName'] . "'s "; ?>Dashboard</title>

<?php
    $temp_ret = getCounts();
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Dashboard</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-mortar-board fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $temp_ret['Employee_Count']; ?></div>
                            <div>Employees</div>
                        </div>
                    </div>
                </div>
                <a href="employees.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-calendar fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $temp_ret['Class_Count']; ?></div>
                            <div>Classes</div>
                        </div>
                    </div>
                </div>
                <a href="classes.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $temp_ret['Instructor_Count']; ?></div>
                            <div>Instructors</div>
                        </div>
                    </div>
                </div>
                <a href="instructors.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
                </a>
            </div>
        </div>
    </div>

<h3 class="page-header">Notifications</h3>
<div class="form-group col-sm-12">
  <ul>
  <?php
    $temp_ret = getLowStudentsClasses();
    foreach($temp_ret as $temp)
    {
      echo "<li>Class " . $temp['Class_Name'] . " has less than 20 students (" . $temp['Class_Filled'] . "/" . $temp['Class_Capacity'] . " Students) Enrolled and it is starting in just " . $temp['Class_Diff'] . " days. <a href='classes.php'>Click here</a> to cancel the class.</li>";
    }
    // print_r($temp_ret);
  ?>
  </ul>
</div>

<h3 class="page-header">Search Certificate</h3>

<div class="form-group col-sm-3">
</div>

<div class="form-group col-sm-6">
<input type="" name="" class="form-control" id="cc" value="" placeholder="Certificate Code">
</div>

<div class="form-group col-sm-3">
</div>

<center>

<div class="col-sm-12">
<button type="button" id="search_btn" class="btn btn-primary">Search</button>
</div>
</center>

<div class="panel-body" id="showtable">
<!-- /.table-responsive -->
</div>

</div>



</div>


<?php
    include 'template2.php';
?>
