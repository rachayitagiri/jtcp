<?php
  error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
    if($_POST['cid']==null)
    {
        header("Location: dashboard.php");
    }
    include 'template1.php';
    $cid = $_POST['cid'];
    $ret = searchEmployeesByClass($cid);

    function compareByFirstName($a, $b) {
    return strcasecmp($a["Employee_FirstName"], $b["Employee_FirstName"]);
    }
    usort($ret, 'compareByFirstName');
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

$(document).ready(function(){
                  $(".written_edit").on('change', function() {
                                        var id = this.value.substr(0,this.value.length-1);
                                        var cc = document.getElementById('cc_'+id).value;
                                        var written = this.value.substr(this.value.length - 1);
                                        $.ajax({
                                               url: "jqScripts/changeCertificateScoreWritten.php",
                                               type: "post",
                                               data: {'cc': cc, 'written': written},
                                               success: function (response) {
                                               alert(response);
                                               },
                                               error: function(jqXHR, textStatus, errorThrown) {
                                               alert(errorThrown);
                                               console.log(textStatus, errorThrown);
                                               }
                                               });
                                        });
                  $(".practical_edit").on('change', function() {
                                          var id = this.value.substr(0,this.value.length-1);
                                          var cc = document.getElementById('cc_'+id).value;
                                          var practical = this.value.substr(this.value.length - 1);
                                          $.ajax({
                                                 url: "jqScripts/changeCertificateScorePractical.php",
                                                 type: "post",
                                                 data: {'cc': cc, 'practical': practical},
                                                 success: function (response) {
                                                 alert(response);
                                                 },
                                                 error: function(jqXHR, textStatus, errorThrown) {
                                                 alert(errorThrown);
                                                 console.log(textStatus, errorThrown);
                                                 }
                                                 });
                                          });
                  $(".certify").click(function(){
                                      var cc = document.getElementById('cc_'+this.id).value;
                                      var cc2 = document.getElementById('cc2_'+this.id).value;
                                      $.ajax({
                                             url: "jqScripts/releaseCertificate.php",
                                             type: "post",
                                             data: {'cc' : cc, 'cc2' : cc2},
                                             success: function (response) {
                                             alert(response);
                                             },
                                             error: function(jqXHR, textStatus, errorThrown) {
                                             alert(errorThrown);
                                             console.log(textStatus, errorThrown);
                                             }
                                             });
                                          });
                  $(".drop").click(function(){
                    var cc2 = document.getElementById('cc2_'+this.id).value;
                    var no_waitlist_email = '0';
                    $('input:checkbox[id=no_waitlist_email]').each(function(){    
                        if($(this).is(':checked'))
                          no_waitlist_email = $(this).val();
                    });
                    $.ajax({
                           url: "jqScripts/dropClassAdmin.php",
                           type: "post",
                           data: {'cc2' : cc2, 'no_waitlist_email': no_waitlist_email},
                           success: function (response) {
                           alert(response);
                           window.location.reload(false);
                           },
                           error: function(jqXHR, textStatus, errorThrown) {
                           alert(errorThrown);
                           console.log(textStatus, errorThrown);
                           }
                           });
                      });
                  });
</script>

<title>Class Roster/Grade/Drop Student</title>


<div class="row">

<div class="col-lg-12">
<h1 class="page-header">Class Roster/Grade/Drop Student</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<a href="downloadRoster.php?cid=<?php echo $cid; ?>" target="_blank"><input type="button" value="Download Roster" style="margin-bottom: 10px;" /></a>
<form action='dropClassAdmin.php' method='post'>
  DON'T send waitlist email to students. <input type='checkbox' id='no_waitlist_email' value='1'>  
</form>
<br>
<div class="row" id="printableArea">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
<b>Class :</b> <?php echo $ret[0]['Class_Name']; ?>
</div>
<div id="statistics">
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>No</th>
<th>Name</th>
<th>Email</th>
<th>Mobile</th>
<th>Company</th>
<th>Practical Grade</th>
<th>Written Grade</th>
<th>Fees Paid</th>
<th>Transcation ID</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php
    $index = 1;
    $total_caltrans = 0;
    $total_calapa = 0;

    for($i=0;$i<count($ret);$i++)
    {
        if(strcmp($ret[$i]['Company_Other'],'Caltrans')==0){
          $total_caltrans++;
        }
        $name_results[] = $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
        $eid_results[] = $ret[$i]['Employee_ID'];
        $lid_results[] = $ret[$i]['Login_ID'];
        ?>
<input type="text" value="<?php echo $ret[$i]['Certificate_Code']; ?>" id="<?php echo 'cc_'.$i; ?>" hidden>

<input type="text" value="<?php echo $ret[$i]['Prerequisite_Certificate_Code']; ?>" id="<?php echo 'cc2_'.$i; ?>" >



<tr class="odd gradeX">
<td><?php echo $index; ?></td>
<td><?php echo $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName']; ?></td>
<td><?php echo $ret[$i]['Login_Email']; ?> </td>
<td><?php echo $ret[$i]['Employee_Mobile']; ?></td>
<td><?php echo $ret[$i]['Company_Other']; ?></td>
<td>
<?php
    echo "<select class='practical_edit'>";

//    if($ret[$i]['Status_ID']==2)
//    {
//        echo "<select class='practical_edit'>";
//    }
//    else
//    {
//        echo "<select class='practical_edit' disabled>";
//    }
    if($ret[$i]['Course_Certificate_Practical']==0)
    {
        ?>
        <option value="<?php echo $i.'1'; ?>" >Passed</option>
        <option value="<?php echo $i.'0'; ?>" selected>Failed</option>
<?php
    }
    else
    {
        ?>
        <option value="<?php echo $i.'1'; ?>" selected>Passed</option>
        <option value="<?php echo $i.'0'; ?>" >Failed</option>
<?php
    }
    ?>
</select>
</td>
<td>
<?php
    echo "<select class='written_edit'>";

//    if($ret[$i]['Status_ID']==2)
//    {
//    }
//    else
//    {
//        echo "<select class='written_edit' disabled>";
//    }
    if($ret[$i]['Course_Certificate_Written']==0)
    {
        ?>
<option value="<?php echo $i.'1'; ?>" >Passed</option>
<option value="<?php echo $i.'0'; ?>" selected>Failed</option>
<?php
    }
    else
    {
        ?>
<option value="<?php echo $i.'1'; ?>" selected>Passed</option>
<option value="<?php echo $i.'0'; ?>">Failed</option>
<?php
    }
    ?>
</select>
</td>
<td>
  <?php
    if(strcmp($ret[$i]['Fees_Paid'],"Yes")==0)
    {
  ?>
    <select>
    <option value="<?php echo $i.'1'; ?>" selected >PayPal-JTCP</option>
    <option value="<?php echo $i.'1'; ?>">CalAPA</option>
    <option value="<?php echo $i.'0'; ?>">CalTrans</option>
    <option value="<?php echo $i.'1'; ?>">Check</option>
    <option value="<?php echo $i.'0'; ?>">Not Paid</option>
    </select>
  <?php
    }
    else {
  ?>
    <select>
    <option value="<?php echo $i.'1'; ?>">PayPal-JTCP</option>
    <option value="<?php echo $i.'1'; ?>">CalAPA</option>
    <option value="<?php echo $i.'0'; ?>">CalTrans</option>
    <option value="<?php echo $i.'1'; ?>">Check</option>
    <option value="<?php echo $i.'0'; ?>" selected>Not Paid</option>
    </select>
  <?php
    }
  ?>
</td>
<td>
  <?php
  if(strcmp($ret[$i]['Fees_Paid'],"Yes")==0){
    $ret2 = getPaypalDataByClassIDEmployeeID($ret[$i]['Class_ID'],$ret[$i]['Employee_ID']);
    $paypal_data =  $ret2['Paypal_Data'];
    $json = json_decode($paypal_data, true); // decode the JSON into an associative array
    echo "<a href=\"transactions.php?tid=".$json['txn_id']."\" target='_blank'>".$json['txn_id']."</a>";
  }
?>
</td>
<td>
<?php
    if($ret[$i]['Status_ID']==2)
    {
        echo "<a href='#' class='certify' id='$i'>Release Certificate</a>";
    }
    else
    {
        echo "<a href='#' class='certify' id='$i'>Update Certificate</a>";
    }
    echo " | <a href='#' class='drop' id='$i'>Drop</a>";
    ?>
</td>
</tr>
<?php
    $index++;
    }
    $total_calapa = $index - $total_caltrans - 1;
    ?>
</tbody>
</table>

<script>
  document.getElementById("statistics").innerHTML = "Total Employees: <?php echo $index - 1; ?> <br>Total Caltrans: <?php echo $total_caltrans; ?> <br>Total CalAPA: <?php echo $total_calapa; ?> <br>";
</script>

<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->

  <!-- /#page-wrapper -->
<?php
    include 'template2.php';
?>
