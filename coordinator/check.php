<?php

    session_start();
    $ret = getCoordinatorDetails($_SESSION['Login_ID']);
    $email = $ret['Login_Email'];
    if($_SESSION['Login_ID']==null || $_SESSION['Role']!=3 || strpos($email, 'dot.ca.gov') != false)
    {
        header("Location: ../login.php");
    }

?>
