<?php
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
    include '../../dbmanage.php';
    $lid_results = array();
    $formdata = array();
    $formdata['company'] = $_POST['company'];
    $formdata['firstname'] = $_POST['firstname'];
    $formdata['lastname'] = $_POST['lastname'];
    $formdata['zipcode'] = $_POST['zipcode'];
    $formdata['city'] = $_POST['city'];
    $formdata['certificate_expire'] = $_POST['certificate_expire'];
    $formdata['coordinator_added'] = $_POST['coordinator_added'];
    $formdata['phone'] = $_POST['phone'];
    if($formdata['certificate_expire']=="")
    {
        $formdata['certificate_expire'] = '2050-12-12';
    }
  /* 
  Pagination code (start) - setting LIMIT and OFFSET
  */
  /*$pg_sql = "SELECT COUNT(employee_id) FROM employee";
  $pg_result = mysql_query($pg_sql);
  $row = mysql_fetch_row($pg_result);
  $total_records = $row[0];
   
  $limit = 10;
  if( isset($_GET['page'] ) ) 
    { $page = $_GET['page'];   }
   else 
    { $page = 1;   }
  $offset = ($page-1) * $limit;
  $total_pages = ceil($total_records / $limit);
  if(!$pg_result) {
    die('Could not get data: ' . mysql_error());
   }*/
  /*
  Pagination code (end)
  */
    //$ret = searchEmployees($formdata,1,$limit,$offset,$total_pages);
    $ret = searchEmployees($formdata,1);
?>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Name</th>
<th>Email</th>
<th>Mobile</th>
<th>Address</th>
<th>Company</th>
<th>Position</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php
    for($i=0;$i<count($ret);$i++)
    {
        $name_results[] = $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
        $eid_results[] = $ret[$i]['Employee_ID'];
        $lid_results[] = $ret[$i]['Login_ID'];
        ?>
<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Employee_Prefix'] . " " . $ret[$i]['Employee_FirstName'] . " " . $ret[$i]['Employee_LastName'];
    echo " | " . $ret[$i]['Employee_ID'];
?>
</td>

<td>
<?php
    echo $ret[$i]['Login_Email'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Employee_Mobile'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Employee_Address'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Company_Name'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Employee_Position'];
    ?>
</td>
<td>

<?php
    echo "<a href='javascript:void(0)' class='clickmodel_edit' id='$i'>Edit</a>";
    echo " | ";
    echo "<a href='javascript:void(0)' class='clickmodel_view' id='$i'>View</a>";
    echo " | ";
    echo "<a href='javascript:void(0)' class='clickmodel_enroll' id='$i'>Enroll</a>";
    echo " | ";
    echo "<a href='javascript:void(0)' class='clickmodel_track' id='$i'>Track</a>";
    echo " | ";
    echo "<a href='payments.php?eid=".$ret[$i]['Employee_ID']."' target='_blank'>View Payments</a>";

?>
</td>
</tr>
<?php
    }
    ?>
</tbody>
</table>





<!-- Modal -->
<div class="modal fade" id="userdatamodal" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" id="usermodal-close">&times;</button>
<h4 class="modal-title" id="usermodal-title">Edit Employee Information</h4>
</div>
<div class="usermodal-body">
<p id="usermodal-table">Loading...</p>
</div>
<div class="usermodal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>

<script>

$(document).ready(function(){
                  $(".clickmodel_edit").click(function(){
                                              var id = this.id;
                                              var lid_jArray= <?php echo json_encode($lid_results); ?>;
                                              $('#userdatamodal').modal('toggle');
                                              $.ajax({
                                                     url: "jqScripts/editEmployee.php",
                                                     type: "post",
                                                     data: {'lid' : lid_jArray[id]},
                                                     success: function (response) {
                                                     $('#usermodal-table').empty();
                                                     $('#usermodal-table').append(response)
                                                     },
                                                     error: function(jqXHR, textStatus, errorThrown) {
                                                     alert(errorThrown);
                                                     console.log(textStatus, errorThrown);
                                                     }
                                                     });
                                              });
                  $(".clickmodel_view").click(function(){
                                         $('#userdatamodal').modal('toggle');
                                         var id = this.id;
                                         var name_jArray= <?php echo json_encode($name_results); ?>;
                                         var eid_jArray= <?php echo json_encode($eid_results); ?>;
                                         var name = name_jArray[id];
                                         document.getElementById('usermodal-title').innerHTML = name;
                                         $.ajax({
                                                url: "jqScripts/getsearch_progress.php",
                                                type: "post",
                                                data: {'eid' : eid_jArray[id]} ,
                                                success: function (response) {
                                                $('#usermodal-table').empty();
                                                $('#usermodal-table').append(response);
                                                },
                                                error: function(jqXHR, textStatus, errorThrown) {
                                                console.log(textStatus, errorThrown);
                                                }
                                                });
                                         });

              $(".clickmodel_track").click(function(){
                                            $('#userdatamodal').modal('toggle');
                                            var id = this.id;
                                            var name_jArray= <?php echo json_encode($name_results); ?>;
                                            var eid_jArray= <?php echo json_encode($eid_results); ?>;
                                            var name = name_jArray[id];
                                            document.getElementById('usermodal-title').innerHTML = name;
                                                  $.ajax({
                                                    url: "jqScripts/getactivity.php",
                                                    type: "post",
                                                    data: {'eid' : eid_jArray[id]} ,
                                                    success: function (response) {
                                                    $('#usermodal-table').empty();
                                                    $('#usermodal-table').append(response);
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus, errorThrown);
                                                    }
                                                    });
                                              });

          $(".clickmodel_enroll").click(function(){
                                              $('#userdatamodal').modal('toggle');
                                              var id = this.id;
                                              var name_jArray= <?php echo json_encode($name_results); ?>;
                                              var eid_jArray= <?php echo json_encode($eid_results); ?>;
                                              var name = name_jArray[id];
                                              document.getElementById('usermodal-title').innerHTML = name;
                                                  $.ajax({
                                                    url: "jqScripts/selectCourseToEnroll.php",
                                                    type: "post",
                                                    data: {'eid' : eid_jArray[id]} ,
                                                    success: function (response) {
                                                    $('#usermodal-table').empty();
                                                    $('#usermodal-table').append(response);
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus, errorThrown);
                                                    }
                                                    });
                                                });


                  });
</script>
