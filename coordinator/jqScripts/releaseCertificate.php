<?php

    include '../../dbmanage.php';
    $cc = $_POST['cc'];
    $cc2 = $_POST['cc2'];


    $ret = releaseCertificate($cc, $cc2);

    if($ret['Status']==1)
    {
        $temp_ret = getEmployeeDetailFromCourseCertificate($cc);
        //Email Students Grades
        include('../../mailAPI/emails.php');
        if($temp_ret['Status']==1)
        {
            grade_email($temp_ret);

            //Update User Activity
            $eid = $temp_ret['Employee_ID'];
            $course_name = $temp_ret['Course_Name'];
            $practical = $temp_ret['Practical'];
            $written = $temp_ret['Written'];
            addActivity($eid, "Completed Course: " . $course_name . "| Practical: " . $practical . ", Written: " . $written, 1);

            $coordinatorid = $_SESSION['Coordinator_ID'];
            addActivity_coordinator($coordinatorid, 'Certifcate Released for Eid: '. $eid . " Course: " . $course_name);

        }

    }

    echo $ret['Message'];

?>
