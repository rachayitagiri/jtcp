<?php

    include '../../dbmanage.php';
    $lid = $_POST['lid'];
    $ret = getEmployeeDetail($lid);

?>

<script>

$(document).ready(function(){
                  $("#changeemployee").click(function(){
                                            var eid = document.getElementById('eid_edit').value;
                                            var firstname = document.getElementById('firstname_edit').value;
                                            var initial = document.getElementById('initial_edit').value;
                                             var lastname = document.getElementById('lastname_edit').value;

                                             var email = document.getElementById('email_edit').value;
                                             var phonenumber = document.getElementById('phonenumber_edit').value;
                                             var address = document.getElementById('address_edit').value;
                                             var city = document.getElementById('city_edit').value;
                                             var postalcode = document.getElementById('postalcode_edit').value;
                                            // var companies = document.getElementById('company_edit');
                                            //  var company_edit = companies.options[companies.selectedIndex].value;
                                            var company_edit = 0;
                                            var other_edit = document.getElementById('other_edit').value;
                                             var position_edit = document.getElementById('position_edit').value;
                                             var password_edit = document.getElementById('password_edit').value;
                                            $.ajax({
                                                   url: "jqScripts/editEmployee2.php",
                                                   type: "post",
                                                   data: {'eid': eid, 'firstname' : firstname, 'initial' : initial, 'lastname' : lastname, 'email' : email, 'password' : password_edit, 'mobile' : phonenumber, 'company_id' : company_edit, 'company_other' : other_edit, 'position' : position_edit, 'address' : address, 'city' : city, 'postal' : postalcode},
                                                   success: function (response) {
                                                  alert(response);
                                                   document.getElementById('usereditmodelclose').click();
                                                   setTimeout(searchemployee, 500);
                                                   },
                                                   error: function(jqXHR, textStatus, errorThrown) {
                                                   alert(errorThrown);
                                                   console.log(textStatus, errorThrown);
                                                   }
                                                   });
                                            });
                  $("#enable").click(function(){
                                             var lid = document.getElementById('lid').value;
                                             $.ajax({
                                                    url: "jqScripts/enableEmployee.php",
                                                    type: "post",
                                                    data: {'lid': lid},
                                                    success: function (response) {
                                                        document.getElementById('usereditmodelclose').click();
                                                        alert(response);
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    alert(errorThrown);
                                                    console.log(textStatus, errorThrown);
                                                    }
                                                    });
                                             });
                  $("#disable").click(function(){
                                     var lid = document.getElementById('lid').value;
                                     $.ajax({
                                            url: "jqScripts/disableEmployee.php",
                                            type: "post",
                                            data: {'lid': lid},
                                            success: function (response) {
                                                document.getElementById('usereditmodelclose').click();
                                                alert(response);
                                            },
                                            error: function(jqXHR, textStatus, errorThrown) {
                                            alert(errorThrown);
                                            console.log(textStatus, errorThrown);
                                            }
                                            });
                                     });
                  });
</script>

<input type="text" value="<?php echo $lid;  ?>" id="lid" name="lid" hidden>

<input type="text" value="<?php echo $ret['Employee_ID'];  ?>" id="eid_edit" name="eid_edit" hidden>
<!-- /.row -->
<div class="row">
<div class="col-lg-12">
<div class="form-group col-lg-5">
<label>First Name</label>
<input type="" name="firstname_edit" class="form-control" id="firstname_edit" value="<?php echo $ret['Employee_FirstName']; ?>">
</div>

<div class="form-group col-lg-2">
<label>Initial</label>
<input type="" name="initial_edit" class="form-control" id="initial_edit" value="<?php echo $ret['Employee_Initial']; ?>" >
</div>

<div class="form-group col-lg-5">
<label>Last Name</label>
<input type="" name="lastname_edit" class="form-control" id="lastname_edit" value="<?php echo $ret['Employee_LastName']; ?>" >
</div>

<div class="form-group col-lg-12">
<label>Email Address</label>
<input type="" name="email_edit" class="form-control" id="email_edit" value="<?php echo $ret['Login_Email']; ?>">
</div>

<div class="form-group col-lg-12">
<label>Phone Number</label>
<input type="" name="phonenumber_edit" class="form-control" id="phonenumber_edit" value="<?php echo $ret['Employee_Mobile']; ?>">
</div>

<div class="form-group col-lg-12">
<label>Address</label>
<input type="" name="address_edit" class="form-control" id="address_edit" value="<?php echo $ret['Employee_Address']; ?>">
</div>

<div class="form-group col-lg-6">
<label>City</label>
<input type="" name="city_edit" class="form-control" id="city_edit" value="<?php echo $ret['Employee_City']; ?>">
</div>

<div class="form-group col-lg-6">
<label>Postal Code</label>
<input type="" name="postalcode_edit" class="form-control" id="postalcode_edit" value="<?php echo $ret['Employee_Postal']; ?>">
</div>

<div class="col-sm-12" style="margin-bottom: 20px;">
<!--
<label>Company</label>
<select name="company_edit" id="company_edit" class="form-control">
<option disabled selected value> -- select your company -- </option>
<?php
/*
    $temp_ret = getCompany();
    for($i=0;$i<count($temp_ret);$i++)
    {
        $company_id = $temp_ret[$i]['Company_ID'];
        $company_name = $temp_ret[$i]['Company_Name'];
        if($ret['Company_ID']==$company_id)
        {
            echo "<option value='$company_id' selected>$company_name</option>";
        }
        else
        {
            echo "<option value='$company_id'>$company_name</option>";
        }
    }
    */
    ?>
</select>
-->
</div>

<div class="col-sm-12" style="margin-bottom: 20px;">
<label>Other Company</label>
<input type="" name="other_edit" class="form-control" id="other_edit" value="<?php
if($ret['Company_ID']==0 || $ret['Company_ID']==null)
{
echo $ret['Company_Other'];
}
?>">
</div>

<div class="form-group col-lg-12">
<label>Position</label>
<input type="" name="position_edit" class="form-control" id="position_edit" value="<?php echo $ret['Employee_Position']; ?>">
</div>

<div class="form-group col-lg-12">
<label>New Password</label>
<input type="password" name="password_edit" class="form-control" id="password_edit" value="">
</div>

<div class="col-sm-12">
<input type="button" id="changeemployee" class="btn btn-primary" name="changeemployee" value="Update Employee">
</div>


</div>
<!-- /.panel -->
</div>

<br>
<?php
    if($ret['Login_Verify']==3)
    {
?>
        <center><input type="button" class="btn btn-info" value="Enable Account" name="enable" id="enable"></center>
<?php
    }
    else
    {
?>
        <center><input type="button" class="btn btn-danger" value="Disable Account" name="disable" id="disable"></center>
<?php
    }

?>

<br>


</div>
<!-- /.row -->
