<?php

    include '../../dbmanage.php';
    $lid = $_POST['lid'];
    $ret = getInstructorDetails($lid);

?>

<script>

$(document).ready(function(){
                  $("#changeinstructor").click(function(){
                                            var lid = document.getElementById('lid_edit').value;
                                            var fn = document.getElementById('fn_edit').value;
                                            var ln = document.getElementById('ln_edit').value;
                                            var email = document.getElementById('email_edit').value;
                                            var password = document.getElementById('password_edit').value;
                                            $.ajax({
                                                   url: "jqScripts/editInstructor2.php",
                                                   type: "post",
                                                   data: {'lid': lid, 'fn' : fn, 'ln' : ln, 'email' : email, 'password' : password},
                                                   success: function (response) {
                                                   document.getElementById('closeinstructormodel').click();
                                                   setTimeout(showinstructors, 500);
                                                   },
                                                   error: function(jqXHR, textStatus, errorThrown) {
                                                   alert(errorThrown);
                                                   console.log(textStatus, errorThrown);
                                                   }
                                                   });
                                            });
                  });
</script>
<!-- /.row -->

<div class="form-group col-lg-6">
<label>Email Address</label>
<input type="" name="email" class="form-control" id="email_edit" value="<?php echo $ret['Login_Email']; ?>">
</div>

<div class="form-group col-lg-6">
<label>New Password</label>
<input type="" name="password" class="form-control" id="password_edit" value="">
</div>

<div class="form-group col-lg-6">
<label>First Name</label>
<input type="" name="fn" class="form-control" id="fn_edit" value="<?php echo $ret['Instructor_FirstName']; ?>">
</div>

<div class="form-group col-lg-6">
<label>Last Name</label>
<input type="" name="ln" class="form-control" id="ln_edit" value="<?php echo $ret['Instructor_LastName']; ?>">
</div>

<input type="hidden" name="lid" class="form-control" id="lid_edit" value="<?php echo $ret['Login_ID']; ?>">

<div class="col-sm-12">
<input type="button" id="changeinstructor" class="btn btn-primary" name="changeinstructor" value="Update Instructor">
</div>
