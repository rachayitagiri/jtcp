<?php

    include '../../dbmanage.php';

    $formdata = array();
    $eid = $_POST['eid'];
    $cid = $_POST['cid'];
    $ret=getClassesByCourse_Coordinator($cid);
?>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<script>
$(document).ready(function(){
//                  selecteddates=document.getElementById('selecteddates0').value;
                  var list = document.getElementsByClassName("selecteddates");
                  for (var i = 0; i < list.length; i++) {
                  selecteddates = list[i].value;
                  new Kalendae({
                                    attachTo:document.getElementById('calendaredit'+i),
                                    months:1,
                                    mode:'multiple',
                                    selected:selecteddates,
                               direction:'future',
                               blackout: function (date) {
                               return true;
                               }
                                    });
                  }
                  $(".waitlistdrop").click(function(){
                                          var cid = $(this).attr('id');
                                          var eid = document.getElementById('eid').value;
                                          $.ajax({
                                                 url: "dropWaitlist.php",
                                                 type: "post",
                                                 data: {'cid' : cid, 'eid' : eid} ,
                                                 success: function (response) {
                                                 alert(response);
                                                 document.getElementById('closeclasscloseenrollmodel').click();
                                                 },
                                                 error: function(jqXHR, textStatus, errorThrown) {
                                                 alert(errorThrown);
                                                 console.log(textStatus, errorThrown);
                                                 }
                                                 });
                                          });
                  $(".classselect").click(function(){
                                              //class id
                                               var cid = $(this).attr('id');
                                                var eid = document.getElementById('eid').value;
                                                var course_id = document.getElementById('course_id').value;
                                                var force_enroll = 0;
                                               $.ajax({
                                                      url: "jqScripts/selectClassToEnroll.php",
                                                      type: "post",
                                                      data: {'cid' : cid, 'eid' : eid, 'course_id':course_id, 'force_enroll':force_enroll} ,
                                                      success: function (response) {
                                                      if(response.startsWith("Class in Full! Do you want to add it to waitlist?"))
                                                      {
                                                        var r = confirm("Class in Full! Do you want to add it to waitlist?");
                                                        if(r==true)
                                                        {
                                                            waitlist(cid);
                                                        }
                                                      }
                                                      else
                                                      {
                                                        alert(response);
                                                        document.getElementById('closeclasscloseenrollmodel').click();
                                                        location.reload();
                                                      }
                                                      },
                                                      error: function(jqXHR, textStatus, errorThrown) {
                                                      alert(errorThrown);
                                                      console.log(textStatus, errorThrown);
                                                      }
                                                      });
                                               });
              $(".classselect_force").click(function(){
                  //class id
                  var cid = $(this).attr('id');
                  var eid = document.getElementById('eid').value;
                  var course_id = document.getElementById('course_id').value;
                  var force_enroll = 1;
                  $.ajax({
                    url: "jqScripts/selectClassToEnroll.php",
                    type: "post",
                    data: {'cid' : cid, 'eid' : eid, 'course_id':course_id, 'force_enroll':force_enroll},
                    success: function (response) {
                          if(response.startsWith("Class in Full! Do you want to add it to waitlist?"))
                          {
                                var r = confirm("Class in Full! Do you want to add it to waitlist?");
                                if(r==true)
                                {
                                      waitlist(cid);
                                }
                          }
                          else
                          {
                                alert(response);
                                document.getElementById('closeclasscloseenrollmodel').click();
                                location.reload();
                          }
                      },
                      error: function(jqXHR, textStatus, errorThrown) {
                          alert(errorThrown);
                          console.log(textStatus, errorThrown);
                     }
                  });
              });
            });
function waitlist(cid)
{
    var eid = document.getElementById('eid').value;
    $.ajax({
           url: "addWaitlist.php",
           type: "post",
           data: {'cid' : cid, 'eid' : eid} ,
           success: function (response) {
            alert(response);
            document.getElementById('closeclasscloseenrollmodel').click();
            location.reload();
           },
           error: function(jqXHR, textStatus, errorThrown) {
           alert(errorThrown);
           console.log(textStatus, errorThrown);
           }
           });
}
</script>

</head>

<input type="text" value="<?php echo $eid; ?>" id="eid" hidden>
<input type="text" value="<?php echo $cid; ?>" id="course_id" hidden>


<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Class Name</th>
<th>Class Capacity</th>
<th>Instructor</th>
<!--<th>Practical Exam Date</th>-->
<!--<th>Written Exam Date</th>-->
<th>Class Dates</th>
<th>Class Location</th>
<th>Register</th>
</tr>
</thead>
<tbody>
    <?php
        for($i=0;$i<count($ret);$i++)
        {
            $selecteddates = "";
            $classid = $ret[$i]['Class_ID'];

            for($j=0;$j<count($ret[$i]['Class_Dates']);$j++)
            {
                $selecteddates =  $ret[$i]['Class_Dates'][$j] . "," . $selecteddates;
            }
            echo "<input type='text' value='$selecteddates' class= 'selecteddates' hidden >";
    ?>
<tr class="odd gradeX">
<td>
<?php
    echo $ret[$i]['Class_Name'];
    ?>
</td>
<td>
<?php
    echo  $ret[$i]['Class_Filled'] . "/" .$ret[$i]['Class_Capacity'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Instructor_FirstName'] . " " . $ret[$i]['Instructor_LastName'];
    ?>
</td>
<!--
<td>
<?php
    echo $ret[$i]['Class_Practical_Date'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Class_Written_Date'];
    ?>
</td>
-->

<!--Calender Click Disabled using z-index so new div will be in front of older one-->
<td style="position: relative;">
<div class="form-group col-lg-12">
<div style="height: 100%;position: absolute;width: 100%;margin: -8px -8px;z-index: 999;"></div>
Class Dates<br>
<?php $calendaredit = "calendaredit" . $i; echo "<center id='$calendaredit'>"; ?>
</center>
<div id="calendartimeedit">

</div>

</div>

<?php
    echo "All Classes will be from " . $ret[0]['Class_TimeFrom'][0] . " to " . $ret[0]['Class_TimeTo'][0] . ".";
   /*
    for($j=0;$j<count($ret[$i]['Class_Dates']);$j++)
        {
            echo $ret[$i]['Class_Dates'][$j];
            echo " (" . $ret[$i]['Class_TimeFrom'][$j];
            echo "-" . $ret[$i]['Class_TimeTo'][$j] . ")";
            echo "<br>";
        }
    */
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Location_Name'];
    ?>
</td>
<td>
<?php
    if($ret[$i]['Class_Capacity']>$ret[$i]['Class_Filled'])
    {
        echo "<a href='#' class='classselect' id='" . $ret[$i]['Class_ID'] . "'>Select</a>";
        echo " | ";
        echo "<a href='#' class='classselect_force' id='" . $ret[$i]['Class_ID'] . "'>Force Select</a>";
    }
    else
    {
        $temp_ret = check_waitlist($classid, $eid);
        if($temp_ret==1)
        {
            echo "User is in the waitlist! <br><a href='javascript: void(0)' class='waitlistdrop' id='" . $ret[$i]['Class_ID'] . "'>Remove from Waitlist!</a>";
        }
        else
        {
            echo "<a href='#' class='classselect' id='" . $ret[$i]['Class_ID'] . "'>Select</a>";
            echo " | ";
            echo "<a href='#' class='classselect_force' id='" . $ret[$i]['Class_ID'] . "'>Force Select</a>";
        }
    }
    ?>
</td>
</tr>
<?php
    }
    ?>
</tbody>
</table>
