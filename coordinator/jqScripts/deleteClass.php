<?php
    include '../../dbmanage.php';
    $cid = $_POST['cid'];
    $ret = deleteClass($cid);
    if($ret['Status']==1)
    {
      //Email Students that class has been dropped
      include('../../mailAPI/emails.php');
      $emails = $ret['Emails'];
      $classdata = $ret['ClassData'];
      $eids = $ret['Employee_IDs'];
      $data = array();
      $data['Email_Addresses'] = $emails;
      $data['Course_Name'] = $classdata['Course_Name'];
      $data['Class_Dates'] = $classdata['Class_Dates'];
      $data['Location_Name'] = $classdata['Location_Name'];
      $data['Class_Name'] = $classdata['Class_Name'];
      cancelclass_email($data);

      $coordinatorid = $_SESSION['Coordinator_ID'];
      addActivity_coordinator($coordinatorid, 'Class Deleted: '. $classdata['Class_Name']);

      for($i=0;$i<count($eids);$i++)
      {
        $eid = $eids[$i];
        addActivity($eid, "Class " . $data['Class_Name'] . "  for course " . $data['Course_Name'] . " starting from " . $data['Class_Dates'][0] . " has been Cancelled. Get refund or enroll in another class.", 1);
      }

    }
    echo $ret['Message'];


?>
