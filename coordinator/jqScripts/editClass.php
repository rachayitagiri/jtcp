<?php

    include '../../dbmanage.php';
    $cid = $_POST['cid'];
    $ret = getClass($cid);
    $selecteddates = "";
    $selectedtimeto = "";
    $selectedtimefrom = "";
    for($i=0;$i<count($ret['Class_Dates']);$i++)
    {
        $selecteddates =  $ret['Class_Dates'][$i] . "," . $selecteddates;
    }

?>

<script>
var myarray;
var k1;
$(document).ready(function(){
                  selecteddates=document.getElementById('selecteddates').value;
                  kl = new Kalendae({
                                        attachTo:document.getElementById('calendaredit'),
                                        months:2,
                                        mode:'multiple',
                                        selected:selecteddates
                                        });
                  showtime();
                  $("#calendaredit").click(function(){
                                           var schedule = kl.getSelected();
                                           myarray = schedule.split(',');
                                           $("#calendartimeedit").empty();
                                           for(var i=0;i<myarray.length;i++)
                                           {
                                           //Set if else to change multiple time to single time...
                                            if(i==0)
                                            {
                                            $("#calendartimeedit").append('<div class="form-group col-lg-5"><label>From:</label><input type="time" name="classtime" class="form-control" id="timefromedit' + i + '" value=""></div><div class="form-group col-lg-5"><label>To:</label><input type="time" name="classtime" class="form-control" id="timetoedit' + i + '" value=""></div>');
                                            }
                                            else
                                            {
                                            $("#calendartimeedit").append('<div class="form-group col-lg-5" hidden ><label>From Time for ' + myarray[i] + '</label><input type="time" name="classtime" class="form-control" id="timefromedit' + i + '" value=""></div><div class="form-group col-lg-5" hidden ><label>To Time for ' + myarray[i] + '</label><input type="time" name="classtime" class="form-control" id="timetoedit' + i + '" value=""></div>');
                                            }
                                           }
                                           if(schedule=="")
                                           {
                                           $("#calendartimeedit").empty();
                                           }
                                       });
                  $("#changeclass").click(function(){
                                            var cid = document.getElementById('classid').value;
                                            var classname = document.getElementById('classname_edit').value;
                                            var instructors = document.getElementById('instructors_edit');
                                            var instructorid = instructors.options[instructors.selectedIndex].value;
                                            var courses = document.getElementById('courses_edit');
                                            var coursesid = courses.options[courses.selectedIndex].value;
                                          var locations = document.getElementById('locations_edit');
                                          var locationid = locations.options[locations.selectedIndex].value;
                                            var classcapacity = document.getElementById('classcapacity_edit').value;
                                            var classpracticalexam = document.getElementById('classpracticalexam_edit').value;
                                            var classwrittenexam = document.getElementById('classwrittenexam_edit').value;
                                          var schedule = kl.getSelected();
                                          myarray = schedule.split(',');
                                          var scheduletimefrom = "";
                                          var scheduletimeto = "";
                                          for(var i=0;i<myarray.length;i++)
                                          {
                                          //Set k to change multiple time to single time...
                                          var k = 0;
                                          scheduletimefrom = scheduletimefrom + document.getElementById('timefromedit'+k).value + ", ";
                                          scheduletimeto = scheduletimeto + document.getElementById('timetoedit'+k).value + ", ";
                                          }

                                            $.ajax({
                                                   url: "jqScripts/editClass2.php",
                                                   type: "post",
                                                   data: {'cid': cid, 'classname': classname, 'instructorid' : instructorid, 'coursesid' : coursesid, 'classcapacity' : classcapacity, 'classpracticalexam' : classpracticalexam, 'classwrittenexam' : classwrittenexam, 'schedule' : schedule, 'scheduletimefrom' : scheduletimefrom, 'scheduletimeto' : scheduletimeto, 'locationid': locationid},
                                                   success: function (response) {
                                                   alert(response);
                                                   document.getElementById('closeclassmodel').click();
                                                   setTimeout(showclass, 500);
                                                   },
                                                   error: function(jqXHR, textStatus, errorThrown) {
                                                   alert(errorThrown);
                                                   console.log(textStatus, errorThrown);
                                                   }
                                                   });
                                            });
                  });

function showtime()
{
    var jArrayFromTime= <?php echo json_encode($ret['Class_TimeFrom']); ?>;
    var jArrayToTime= <?php echo json_encode($ret['Class_TimeTo']); ?>;

    var schedule = kl.getSelected();
    myarray = schedule.split(',');
    $("#calendartimeedit").empty();
    /*
    for(var i=0;i<myarray.length;i++)
    {
        $("#calendartimeedit").append('<div class="form-group col-lg-5"><label>From Time for ' + myarray[i] + '</label><input type="time" name="classtime" class="form-control" id="timefromedit' + i + '" value="' + jArrayFromTime[i] + '"></div><div class="form-group col-lg-5"><label>To Time for ' + myarray[i] + '</label><input type="time" name="classtime" class="form-control" id="timetoedit' + i + '" value="' + jArrayToTime[i] + '"></div>');
    }
     */
    //Set k to change multiple time to single time...
    var k=0;
    $("#calendartimeedit").append('<div class="form-group col-lg-5"><label>From:</label><input type="time" name="classtime" class="form-control" id="timefromedit' + k + '" value="' + jArrayFromTime[k] + '"></div><div class="form-group col-lg-5"><label>To:</label><input type="time" name="classtime" class="form-control" id="timetoedit' + k + '" value="' + jArrayToTime[k] + '"></div>');
    if(schedule=="")
    {
        $("#calendartimeedit").empty();
    }
}
</script>

<input type="text" value="<?php echo $selecteddates; ?>" id= "selecteddates" hidden >
<input type="text" value="<?php echo $cid; ?>" id= "classid" hidden >


<div class="form-group col-lg-4">
<label>Class Name</label>
<input type="" name="classname_edit" class="form-control" id="classname_edit" value="<?php echo $ret['Class_Name']; ?>">
</div>

<div class="form-group col-lg-4">
<label>Select Instructor</label>
<select name="instructors_edit" id="instructors_edit" class="form-control">
<option disabled selected value> -- select Instructor -- </option>
<?php
    $temp_ret = getInstructorsDetails();
    for($i=0;$i<count($temp_ret);$i++)
    {
        $instructor_id = $temp_ret[$i]['Instructor_ID'];
        $instructor_fullname = $temp_ret[$i]['Instructor_FirstName']. " " . $temp_ret[$i]['Instructor_LastName'];
        if($instructor_id==$ret['Instructor_ID'])
        {
            echo "<option value='$instructor_id' selected>$instructor_fullname</option>";
        }
        else
        {
            echo "<option value='$instructor_id'>$instructor_fullname</option>";
        }
    }
    ?>
</select>
</div>

<div class="form-group col-lg-4">
<label>Select Course</label>
<select name="courses_edit" id="courses_edit" class="form-control">
<option disabled selected value> -- select Course -- </option>
<?php
    $temp_ret = getCourses();
    for($i=1;$i<count($temp_ret);$i++)
    {
        $course_id = $temp_ret[$i]['Course_ID'];
        $course_name = $temp_ret[$i]['Course_Name'];
        if($course_id==$ret['Course_ID'])
        {
            echo "<option value='$course_id' selected>$course_name</option>";
        }
        else
        {
            echo "<option value='$course_id'>$course_name</option>";
        }
    }
    ?>
</select>
</div>

<div class="form-group col-lg-4">
<label>Select Location</label>
<select name="locations_edit" id="locations_edit" class="form-control">
<option disabled selected value> -- select Location -- </option>
<?php
    $temp_ret = getLocations();
    for($i=0;$i<count($temp_ret);$i++)
    {
        $location_id = $temp_ret[$i]['Location_ID'];
        $location_name = $temp_ret[$i]['Location_Name'];
        if($location_id==$ret['Location_ID'])
        {
            echo "<option value='$location_id' selected>$location_name</option>";
        }
        else
        {
            echo "<option value='$location_id'>$location_name</option>";
        }
    }
    ?>
</select>
</div>

<div class="form-group col-lg-4">
<label>Class Capacity</label>
<input type="" name="classcapacity_edit" class="form-control" id="classcapacity_edit" value="<?php echo $ret['Class_Capacity']; ?>">
</div>

<div class="form-group col-lg-4">
<label>Practical Exam Date</label>
<input type="" name="classpracticalexam_edit" class="form-control" id="classpracticalexam_edit" value="<?php echo $ret['Class_Practical_Date']; ?>">
</div>

<div class="form-group col-lg-4">
<label>Written Exam Date</label>
<input type="" name="classwrittenexam_edit" class="form-control" id="classwrittenexam_edit" value="<?php echo $ret['Class_Written_Date']; ?>">
</div>

<div class="form-group col-lg-12">
<label>Select Class Dates</label>
<center id="calendaredit">
</center>
<div id="calendartimeedit">

</div>

</div>

<div class="col-sm-12">
<input type="button" id="changeclass" class="btn btn-primary" name="changeclass" value="Update Class">
</div>
