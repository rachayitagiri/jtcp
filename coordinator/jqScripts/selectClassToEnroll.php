<?php
//Select Class to Enroll directly from Program Coordinator
    include '../../dbmanage.php';
    $cid = $_POST['cid'];
    $course_id = $_POST['course_id'];
    $eid = $_POST['eid'];
    $force_enroll = $_POST['force_enroll'];


    //Generate New Prereq Certificate
    $ret = getUnexpiredPrereqCertificateByEmployeeIDCourseId($eid,$course_id);
    if($ret['Status']==0 || $force_enroll==1){
      $ret = setCertificate($eid,$course_id);
    }

    if($ret['Status']==1){
      $cc = $ret['Certficate_Code'];
      //Enroll Employee

      $ret = enrollEmployee($eid, $cid, $cc,1);

      if($ret['Status']==1)
      {
        $ret2 = getClass($cid);
        addActivity($eid, "Enrolled by Program Coordinator to Class: " . $ret2['Class_Name'] . ' and Course: ' . $ret2['Course_Name']);

        include('../../mailAPI/emails.php');
        $emp_details = getEmployeeDetailsByEmployeeID($eid);
        $class_details = getClass($cid);
        $email_message = 'Hello, You have been enrolled to JTCP Class: ' . $class_details['Class_Name'] .' by Program Coordinator. Please contact us if you have any questions.';
        generalEmail('Enrollment Confirmation',$emp_details['FirstName'],$emp_details['LastName'],$emp_details['Email'],$email_message);

      }

      $coordinatorid = $_SESSION['Coordinator_ID'];
      addActivity_coordinator($coordinatorid, 'Employee Enrolled: '. $eid . 'Class: ' . $ret2['Class_Name']);

      echo $ret['Message'];
    }
    else{
      echo "Something went wrong, please try again in a while!";
    }

?>
