<?php
    include '../../dbmanage.php';
    $eid = $_POST['eid'];
    $firstname = $_POST['firstname'];
    $initial = $_POST['initial'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $mobile = $_POST['mobile'];
    $company_id = $_POST['company_id'];
    $company_other = $_POST['company_other'];
    $position = $_POST['position'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $postal = $_POST['postal'];
    $ret = updateEmployee($eid, $firstname, $initial, $lastname, $email, $password, $mobile, $company_id, $company_other, $position, $address, $city, $postal);
    echo $ret['Message'];

    $coordinatorid = $_SESSION['Coordinator_ID'];
    addActivity_coordinator($coordinatorid, 'Employee Updated: '. $eid);

    ?>
