<?php

    include '../../dbmanage.php';

    $formdata = array();
    $eid = $_POST['eid'];
    $ret = getEmployeeProgress($eid);
?>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<tr>
<th>Course</th>
<th>Ferpa</th>
<th>Safety Quiz</th>
<th>Prerequisite</th>
<th>Enroll</th>
<th>Written Test</th>
<th>Practical Test</th>
<th>Certified</th>
</tr>
</thead>
<tbody>
<?php
    for($i=0;$i<count($ret);$i++)
    {
?>
<tr class="odd gradeX">
<td>
<?php echo $ret[$i]['Course_Name']; ?>
</td>
<?php
    for($j=0;$j<7;$j++)
    {
?>
<td>
<?php
        if($ret[$i][$j]['Status']==1)
        {
?>
            <span class="glyphicon glyphicon-ok"></span>
<?php
            echo "<br>" . $ret[$i][$j]['Date'];
    if($j==3)
    {
        echo "<br><b>" . $ret[$i][$j]['Batch'] . "</b>";
    }
        }
        else
        {
?>
            <span class="glyphicon glyphicon-remove"></span>
<?php
        }
?>
</td>
<?php
    }
?>
</tr>
<?php
    }
?>
</tbody>
</table>
