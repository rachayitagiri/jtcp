<?php
    include '../../dbmanage.php';
    $cid = $_POST['cid'];
    $classname = $_POST['classname'];
    $instructorid = $_POST['instructorid'];
    $courseid = $_POST['coursesid'];
    $locationid = $_POST['locationid'];
    $classcapacity = $_POST['classcapacity'];
    $classpracticalexam = $_POST['classpracticalexam'];
    $classwrittenexam = $_POST['classwrittenexam'];
    $schedule = $_POST['schedule'];
    $scheduletimeto = $_POST['scheduletimeto'];
    $scheduletimefrom = $_POST['scheduletimefrom'];

    $ret = updateClass($cid, $classname, $instructorid, $courseid, $locationid, $schedule, $scheduletimeto, $scheduletimefrom, $classcapacity, $classpracticalexam, $classwrittenexam);
    echo $ret['Message'];

    $coordinatorid = $_SESSION['Coordinator_ID'];
    addActivity_coordinator($coordinatorid, 'Class Edited: '. $classname);


?>
