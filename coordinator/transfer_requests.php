<?php
    include 'template1.php';

    if($_POST)
    {
      if($_POST['change'])
      {
        $cid = $_POST['Class_ID'];
        $eid = $_POST['Employee_ID'];
        $status = $_POST['Status'];
        if($status == 0){
          $status = 1;
        }
        else {
          $status = 0;
        }
        $ret = updateTransferRequestByEmployeeIDClassID($eid,$cid,$status);
      }
      if($_POST['delete'])
      {
        $cid = $_POST['Class_ID'];
        $eid = $_POST['Employee_ID'];
        $ret = removeTransferRequest($cid,$eid);
      }
    }


?>

<title>Transfer Requests</title>
<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Transfer Requests</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
Transfer List
</div>
<!-- /.panel-heading -->
<div class="panel-body" id="">
<!-- /.table-responsive -->
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
<form method="POST">
<tr>
<th>No</th>
<th>Class</th>
<th>Request From</th>
<th>Transfer Email</th>
<th>Date</th>
<th>Notes</th>
<th>Status</th>
<th>Action</th>

</tr>
</thead>
<tbody>
<?php
    $ret=getTransferRequest();
    for($i=0;$i<count($ret);$i++)
    {
        ?>
<tr class="odd gradeX">
<td>
<?php
    echo $i+1;
    ?>
</td>
<td>
<?php
    $class_id = $ret[$i]['Class_ID'];
    $ret3 = getClass($class_id);
    echo $ret3['Class_Name'];
    ?>
</td>
<td>
<?php
    $temp_eid = $ret[$i]['Employee_ID'];
    $ret3 = getEmployeeDetailsByEmployeeID($temp_eid);
    echo $ret3['FirstName'] . " " . $ret3['LastName'] . " - " . $ret3['Email'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Email'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Timestamp'];
    ?>
</td>
<td>
<?php
    echo $ret[$i]['Notes'];
    ?>
</td>
<td>
<select name="Status">
<?php
    if($ret[$i]['Status']==0)
    {
        echo "<option value='0' selected class='approvestatus'>Not Approved</option>";
        echo "<option value='1' class='approvestatus'>Approved</option>";
    }
    else
    {
        echo "<option value='0' class='approvestatus'>Not Approved</option>";
        echo "<option value='1' selected class='approvestatus'>Approved</option>";
    }
    echo "<input type='text' name='Status' value='" . $ret[$i]['Status'] . "' hidden >";
    echo "<input type='text' name='Employee_ID' value='" . $ret[$i]['Employee_ID'] . "' hidden >";
    echo "<input type='text' name='Class_ID' value='" . $ret[$i]['Class_ID'] . "' hidden >";

    ?>
</select>
</td>
<td>
<?php
    echo "<input type='submit' name='change' value='change'>";
    echo "<input type='submit' name='delete' value='delete'>";
    ?>
</td>
</tr>
</form>

<?php
    }
    ?>
</tbody>
</table>







</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->


<?php
    include 'template2.php';
?>
