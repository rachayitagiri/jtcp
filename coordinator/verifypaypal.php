<!-- This page verifies and checks for the availability of the PayPal transactions made in the name of CSULB.-->
<?php
include 'template1.php';
?>

<!DOCTYPE html>
<html>
<body>
<title>Verify PayPal Transactions</title>
<div class="row">
<div class="col-lg-12">
  <h1 class="page-header">Verify PayPal Transactions</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<script>
$(function() {
  $('#startdate').datepicker({
      dateFormat: 'yy-mm-dd'
});
$('#enddate').datepicker({
      dateFormat: 'yy-mm-dd'
});
  });

  $(document).ready(function() {
    function updateSum() {
      var total = 0;
      $(".sum:checked").each(function(i, n) {total += parseInt($(n).val());})
      $("#total").val(total);
    }
    updateSum();
    // run the update on every checkbox change and on startup
    $('.sum').change(function() {
      updateSum();
    })
})
</script>

<form action="" method="post" enctype="multipart/form-data">
  <div class="form-group col-lg-6">
  <label>Start Date</label>
  <input type="" name="startdate" class="form-control" id="startdate" value="" required>
  </div>
  <div class="form-group col-lg-6">
  <label>End Date</label>
  <input type="" name="enddate" class="form-control" id="enddate" value="" required>
  </div>

    Select Excel File to upload:
    <input type="file" name="fileToUpload" id="fileToUpload" required>
    <br>
    <input type="submit" value="Check Availability" name="submit">
    <br>
</form>
<table class="table">
  <thead>
    <tr>
      <th>No</th>
      <th>Transaction ID</th>
      <th>Excel Sheet Gross</th>
      <th>JTCP Gross</th>
      <th>Status</th>
      <th>Message</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  		<?php

  				if ($_POST) {

				// Include PHPExcel_IOFactory
				include '../ExcelToSQL/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';
				$inputFileName = $_FILES['fileToUpload']['tmp_name'];

        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];

				//Read the Excel worksheet
				try {
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
				} catch (Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName, PATHINFO_BASENAME).'": '.$e->getMessage());
				}
        $retPaypal = getPaypalDataByDate($startdate, $enddate);

				//Get worksheet dimensions
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();

        $index_count = 0;
				//Loop through each row of the worksheet in turn
				for ($row=3; $row<=$highestRow ; $row++) {
					//echo "\nrow: ".$row;

					//Read the row data into a PHP array
					$rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL, TRUE, FALSE);

					//Check for transactions by the Transaction ID provided in the Excel Sheet
					//where Transaction ID is the 2nd column in the sheet
          $temp_tid = $rowData[0][1];
          $temp_gross = $rowData[0][3];

          //Display the verification data in the table
					$index_count++;
					echo "<tr>";
					echo "<td>".$index_count."</td>";		//serial number
          echo "<td><a target='_blank' href='transactions.php?tid=$temp_tid'>".$temp_tid."</td>";			//transaction ID

          if (array_key_exists($temp_tid, $retPaypal)) {
            $temp_paypyal = $retPaypal[$temp_tid];
            echo "<td>".$temp_gross."</td>";			//sheet gross
            $temp_jtcp_gross = $temp_paypyal['Gross'];
            echo "<td>".$temp_jtcp_gross."</td>";			//jtcp gross
            if($temp_jtcp_gross==$temp_gross){
              echo "<td><span class='glyphicon glyphicon-ok'></span></td>";			   //If transaction record found, show "Available"
              echo "<td style='color:white; background-color:green'>Available and Matched</td>";
              echo '<td><input type="checkbox" name="checkbox" class="sum" value='.$temp_jtcp_gross.' data-toggle="checkbox" checked></td>';
            }
            else{
              echo "<td><span class='glyphicon glyphicon-remove'></span></td>";			//amount mismatched
              echo "<td style='color:white; background-color:red'>Available but Amount Mismatched</td>";
              echo '<td><input type="checkbox" name="checkbox" class="sum" value='.$temp_jtcp_gross.' data-toggle="checkbox"></td>';
            }
            unset($retPaypal[$temp_tid]);

          }
          else{
            echo "<td>".$temp_gross."</td>";			//sheet gross
            echo "<td>Not Available</td>";
            echo "<td><span class='glyphicon glyphicon-remove'></span></td>";			//If transaction record not found, show "Unavailable"
            echo "<td style='color:white; background-color:red'>Not available in JTCP Database/In Date Range</td>";
            echo '<td><input type="checkbox" name="checkbox" class="sum" value='.$temp_gross.' data-toggle="checkbox"></td>';
          }
          echo "</tr>";
	  			}
          foreach ($retPaypal as $key => $value) {
            $index_count++;
            echo "<tr>";
            echo "<td>".$index_count."</td>";
            $temp_tid = $value['Transcation_ID'];
            echo "<td><a target='_blank' href='transactions.php?tid=$temp_tid'>".$temp_tid."</td>";			//transaction ID
            echo "<td>Not Available</td>";      //sheet gross
            $temp_gross = $value['Gross'];
            echo "<td>".$temp_gross."</td>";
            echo "<td><span class='glyphicon glyphicon-remove'></span></td>";			//Available in JTCP but not in Excel
            echo "<td style='color:white; background-color:red'>Not available in Excel Sheet</td>";
            echo '<td><input type="checkbox" name="checkbox" class="sum" value='.$temp_gross.' data-toggle="checkbox"></td>';
            echo "</tr>";
          }
	  		}
  			?>
  	</tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Total Gross (USD): </td>
      <td><input type='text' disabled id='total'></td>
    </tr>
  </tbody>
  </table>
</body>
</html>

<?php
	include 'template2.php';
?>
