<?php
    include 'template1.php';
?>

<input type="text" id="coordinatorid" value="<?php echo $ret['Coordinator_ID']; ?>" hidden>

<script>
$(document).ready(function(){
                  var kl = new Kalendae({
                                   attachTo:document.getElementById('calendar'),
                                   months:3,
                                   mode:'multiple',
                                   });

                  showclass();
                  var myarray;
                  $("#calendar").click(function(){
                                       var schedule = kl.getSelected();
                                       myarray = schedule.split(',');
                                       $("#calendartime").empty();
                                       for(var i=0;i<myarray.length;i++)
                                       {
                                       //Set if else to change multiple time to single time...
                                        if(i==0)
                                        {
                                            $("#calendartime").append('<div class="form-group col-lg-5"><label>From:</label><input type="time" name="classtime" class="form-control" id="timefrom' + i + '" value=""></div><div class="form-group col-lg-5"><label>To:</label><input type="time" name="classtime" class="form-control" id="timeto' + i + '" value=""></div>');
                                        }
                                        else
                                        {
                                            $("#calendartime").append('<div class="form-group col-lg-5" hidden ><label>From Time for ' + myarray[i] + '</label><input type="time" name="classtime" class="form-control" id="timefrom' + i + '" value=""></div><div class="form-group col-lg-5" hidden ><label>To Time for ' + myarray[i] + '</label><input type="time" name="classtime" class="form-control" id="timeto' + i + '" value=""></div>');
                                        }
                                       }
                                       if(schedule=="")
                                       {
                                        $("#calendartime").empty();
                                       }

                                       });
                    $("#addclass").click(function(){
                                        var classname = document.getElementById('classname').value;
                                        var instructors = document.getElementById('instructors');
                                        var instructorid = instructors.options[instructors.selectedIndex].value;
                                        var courses = document.getElementById('courses');
                                        var coursesid = courses.options[courses.selectedIndex].value;
                                         var locations = document.getElementById('locations');
                                         var locationid = locations.options[locations.selectedIndex].value;
                                        var classcapacity = document.getElementById('classcapacity').value;
                                        var coordinatorid = document.getElementById('coordinatorid').value;
                                        var classpracticalexam = document.getElementById('classpracticalexam').value;
                                        if(classpracticalexam!="")
                                        {
                                         var dateParts = classpracticalexam.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
                                         classpracticalexam = dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
                                        }
                                        var classwrittenexam = document.getElementById('classwrittenexam').value;
                                        if(classwrittenexam!="")
                                        {
                                         var dateParts = classwrittenexam.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
                                         classwrittenexam = dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
                                        }
                                        var schedule = kl.getSelected();
                                         var scheduletimefrom = "";
                                         var scheduletimeto = "";

                                         for(var i=0;i<myarray.length;i++)
                                         {
                                         //Set k to change multiple time to single time...
                                            var k = 0;
                                            scheduletimefrom = scheduletimefrom + document.getElementById('timefrom'+k).value + ", ";
                                            scheduletimeto = scheduletimeto + document.getElementById('timeto'+k).value + ", ";
                                         }

                                        $.ajax({
                                            url: "jqScripts/addClass.php",
                                            type: "post",
                                               data: {'classname' : classname, 'coordinatorid' : coordinatorid,'instructorid' : instructorid, 'coursesid' : coursesid, 'classcapacity' : classcapacity, 'schedule' : schedule, 'classpracticalexam' : classpracticalexam, 'classwrittenexam' : classwrittenexam, 'scheduletimefrom' : scheduletimefrom, 'scheduletimeto' : scheduletimeto, 'locationid': locationid},
                                            success: function (response) {
                                               alert(response);
                                            showclass();
                                            },
                                                error: function(jqXHR, textStatus, errorThrown) {
                                                alert(errorThrown);
                                                console.log(textStatus, errorThrown);
                                            }
                                        });
                                });
                  });
function showclass()
{
    $.ajax({
           url: "jqScripts/showClasses.php",
           type: "post",
           data: {} ,
           success: function (response) {
           $('#showtable_classes').empty();
           $('#showtable_classes').append(response);
           },
           error: function(jqXHR, textStatus, errorThrown) {
           alert(errorThrown);
           console.log(textStatus, errorThrown);
           }
           });
}
</script>



<script>
$(function() {
  $( "#classpracticalexam" ).datepicker();
  $( "#classwrittenexam" ).datepicker();

  });
</script>

<title>Manage Classes</title>


<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Manage Classes</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="form-group col-lg-4">
<label>Class Name</label>
<input type="" name="classname" class="form-control" id="classname" value="">
</div>

<div class="form-group col-lg-4">
<label>Select Instructor</label>
<select name="instructors" id="instructors" class="form-control">
<option disabled selected value> -- select Instructor -- </option>
<?php
    $ret = getInstructorsDetails();
    for($i=0;$i<count($ret);$i++)
    {
        $instructor_id = $ret[$i]['Instructor_ID'];
        $instructor_fullname = "ID: " . $ret[$i]['Instructor_ID'] . " | " . $ret[$i]['Instructor_FirstName']. " " . $ret[$i]['Instructor_LastName'];
        echo "<option value='$instructor_id'>$instructor_fullname</option>";
    }
    ?>
</select>
</div>

<div class="form-group col-lg-4">
<label>Select Course</label>
<select name="courses" id="courses" class="form-control">
<option disabled selected value> -- select Course -- </option>
<?php
    $ret = getCourses();
    for($i=1;$i<count($ret);$i++)
    {
        $course_id = $ret[$i]['Course_ID'];
        $course_name = $ret[$i]['Course_Name'];
        echo "<option value='$course_id'>" . "ID: " . $course_id . " | " . $course_name . "</option>";
    }
    ?>
</select>
</div>

<div class="form-group col-lg-4">
<label>Select Location</label>
<select name="locations" id="locations" class="form-control">
<option disabled selected value> -- select Location -- </option>
<?php
    $ret = getLocations();
    for($i=0;$i<count($ret);$i++)
    {
        $location_id = $ret[$i]['Location_ID'];
        $location_name = $ret[$i]['Location_Name'];
        echo "<option value='$location_id'>". "ID: " . $location_id . " | " . $location_name . "</option>";
    }
    ?>
</select>
</div>

<div class="form-group col-lg-4">
<label>Class Capacity</label>
<input type="" name="classcapacity" class="form-control" id="classcapacity" value="">
</div>

<div class="form-group col-lg-4">
<label>Practical Exam Date</label>
<input type="" name="classpracticalexam" class="form-control" id="classpracticalexam" value="">
</div>

<div class="form-group col-lg-4">
<label>Written Exam Date</label>
<input type="" name="classwrittenexam" class="form-control" id="classwrittenexam" value="">
</div>


<div class="form-group col-lg-12">
<label>Select Class Dates</label>
<center id="calendar">
</center>
<div id="calendartime">

</div>
</div>


<div class="col-sm-12">
<input type="button" id="addclass" class="btn btn-primary" name="addclass" value="Add Class">
</div>


<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
Classes
</div>
<!-- /.panel-heading -->
<div class="panel-body" id="showtable_classes">
<!-- /.table-responsive -->

</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->


<?php
    include 'template2.php';
?>
