<?php
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
    include 'template1.php';

    if($_POST)
    {
        $prefix="";
        $firstname = $_POST['firstname'];
        $initial = $_POST['initial'];
        $lastname = $_POST['lastname'];
        $emailaddress = $_POST['emailaddress'];

        //Random Password
        $password_temp = randomPassword();

        $password = password_hash($password_temp, PASSWORD_DEFAULT);

        $phonenumber = $_POST['phonenumber'];
        $addressline = $_POST['addressline1'];
        $city = $_POST['city'];
        $company = $_POST['company'];
        $other = $_POST['other'];
        $postal = $_POST['postalcode'];
        $position = $_POST['position'];
        $ret = signup($prefix, $firstname, $initial, $lastname, $emailaddress, $password, $phonenumber, $company, $other, $position, $addressline, $city, $postal, 4);

        if($ret['Status']==1)
        {
            //Send Email
            include('../mailAPI/emails.php');
            welcome_email_byCoordinator($firstname,$lastname,$emailaddress, $password_temp);
            $msg = $ret['Message'];
            echo "<script>alert('$msg');</script>";
        }
        else
        {

            $msg = $ret['Message'];
            echo "<script>alert('$msg');</script>";
        }
    }

?>

<script type="text/javascript">

$(document).ready(function(){

                  $("#search_btn").click(function(){
                                         searchemployee();
                                         });
            });
function searchemployee()
{
    var company = document.getElementById('comp').value;
    var firstname = document.getElementById('fn').value;
    var lastname = document.getElementById('ln').value;
    var zipcode = document.getElementById('zc').value;
    var city = document.getElementById('c').value;
    var certificate_expire = document.getElementById('calendar').value;
    var phone = document.getElementById('phn').value;
    var coordinator_added = 0;
    if(document.getElementById('coordinator_added').checked)
    {
        coordinator_added=1;
    }
    if(certificate_expire!="")
    {
        var dateParts = certificate_expire.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
        certificate_expire = dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }
    $.ajax({
           url: "jqScripts/getSearch.php",
           type: "post",
           data: {'company' : company, 'firstname':firstname, 'lastname':lastname, 'zipcode' : zipcode, 'city' : city, 'certificate_expire':certificate_expire, 'coordinator_added': coordinator_added, 'phone':phone} ,
           success: function (response) {
           $('#showtable').empty();
           $('#showtable').append(response);
           },
           error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
           }
           });
}

function validateCompany(){
    if(company.value=="none" && othercompany.value.length==0)
    {
        document.getElementById("othercompany").setCustomValidity("Select company from the list or enter Other Company Name.");
    }
    else
    {
        document.getElementById("othercompany").setCustomValidity('');

    }

}
function checkForm(){
    //var ferba = document.getElementById("ferba");
    if(company.value=="none" && othercompany.value.length==0)
    {
        document.getElementById("othercompany").setCustomValidity("Select company from the list or enter Other Company Name.");
        checkForm();
        return false;

    }
    else
    {
        document.getElementById("othercompany").setCustomValidity('');
        return true;

    }
    /*
     if(!(ferba.checked==true))
     {
     alert("Ferba must be agreed in order to Register!");
     return false;
     }
     */
    return false;
}

</script>

<script>

$(function() {
  $( "#calendar" ).datepicker();
  });
</script>


<title>Manage Students</title>

<div class="row">
<div class="col-lg-12">
<h1 class="page-header">Manage Students</h1>
<button type="button" id="add_btn" class="btn btn-primary" data-toggle="modal" data-target="#myAddEmployeeModal" >Add Caltrans Employee</button>
<hr>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="form-group col-lg-4">
<label>Company</label>
<select name="company" class="form-control" id="comp">
<option disabled selected value> -- select your company -- </option>
<?php
    $ret = getCompany();
    for($i=0;$i<count($ret);$i++)
    {
        $company_id = $ret[$i]['Company_ID'];
        $company_name = $ret[$i]['Company_Other'];
        echo "<option value='$company_id'>$company_name</option>";
    }
?>
</select>
</div>

<div class="form-group col-lg-4">
<label>First Name</label>
<input type="" name="" class="form-control" id="fn" value="">
</div>

<div class="form-group col-lg-4">
<label>Last Name</label>
<input type="" name="" class="form-control" id="ln" value="">
</div>

<div class="form-group col-lg-4">
<label>Zip Code</label>
<input type="" name="" class="form-control" id="zc" value="">
</div>

<div class="form-group col-lg-4">
<label>City</label>
<input type="" name="" class="form-control" id="c" value="">
</div>

<div class="form-group col-lg-4">
<label>Certificate Expiration</label>
<input type="" name="" class="form-control" id="calendar" value="">
</div>

<div class="form-group col-lg-4">
<label>Phone Number</label>
<input type="" name="" class="form-control" id="phn" value="">
</div>

<div class="form-group col-lg-12">
<input type="checkbox" name="" class="" id="coordinator_added" value=""> Show only Caltrans Employees (Added By Program Coordinator)
</div>

<div class="col-sm-12">
<button type="submit" id="search_btn" class="btn btn-primary">Search</button>
</div>

<div class="row">
<div class="col-lg-12">
<hr>

<div class="panel panel-default">
<div class="panel-heading">
Search Result
</div>
<!-- /.panel-heading -->
<div class="panel-body" id="showtable">
<!-- /.table-responsive -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
</div>
<!-- /.row -->



<!-- Add Caltrans Employee Modal -->
<div class="modal fade" id="myAddEmployeeModal" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add Caltrans Employee</h4>
</div>
<div class="modal-body">

<form method="POST" onsubmit="return checkForm(this);">

<div class="form-group col-lg-5">
<label>First Name*</label>
<input type="" name="firstname" class="form-control" id="" value="" required>
</div>

<div class="form-group col-lg-2">
<label>Initial</label>
<input type="" name="initial" class="form-control" id="" value="" >
</div>

<div class="form-group col-lg-5">
<label>Last Name*</label>
<input type="" name="lastname" class="form-control" id="" value="" required>
</div>


<div class="form-group col-lg-12">
<label>Email Address*</label>
<input type="email" name="emailaddress" class="form-control" id="emailaddress" value="" required >
</div>

<div class="form-group col-lg-12">
<label>Phone Number*</label>
<input type="" name="phonenumber" class="form-control" id="" value="" pattern="[a-zA-Z0-9]+" required>
</div>

<div class="form-group col-lg-12">
<label>Address line*</label>
<input type="" name="addressline1" class="form-control" id="" value="" required >
</div>

<div class="form-group col-lg-6">
<label>City*</label>
<input type="" name="city" class="form-control" id="city" value="" pattern="^([a-zA-Z\u0080-\u024F]+(?:. |-| |'))*[a-zA-Z\u0080-\u024F]*$" required>
</div>

<div class="form-group col-lg-6">
<label>Postal Code*</label>
<input type="" name="postalcode" class="form-control" id="" value="" required>
</div>

<h4>Employer Information</h4>

<div class="col-sm-12" style="margin-bottom: 20px;">
<label>Company*</label>
<select name="company" class="form-control" id="company">
<option disabled selected value="none"> -- select your company -- </option>
<?php
    $ret = getCompany();
    for($i=0;$i<count($ret);$i++)
    {
        $company_id = $ret[$i]['Company_ID'];
        $company_name = $ret[$i]['Company_Other'];
        echo "<option value='$company_id'>$company_name</option>";
    }
    ?>
</select>
</div>

<div class="form-group col-lg-12">
<label>Other</label>
<input type="text" name="other" class="form-control" id="othercompany" value="">
</div>

<div class="form-group col-lg-12">
<label>Position</label>
<input type="" name="position" class="form-control" id="" value="" >
</div>

<!--
<div class="form-group col-lg-12">
<div class="checkbox">
<label><input type="checkbox" value="" id="ferba" name="ferba">I agree to the terms and condition of CalTrans.</label>
</div>
</div>
-->

<div class="col-sm-12">
* Fields required.<br>
<button type="submit" class="btn btn-primary">Register</button>
</div>

</form>

</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>


<?php
    include 'template2.php';
?>
