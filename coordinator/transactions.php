<?php
    include 'template1.php';

    if($_POST)
    {
      if($_POST['transfer'])
      {
        $cid = $_POST['Class_ID'];
        $new_Class_ID = $_POST['new_Class_ID'];
        $eid = $_POST['Employee_ID'];
        $status = $_POST['Status'];
        if($status == 0){
          $status = 1;
        }
        else {
          $status = 0;
        }
        $ret = updatePaypalRefundTableTrasnferredByClasIDEmployeeID($cid,$eid,$new_Class_ID);
        print_r($ret);
      }
    }
?>

<title>Transactions</title>
<div class="row">
<div class="col-lg-12">
  <h1 class="page-header">Transactions</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php
  if ($_GET['tid']!='') {
    $ret2 = getPaypalDataByTransactionID($_GET['tid']);
  }
  elseif ($_GET['payee_name']!='') {
    $ret2 = getPaypalDataByPayeeName($_GET['payee_name']);
  }
  else {
    $ret2 = getPaypalData();
  }

  ?>

  <form>
    TransactionID: <input type="text" name="tid">
    Payee Name: <input type="text" name="payee_name">
    <input type="submit">
  </form>

  <h3> Refund Status </h3>
  <ul>
    <li> 0 - Pending Refund</li>
    <li> 1 - Refunded</li>
    <li> 2 - Course Trasnferred to another Class </li>
  </ul>

  <table class="table">
  <thead>
    <tr>
      <th>No</th>
      <th>Class</th>
      <th>Employee</th>
      <th>Amount</th>
      <th>Timestamp</th>
      <th>Refund Status</th>
      <th>Additional Paypal Data</th>
    </tr>
  </thead>
  <tbody>
    <?php
      for($i=0;$i<count($ret2);$i++)
      {
        $ret3 = getClass($ret2[$i]['Class_ID']);
        $paypal_json = json_decode($ret2[$i]['data'], true);
        $amount = $paypal_json['payment_gross'];

        $ret4 = getEmployeeDetailsByEmployeeID($ret2[$i]['Employee_ID']);

    ?>
    <tr>
      <td><?php echo $i+1; ?></td>
      <td><?php echo $ret3['Class_Name'] . " / " . $ret3['Course_Name']; ?></td>
      <td><?php echo $ret4['FirstName'] . " " . $ret4['LastName']; ?></td>
      <td><?php echo $amount; ?></td>
      <td><?php echo $ret2[$i]['TimeStamp']; ?></td>
      <td><?php echo $ret2[$i]['Refund_Status']; ?></td>
      <td><?php echo $ret2[$i]['data']; ?></td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>

<script>
  $(function() {
  $('#datepicker').datepicker({
      dateFormat: 'yy-mm-dd'
  });
  });
</script>

<?php
    include 'template2.php';
?>
