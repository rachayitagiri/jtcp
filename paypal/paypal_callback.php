<?php

require '../dbmanage.php';
require '../mailAPI/emails.php';

set_time_limit(300);

$classid = $_GET['classid'];
$employee_id = $_GET['employee_id'];

$myObj['payment_status'] = $_POST['payment_status'];
$myObj['txn_id'] = $_POST['txn_id'];
$myObj['invoice'] = $_POST['invoice'];
$myObj['payment_date'] = $_POST['payment_date'];
$myObj['address_zip'] = $_POST['address_zip'];
$myObj['first_name']= $_POST['first_name'];
$myObj['address_name'] = $_POST['address_name'];
$myObj['address_city'] = $_POST['address_city'];
$myObj['address_country'] = $_POST['address_country'];
$myObj['payer_email'] = $_POST['payer_email'];
$myObj['payment_type'] = $_POST['payment_type'];
$myObj['payment_gross'] = $_POST['payment_gross'];

//Refund
if(strcmp($myObj['payment_status'], 'Refunded') == 0){
  //Update Paypal_Refund Table
  updatePaypalRefundTableByClasIDEmployeeID($classid,$employee_id);
}
$myJSON = json_encode($myObj);

print_r(addPaypalData($myJSON, $classid, $employee_id));

//AddActivity

$ret2 = getClass($classid);
$course_name = $ret2['Course_Name'];
$class_name = $ret2['Class_Name'];

$message2 = "Payment Confirmation for Class: " . $class_name . " for Course " . $course_name . " Payment Status : " . $myObj['payment_status'] .  " | Amount Charged: " . $myObj['payment_gross'] . " | Txt No: " . $myObj['txn_id'];
addActivity($employee_id, $message2, 1);


//Email Payment Config
$ret = getEmployeeDetailsByEmployeeID($employee_id);
$firstname = $ret['FirstName'];
$lastname = $ret['LastName'];
$email = $ret['Email'];
$subject = "Fees Payment Confirmation";
$message = "This is to confirm that your payment for Class: " . $class_name . " for Course " . $course_name . " has been successful. <br><br> Payment Status : " . $myObj['payment_status'] .  "<br>Amount Charged: " . $myObj['payment_gross'] . "<br> Txt No: " . $myObj['txn_id'] . "<br> Please save this email in case of a 'refund' or as a 'copy of proof'. Kindly make sure you are enrolled in the class you paid for by viewing your schedule. In case of a dsicrepancy, contact us on the website.";
generalEmail($subject, $firstname, $lastname, $email, $message);

?>
