<?php

    /*
      Developed by Rushi Jash at CSULB
    */

    function return_paypal_code($course_id,$cid,$eid){
      if($course_id==2){
        //HMA1
        ?>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
          <input type="hidden" name="cmd" value="_s-xclick"> <input type="hidden" name="hosted_button_id" value="J2MTG2NQU79YW">
          <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
          <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
          <input name="notify_url" value="http://web.csulb.edu/colleges/coe/jtcp/paypal/paypal_callback.php?classid=<?php echo $cid; ?>&employee_id=<?php echo $eid;?>" type="hidden">
        </form>
        <?php
      }
      else if($course_id==3){
        //HMA2
        ?>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
          <input type="hidden" name="cmd" value="_s-xclick"> <input type="hidden" name="hosted_button_id" value="LQ7YPBRV9W6ZN">
          <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
          <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
          <input name="notify_url" value="http://web.csulb.edu/colleges/coe/jtcp/paypal/paypal_callback.php?classid=<?php echo $cid; ?>&employee_id=<?php echo $eid;?>" type="hidden">
        </form>
        <?php
      }
      else if($course_id==4){
        //SA
        ?>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
          <input type="hidden" name="cmd" value="_s-xclick"> <input type="hidden" name="hosted_button_id" value="J2MTG2NQU79YW">
          <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
          <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
          <input name="notify_url" value="http://web.csulb.edu/colleges/coe/jtcp/paypal/paypal_callback.php?classid=<?php echo $cid; ?>&employee_id=<?php echo $eid;?>" type="hidden">
        </form>
        <?php
      }
      else if($course_id==5){
        //PCC
        ?>
        <!-- <h4 style="color: red;">Warning: PCC Payments are for Testing Purpose Only! You would not be able to Pay for PCC Course right now!</h4>
        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="V4GF7MN7BQ45L">
        <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
        <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
        <input name="notify_url" value="http://web.csulb.edu/colleges/coe/jtcp/paypal/paypal_callback.php?classid=<?php echo $cid; ?>&employee_id=<?php echo $eid;?>" type="hidden">
        </form> -->
        <?php
      }

    }

    ?>
